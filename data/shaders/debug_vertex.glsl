#version 130
/****************************************************************************/
/* Copyright (c) 2011, Ola Olsson, Ulf Assarsson
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
/****************************************************************************/

in vec3		position;
in	vec3	normalIn;
in	vec2	texCoordIn;

out vec3 v2f_normal;
out vec3 v2f_worldPos;
out vec4 v2f_color;

uniform mat4 normalMat;
uniform mat4 modelTransform;
uniform mat4 viewProjectionTransform;
uniform vec4 color;

void main()
{
	v2f_worldPos = (modelTransform * vec4(position, 1.0)).xyz;
	v2f_normal = normalize(mat3(normalMat) * normalIn);
	v2f_color = color;
	gl_Position = viewProjectionTransform * modelTransform * vec4(position, 1.0);
}
