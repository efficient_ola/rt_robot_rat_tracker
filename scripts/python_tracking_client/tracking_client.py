"""
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


This script is a super-simple example of how to receive tracking updates using python. Note that it is not dependent on the 
tracker and thus manual care is needed to keep the data unpacking in sync with what is in src/shared/ClientLocationMessage.h
"""
import struct
import os
from collections import defaultdict
import numpy as np
import threading
from time import sleep
import socket


class ValueContainerObject:
    def __init__(self, **entries):
        self.__dict__.update(entries)


# Though a separate thread is not needed for this example, the threaded concept enables the client to receive 
# the updates as soon as possible. The thread will also retry connecting to the server.
class SocketThread(threading.Thread):
    shouldStop = False
    socket = None
    messageCallback = None


    def recieveFixedSizeMessage(self, msgSize):
        chunks = []
        bytes_recd = 0
        while bytes_recd < msgSize:
            try:
                chunk = self.socket.recv(msgSize - bytes_recd)
            except:
                print("socket connection broken, reconnecting (1).")
                return None
            if chunk == b'':
                print("socket connection broken, reconnecting (2).")
                return None
            chunks.append(chunk)
            bytes_recd = bytes_recd + len(chunk)
        return b''.join(chunks)



    def run(self):
        print_count = 0

        while not self.shouldStop:
            if self.socket:
                lengthByteBuffer = self.recieveFixedSizeMessage(8)
                dataByteBuffer = None
                if lengthByteBuffer:
                    poses = []
                    msgLen = int(np.fromstring(lengthByteBuffer, dtype=np.uint64))
                    #print("got message of len: %d"%msgLen)
                    dataByteBuffer = self.recieveFixedSizeMessage(msgLen)
                    # 64 bytes per message + 4 annotations of 40 bytes.
                    singleLocationSize = 64 + 40 * 4
                    for ind in range(0, int(msgLen / singleLocationSize)):
                        startOffset = ind*singleLocationSize
                        x,y,z,o,id,objectClass = struct.unpack("ffff24s24s", dataByteBuffer[startOffset:startOffset+64])
                        id = id.partition(b'\0')[0].decode("utf-8")
                        objectClass = objectClass.partition(b'\0')[0].decode("utf-8")
                        # hard coded 4 annotations to follow
                        annotations = []
                        for aInd in range(0,4):
                            aOffset = startOffset + 64 + aInd * 40
                            ax,ay,az,ao,aid = struct.unpack("ffff24s", dataByteBuffer[aOffset:aOffset+40])
                            aid = aid.partition(b'\0')[0].decode("utf-8")
                            if aid:
                                annotations.append(ValueContainerObject(id = aid, x = ax, y = ay, z = az, orientationAngle = ao))

                        poses.append(ValueContainerObject(objectClass = objectClass, x = x, y = y, z = z, orientationAngle = o, id = id, annotations = annotations))

                    if len(poses) and self.messageCallback:
                        self.messageCallback(poses)

                if not (lengthByteBuffer or dataByteBuffer):
                    self.socket.close()
                    self.socket = None
                    print("Connection was closed, resetting and reconnecting.")
            else:
                try:
                    if print_count % 100 == 0:
                        print("Connecting...")
                    print_count += 1
                    self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    self.socket.connect(("127.0.0.1", 28542))
                    print("Connected!")
                except :
                    sleep(0.1)
                    if self.socket:
                        self.socket.close()
                        self.socket = None
        if self.socket:
            self.socket.close()
            self.socket = None

    # Does not have to be called, the daemon bit takes care of that
    # but calling this stops the thread early
    def stop(self):
        self.shouldStop = True


def onTrackerMessage(poses):
    for p in poses:
        print("%s[%s]{x = %4.2f, y = %4.2f, z = %4.2f}, %0.2f"%(p.id, p.objectClass, p.x, p.y, p.z, p.orientationAngle));
        for a in p.annotations:
            print("   %s{x = %4.2f, y = %4.2f, z = %4.2f}, %0.2f"%(a.id, a.x, a.y, a.z, a.orientationAngle));
                


socket_thread = SocketThread()
socket_thread.daemon = True
socket_thread.messageCallback = onTrackerMessage
socket_thread.start()

input("This statement is just to keep the program alive, hitting ENTER will make it end")

socket_thread.stop()
