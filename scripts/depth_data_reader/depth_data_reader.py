"""
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
import struct
import os
from collections import defaultdict

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, RadioButtons

g_inputFile = "../recordings/dumpo_20190214-03_00_54.bdsx"
g_outDir = "output"

def unpackSingleIf(fmt, f, num):
    bs = f.read(num)
    if bs:
        return struct.unpack(fmt, bs)[0]
    return None

def readUint16(f):
    return unpackSingleIf("H", f, 2)

def readInt(f):
    return unpackSingleIf("i", f, 4)

def readUint64(f):
    return unpackSingleIf("Q", f, 8)

def readDouble(f):
    return unpackSingleIf("d", f, 8)

def readString(f):
    strSize = readUint64(f)
    if strSize:
        s = struct.unpack("%ds"%strSize, f.read(strSize))[0].decode("utf-8")
        return s
    return None

def readArray(f, fmt, bytesPerElement):
    dataLen = readUint64(f)
    if dataLen:
        a = struct.unpack("%d%s"%(dataLen, fmt), f.read(dataLen * bytesPerElement))
        return a
    return None

def getExt(fileName):
    outName, ext = os.path.splitext(fileName)
    return ext

class Frame:
    depthWidth = 0
    depthHeight = 0
    bytesDepthSample = 0
    minDepth = 0
    maxDepth = 0
    depthData = None
    colorWidth = 0
    colorHeight = 0
    cBytesPerPixel = 0
    colorData = None
    depthDataFloat = None
    sequenceNumber = -1
    deviceTimeStamp = -1.0
    timeStamp = -1.0




class BlockHeader:
    timeStamp = -1.0
    tag = ""
    version = ""
    size = -1
    headerStartOffset = -1
    dataStartOffset = -1
    


class FrameReader:
    startOffset = 0
    endOffset = 0
    numFrames = 0
    coeffs = None
    file = None
    fileName = None
    blockIndex = []
    blockTagIndex = defaultdict(list)
    depthFrameTag = "depth_frame"
    readerFn = defaultdict(dict)

    def __init__(self, fileName):
        self.fileName = fileName
        
        self.readerFn["depth_frame"]["1.0"] = self.readDepthFrameData
    
        if fileName:
            self.open(fileName)

    # reads depth frame data from the file-like object 'f', the assumption being that the file has been positioned at the start of a block of depth frame data.
    def readDepthFrameData(self, f):
        frame = Frame()
        frame.sequenceNumber = readInt(f)
        frame.timeStamp = readDouble(f)
        frame.deviceTimeStamp = readDouble(f)
        frame.depthWidth = readInt(f)
        frame.depthHeight = readInt(f)
        frame.bytesDepthSample = readInt(f)
        frame.minDepth = readInt(f)
        frame.maxDepth = readInt(f)
        frame.depthData = readArray(f, "h", 2)
        frame.depthDataFloat = [float(v) / 1000.0 for v in frame.depthData]
        frame.colorWidth = readInt(f)
        frame.colorHeight = readInt(f)
        frame.cBytesPerPixel = readInt(f)
        frame.colorData = readArray(f, "B", 1)
        return frame

    def readBlockHeader(self, f):
        block = BlockHeader()
        block.headerStartOffset = f.tell()
        block.size = readUint64(f)
        if not block.size:
            return None
        block.tag = readString(f)
        block.version = readString(f)
        block.timeStamp = readDouble(f)
        block.dataStartOffset = f.tell()
        
        return block
 
    # Seek past the given block (header) (irrespective of current location)
    def skipBlock(self, f, bh):
        # Note the extra 8-byte to skip the size itself, which is not included in 'bh.size'
        f.seek(bh.headerStartOffset + bh.size + 8, os.SEEK_SET)
    
    # scans the file 'f' and reads all block headers until end of file (or rather read-failure) is encountered.
    # builds the list 'blockIndex', which maps out all blocks in the file (in order of appearance)
    # and 'blockTagIndex' which stores the same info, but categorized by the tag.
    def buildIndex(self, f):
        while True:
            bh = self.readBlockHeader(f)
            if bh:
                self.blockTagIndex[bh.tag].append(bh)
                self.blockIndex.append(bh)
                self.skipBlock(f, bh)
            else:
                break

    def open(self, fileName):
        self.fileType = getExt(self.fileName).lower()
        self.file = open(fileName, "rb")
        if not self.file:
            print("File failed to open!");
            return False
        tag = struct.unpack("4s", self.file.read(4))[0].decode("utf-8")
        if tag != "bdsx":
            print("Missing file format tag!");
            return False
        # BDSX is a general format which contains named chunks of some size. Since chunks are not 
        # of pre-known or uniform  size, it must be scanned.
        self.buildIndex(self.file)
        self.numFrames = len(self.blockTagIndex[self.depthFrameTag])
        return True

    def read(self, frameNumber):
        if isinstance(frameNumber, BlockHeader):
            bh = frameNumber
        else:
            bh = self.blockTagIndex[self.depthFrameTag][frameNumber]
        self.file.seek(bh.dataStartOffset, os.SEEK_SET)
        return self.readerFn[frameNumber.tag][frameNumber.version](self.file)


frameReader = FrameReader(g_inputFile)

print(frameReader.blockTagIndex.keys())


if not os.path.exists(g_outDir):
    os.makedirs(g_outDir)


frames = frameReader.blockTagIndex["depth_frame"]


def updateKinect(fh, frameInd, save = False):
    fr = frameReader.read(fh)

    # Show the depth data
    depthImg = np.array(fr.depthDataFloat).reshape((fr.depthHeight, fr.depthWidth))
    ax2.imshow(depthImg, cmap='hot', interpolation='nearest')

    img = np.array(fr.colorData, np.uint8).reshape((fr.colorHeight, fr.colorWidth, 4))
    # The data is in BGRA so we swizzle it to the right format.
    imgRgba = img[:,:,[2,1,0,3]]
    ax.imshow(imgRgba)
    ax.set_title('Frame t = %4.2f'%fh.timeStamp)
    if save:
        plt.imsave(os.path.join(g_outDir, "color_%s_%05d.png"%(fh.tag, frameInd)), imgRgba)
        plt.imsave(os.path.join(g_outDir, "depth_%s_%05d.png"%(fh.tag, frameInd)), depthImg, cmap='hot')


def update(val, save = False):
    fh = frames[int(val)]
    if fh.tag  == "depth_frame":
        updateKinect(fh, int(val), save)

def save(event):
    update(frameSlider.val, True)

fig, [ax,ax2] = plt.subplots(nrows=1, ncols=2, figsize=(12,6))
plt.subplots_adjust(left=0.1, bottom=0.25)
axcolor = 'lightgoldenrodyellow'
frameAxis = plt.axes([0.25, 0.1, 0.65, 0.03], facecolor=axcolor)
frameSlider = Slider(frameAxis, 'Frame', 0, len(frames) - 1, valinit=0)
frameSlider.on_changed(lambda a:update(a, False))

saveButtonAxis = plt.axes([0.8, 0.025, 0.1, 0.04])
saveButton = Button(saveButtonAxis, 'Save', color=axcolor, hovercolor='0.975')
saveButton.on_clicked(save)

update(0)

plt.show()
