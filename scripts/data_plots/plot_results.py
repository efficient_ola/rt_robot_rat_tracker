"""
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


This script is a super-simple example of how to extract and plot some data from the data logs.

Note that with the info in the logs, you can also use the bdsx reader, to fetch the frame / 3D data:
each 'tracking_frame' has a 'source_frame_number' which lets you do this.
Further, you can also go an load the template libraries (from "TrackedObjectTypes" you can load this info).
Mostly, it is all stored as json, and so is trivial to load and process in Python.
"""
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as matplotlib
import os as os
import itertools as itertools
from collections import defaultdict
import matplotlib.pyplot as plt
import json
from linmath import Mat4,vec3

g_showPlots = True
g_plotPath = "./plots/"

resultsPath = r"sample_logs/"

inNames = [
    ( "single_pirat.json", "Single pirat."),
    ( "two_pirats.json", "Tracking 1 pirat & 1 just placed on arena."),
    ( "crazy_one.json", "Changed tracked object types in mid sequence."),
]

def jsonToVec4(jd):
    return [jd["x"], jd["y"], jd["z"], jd["w"]]

def jsonToMat4(jd):
    d = [   jsonToVec4(jd["c1"]),
            jsonToVec4(jd["c2"]),
            jsonToVec4(jd["c3"]),
            jsonToVec4(jd["c4"])]
    return Mat4(d)._transpose()


for inFileName,dataSetDesc in inNames:
    logData = {}
    try:
        with open(resultsPath + inFileName, 'r') as inFile:
            logData = json.load(inFile)
    except:
        print("Opening '%s' failed"%(resultsPath + inFileName))
        continue

    if not logData:
        continue

    dataSetName = logData["intro"][0]["parameters"][0]["input_source_id"]
    print("Processing '%s'"%dataSetName, end='')

    lastFrame = -1
    worldToArenaTfm = jsonToMat4(logData["ArenaTransform"][0]["worldToArenaTfm"])
    arenaSpacePositionsX = defaultdict(list)
    arenaSpacePositionsY = defaultdict(list)
    arenaRadius = float(logData["outro"][0]["settings"][0]["arenaRadius"])
    arenaCircle = plt.Circle((0,0), arenaRadius, color='grey', linewidth=2.0, linestyle=':', fill=False)

    for fd in logData["tracking_frame"]:
        if "tracked_objects" in fd.keys():
            # if recording wrapped:
            if lastFrame > fd["source_frame_number"]:
                break
            lastFrame = fd["source_frame_number"]
            #print("Frame: %d"%lastFrame)
            for t in fd["tracked_objects"][0]["object"]:
                objectToWorldTfm = jsonToMat4(t["transform"])
                objectToArenaTfm = worldToArenaTfm * objectToWorldTfm
                # Position/Translation is stored in the 4th column:
                arenaSpacePositionsX[t["name"]].append(objectToArenaTfm[0,3])
                arenaSpacePositionsY[t["name"]].append(objectToArenaTfm[1,3])

    fig, ax = plt.subplots(figsize=(6,6))
    ax.add_artist(arenaCircle)
    ax.set_xlim((-arenaRadius, arenaRadius))
    ax.set_ylim((-arenaRadius, arenaRadius))
    for n,xs in arenaSpacePositionsX.items():
        ys = arenaSpacePositionsY[n]
        ax.plot(xs, ys, label=n)
    ax.legend()
    ax.set_title("Arena Space Positions: " + dataSetDesc)

if g_showPlots:
    plt.show()
