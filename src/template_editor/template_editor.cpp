/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#define _USE_MATH_DEFINES 1

//#include "Config.h"

#include <iostream>
#include <algorithm>
#include <numeric>
#include <utils/PerformanceTimer.h>
#include <assert.h>
#include <ctime>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "imgui/imgui.h"
#include "imgui/imgui_impl_glfw_gl3.h"
#include <utils/CheckGLError.h>
#include <utils/GlBufferObject.h>
#include <utils/Assert.h>
#include "utils/Rendering.h"
#include "utils/SimpleShader.h"
#include "utils/Random.h"
#include "utils/DebugRenderer.h"
#include "utils/PrimitiveRenderer.h"
#include "utils/ChagIntersection.h"
#include <utils/OBJModel.h>

#include "profiler/Profiler.h"
#include "profiler/PerfTreeBuilder.h"

#include "linmath/Aabb.h"
#include "linmath/IOStreamUtils.h"

#include "../tracker/TemplateLibrary.h"
#include "../tracker/TemplateEditor.h"
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

#include <nlohmann/json.hpp>

using json = nlohmann::json;

std::string g_templateLibraryName;// = "template_libs/lib_7";

#undef max

using namespace chag;

PrimitiveRenderer *g_primitiveRenderer = 0;

static boost::filesystem::path g_envRootPath;

static SimpleShader *g_debugShader = 0;
static SimpleShader *g_objModelShader = 0;

const int g_startWidth  = 1920; // 1280; // 1920; // 
const int g_startHeight = 720; // 480;  // 1080; // 

static int g_width = g_startWidth;
static int g_height = g_startHeight;

static PerformanceTimer g_timer;


static const int2 g_templateSize = { 100, 100 };
static int2 g_mousePos = { 0, 0 };
static float g_fov = 60.0f;

TemplateLibraryPtr g_templateLibrary;


template <typename T>
inline T *safe_ptr(std::vector<T> &v)
{
	return v.empty() ? (T*)0 : &v[0];
}

template <typename T>
inline const T *safe_ptr(const std::vector<T> &v)
{
	return v.empty() ? (const T*)0 : &v[0];
}

template <typename T>
static void deleteIfThere(T *&shader)
{
	if (shader)
	{
		delete shader;
		shader = 0;
	}
}



TemplateEditor *g_templateGui = nullptr;




static std::string getTimeStampStr()
{
	time_t rawtime;
	struct tm * timeinfo;
	char buffer[80];

	time(&rawtime);
	timeinfo = localtime(&rawtime);

	strftime(buffer, sizeof(buffer), "%Y%m%d-%I_%M_%S", timeinfo);
	return std::string(buffer);
}



static void renderFrame(int width, int height)
{
	g_width = width;
	g_height = height;
	{
	PROFILE_SCOPE("Frame", TT_Cpu);

	g_templateGui->render(width, height);
	
	} // End profiler scope

	//ImGui::ShowDemoWindow();
	//printInfo();

	Profiler::instance().clearCommandBuffer();
}




static void update(GLFWwindow* window, float dt)
{
	ImGuiIO& io = ImGui::GetIO();

	if (!io.WantCaptureMouse)
	{
		g_templateGui->onMouseMotion(g_mousePos, glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS, glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS, glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_MIDDLE) == GLFW_PRESS);
		g_templateGui->update(dt);
	}
	//g_realTime += dt;
}



static void onKeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	ImGui_ImplGlfw_KeyCallback(window, key, scancode, action, mods);

	ImGuiIO& io = ImGui::GetIO();

	if (io.WantCaptureKeyboard)
	{
		return;
	}
	if (action == GLFW_REPEAT)
	{
		return;
	}

	g_templateGui->onKey(key, action);
}


void onScrollCallback(GLFWwindow *window, double xOffset, double yOffset)
{
	ImGui_ImplGlfw_ScrollCallback(window, xOffset, yOffset);
	ImGuiIO& io = ImGui::GetIO();
	if (io.WantCaptureMouse)
	{
		return;
	}

	g_templateGui->onMouseScroll(float(yOffset));
}


void onMouseButtonCallback(GLFWwindow*window, int button, int action, int mods)
{
	ImGui_ImplGlfw_MouseButtonCallback(window, button, action, mods);

	ImGuiIO& io = ImGui::GetIO();
	//g_mousePos = { int(io.MousePos.x), int(io.MousePos.y) };

	// stop events from getting to other parts if they hit the ImGUI
	if (io.WantCaptureMouse)
	{
		return;
	}

	g_templateGui->onMouseButton(button, action, g_mousePos);
}


static void bindObjModelAttributes(SimpleShader *shader)
{
	shader->bindAttribLocation(OBJModel::AA_Position, "position");
	shader->bindAttribLocation(OBJModel::AA_Normal, "normalIn");
	shader->bindAttribLocation(OBJModel::AA_TexCoord, "texCoordIn");
	shader->bindAttribLocation(OBJModel::AA_Tangent, "tangentIn");
	shader->bindAttribLocation(OBJModel::AA_Bitangent, "bitangentIn");
}

static void setObjModelUniformBindings(SimpleShader *shader)
{
	shader->begin();
	shader->setUniform("diffuse_texture", OBJModel::TU_Diffuse);
	shader->setUniform("opacity_texture", OBJModel::TU_Opacity);
	shader->setUniform("specular_texture", OBJModel::TU_Specular);
	shader->setUniform("normal_texture", OBJModel::TU_Normal);
	shader->end();
	shader->setUniformBufferSlot("MaterialProperties", OBJModel::UBS_MaterialProperties);
	//shader->setUniformBufferSlot("InstanceProperties", OBJModel::UBS_InstanceProperties);
	//shader->setUniformBufferSlot("WorldMatrixPalette", OBJModel::UBS_WorldMatrixPalette);

	//shader->setUniformBufferSlot("Globals", UBS_Globals);
}


static void createShaders()
{
	boost::filesystem::path shadersPath(g_envRootPath / "data" / "shaders");

	SimpleShader::Context shaderCtx;
	shaderCtx.m_includePath = shadersPath.string();

	deleteIfThere(g_debugShader);
	g_debugShader = new SimpleShader((shadersPath / "debug_vertex.glsl").string(), (shadersPath / "debug_fragment.glsl").string(), shaderCtx);
	g_debugShader->bindFragDataLocation(0, "resultColor");
	g_debugShader->link();
	g_debugShader->begin();
	g_debugShader->setUniform("modelTransform", chag::make_identity<chag::float4x4>());
	g_debugShader->end();

	deleteIfThere(g_objModelShader);
	g_objModelShader = new SimpleShader((shadersPath / "SimpleObjModel_vertex.glsl").string(), (shadersPath / "SimpleObjModel_fragment .glsl").string(), shaderCtx);
	g_objModelShader->bindFragDataLocation(0, "resultColor");
	bindObjModelAttributes(g_objModelShader);

	g_objModelShader->link();
	setObjModelUniformBindings(g_objModelShader);
	g_objModelShader->begin();
	g_objModelShader->setUniform("modelTransform", chag::make_identity<chag::float4x4>());
	g_objModelShader->end();
}


void debugMessageCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam)
{
	static std::set<std::tuple<GLenum, GLuint, GLenum>> suppressable =
	{
		{ 33357/*???*/, 7U, GL_DEBUG_SEVERITY_MEDIUM },
	};
	static std::set<std::tuple<GLenum, GLuint, GLenum>> seen;
	auto eid = std::make_tuple(type, id, severity);
	// filter out seen & suppressable messages (as decided by programmer)
	if (seen.count(eid))
	{
		return;
	}

	const char *severityStr = "GL_DEBUG_SEVERITY_NOTIFICATION";
	switch (severity)
	{
	case GL_DEBUG_SEVERITY_HIGH:
		severityStr = "GL_DEBUG_SEVERITY_HIGH";
		break;
	case GL_DEBUG_SEVERITY_MEDIUM:
		severityStr = "GL_DEBUG_SEVERITY_MEDIUM";
		break;
	case GL_DEBUG_SEVERITY_LOW:
		severityStr = "GL_DEBUG_SEVERITY_LOW";
		break;
	default:
		break;
	};
	fprintf(stderr, "OpenGL Error: %s\n  Severity: %s\n", message, severityStr);
	if (suppressable.count(eid))
	{
		seen.insert(eid);
	}
	else
	{
		if (severity != GL_DEBUG_SEVERITY_NOTIFICATION)
		{
#if defined(_MSC_VER) && defined(_DEBUG)
			// Visual C++ specific break-point trigger, which makes the debugger break on this very line and does not cause the application to exit (unlike 'assert(false)').
			__debugbreak();
#endif //_MSC_VER
		}
	}
	if (severity != GL_DEBUG_SEVERITY_HIGH)
	{
		seen.insert(eid);
	}
}

static void glfwErrorCallback(int error, const char* description)
{
	fprintf(stderr, "Error: %s\n", description);
	fflush(stderr);
}


int main(int argc, char* argv[])
{
	// 1. get root path, assumes that executable is in root/bin/platform
	g_envRootPath = boost::filesystem::canonical(boost::filesystem::path(argv[0])).parent_path().parent_path().parent_path();
	printf("Using root directory '%s' for options etc\n", g_envRootPath.string().c_str());
	// ensure subdirectories are present
	boost::filesystem::create_directories(g_envRootPath / "logs");
	boost::filesystem::create_directories(g_envRootPath / "options");

	glfwSetErrorCallback(glfwErrorCallback);

	//readVars("options/default.txt");

	{
		bool showHelp = false;

		namespace po = boost::program_options;
		po::options_description desc("Allowed options");
		desc.add_options()
			("help,h", "produce help message")
			("templateLib,l", po::value<std::string>(&g_templateLibraryName), "");
		try
		{
			po::variables_map vm;
			po::store(po::command_line_parser(argc, argv).
				options(desc).run(), vm);
			po::notify(vm);

			showHelp = vm.count("help") != 0;
			//useOurTree = !vm["use_their"].as<bool>();
		}
		catch (const po::unknown_option& eUO)
		{
			fprintf(stderr, "<error> %s\n", eUO.what());
			return 1;
		}
		if (showHelp)
		{
			std::cerr << desc << "\n";
			return 1;
		}
	}
	
	boost::filesystem::path optionsPath(g_envRootPath / "options/template_editor.json");
	boost::filesystem::path templateLibsPath(g_envRootPath / "template_libs");

	//glfwSetErrorCallback(glfwErrorCallback);
	if (!glfwInit())
	{
		exit(EXIT_FAILURE);
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);


	GLFWwindow* window = glfwCreateWindow(g_startWidth, g_startHeight, "Template Editor V0.0.0.1", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	//glfwSetKeyCallback(window, glfwKeyCallback);

	glfwMakeContextCurrent(window);

	gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);


#if 1
	// This is only available in OpenGL 4.3, in unextended OpenGL, so unfortunately we'll leave it disabled for now
	// Set up OpenGL debug callback and turn on...
	glDebugMessageCallback(debugMessageCallback, 0);
	// (although this glEnable(GL_DEBUG_OUTPUT) should not have been needed when using the GLUT_DEBUG flag above...)
	glEnable(GL_DEBUG_OUTPUT);
	// This ensures that the callback is done in the context of the calling function, which means it will be on the stack in the debugger, which makes it
	// a lot easier to figure out why it happened.
	glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
#endif

	glfwSwapInterval(1);


	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);
	glEnable(GL_LINE_SMOOTH);


	createShaders();

	ImGui::CreateContext();
	static std::string imguiIniPath = (g_envRootPath / "options" / "template_editor_imgui.ini").string();
	ImGui::GetIO().IniFilename = imguiIniPath.c_str();
	ImGui_ImplGlfwGL3_Init(window, true);


	glfwSetMouseButtonCallback(window, onMouseButtonCallback);
	glfwSetKeyCallback(window, onKeyCallback);
	glfwSetScrollCallback(window, onScrollCallback);

	ImGui::StyleColorsDark();

	g_primitiveRenderer = new PrimitiveRenderer;
	g_primitiveRenderer->init(g_debugShader);

	bool exitProgramFlag = false;

	g_templateLibrary = TemplateLibraryPtr(new TemplateLibrary);
	g_templateLibrary->load(templateLibsPath, g_templateLibraryName);


	g_templateGui = new TemplateEditor(g_templateLibrary, g_primitiveRenderer, g_objModelShader, (g_envRootPath / "data").string());

	g_templateGui->loadOptions(optionsPath.string());

	g_timer.start();
	double currentTime = g_timer.getElapsedTime();
	while (!glfwWindowShouldClose(window) && !exitProgramFlag)
	{
		int width, height;
		glfwGetFramebufferSize(window, &width, &height);
		ImGui_ImplGlfwGL3_NewFrame();


		double newTime = g_timer.getElapsedTime();
		float dt = float(newTime - currentTime);
		currentTime = newTime;

		double mouse_x, mouse_y;
		glfwGetCursorPos(window, &mouse_x, &mouse_y);
		g_mousePos = make_vector<int>(mouse_x, mouse_y);

		update(window, dt);
		renderFrame(width, height);

		// Creates command lists from all the UI rendering calls in the frame and passes this to the implementations that uses opengl to render them.
		ImGui::Render();
		ImGui_ImplGlfwGL3_RenderDrawData(ImGui::GetDrawData());

		glfwSwapBuffers(window);
		glfwPollEvents();
	}
	g_templateGui->saveOptions(optionsPath.string());

	delete g_templateGui;
	g_templateGui = nullptr;

	if (!g_templateLibraryName.empty())
	{
		g_templateLibrary->save(templateLibsPath);
	}

	ImGui_ImplGlfwGL3_Shutdown();

	return 0;
}



void saveFrameBufferAsTga(const char *destFile)
{
	// ensure all drawing commands are finished (really should not be needed...)
	glFinish();

	int W = g_width;
	int H = g_height;

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	// ensure no buffer object bound
	glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);

	FILE   *out = fopen(destFile, "wb");
	char   *pixel_data = new char[3 * W*H];
	short  TGAhead[] = { 0, 2, 0, 0, 0, 0, short(W), short(H), 24 };
	//glReadBuffer(readBuffer);
	glPixelStorei(GL_PACK_ALIGNMENT, 1);
	glReadPixels(0, 0, W, H, GL_BGR, GL_UNSIGNED_BYTE, pixel_data);
	fwrite(&TGAhead, sizeof(TGAhead), 1, out);
	fwrite(pixel_data, 3 * W*H, 1, out);

#if 0//defined(_WIN32)
	// attempts to get the data to disc before any crash at the end of the program (not uncommon...)
	FlushFileBuffers((HANDLE)_get_osfhandle(_fileno(out)));
#endif // defined(_WIN32)

	fclose(out);
	delete[] pixel_data;


	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}



static PerfTreeBuilder g_perfTreeBuilder;


static void drawPerfNodeImGUI(const PerfTreeNode* n)
{
	if (n->message.empty())
	{
		ImGui::Text("%8.2f %8.2f %8I64d", 1000.0 * n->time, 1000.0 * n->average, n->count);
	}
	else
	{
		ImGui::Text("%17s %8I64d", n->message.c_str(), n->count);
	}
}


static void drawPerfNodeListImGUI(const std::vector<PerfTreeNode*> &nl)
{
	for (std::vector<PerfTreeNode*>::const_iterator it = nl.begin(); it != nl.end(); ++it)
	{
		if (!(*it)->children.empty())
		{
			ImGui::PushID((*it)->label.c_str());                      // Use object uid as identifier. Most commonly you could also use the object pointer as a base ID.
			ImGui::AlignTextToFramePadding();  // Text and Tree nodes are less high than regular widgets, here we add vertical spacing to make the tree lines equal high.
			bool node_open = ImGui::TreeNodeEx((*it)->label.c_str(), ImGuiTreeNodeFlags_SpanAllColumns | ImGuiTreeNodeFlags_DefaultOpen);
			ImGui::NextColumn();
			ImGui::AlignTextToFramePadding();
			drawPerfNodeImGUI(*it);
			ImGui::NextColumn();
			if (node_open)
			{
				drawPerfNodeListImGUI((*it)->children);
				ImGui::TreePop();
			}
			ImGui::PopID();
		}
		else
		{
			ImGui::Selectable((*it)->label.c_str(), false, ImGuiSelectableFlags_SpanAllColumns);
			ImGui::SetItemAllowOverlap();

			ImGui::NextColumn();
			ImGui::PushItemWidth(-1);
			drawPerfNodeImGUI(*it);
			ImGui::PopItemWidth();
			ImGui::NextColumn();
		}
		//printString(indent * 20 + g_width / 2, yOffset, "%-25s: %8.2f %8.2f %8I64d", (*it)->label.c_str(), 1000.0 * (*it)->time, 1000.0 * (*it)->average, (*it)->count);
		//yOffset += yStep;
		//screenPrintNodeList((*it)->children, yOffset, yStep, indent + 1);
	}
}
#if 0
static void drawPerfResImGUI()
{
	if (g_showProfilerInfo)
	{
		std::vector<PerfTreeNode*> roots = g_perfTreeBuilder.build(Profiler::instance());

		ImGui::SetNextWindowSize(ImVec2(430, 450), ImGuiCond_FirstUseEver);
		if (!ImGui::Begin("Profiler Info", &g_showProfilerInfo))
		{
			ImGui::End();
			return;
		}
		ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(2, 2));
		ImGui::Columns(2);


		drawPerfNodeListImGUI(roots);


		ImGui::Columns(1);
		ImGui::PopStyleVar();

		ImGui::End();
	}
}

static void printInfo()
{
	if (!g_showTemplatesGui)
	{
		{
			const float DISTANCE = 10.0f;
			static int corner = 1;
			ImVec2 window_pos = ImVec2((corner & 1) ? ImGui::GetIO().DisplaySize.x - DISTANCE : DISTANCE, (corner & 2) ? ImGui::GetIO().DisplaySize.y - DISTANCE : DISTANCE);
			ImVec2 window_pos_pivot = ImVec2((corner & 1) ? 1.0f : 0.0f, (corner & 2) ? 1.0f : 0.0f);
			ImGui::SetNextWindowPos(window_pos, ImGuiCond_Always, window_pos_pivot);
			ImGui::SetNextWindowBgAlpha(0.5f); // Transparent background
			if (ImGui::Begin("Example: Fixed Overlay", 0, ImGuiWindowFlags_NoTitleBar|ImGuiWindowFlags_NoResize|ImGuiWindowFlags_AlwaysAutoResize|ImGuiWindowFlags_NoMove|ImGuiWindowFlags_NoSavedSettings|ImGuiWindowFlags_NoFocusOnAppearing|ImGuiWindowFlags_NoNav))
			{
				if (ImGui::IsMouseHoveringWindow())
				{
					ImGui::SetTooltip("(right-click to change position)");
				}
				ImGui::Text("Toggle parameters UI: <F1>");
				ImGui::Text("Toggle profiler info: <F2>");

				ImGui::Separator();
				for (int nameInd = 0; nameInd < sizeof(g_templateNames) / sizeof(g_templateNames[0]); ++nameInd)
				{
					if (TemplateTracker::TrackedObjectPtr o = g_tracker.get(g_templateNames[nameInd]))
					{
						float4x4 tmp = g_toArenaTfm * o->getTfm();
						ImGui::Text("%s: {%0.3f, %0.3f, %0.3f}", g_templateNames[nameInd], tmp.getTranslation().x, tmp.getTranslation().y, tmp.getTranslation().z);
					}
				}
				ImGui::Separator();

				if (g_fileToDumpTo)
				{
					ImGui::PushStyleColor(ImGuiCol_Text, (ImVec4)ImColor(sinf(g_realTime * 8.0f) * 0.45f + 0.55f, 0.1f, 0.1f));
					ImGui::Text("DUMPING To FILE\n(press 'f' to toggle)");
					ImGui::PopStyleColor();
				}

				if (ImGui::BeginPopupContextWindow())
				{
					if (ImGui::MenuItem("Top-left", NULL, corner == 0)) corner = 0;
					if (ImGui::MenuItem("Top-right", NULL, corner == 1)) corner = 1;
					if (ImGui::MenuItem("Bottom-left", NULL, corner == 2)) corner = 2;
					if (ImGui::MenuItem("Bottom-right", NULL, corner == 3)) corner = 3;
					ImGui::EndPopup();
				}
				ImGui::End();
			}
		}

		if (g_source->getFrameCount() >= 0)
		{
			float height = 50.0f;
			ImGui::SetNextWindowPos(ImVec2(ImGui::GetIO().DisplaySize.x * 0.5f, ImGui::GetIO().DisplaySize.y - 5.0f), ImGuiCond_Always, ImVec2(0.5f, 1.0f));
			ImGui::SetNextWindowSize(ImVec2(ImGui::GetIO().DisplaySize.x * 0.75f, height), ImGuiCond_Always);
			ImGui::SetNextWindowBgAlpha(0.5f); // Transparent background
			if (ImGui::Begin("Frame Controller", 0, ImGuiWindowFlags_NoTitleBar|ImGuiWindowFlags_NoResize|ImGuiWindowFlags_NoFocusOnAppearing))
			{
				int frameInd = g_source->getCurrentFrame();
				ImGui::PushItemWidth(-1);
				if (ImGui::SliderInt("##Frame", &frameInd, 0, g_source->getFrameCount()-1))
				{
					// Don't do anything if set back to the same as before...
					if (frameInd != g_source->getCurrentFrame())
					{
						g_frameIndexSet = true;
						g_source->seek(frameInd);
					}
				}
				ImGui::PopItemWidth();
				if (ImGui::Button("Play/Pause"))
				{
					g_paused = !g_paused;
				}
				ImGui::SameLine();
				if (ImGui::Button("Step Forwards"))
				{
					g_pauseFrameCount = 1;
				}

				//ImGui::Separator();
				ImGui::End();
			}
		}
	}
	drawPerfResImGUI();
}
#endif


namespace TweakableVar
{
	// This function must be supplied to map keys to strings for printing GUI info
	std::string keyToString(int key)
	{
		return "NOT IMPL";
	}
}