/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/


// This program is a trivial demonstration on how to connect to the Tcp client output and receive data.
// The program is hardcoded to use address '127.0.0.1:28542'

#include <stdint.h>
#include <vector>
#include <windows.h>
#include <assert.h>
// This header is in the src/shared folder, and guarantees that we use the same definition as the tracker.
#include <ClientLocationMessage.h>


#ifdef _WIN32
#pragma comment(lib, "Ws2_32.lib")
#endif // _WIN32

using namespace ClientLocationMessage;


// Helper that ensures a complete message is received, no idea if this ever fails anyway, but there it is...
template <typename T>
bool recvFixedSizeMessage(SOCKET s, T *buffer, int numBytes)
{
	char *tmpPtr = reinterpret_cast<char*>(buffer);
	for (int totalRec = 0; totalRec < numBytes;)
	{
		int recvd = recv(s, reinterpret_cast<char*>(&tmpPtr[totalRec]), numBytes - totalRec, 0);
		if (recvd < 0)
		{
			// Signal failure
			return false;
		}

		totalRec += recvd;
	}
	return true;
}

int main()
{
	WSADATA wsaData;
	int iResult;
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != NO_ERROR) {
		printf("WSAStartup failed: %d\n", iResult);
		return 1;
	}

	SOCKET ConnectSocket;
	ConnectSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (ConnectSocket == INVALID_SOCKET) {
		wprintf(L"socket function failed with error: %ld\n", WSAGetLastError());
		WSACleanup();
		return 1;
	}
	//----------------------
	// The sockaddr_in structure specifies the address family,
	// IP address, and port of the server to be connected to.
	sockaddr_in clientService;
	clientService.sin_family = AF_INET;
	clientService.sin_addr.s_addr = inet_addr("127.0.0.1");
	clientService.sin_port = htons(28542);

	//----------------------
	// Connect to server.
	iResult = connect(ConnectSocket, (SOCKADDR *)& clientService, sizeof(clientService));
	if (iResult == SOCKET_ERROR) {
		wprintf(L"connect function failed with error: %ld\n", WSAGetLastError());
		iResult = closesocket(ConnectSocket);
		if (iResult == SOCKET_ERROR)
			wprintf(L"closesocket function failed with error: %ld\n", WSAGetLastError());
		WSACleanup();
		return 1;
	}

	

	for (;;)
	{
		uint64_t msgLen = 0;
		if (recvFixedSizeMessage(ConnectSocket, &msgLen, sizeof(msgLen)))
		{
			assert((msgLen % sizeof(Location)) == 0);
			std::vector<Location> tmp(msgLen / sizeof(Location));
			assert(!tmp.empty());

			if (recvFixedSizeMessage(ConnectSocket, &tmp[0], int(msgLen)))
			{
				printf("Got messages!\n");
				for (auto m : tmp)
				{
					printf("%s: {%0.2f, %0.2f}\n", m.id, m.x, m.y);
				}
			}
			else
			{
				printf("Receive error!\n");
				break;
			}
		}
		else
		{
			printf("Receive error!\n");
			break;
		}
	}


	iResult = closesocket(ConnectSocket);
	if (iResult == SOCKET_ERROR) {
		wprintf(L"closesocket function failed with error: %ld\n", WSAGetLastError());
		WSACleanup();
		return 1;
	}
}

