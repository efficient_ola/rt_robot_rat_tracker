/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#ifndef _ImGuiX_h_
#define _ImGuiX_h_

#include <imgui/imgui.h>
#include <imgui/imgui_internal.h>
#include <string>
#include <algorithm>
#include <vector>
#include <map>
#include <functional>
#include <linmath/float3.h>
#include <linmath/float4.h>

namespace ImGuiX
{
	std::string &getTipText();
	bool wasHotKeyPressed(int keyId);
	std::string getHotKeyInfo(int keyId);
	std::function<std::string(int)> &getKeyToTextMap();
	void onKeyPress(int keyId);
	void clearHotKeyState();

	inline bool Button(const std::string &label, bool enabled = true, int hotKey = -1)
	{
		if (!enabled)
		{
			ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
			ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
		}
		bool tmp = ImGui::Button(label.c_str(), ImVec2(-1.0f, 0.0f));
		if (!enabled)
		{
			ImGui::PopItemFlag();
			ImGui::PopStyleVar();
		}
		if (enabled && wasHotKeyPressed(hotKey))
		{
			return true;
		}
		return tmp;
	}
	inline std::string InputText(const std::string &inStr, const char *label = "##hidden")
	{
		char buffer[256];
		strncpy(buffer, inStr.c_str(), std::min(sizeof(buffer), inStr.size() + 1));
		buffer[sizeof(buffer) - 1] = 0;

		ImGui::InputText(label, buffer, sizeof(buffer));

		return buffer;
	}

	namespace detail
	{
		// Wow, did this ever get far too complicated...
		struct InputBackCaller
		{
			std::function<std::string(std::string)> strModFn;
			std::function<int(ImWchar)> charFilterFn;
			static int callback(ImGuiTextEditCallbackData *data)
			{
				InputBackCaller *thisone = reinterpret_cast<InputBackCaller *>(data->UserData);
				if (data->EventFlag == ImGuiInputTextFlags_CallbackCharFilter)
				{
					if (thisone->charFilterFn)
					{
						return (thisone->charFilterFn)(data->EventChar);
					}
				}
				else if (thisone->strModFn)
				{
					std::string tmp = data->Buf;
					tmp = (thisone->strModFn)(tmp);
					int i = 0;
					for (auto c : tmp)
					{
						data->Buf[i++] = c;
					}
					data->Buf[i] = 0;
					data->CursorPos = std::min(i, data->CursorPos);
					data->BufTextLen = i;
					data->BufDirty = true;
				}
				return 0;
			}
		};
	}

	template <typename CB_FN_T, typename CHAR_CB_FN_T>
	inline std::string InputText(const std::string &inStr, CB_FN_T strChangeCb, CHAR_CB_FN_T charCheckFn = std::function<int(ImWchar)>(), const char *label = "##hidden")
	{
		char buffer[256];
		strncpy(buffer, inStr.c_str(), std::min(sizeof(buffer), inStr.size() + 1));
		buffer[sizeof(buffer) - 1] = 0;

		detail::InputBackCaller bc;
		bc.strModFn = strChangeCb;
		bc.charFilterFn = charCheckFn;
		ImGui::InputText(label, buffer, sizeof(buffer), ImGuiInputTextFlags_CallbackAlways | ImGuiInputTextFlags_CallbackCharFilter, &detail::InputBackCaller::callback, &bc);

		return buffer;
	}
	inline bool PropertyBegin(const std::string &name, const std::string &tipText)
	{
		ImGui::AlignTextToFramePadding();
		bool clicked = ImGui::Selectable(name.c_str(), false, ImGuiSelectableFlags_SpanAllColumns);
		ImGui::SetItemAllowOverlap();
		if (ImGui::IsItemHovered())
		{
			getTipText() = tipText;
		}

		ImGui::NextColumn();

		ImGui::PushItemWidth(-1);
		ImGui::PushID(name.c_str());

		return clicked;
	}

	inline void PropertyEnd()
	{
		ImGui::PopID();
		ImGui::PopItemWidth();
		ImGui::NextColumn();
	}

	inline void Property(const std::string &name, std::string &value, const std::string &tipText = std::string())
	{
		PropertyBegin(name, tipText);
		value = InputText(value);
		PropertyEnd();
	}


	inline void Property(const std::string &name, chag::float3 &value, const std::string &tipText = std::string())
	{
		PropertyBegin(name, tipText);
		ImGui::InputFloat3("##value", &value[0], 3);
		PropertyEnd();
	}


	inline void Property(const std::string &name, float &value, const std::string &tipText = std::string())
	{
		PropertyBegin(name, tipText);
		ImGui::InputFloat("##value", &value, 0.0f, 0.0f, 3);
		PropertyEnd();
	}

	inline void Property(const std::string &name, int &value, const std::string &tipText = std::string())
	{
		PropertyBegin(name, tipText);
		ImGui::InputInt("##value", &value);
		PropertyEnd();
	}
	
	inline void Property(const std::string &name, bool &value, const std::string &tipText = std::string())
	{
		PropertyBegin(name, tipText);
		ImGui::Checkbox("##value", &value);
		PropertyEnd();
	}

	inline void PropertyPopupSelector(const std::string &name, std::string &value, const std::vector<std::string> &options, const std::string &tipText = std::string())
	{
		bool propClick = PropertyBegin(name, tipText);
		if (ImGui::Button(value.c_str()) || propClick)
		{
			ImGui::OpenPopup("Selector");
		}

		if (ImGui::BeginPopup("Selector"))
		{
			for (const std::string &o : options)
			{
				if (ImGui::MenuItem(o.c_str(), nullptr, o == value))
				{
					value = o;
				}
			}
			ImGui::EndPopup();
		}

		PropertyEnd();
	}

	inline bool Property(const std::string &name, std::string &value, const std::vector<std::string> &options, const std::string &tipText = std::string())
	{
		std::string prevValue = value;
		PropertyBegin(name, tipText);

		int index = -1;
		std::vector<const char *> names;
		int numItems = 0;
		for (const std::string &o : options)
		{
			if (o == value)
			{
				index = numItems;
			}
			names.push_back(o.c_str());
			++numItems;
		}

		ImGui::Combo("##value", &index, names.data(), numItems);
		if (index >= 0 && index < options.size())
		{
			value = options[index];
		}
		PropertyEnd();

		return prevValue != value;
	}	
	
	inline void PropertyListBegin()
	{
		ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(2, 1));
		ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(4, 2));
		ImGui::PushStyleColor(ImGuiCol_HeaderHovered, (ImVec4)ImColor(0.2f, 0.2f, 0.3f, 1.0f));
		ImGui::Columns(2);
	}
	
	inline bool PropertyButton(const std::string &label, const std::string &tipText = std::string(), bool enabled = true, int hotKey = -1)
	{
		ImGui::Columns(1);
		ImGui::PushItemWidth(-1.0f);
		bool res = ImGuiX::Button(label.c_str(), enabled, hotKey);
		ImGui::PopItemWidth();
		ImGui::Columns(2);
		if (ImGui::IsItemHovered())
		{
			getTipText() = tipText + getHotKeyInfo(hotKey);
		}
		return res;
	}

	inline void PropertyListEnd(const std::string &infoText = std::string())
	{
		ImGui::Columns(1);

		const std::string &tiptext = !infoText.empty() ? infoText : getTipText();
		if (!tiptext.empty())
		{
			ImGui::Separator();
			ImGui::PushItemWidth(-1);
			ImGui::TextWrapped(tiptext.c_str());
			ImGui::PopItemWidth();
		}

		ImGui::PopStyleColor();
		ImGui::PopStyleVar(2);
	}
};


#endif // _ImGuiX_h_
