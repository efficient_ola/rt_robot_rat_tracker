/*********************************************************************************
Copyright (c) 2011-2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/

#ifndef _PerformanceTimer_h_
#define _PerformanceTimer_h_

#include "IntTypes.h"
#include "Assert.h"
#include <stdio.h>

/**
 * Used to time some section of code, is reasonably accurate.
 */
class PerformanceTimer
{
public:
  PerformanceTimer() : m_ticksAtStart(0), m_accumulatedTicks(0), m_isRunning(false)
  {
  }

  /**
   * Start the performance timer, after this the getElapsedTime() will return meaningfuil values.
   */
  void start()
  {
    ASSERT(!m_isRunning);
    m_isRunning = true;
    m_ticksAtStart = getTickCount();
  }

  /**
   * After the timer is stopped getElapsedTime() will return the same value until started again.
   */
  void stop()
  {
    ASSERT(m_isRunning);
    m_accumulatedTicks += getTickCount() - m_ticksAtStart;
    m_isRunning = false;
  }

  /**
   * restart is just a shorthand for stop();start();
   */
  void restart() { stop(); start(); }

  /**
   * Get the elapsed time in seconds, if the timer is running it gets the current elapsed time 
   * if it has been stopped, it takes the elapsed time when stop() was called.
   */
  double getElapsedTime()
  {
    uint64_t ticks = getTickCount();
    // SUPERHACK, my computer reports decreasing time at times, need mobo driver update perhaps... weird.
    // - Does not seem to happen after an AMD processor driver install...
    if (ticks < m_ticksAtStart)
    {
    //printf("get: %I64d\n", ticks);
      ticks = m_ticksAtStart;
    }
    return ticksToSeconds((m_isRunning ? ticks - m_ticksAtStart : m_accumulatedTicks));
  }

  /**
   * Get the current tick count, in whatever unit the machine pleases.
   */
  static uint64_t getTickCount();
  /**
   * Returns the number of ticks per second, use for converting the above tick count to something useful.
   */
  static uint64_t getTicksPerSecond()
  {
    static uint64_t ticksPerSecond = initTicksPerSecond();
    return ticksPerSecond;
  }

  /**
   * converts a given number of ticks to seconds.
   */
  static double ticksToSeconds(uint64_t ticks)
  {
    return double(ticks) / double(getTicksPerSecond());
  }

private:
  static uint64_t initTicksPerSecond();
  uint64_t m_ticksAtStart;
  uint64_t m_accumulatedTicks;
  bool m_isRunning;
};

#endif // _PerformanceTimer_h_
