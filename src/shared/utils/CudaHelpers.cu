/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#include "CudaHelpers.h"
#if ENABLE_CUDA
#include "CudaCheckError.h"


void megaMemset(uint32_t *ptr, uint32_t value, size_t size)
{
	CUDA_CHECK_ERROR("megaMemset_pre");
  if ((size & 1) == 0)
  {
    megaMemsetKernel<uint2, 120 * 256><<<120, 256>>>(reinterpret_cast<uint2*>(ptr), make_uint2(value, value), uint32_t(size / 2));
  }
  else
  {
    megaMemsetKernel<uint32_t, 120 * 256><<<120, 256>>>(ptr, value, uint32_t(size));
  }
	CUDA_CHECK_ERROR("megaMemset_post");
}

#if 0
void CudaHelpers_setupSpeadBitsTable()
{
	static uint32_t mortons[1024];
	for (uint32_t i = 0; i < 1024; ++i)
	{
		mortons[i] = spreadBits<10>(i, 3, 0);
	}
	cudaMemcpyToSymbol(g_mortonLut_3_10bit, mortons, sizeof(mortons));
}
#endif 

#endif // ENABLE_CUDA
