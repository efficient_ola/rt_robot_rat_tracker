/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#ifndef _ChagJsonUtils_h_
#define _ChagJsonUtils_h_

#include <nlohmann/json.hpp>
#include <linmath/float3.h>
#include <linmath/float4.h>
#include <linmath/float4x4.h>
#include <linmath/Aabb.h>

namespace chag
{
	inline void to_json(nlohmann::json& j, const float3 &p)
	{
		j["x"] = p.x;
		j["y"] = p.y;
		j["z"] = p.z;
	}
	inline void from_json(const nlohmann::json& j, float3 &p)
	{
		p.x = j.at("x").get<float>();;
		p.y = j.at("y").get<float>();;
		p.z = j.at("z").get<float>();;
	}


	inline void to_json(nlohmann::json& j, const float4 &p)
	{
		j["x"] = p.x;
		j["y"] = p.y;
		j["z"] = p.z;
		j["w"] = p.w;
	}
	inline void from_json(const nlohmann::json& j, float4 &p)
	{
		p.x = j.at("x").get<float>();;
		p.y = j.at("y").get<float>();;
		p.z = j.at("z").get<float>();;
		p.w = j.at("w").get<float>();;
	}


	inline void to_json(nlohmann::json& j, const float4x4 &m)
	{
		j["c1"] = m.c1;
		j["c2"] = m.c2;
		j["c3"] = m.c3;
		j["c4"] = m.c4;
	}
	inline void from_json(const nlohmann::json& j, float4x4 &m)
	{
		m.c1 = j.at("c1").get<float4>();;
		m.c2 = j.at("c2").get<float4>();;
		m.c3 = j.at("c3").get<float4>();;
		m.c4 = j.at("c4").get<float4>();;
	}

	inline void to_json(nlohmann::json& j, const float3x3 &m)
	{
		j["c1"] = m.c1;
		j["c2"] = m.c2;
		j["c3"] = m.c3;
	}
	inline void from_json(const nlohmann::json& j, float3x3 &m)
	{
		m.c1 = j.at("c1").get<float3>();;
		m.c2 = j.at("c2").get<float3>();;
		m.c3 = j.at("c3").get<float3>();;
	}

	inline void to_json(nlohmann::json& j, const Aabb &aabb)
	{
		j["min"] = aabb.min;
		j["max"] = aabb.max;
	}
	inline void from_json(const nlohmann::json& j, Aabb &aabb)
	{
		aabb.min = j.at("min").get<float3>();;
		aabb.max = j.at("max").get<float3>();;
	}

}

#endif // _ChagJsonUtils_h_
