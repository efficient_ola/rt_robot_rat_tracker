/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#ifndef _TweakableVarImGui_h_
#define _TweakableVarImGui_h_

#include "TweakableVar.h"

#include <imgui/imgui.h>
#include <algorithm>
#include "ImGuiX.h"

namespace TweakableVar
{


// waiting for C++17
template <typename T>
inline T clamp(const T& v, const T& lo, const T& hi)
{
	return std::min(std::max(v, lo), hi);
}

template <typename T, typename WIDGET_T>
struct TweakVarUiBaseImpl : public TweakVarUiBase
{
	using WidgetType = WIDGET_T;
	TweakVarUiBaseImpl(Variable<T> *var, const WidgetType &widget, bool modifiable) :
		TweakVarUiBase(widget.getInfo(), modifiable),
		m_var(var),
		m_widget(widget)
	{
	}

	virtual const std::string &getId() { return m_var->m_name; }

	virtual void onKey(int ch)
	{
		T val = m_var->value();
		m_widget.onKey(ch, val);
		m_var->setValue(val);
	}

	Variable<T> *m_var;
	WidgetType m_widget;
};


// Empty 'base template' for the variable UI implementations, specialized below for each widget and type, and imlemented usim ImGui.
// This should be the only UI-library specific part to this code.
template <typename T, typename WIDGET_T>
struct TweakVarUiImGui : public TweakVarUiBaseImpl<T, WIDGET_T>
{
};

template <>
struct TweakVarUiImGui<float, TweakVarUiWidgetTypes::Selector<float>::Slider> : public TweakVarUiBaseImpl<float, TweakVarUiWidgetTypes::Selector<float>::Slider>
{
	using TweakVarUiBaseImpl<float, TweakVarUiWidgetTypes::Selector<float>::Slider>::TweakVarUiBaseImpl;

	virtual void render()
	{
		//ImGui::InputFloat("##value", &m_var->m_value, 1.0f);

		float val = m_var->value();
		//ImGui::DragFloat("##value", &m_var->m_value, m_widget.step, m_widget.min, m_widget.max);
		if (m_widget.max <= 0.01)
		{
			ImGui::SliderFloat("##value", &val, /*m_widget.step,*/ m_widget.min, m_widget.max, "%0.5e");
		}
		else
		{
			ImGui::SliderFloat("##value", &val, /*m_widget.step,*/ m_widget.min, m_widget.max);
		}
		m_var->setValue(val);
	}

	virtual std::string getInfo()
	{
		return m_widget.getInfo() + "\nuse CTRL+click to edit";
	}
};

template <>
struct TweakVarUiImGui<float, TweakVarUiWidgetTypes::Selector<float>::Input> : public TweakVarUiBaseImpl<float, TweakVarUiWidgetTypes::Selector<float>::Input>
{
	using TweakVarUiBaseImpl<float, TweakVarUiWidgetTypes::Selector<float>::Input>::TweakVarUiBaseImpl;

	virtual void render()
	{
		//ImGui::InputFloat("##value", &m_var->m_value, 1.0f);

		//ImGui::DragFloat("##value", &m_var->m_value, m_widget.step, m_widget.min, m_widget.max);
		float v = m_var->value();
		ImGui::InputFloat("##value", &v, m_widget.step, 100, 3, m_modifiable ? 0 : ImGuiInputTextFlags_ReadOnly);
		m_var->setValue(clamp(v, m_widget.min, m_widget.max));
	}
};

template <>
struct TweakVarUiImGui<int, TweakVarUiWidgetTypes::Selector<int>::Slider> : public TweakVarUiBaseImpl<int, TweakVarUiWidgetTypes::Selector<int>::Slider>
{
	using TweakVarUiBaseImpl<int, TweakVarUiWidgetTypes::Selector<int>::Slider>::TweakVarUiBaseImpl;

	virtual void render()
	{
		//ImGui::InputFloat("##value", &m_var->m_value, 1.0f);

		//ImGui::DragFloat("##value", &m_var->m_value, m_widget.step, m_widget.min, m_widget.max);
		int tmp = m_var->value();
		ImGui::SliderInt("##value", &tmp, /*m_widget.step,*/ m_widget.min, m_widget.max);
		m_var->setValue(clamp(tmp, m_widget.min, m_widget.max));
	}

	virtual std::string getInfo()
	{
		return m_widget.getInfo() + "\nuse CTRL+click to edit";
	}
};

template <>
struct TweakVarUiImGui<int, TweakVarUiWidgetTypes::Selector<int>::Input> : public TweakVarUiBaseImpl<int, TweakVarUiWidgetTypes::Selector<int>::Input>
{
	using TweakVarUiBaseImpl<int, TweakVarUiWidgetTypes::Selector<int>::Input>::TweakVarUiBaseImpl;

	virtual void render()
	{
		//ImGui::InputFloat("##value", &m_var->m_value, 1.0f);

		//ImGui::DragFloat("##value", &m_var->m_value, m_widget.step, m_widget.min, m_widget.max);
		int tmp = m_var->value();
		ImGui::InputInt("##value", &tmp, m_widget.step, 100, m_modifiable ? 0 : ImGuiInputTextFlags_ReadOnly);
		m_var->setValue(clamp(tmp, m_widget.min, m_widget.max));
	}
};

template <>
struct TweakVarUiImGui<bool, TweakVarUiWidgetTypes::Selector<bool>::Toggle> : public TweakVarUiBaseImpl<bool, TweakVarUiWidgetTypes::Selector<bool>::Toggle>
{
	using TweakVarUiBaseImpl<bool, TweakVarUiWidgetTypes::Selector<bool>::Toggle>::TweakVarUiBaseImpl;

	virtual void render()
	{
		//ImGui::InputFloat("##value", &m_var->m_value, 1.0f);
		//ImGui::DragFloat("##value", &m_var->m_value, m_widget.step, m_widget.min, m_widget.max);
		bool tmp = m_var->value();
		ImGui::Checkbox("##value", &tmp);
		m_var->setValue(tmp);
	}
};

template <>
struct TweakVarUiImGui<int, TweakVarUiWidgetTypes::Selector<int>::Enum> : public TweakVarUiBaseImpl<int, TweakVarUiWidgetTypes::Selector<int>::Enum>
{
	using TweakVarUiBaseImpl<int, TweakVarUiWidgetTypes::Selector<int>::Enum>::TweakVarUiBaseImpl;

	virtual void render()
	{
		int index = 0;

		int v = m_var->value();
		std::vector<const char *> names;
		int numItems = 0;
		for (auto kv : m_widget.items)
		{
			if (kv.second == v)
			{
				index = numItems;
			}
			names.push_back(kv.first);
			++numItems;
		}

		bool ret = ImGui::Combo("##value", &index, &names[0], numItems);
		m_var->setValue(m_widget.items[index].second);
	}
};





// Implementation/Extension of Tweakable var that also registers UI component when created.
template <typename T, typename WIDGET_T>
struct TweakVarImGui : public Variable<T>
{
	TweakVarImGui(const T &value, const std::string &name, bool serialize, bool modifiable, WIDGET_T widget)
		: Variable<T>(value, name, serialize),
		m_ui(this, widget, modifiable)
	{
		VariableManager::get().addUi(&m_ui);
	}

	void operator = (const T &v)
	{
		setValue(v);
	}

	operator T() const
	{
		return value();
	}

	void operator += (const T &v)
	{
		setValue(value() + v);
	}


	void operator -= (const T &v)
	{
		setValue(value() - v);
	}

	TweakVarUiImGui<T, WIDGET_T> m_ui;
};

// ... and marcos to help
// TODO: work on simplifying/reducing need for macros
#define DECLARE_TWEAKABLEVAR_UI_G(_type_, _name_, _value_, _def_) TweakableVar::TweakVarImGui<_type_, decltype(TweakableVar::TweakVarUiWidgetTypes::Selector<_type_>::_def_)> _name_(_value_, #_name_ + 2, true, true, TweakableVar::TweakVarUiWidgetTypes::Selector<_type_>::_def_)
#define DECLARE_TWEAKABLEVAR_UI_G_NS(_type_, _name_, _value_, _def_) TweakableVar::TweakVarImGui<_type_, decltype(TweakableVar::TweakVarUiWidgetTypes::Selector<_type_>::_def_)> _name_(_value_, #_name_ + 2, false, true, TweakableVar::TweakVarUiWidgetTypes::Selector<_type_>::_def_)
#define DECLARE_TWEAKABLEVAR_UI_G_RO(_type_, _name_, _value_, _def_) TweakableVar::TweakVarImGui<_type_, decltype(TweakableVar::TweakVarUiWidgetTypes::Selector<_type_>::_def_)> _name_(_value_, #_name_ + 2, false, false, TweakableVar::TweakVarUiWidgetTypes::Selector<_type_>::_def_)


template <typename T, typename D_T>
inline T getDynTweakVarValue(const char *name, const T &defaultValue, const D_T &def)
{
	// check for existence and create variable in atomic fashion
	Variable<T> *v = dynamic_cast<Variable<T> *>(VariableManager::get().atomicGetCreate(name, [&]()
	{
		// this auto-registers the variable (we do it using lambda to be able to create using types at this level...
		Variable<T> *v = new TweakableVar::TweakVarImGui<T, D_T>(defaultValue, name, true, true, def);
		v->loadFromConfig();
	}));

	if (v)
	{
		return v->value();
	}

	return defaultValue;
}

// pure Get without create as if not existing
template <typename T>
inline T getDynTweakVarValue(const char *name, const T &defaultValue)
{
	// check for existence and create variable in atomic fashion
	Variable<T> *v = dynamic_cast<Variable<T> *>(VariableManager::get().atomicGetCreate(name, []() {}));

	if (v)
	{
		return v->value();
	}

	return defaultValue;
}


template <typename T>
inline void setDynTweakVarValue(const char *name, const T &value)
{
	VariableBase *bv = VariableManager::get().getVar(name);
	if (bv)
	{
		Variable<T> *v = dynamic_cast<Variable<T> *>(bv);
		if (v)
		{
			v->setValue(value);
		}
	}
}

#define DYNAMIC_TWEAKABLEVAR_GET(_type_, _name_, _value_) TweakableVar::getDynTweakVarValue<_type_>(_name_, _value_)


#define DYNAMIC_TWEAKABLEVAR_UI(_type_, _name_, _value_, _def_) TweakableVar::getDynTweakVarValue<_type_>(_name_, _value_, TweakableVar::TweakVarUiWidgetTypes::Selector<_type_>::_def_)

//#define DYNAMIC_TWEAKABLEVAR_UI(_type_, _name_, _value_, _def_) (_value_)

} // namespace TweakableVar


// we add some wrapper functions to make the ImGuiX functions work for tweakable variables
namespace ImGuiX
{

template <typename T>
inline void Property(const std::string &name, TweakableVar::Variable<T> &var, const std::string &tipText = std::string())
{
	T value = var.value();
	Property(name, value, tipText);
	var.setValue(value);
}


inline bool Property(const std::string &name, TweakableVar::Variable<std::string> &var, const std::vector<std::string> &options, const std::string &tipText = std::string())
{
	std::string value = var.value();
	bool res = Property(name, value, options, tipText);
	var.setValue(value);
	return res;
}

}


#endif // _TweakableVarImGui_h_
