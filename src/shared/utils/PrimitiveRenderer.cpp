/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#include "PrimitiveRenderer.h"
#include <glad/glad.h>
#include <linmath/float4x4.h>
#include <utils/SimpleShader.h>
#include <vector>
#include <cmath>

namespace chag
{


void PrimitiveRenderer::init(SimpleShader *defaultShader)
{
	m_transform = chag::make_identity<float4x4>();
	m_defaultShader = defaultShader;
	createSphereModel();
	buildAabb();
	buildPlane();
	buildQuadGrid();
	buildLine();
	buildCone();
	buildDisc();

	m_pointsPosBuffer.init(1014*1024, nullptr, GL_STREAM_DRAW);
	m_pointsColBuffer.init(1014*1024, nullptr, GL_STREAM_DRAW);
	//m_pointsSizeBuffer.init(1);

	glGenVertexArrays(1, &m_pointsVao);
	glBindVertexArray(m_pointsVao);

	m_pointsPosBuffer.bind();
	glVertexAttribPointer(AA_Position, m_pointsPosBuffer.elementSize() / sizeof(float), GL_FLOAT, false, 0, 0);
	glEnableVertexAttribArray(AA_Position);

	m_pointsColBuffer.bind();
	glVertexAttribPointer(AA_Colour, m_pointsColBuffer.elementSize() / sizeof(float), GL_FLOAT, false, 0, 0);
	glEnableVertexAttribArray(AA_Colour);

	//m_pointsSizeBuffer.bind();
	//glVertexAttribPointer(AA_Position, 3, GL_FLOAT, false, 0, 0);
	//glEnableVertexAttribArray(AA_Position);
	glBindVertexArray(0);



	m_quadVerts.init(4);
	float2 quadUvs[4] = { { 0.0f, 0.0f },{ 0.0f, 1.0f },{ 1.0f, 1.0f },{ 1.0f, 0.0f } };
	m_quadUvs.init(4, quadUvs);
	//m_pointsSizeBuffer.init(1);
	int quadInds[6] = { 0,1,2, 2,3,0 };
	m_quadInds.init(6, quadInds);

	glGenVertexArrays(1, &m_quadVao);
	glBindVertexArray(m_quadVao);

	m_quadVerts.bind();
	glVertexAttribPointer(AA_Position, m_quadVerts.elementSize() / sizeof(float), GL_FLOAT, false, 0, 0);
	glEnableVertexAttribArray(AA_Position);

	m_quadUvs.bind();
	glVertexAttribPointer(AA_TexCoord, m_quadUvs.elementSize() / sizeof(float), GL_FLOAT, false, 0, 0);
	glEnableVertexAttribArray(AA_TexCoord);

	m_quadInds.bind(GL_ELEMENT_ARRAY_BUFFER);

	//m_pointsSizeBuffer.bind();
	//glVertexAttribPointer(AA_Position, 3, GL_FLOAT, false, 0, 0);
	//glEnableVertexAttribArray(AA_Position);
	glBindVertexArray(0);

}

const int g_numSphereSubdivs = 2;
#define POW4(x) (1 << (2 * (x)))
#define SPHERE_SIZE (8 * 3 * POW4(g_numSphereSubdivs))


static void subDivide(chag::float3 *&dest, const chag::float3 &v0, const chag::float3 &v1, const chag::float3 &v2, int level)
{
	if (level)
	{
		chag::float3 v3 = normalize(v0 + v1);
		chag::float3 v4 = normalize(v1 + v2);
		chag::float3 v5 = normalize(v2 + v0);

		subDivide(dest, v0, v3, v5, level - 1);
		subDivide(dest, v3, v4, v5, level - 1);
		subDivide(dest, v3, v1, v4, level - 1);
		subDivide(dest, v5, v4, v2, level - 1);
	}
	else
	{
		*dest++ = v0;
		*dest++ = v1;
		*dest++ = v2;
	}
}

void PrimitiveRenderer::createSphereModel()
{
	chag::float3 sphere[SPHERE_SIZE];
	chag::float3 *dest = sphere;

	subDivide(dest, chag::make_vector<float>(0, -1, 0), chag::make_vector<float>(1, 0, 0), chag::make_vector<float>(0, 0, 1), g_numSphereSubdivs);
	subDivide(dest, chag::make_vector<float>(0, 1, 0), chag::make_vector<float>(0, 0, 1), chag::make_vector<float>(1, 0, 0), g_numSphereSubdivs);
	subDivide(dest, chag::make_vector<float>(0, 1, 0), chag::make_vector<float>(-1, 0, 0), chag::make_vector<float>(0, 0, 1), g_numSphereSubdivs);
	subDivide(dest, chag::make_vector<float>(0, -1, 0), chag::make_vector<float>(0, 0, 1), chag::make_vector<float>(-1, 0, 0), g_numSphereSubdivs);

	subDivide(dest, chag::make_vector<float>(0, 1, 0), chag::make_vector<float>(1, 0, 0), chag::make_vector<float>(0, 0, -1), g_numSphereSubdivs);
	subDivide(dest, chag::make_vector<float>(0, -1, 0), chag::make_vector<float>(-1, 0, 0), chag::make_vector<float>(0, 0, -1), g_numSphereSubdivs);
	subDivide(dest, chag::make_vector<float>(0, -1, 0), chag::make_vector<float>(0, 0, -1), chag::make_vector<float>(1, 0, 0), g_numSphereSubdivs);
	subDivide(dest, chag::make_vector<float>(0, 1, 0), chag::make_vector<float>(0, 0, -1), chag::make_vector<float>(-1, 0, 0), g_numSphereSubdivs);

	g_sphereVertexBuffer.init(SPHERE_SIZE, sphere);

	glGenVertexArrays(1, &m_sphereVaob);
	glBindVertexArray(m_sphereVaob);

	g_sphereVertexBuffer.bind();
	glVertexAttribPointer(AA_Position, 3, GL_FLOAT, false, 0, 0);
	glEnableVertexAttribArray(AA_Position);

	glVertexAttribPointer(AA_Normal, 3, GL_FLOAT, false, 0, 0);
	glEnableVertexAttribArray(AA_Normal);

	g_sphereVertexBuffer.unbind();
	glBindVertexArray(0);
}


void PrimitiveRenderer::drawSphere(const float3 &pos, float size, SimpleShader *shader, bool wireFrame)
{
	float4x4 tfm = make_translation(pos) * make_scale<float4x4>(size);
	shader->setUniform("modelTransform", m_transform * tfm);
	glBindVertexArray(m_sphereVaob);
	glDrawArrays(wireFrame ? GL_LINE_LOOP : GL_TRIANGLES, 0, SPHERE_SIZE);
}


void PrimitiveRenderer::drawHemiSphere(const float3 &pos, float size, SimpleShader *shader, bool wireFrame)
{
	float4x4 tfm = make_translation(pos) * make_scale<float4x4>(size);
	shader->setUniform("modelTransform", m_transform * tfm);
	glBindVertexArray(m_sphereVaob);
	glDrawArrays(wireFrame ? GL_LINE_LOOP : GL_TRIANGLES, 0, SPHERE_SIZE / 2);
}



void PrimitiveRenderer::buildCone()
{
	// build cone of height 1 and base diameter 1, aligned with z axis
	// have to duplicate apex for normals
	const int baseVerts = 16;
	float3 verts[baseVerts * 2];
	float3 normals[baseVerts * 2];

	// positions and normals for base verts
	for (int i = 0; i < baseVerts; ++i)
	{
		float a = 2.0f * g_pi * float(i) / float(baseVerts);
		verts[i] = make_vector(cosf(a), sinf(a), 1.0f);

		// Vetor from base through middle of edge
		float3 normal =  normalize(verts[i] * 0.5f - make_vector(0.0f, 0.0f, 1.0f));
		normals[i] = normal;
	}

	// positions and normals for apex verts
	for (int i = 0; i < baseVerts; ++i)
	{
		verts[baseVerts + i] = make_vector(0.0f, 0.0f, 0.0f);

		// Vetor average of 2 verts
		float3 n0 = normals[i];
		float3 n1 = normals[(i+1) % baseVerts];
		normals[baseVerts + i] = normalize(n0 + n1);
	}

	int inds[baseVerts * 3];
	for (int i = 0; i < baseVerts; ++i)
	{
		// apex
		inds[i * 3 + 0] = i + baseVerts;
		// v0
		inds[i * 3 + 1] = (i+1) % baseVerts;
		// v1
		inds[i * 3 + 2] = i;
	}



	m_coneVerts.init(baseVerts * 2, verts);
	m_coneNormals.init(baseVerts * 2, normals);
	//m_coneUvs.init(numVerts, uvOut);
	m_coneInds.init(baseVerts * 3, inds);

	glGenVertexArrays(1, &m_coneVao);
	glBindVertexArray(m_coneVao);

	m_coneInds.bind(GL_ELEMENT_ARRAY_BUFFER);

	m_coneVerts.bind();
	glVertexAttribPointer(AA_Position, 3, GL_FLOAT, false, 0, 0);
	glEnableVertexAttribArray(AA_Position);

	m_coneNormals.bind();
	glVertexAttribPointer(AA_Normal, 3, GL_FLOAT, false, 0, 0);
	glEnableVertexAttribArray(AA_Normal);

	//g_coneUvs.bind();
	//glVertexAttribPointer(AA_TexCoord, 2, GL_FLOAT, false, 0, 0);
	//glEnableVertexAttribArray(AA_TexCoord);
	//g_coneUvs.unbind();
	glBindVertexArray(0);

}

void PrimitiveRenderer::buildDisc()
{
	const int ringVerts = 16;
	float3 verts[ringVerts];
	float3 normals[ringVerts];
	float2 uvs[ringVerts];

	for (int i = 0; i < ringVerts; ++i)
	{
		float a = 2.0f * g_pi * float(i) / float(ringVerts);
		verts[i] = make_vector(cosf(a), sinf(a), 0.0f);
		normals[i] = make_vector(0.0f, 0.0f, 1.0f);
		uvs[i] = make_vector(cosf(a), sinf(a)) * 0.5f + 0.5f;
	}
	m_discVerts.init(ringVerts, verts);
	m_discNormals.init(ringVerts, normals);
	m_discUvs.init(ringVerts, uvs);

	glGenVertexArrays(1, &m_discVao);
	glBindVertexArray(m_discVao);

	m_discVerts.bind();
	glVertexAttribPointer(AA_Position, 3, GL_FLOAT, false, 0, 0);
	glEnableVertexAttribArray(AA_Position);

	m_discNormals.bind();
	glVertexAttribPointer(AA_Normal, 3, GL_FLOAT, false, 0, 0);
	glEnableVertexAttribArray(AA_Normal);

	m_discUvs.bind();
	glVertexAttribPointer(AA_TexCoord, 2, GL_FLOAT, false, 0, 0);
	glEnableVertexAttribArray(AA_TexCoord);
	m_discUvs.unbind();
}


void PrimitiveRenderer::drawDisc(const float3 &pos, const float3 &axis, float radius, SimpleShader *shader, bool wireFrame)
{
	float3 nAxis = normalize(axis);
	float3 up = { 0.0f, 0.0f, 1.0f };
	// nearly co-linear, make arbitrary perpendicular vector
	if (std::abs(dot(up, nAxis)) > 0.99f)
	{
		up = perpendicular(nAxis);
	}
	float4x4 tfm = make_matrix_from_zAxis(pos, axis, up) * make_scale<float4x4>(make_vector(radius, radius, 1.0f));
	shader->setUniform("modelTransform", m_transform * tfm);
	glBindVertexArray(m_discVao);
	glDrawArrays(wireFrame ? GL_LINE_LOOP : GL_TRIANGLE_FAN, 0, GLsizei(m_discVerts.size()));
	CHECK_GL_ERROR();
	glBindVertexArray(0);
}

void PrimitiveRenderer::drawPoints(int numPoints, const chag::float3 * positions, const chag::float3* colours)
{
	m_pointsPosBuffer.copyFromHost(positions, numPoints, true);
	m_pointsColBuffer.copyFromHost(colours, numPoints, true);
	//m_pointsSizeBuffer.copyFromHost(sizes, numPoints);

	glBindVertexArray(m_pointsVao);
	glDrawArrays(GL_POINTS, 0, GLsizei(numPoints));

}


void PrimitiveRenderer::drawCone(const float3 &pos, const float3 &axis, float height, float angleRad, SimpleShader *shader, bool wireFrame)
{
	float3 nAxis = normalize(axis);
	float3 up = { 0.0f, 0.0f, 1.0f };
	// nearly co-linear, make arbitrary perpendicular vector
	if (std::abs(dot(up, nAxis)) > 0.99f)
	{
		up = perpendicular(nAxis);
	}
	float b = std::tan(angleRad) * height;
	float3 scale = { b,b,height };
	float4x4 tfm = make_matrix_from_zAxis(pos, axis, up) * make_scale<float4x4>(scale);
	shader->setUniform("modelTransform", m_transform * tfm);
	glBindVertexArray(m_coneVao);
	glDrawElements(wireFrame ? GL_LINE_LOOP : GL_TRIANGLES, GLsizei(m_coneInds.size()), GL_UNSIGNED_INT, 0);
	CHECK_GL_ERROR();
	glBindVertexArray(0);
}



void PrimitiveRenderer::buildAabb()
{
#define CUBE_NUM_VERT           8
#define CUBE_NUM_FACES          6
#define CUBE_NUM_EDGE_PER_FACE  4
#define CUBE_VERT_PER_OBJ       (CUBE_NUM_FACES*CUBE_NUM_EDGE_PER_FACE)
#define CUBE_VERT_ELEM_PER_OBJ  (CUBE_VERT_PER_OBJ*3)
#define CUBE_VERT_PER_OBJ_TRI   (CUBE_VERT_PER_OBJ+CUBE_NUM_FACES*2)    /* 2 extra edges per face when drawing quads as triangles */

	const int numFaces = 6;
	const int numEdgePerFace = 4;

	static chag::float3 cube_v[CUBE_NUM_VERT] =
	{
		{  .5f, .5f, .5f},
		{ -.5f, .5f, .5f},
		{ -.5f, -.5f, .5f},
		{ .5f, -.5f, .5f},
		{ .5f, -.5f, -.5f},
		{ .5f, .5f, -.5f},
		{ -.5f, .5f, -.5f},
		{ -.5f, -.5f, -.5f },
	};
	/* Normal Vectors */
	static chag::float3 cube_n[numFaces] =
	{
		{ 0.0f, 0.0f, 1.0f },
		{ 1.0f, 0.0f, 0.0f },
		{ 0.0f, 1.0f, 0.0f },
		{ -1.0f, 0.0f, 0.0f },
		{ 0.0f, -1.0f, 0.0f },
		{ 0.0f, 0.0f, -1.0f },
	};

	static GLubyte cube_vi[CUBE_VERT_PER_OBJ] =
	{
		0, 1, 2, 3,
		0, 3, 4, 5,
		0, 5, 6, 1,
		1, 6, 7, 2,
		7, 4, 3, 2,
		4, 7, 6, 5
	};
	const int numVerts = numFaces * numEdgePerFace;
	static chag::float3 vertOut[numVerts];
	static chag::float3 normOut[numVerts];
	static chag::float2 uvOut[numVerts];

	const int numInds = numFaces * (numEdgePerFace + 1);
	static int inds[numInds];
	glEnable(GL_PRIMITIVE_RESTART_FIXED_INDEX);
	for (int i = 0; i<numFaces; i++)
	{
		int normIdx = i;
		int faceIdxVertIdx = i*numEdgePerFace; /* index to first element of "row" in vertex indices */
		
		for (int j = 0; j < numEdgePerFace; j++)
		{
			int outIdx = i*numEdgePerFace + j;
			int vertIdx = cube_vi[faceIdxVertIdx + j];

			vertOut[outIdx] = cube_v[vertIdx];
			normOut[outIdx] = cube_n[normIdx];
			uvOut[outIdx] = chag::make_vector(float(j % 2), float((j / 2) % 2));

			inds[i*(numEdgePerFace + 1) + j] = outIdx;
		}
		inds[i*(numEdgePerFace + 1) + numEdgePerFace] = 0xffffffff;
	}

	g_aabbVerts.init(numVerts, vertOut);
	g_aabbNormals.init(numVerts, normOut);
	g_aabbUvs.init(numVerts, uvOut);
	g_aabbInds.init(numInds, inds);

	glGenVertexArrays(1, &g_aabbVaob);
	glBindVertexArray(g_aabbVaob);

	g_aabbInds.bind(GL_ELEMENT_ARRAY_BUFFER);

	g_aabbVerts.bind();
	glVertexAttribPointer(AA_Position, 3, GL_FLOAT, false, 0, 0);
	glEnableVertexAttribArray(AA_Position);

	g_aabbNormals.bind();
	glVertexAttribPointer(AA_Normal, 3, GL_FLOAT, false, 0, 0);
	glEnableVertexAttribArray(AA_Normal);

	g_aabbUvs.bind();
	glVertexAttribPointer(AA_TexCoord, 2, GL_FLOAT, false, 0, 0);
	glEnableVertexAttribArray(AA_TexCoord);
	g_aabbUvs.unbind();
	glBindVertexArray(0);
}



void PrimitiveRenderer::buildLine()
{
	chag::float3 pts[2] = { {0.0f } };
	m_linePointsBuffer.init(2, pts);

	glGenVertexArrays(1, &m_lineVaob);
	glBindVertexArray(m_lineVaob);

	m_linePointsBuffer.bind();
	glVertexAttribPointer(AA_Position, 3, GL_FLOAT, false, 0, 0);
	glEnableVertexAttribArray(AA_Position);

	m_linePointsBuffer.unbind();
	glBindVertexArray(0);
}

void PrimitiveRenderer::drawAabb(const chag::Aabb &aabb, SimpleShader *shader, bool wireFrame)
{
	glBindVertexArray(g_aabbVaob);

	chag::float4x4 tfm = chag::make_translation(aabb.getCentre()) * chag::make_scale<chag::float4x4>(aabb.getDiagonal());

	shader->setUniform("modelTransform", m_transform * tfm);
	glEnable(GL_PRIMITIVE_RESTART_FIXED_INDEX);
	CHECK_GL_ERROR();

	glDrawElements(wireFrame ? GL_LINE_LOOP : GL_TRIANGLE_FAN, GLsizei(g_aabbInds.size()), GL_UNSIGNED_INT, 0);
	CHECK_GL_ERROR();
	glBindVertexArray(0);
}




void PrimitiveRenderer::drawLine(chag::float3 pos0, chag::float3 pos1, SimpleShader *shader)
{
	float3 pts[2] = { pos0, pos1 };
	m_linePointsBuffer.copyFromHost(pts, 2);

	glBindVertexArray(m_lineVaob);

	// TODO: use unit line and build tfm with scale from the points...
	chag::float4x4 tfm = chag::make_identity<chag::float4x4>();

	shader->setUniform("modelTransform", m_transform * tfm);
	CHECK_GL_ERROR();

	glDrawArrays(GL_LINES, 0, 2);
	CHECK_GL_ERROR();
	glBindVertexArray(0);
}


void PrimitiveRenderer::buildPlane()
{
	static chag::float3 plane_v[4] =
	{
		{1.0f, 1.0f, 0.0f  },
		{-1.0f, 1.0f, 0.0f },
		{-1.0f, -1.0f, 0.0f},
		{1.0f, -1.0f, 0.0f },
	};

	/* Normal Vectors */
	static chag::float3 plane_n[4] =
	{
		{ 0.0f, 0.0f, 1.0f },
		{ 0.0f, 0.0f, 1.0f },
		{ 0.0f, 0.0f, 1.0f },
		{ 0.0f, 0.0f, 1.0f },
	};
	static chag::float2 plane_uv[4] =
	{
		{ 1.0f, 1.0f },
		{ 0.0f, 1.0f },
		{ 0.0f, 0.0f },
		{ 1.0f, 0.0f },
	};

	g_planeVerts.init(4, plane_v);
	g_planeNormals.init(4, plane_n);
	g_planeUvs.init(4, plane_uv);

	glGenVertexArrays(1, &g_planeVaob);
	glBindVertexArray(g_planeVaob);

	g_planeVerts.bind();
	glVertexAttribPointer(AA_Position, 3, GL_FLOAT, false, 0, 0);
	glEnableVertexAttribArray(AA_Position);

	g_planeNormals.bind();
	glVertexAttribPointer(AA_Normal, 3, GL_FLOAT, false, 0, 0);
	glEnableVertexAttribArray(AA_Normal);

	g_planeUvs.bind();
	glVertexAttribPointer(AA_TexCoord, 2, GL_FLOAT, false, 0, 0);
	glEnableVertexAttribArray(AA_TexCoord);
	g_planeUvs.unbind();
	glBindVertexArray(0);
}



void PrimitiveRenderer::drawPlane(chag::float3 normal, chag::float3 point, chag::float3 alignToDir, float planeSize, SimpleShader *shader)
{
	normal = normalize(normal);
	chag::float3 biTangent = normalize(cross(alignToDir, normal));
	chag::float3 tangent = normalize(cross(biTangent, normal));

	glBindVertexArray(g_planeVaob);

	chag::float4x4 tfm = chag::make_matrix_from_zAxis(point, normal, biTangent) * chag::make_scale<chag::float4x4>(planeSize);
	shader->setUniform("modelTransform", m_transform * tfm);

	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}



void PrimitiveRenderer::drawPlane(chag::float3 normal, chag::float3 point, float planeSize, SimpleShader *shader)
{
	drawPlane(normal, point, perpendicular(normal), planeSize, shader);
}



void PrimitiveRenderer::buildQuadGrid()
{
	const int numPointsXY = s_maxQuadGrid + 1;
	std::vector<chag::float3> grid_v(numPointsXY * numPointsXY);
	std::vector<chag::float3> grid_n(numPointsXY * numPointsXY);
	std::vector<chag::float2> grid_uv(numPointsXY * numPointsXY);
	// identity rect, centered on origine in XY plane.
	for (int j = 0; j < numPointsXY; ++j)
	{
		for (int i = 0; i < numPointsXY; ++i)
		{
			chag::float2 uv = chag::make_vector(float(i), float(j)) / float(s_maxQuadGrid);
			grid_v[j * numPointsXY + i] = chag::make_vector3(uv * 2.0f - 1.0f, 0.0f);
			grid_n[j * numPointsXY + i] = chag::make_vector(0.0f, 0.0f, 1.0f);
			grid_uv[j * numPointsXY + i] = uv;
		}
	}

	// triangle list.
	std::vector<int> inds(s_maxQuadGrid * s_maxQuadGrid * 6);
	for (int j = 0; j < s_maxQuadGrid; ++j)
	{
		for (int i = 0; i < s_maxQuadGrid; ++i)
		{
			int *t = &inds[(j * s_maxQuadGrid + i) * 6];
			t[0] = (j * numPointsXY) + i;
			t[1] = (j * numPointsXY) + i + 1;
			t[2] = ((j + 1) * numPointsXY) + i;

			t[3] = t[2];// ((j + 1) * numPointsXY) + i;
			t[4] = t[1];// (j * numPointsXY) + i + 1;
			t[5] = ((j + 1) * numPointsXY) + i + 1;
		}
	}

	g_quadGridVerts.init(grid_v.size(), &grid_v[0]);
	g_quadGridNormals.init(grid_n.size(), &grid_n[0]);
	g_quadGridUvs.init(grid_uv.size(), &grid_uv[0]);
	g_quadGridInds.init(inds.size(), &inds[0]);

	glGenVertexArrays(1, &g_quadGridVaob);
	glBindVertexArray(g_quadGridVaob);

	g_quadGridInds.bind(GL_ELEMENT_ARRAY_BUFFER);

	g_quadGridVerts.bind();
	glVertexAttribPointer(AA_Position, 3, GL_FLOAT, false, 0, 0);
	glEnableVertexAttribArray(AA_Position);

	g_quadGridNormals.bind();
	glVertexAttribPointer(AA_Normal, 3, GL_FLOAT, false, 0, 0);
	glEnableVertexAttribArray(AA_Normal);

	g_quadGridUvs.bind();
	glVertexAttribPointer(AA_TexCoord, 2, GL_FLOAT, false, 0, 0);
	glEnableVertexAttribArray(AA_TexCoord);
	g_quadGridUvs.unbind();
	glBindVertexArray(0);
}



void PrimitiveRenderer::drawQuadGrid(chag::float3 normal, chag::float3 point, chag::float3 alignToDir, float planeSize, SimpleShader *shader)
{
	normal = normalize(normal);
	chag::float3 biTangent = normalize(cross(alignToDir, normal));
	chag::float3 tangent = normalize(cross(biTangent, normal));

	chag::float4x4 tfm = chag::make_matrix_from_zAxis(point, normal, biTangent) * chag::make_scale<chag::float4x4>(planeSize);
	shader->setUniform("modelTransform", m_transform * tfm);

	drawQuadGrid();
}


void PrimitiveRenderer::drawQuadGrid()
{
	glBindVertexArray(g_quadGridVaob);
	glDrawElements(GL_TRIANGLES, GLsizei(g_quadGridInds.size()), GL_UNSIGNED_INT, 0);
}


void PrimitiveRenderer::drawQuad(chag::float3 minV, chag::float3 maxV)
{
	//float2 quadUvs[4] = { { 0.0f, 0.0f },{ 0.0f, 1.0f },           { 1.0f, 1.0f }, { 1.0f, 0.0f } };
	float3 verts[4] =  {   minV,         {minV.x, maxV.y, minV.z}, maxV,           { maxV.x, minV.y, minV.z },	};
	m_quadVerts.copyFromHost(verts, 4);

	glBindVertexArray(m_quadVao);
	glDrawElements(GL_TRIANGLES, GLsizei(m_quadInds.size()), GL_UNSIGNED_INT, 0);
}
}; // namespace chag


