/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#ifndef _TweakableVarImGuiLinmath_h_
#define _TweakableVarImGuiLinmath_h_

#include "TweakableVarImGui.h"
#include <linmath/float2.h>

namespace TweakableVar
{


	namespace TweakVarUiWidgetTypes
	{
		template <typename T>
		struct LinmathWidget
		{
			LinmathWidget()
			{
			}

			const std::string &getInfo() const
			{
				return info;
			}

			void onKey(int ch, T &value)
			{
			}

			std::string info;
		};


		template <>
		struct Selector<chag::float2>
		{
			using Input = LinmathWidget<chag::float2>;
		};

	};

template <>
struct TweakVarUiImGui<chag::float2, TweakVarUiWidgetTypes::Selector<chag::float2>::Input> : public TweakVarUiBaseImpl<chag::float2, TweakVarUiWidgetTypes::Selector<chag::float2>::Input>
{
	using TweakVarUiBaseImpl<chag::float2, TweakVarUiWidgetTypes::Selector<chag::float2>::Input>::TweakVarUiBaseImpl;

	virtual void render()
	{
		//ImGui::InputFloat("##value", &m_var->m_value, 1.0f);

		//ImGui::DragFloat("##value", &m_var->m_value, m_widget.step, m_widget.min, m_widget.max);
		chag::float2 v = m_var->value();
		ImGui::InputFloat2("##value", &v.x, -1, m_modifiable ? 0 : ImGuiInputTextFlags_ReadOnly);
		//m_var->setValue(clamp(v, m_widget.min, m_widget.max));
		m_var->setValue(v);
	}
};

}


#endif // _TweakableVarImGuiLinmath_h_
