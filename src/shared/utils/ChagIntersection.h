/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#ifndef _chag_Intersection_h_
#define _chag_Intersection_h_

#include "Math.h"
#include <linmath/int2.h>
#include <linmath/int3.h>
#include <linmath/float3.h>
#include <linmath/float4.h>
#include <linmath/float3x3.h>
#include <linmath/Aabb.h>

/**
 * This file is a repository of intersection, culling, and suchlike, routines, using the chag::linmath types. 
 * In general the idea is to rip a useful function out of here, if it doesnt match completely, then tailor it.
 * It could probably be generalized to using templated idioms for the various bounding volume types, to make 
 * tests more useful. Templating on basic types is of course also possible, but well, I guess...
 */

namespace chag
{



/**
 * Single sided Moller-Trumbore test.
 */
inline bool intersectTriRay2(const float3 *verts, const float3 &rayDir, const float3 &rayOrig, float &resultT, float &resultU, float &resultV)
{
	const float g_defaultEpsilon = 0.000001f;
	const float EPSILON = g_defaultEpsilon;
	/* find vectors for two edges sharing vert0 */
	float3 edge1 = verts[1] - verts[0];
	float3 edge2 = verts[2] - verts[0];

	/* begin calculating determinant - also used to calculate U parameter */
	float3 pvec = cross(rayDir, edge2);

	/* if determinant is near zero, ray lies in plane of triangle */
	float det = dot(edge1, pvec);

	if (det < EPSILON)
			return false;

	/* calculate distance from vert0 to ray origin */
	float3 tvec = rayOrig - verts[0];

	/* calculate U parameter and test bounds */
	float u = dot(tvec, pvec);
	if (u < 0.0f || u > det)
			return false;

	/* prepare to test V parameter */
	float3 qvec = cross(tvec, edge1);

		/* calculate V parameter and test bounds */
	float v = dot(rayDir, qvec);
	if(v < 0.0f || u + v > det)
		return false;

	/* calculate t, scale parameters, ray intersects triangle */
	float t = dot(edge2, qvec);
	float inv_det = 1.0f / det;
	t *= inv_det;
	u *= inv_det;
	v *= inv_det;

	if(t > 0.0f && t < resultT)
	{
		resultT = t;
		resultU = u;
		resultV = v;
		return true;
	}

	return false;
}

/**
 * M�ller-Trumbore test which always compute the hit time and barycentric coords regardless of whether they are inside the triangle.
 * It still returns false for all misses. This is useful for example when performing tests, to allow for precision differences that
 * cause near misses to become hits and vice versa.
 */
inline bool intersectTriRay3(const float3 verts[], const float3 &rayDir, const float3 &rayOrig, float &resultT, float &resultU, float &resultV)
{
	const float g_defaultEpsilon = 0.000001f;
	const float EPSILON = g_defaultEpsilon;
	/* find vectors for two edges sharing vert0 */
	float3 edge1 = verts[1] - verts[0];
	float3 edge2 = verts[2] - verts[0];

	/* begin calculating determinant - also used to calculate U parameter */
	float3 pvec = cross(rayDir, edge2);

	/* if determinant is near zero, ray lies in plane of triangle */
	float det = dot(edge1, pvec);

	if (det < EPSILON)
			return false;

	/* calculate distance from vert0 to ray origin */
	float3 tvec = rayOrig - verts[0];

	/* calculate U parameter and test bounds */
	float u = dot(tvec, pvec);

	/* prepare to test V parameter */
	float3 qvec = cross(tvec, edge1);

		/* calculate V parameter and test bounds */
	float v = dot(rayDir, qvec);

	/* calculate t, scale parameters, ray intersects triangle */
	float t = dot(edge2, qvec);
	float inv_det = 1.0f / det;
	t *= inv_det;
	u *= inv_det;
	v *= inv_det;

	resultT = t;
	resultU = u;
	resultV = v;
	
	return t > 0.0f && u >= 0.0f && v >= 0.0f && u + v <= 1.0f;
}

// Overlap routines, compute an exact ovelap, but do not report exact intersection point or penetration

inline bool aabbOutsidePlane(float3 center, float3 extents, float4 plane)
{
	float dist = dot(make_vector4(center, 1.0f), plane);
	float radius = dot(extents, abs(make_vector3(plane)));

	return dist > radius;
}

inline bool aabbOutsidePlane(const Aabb &aabb, const float4 &plane)
{
	float3 center = aabb.getCentre();
	float3 extents = aabb.getHalfSize();

	float dist = dot(make_vector4(center, 1.0f), plane);
	float radius = dot(extents, abs(make_vector3(plane)));

	return dist > radius;
}



inline bool overlapsAabbSphere(const chag::Aabb &aabb, const chag::float3 &sphereCentre, float sphereRadius)
{
	float dmin = 0.0f;
	for(int i = 0; i < 3; i++ ) 
	{
		if( sphereCentre[i] < aabb.min[i] ) 
		{
			dmin += square(sphereCentre[i] - aabb.min[i] ); 
		}
		else if( sphereCentre[i] > aabb.max[i] ) 
		{
			dmin += square( sphereCentre[i] - aabb.max[i] );     
		}
	}
	return dmin <= square(sphereRadius);
} 


inline bool overlapsSphereSphere(const chag::float3 &sphereCentre1, float sphereRadius1, const chag::float3 &sphereCentre2, float sphereRadius2)
{
	return lengthSquared(sphereCentre1 - sphereCentre2) <= square(sphereRadius1 + sphereRadius2);
} 


// Culling routines, that is, such that do not compute an exact result, but rely on a heuristic.


/**
 * The plan is to 'simply', find the plane tangent to the cone, which is perpendicular to the plane spanned by
 * the cone axis and centre of the aabb. Then we perform a plane aabb test. I'm sure there are cases where the
 * test gives false positives, but what the heck.
 */
inline bool cullConeAabb(const float3 &coneApex, const float3 &coneAxis, const float coneAngle, const Aabb &aabb)
{
	// 1. find plane (well, base) in which normal lies, and which is perpendicular to axis and centre of aabb.
	float3 d = aabb.getCentre() - coneApex;
	// perpendicular to cone axis in plane of cone axis and aabb centre.
	float3 m = -normalize(cross(cross(d, coneAxis), coneAxis));
	// if coneAxis is already  we can skip that here (but then we need a lenght too for below!).
	float3 n = -tanf(coneAngle) * normalize(coneAxis) + m;
	float4 plane = make_vector4(n, -dot(n, coneApex));

	return !aabbOutsidePlane(aabb, plane) && overlapsAabbSphere(aabb, coneApex, length(coneAxis));
}


// Miscelaneous 


inline const float3 closestPointOnAabb(const Aabb& aabb, const float3& pt)
{
	return chag::min(chag::max(aabb.min, pt), aabb.max);
} 


inline const float distanceAabbPoint(const Aabb& aabb, float3& pt)
{
	float s, d = 0; 

	//find the square of the distance
	//from the sphere to the box

	for( long i=0 ; i<3 ; i++ )
	{
		if( pt[i] < aabb.min[i] )
		{ 
			s = pt[i] - aabb.min[i];
			d += s*s; 
		}
		else if( pt[i] > aabb.max[i] )
		{

			s = pt[i] - aabb.max[i];
			d += s*s; 
		}
	}
	return sqrtf(d);
} 

// NOTE: unsigned distance
inline float unsignedDistanceAabbPlane(const Aabb &aabb, const float4 &plane)
{
	float3 center = aabb.getCentre();
	float3 extents = aabb.getHalfSize();

	float dist = dot(make_vector4(center, 1.0f), plane);
	float radius = dot(extents, abs(make_vector3(plane)));

	return std::max(0.0f, fabsf(dist) - radius);
}

inline void intersectRaySlab(float min, float max, float rayO, float rayDir, float &t0, float &t1)
{
	float invRayDir = 1.0f / rayDir;
	t0 = (min - rayO) * invRayDir;
	t1 = (max - rayO) * invRayDir;
	if(t0 > t1)
	{
		std::swap(t0, t1);
	}
}



inline bool intersectRayAabb(const chag::float3 & rayO, const chag::float3 & rayDir, const chag::Aabb &aabb, float &tMin, float &tMax)
{
	intersectRaySlab(aabb.min.x, aabb.max.x, rayO.x, rayDir.x, tMin, tMax);

	float t0, t1;
	intersectRaySlab(aabb.min.y, aabb.max.y, rayO.y, rayDir.y, t0, t1);
	tMin = std::max(t0, tMin);
	tMax = std::min(t1, tMax);

	intersectRaySlab(aabb.min.z, aabb.max.z, rayO.z, rayDir.z, t0, t1);
	tMin = std::max(t0, tMin);
	tMax = std::min(t1, tMax);

	// if entry occurs before exit, and exit occurs in front of origin, its a hit
	if(tMin < tMax && tMax > 0.0f)
	{
		return true;
	}
	return false;
}	



inline bool intersectRaySphere(const float3 &rayO, const float3 &rayD, const float3 &spherePos, float sphereRad, float &t0, float &t1)
{
	// vector from sphere to ray
	float3 m = rayO - spherePos;
	// Project on ray direction.
	float b = dot(m, rayD);
	// Hm, not sure, best check the book
	float c = dot(m, m) - sphereRad * sphereRad;

	// Exit if r�s origin outside s (c > 0) and r pointing away from s (b > 0) 
	float discr = b * b - c;

	// A negative discriminant corresponds to ray missing sphere 
	if (discr < 0.0f)
	{
		return false;
	}
	// Ray now found to intersect sphere, compute smallest t value of intersection
	// If t is negative, ray started inside sphere so clamp t to zero 
	t0 = -b - sqrtf(discr);
	t1 = -b + sqrtf(discr);
	if (t0 > t1)
	{
		std::swap(t0, t1);
	}
	return true;
}



} // namespace chag

#endif // _chag_Intersection_h_
