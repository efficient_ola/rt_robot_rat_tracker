/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#include "ImGuiX.h"

namespace ImGuiX
{

std::vector<bool>& getHotKeyState()
{
	static std::vector<bool> s_hotkeyState;
	return s_hotkeyState;
}
std::string &getTipText()
{
	static thread_local std::string tipText;
	return tipText;
}

bool wasHotKeyPressed(int keyId)
{
	std::vector<bool>& ks = getHotKeyState();

	if (keyId >= 0 && keyId < int(ks.size()))
	{
		return ks[keyId];
	}

	return false;
}

std::string getHotKeyInfo(int keyId)
{
	if (keyId >= 0)
	{
		std::function<std::string(int)> &ktm = getKeyToTextMap();
		if (ktm)
		{
			return ktm(keyId);
		}
	}

	return std::string();
}

std::function<std::string(int)>& getKeyToTextMap()
{
	static std::function<std::string(int)> s_keyToTextMap;
	return s_keyToTextMap;
}

void onKeyPress(int keyId)
{
	if (keyId >= 0)
	{
		std::vector<bool>& ks = getHotKeyState();

		if (keyId >= int(ks.size()))
		{
			ks.resize(keyId + 1, false);
		}
		ks[keyId] = true;
	}
}

void clearHotKeyState()
{
	std::vector<bool>& ks = getHotKeyState();
	std::fill(ks.begin(), ks.end(), false);
}

};


