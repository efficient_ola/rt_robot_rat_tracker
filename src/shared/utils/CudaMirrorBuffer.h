/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#ifndef _CudaMirrorBuffer_h_
#define _CudaMirrorBuffer_h_

#include <cstdio>
#include <cassert>
#include <iostream>

#include "Assert.h"

#include "CudaCompat.h"
#include "CudaRuntimeApiWrapper.h"
#include "CudaDeviceBufferRef.h"

// This guy helps with moving data between host and device. The usage relies on the programmer 
// requesting transfer
// TODO: Add support for streams
// TODO: Add support for unified memory
// TODO?: Maybe add modification tracking? Maybe not...
template <typename T>
class CudaMirrorBuffer
{
public:
	typedef T *PtrType;

	// these should not be accessed from device code
	FUNC_CUDA_H CudaMirrorBuffer(size_t capacity = 0) :
		m_data(0),
		m_hostData(0),
		m_asyncInProgress(false)
	{ 
		cuda_wrapper::EventCreate(&m_asyncCopyEvent);
		init(capacity);
	} 

	FUNC_CUDA_H CudaMirrorBuffer(const T *hostData, size_t size) :
		m_data(0),
		m_hostData(0),
		m_asyncInProgress(false)
	{ 
		cuda_wrapper::EventCreate(&m_asyncCopyEvent);
		init(hostData, size);
		//copyFromHost(size);
	} 


	CudaMirrorBuffer(const CudaMirrorBuffer &other) : 
		m_data(0),
		m_hostData(0),
		m_asyncInProgress(false)
	{
		cuda_wrapper::EventCreate(&m_asyncCopyEvent);

		init(other.capacity());
		copy(other);
	}


	FUNC_CUDA_H ~CudaMirrorBuffer()
	{
		cuda_wrapper::EventDestroy(m_asyncCopyEvent);
		clear(true);
	}

	T *data() 
	{
		//assert(m_hostData);
		// return pointer to just beyond meta data block
		return m_hostData ? reinterpret_cast<T*>(m_hostData + 1) : nullptr;
	}
	const T *data() const
	{
		//assert(m_hostData);
		// return pointer to just beyond meta data block
		return m_hostData ? reinterpret_cast<T*>(m_hostData + 1) : nullptr;
	}
	
	// Begin / end or [] access is all host-side
	T &operator[](size_t index)
	{
		ASSERT(!m_asyncInProgress);
		ASSERT(index < size());
		return data()[index];
	}

	const T &operator[](size_t index) const
	{
		ASSERT(!m_asyncInProgress);
		ASSERT(index < size());
		return data()[index];
	}

	// This one is just for convenience of being able to use the same interface to the buffer (in CPU code) as the device reference (in device code)
	// NOTE: never pass a CudaMirrorBuffer to a device function, you must get a device reference by calling 'deviceRef()/deviceRefRo()'
	// TODO: these should be automatically selected when the data size is of an aligned type for const interface... (and then not needed here)
	FUNC_CUDA_H const T &loadAligned(size_t index) const
	{
		ASSERT(!m_asyncInProgress);
		ASSERT(index < m_hostData->size);
		return data()[index];
	}

	const T *begin() const { return data(); }
	const T *end() const { return data() + size(); }
	T *begin() { return data(); }
	T *end() { return data() + size(); }

	const T *cudaPtr() const { return m_data ? reinterpret_cast<T*>(m_data + 1) : nullptr; }
	T *cudaPtr() { return m_data ? reinterpret_cast<T*>(m_data + 1) : nullptr; }

	void push_back(const T &v)
	{
		ASSERT(size() < capacity());
		data()[m_hostData->size++] = v;
	}

	// Note: only resets the size counter, unless 'freeMem' is true
	void clear(bool freeMem = false)
	{
		if (freeMem)
		{
			reserve(0U);
		}
		else
		{
			resize(0, true);
		}
	}

	void init(size_t size)
	{
		reserve(size);
	}

	void init(const T *hostData, size_t size)
	{
		reserve(size);
		copyFromHost(hostData, size);
	}
	
	// Copy size from the other one on both host & device side, performs no allocation, requres that the destination has a greater or equal capacity
	void copySize(const CudaMirrorBuffer<T> &src)
	{
		assert(capacity() >= src.capacity());
		if (m_hostData)
		{
			m_hostData->size = uint32_t(src.size());
		}
		if (m_data && src.m_data)
		{
			cuda_wrapper::Memcpy(&m_data->size, &src.m_data->size, sizeof(src.m_data->size), cuda_wrapper::MemcpyDeviceToDevice);
		}
	}

	void resize(size_t newSize, bool growOnly = false, bool keepData = false)
	{
		reserve(newSize, growOnly, keepData);
		assert(newSize <= capacity());
		assert(newSize <= std::numeric_limits<uint32_t>::max());
		assert(m_hostData || newSize == 0);
		if (m_hostData)
		{
			m_hostData->size = uint32_t(newSize);
		}
		reflectMetaDataToDevice(true);
	}

	// TODO: add stream support
	void reserve(size_t _capacity, bool growOnly = false, bool keepData = false)
	{
		// internally we ues only 32 bit values because of atomic limitations.
		assert(_capacity <= std::numeric_limits<uint32_t>::max());
		uint32_t newCapacity = uint32_t(_capacity);

		if (!growOnly || newCapacity > capacity())
		{
			MetaData *oldData = nullptr;
			MetaData *oldHostData = nullptr;
			uint32_t oldSize = 0U;
			if (m_data || m_hostData)
			{
				// defer freeing of data...
				if (keepData && newCapacity > 0)
				{
					oldData = m_data;
					oldHostData = m_hostData;
					oldSize = m_hostData->size;
				}
				else
				{
					cuda_wrapper::Free(m_data);
					cuda_wrapper::FreeHost(m_hostData);
				}
				m_hostData = nullptr;
				m_data = nullptr;
			}
			//m_hostData->size = 0;
			//m_hostData->capacity = capacity;
			if (newCapacity)
			{
				{
					void *tmp = nullptr;
					cuda_wrapper::HostAlloc(&tmp, sizeof(MetaData) + newCapacity * sizeof(T), cuda_wrapper::HostAllocDefault);
					assert(tmp);
					m_hostData = static_cast<MetaData*>(tmp);
				}
				m_hostData->capacity = newCapacity;
				m_hostData->size = 0;
				{
					void *tmp = nullptr;
					cuda_wrapper::Malloc(&tmp, sizeof(MetaData) +  newCapacity * sizeof(T));
					// Allowed to be NULL as there may be no CUDA... assert(tmp);
					m_data = static_cast<MetaData*>(tmp);
				}
			}
			// preserve data and deallocate
			if (oldData)
			{
				m_hostData->size = std::min(oldSize, newCapacity);
				// copy the device side data (but not meta-data -> which may now be out of sync.
				cuda_wrapper::Memcpy(cudaPtr(), oldData + 1, m_hostData->size * sizeof(T), cuda_wrapper::MemcpyDeviceToDevice);
				cuda_wrapper::Free(oldData);

				// copy the host side data (but not meta-data -> which may now be out of sync.
				cuda_wrapper::Memcpy(data(), oldHostData + 1, m_hostData->size * sizeof(T), cuda_wrapper::MemcpyHostToHost);
				cuda_wrapper::FreeHost(oldHostData);
			}

			// TODO: this is not actually needed for example for the host-side copy operations (as they imply the user will have to sync later anyway)
			// since the user is responsible for managing the data reflections we don't know if they are intending to use/update the device side or 
			// host-side data thus we have to update the device side meta-data, just in case. NOTE: this must be done in the correct stream.  
			reflectMetaDataToDevice(true);
		}
	}

	// needs to get called whenever the meta data changes on the host in such a way that it is not obvious that a reflect will happen later
	// e.g, resize... (otoh push_back implies it is the users responsibility).
	void reflectMetaDataToDevice(bool async = false)
	{
		if (m_hostData && m_data)
		{
			if (async)
			{
				cuda_wrapper::MemcpyAsync(m_data, m_hostData, sizeof(MetaData), cuda_wrapper::MemcpyHostToDevice);
			}
			else
			{
				cuda_wrapper::Memcpy(m_data, m_hostData, sizeof(MetaData), cuda_wrapper::MemcpyHostToDevice);
			}
		}
	}

	bool empty() const
	{
		return size() == 0;
	}

	size_t size() const
	{
		return m_hostData ? m_hostData->size : 0U;
	}

	size_t byteSize() const
	{
		return size() * sizeof(T);
	}

	size_t capacity() const
	{
		return m_hostData ? m_hostData->capacity : 0U;
	}

	size_t byteCapacity() const
	{
		return capacity() * sizeof(T);
	}

	CudaDeviceBufferRef<T> deviceRef()
	{
		return CudaDeviceBufferRef<T> { cudaPtr(), m_data ? &m_data->size : nullptr, size(), capacity() };
	}

	
	CudaDeviceBufferRef<const T> deviceRef() const
	{
		return CudaDeviceBufferRef<const T> { cudaPtr(), m_data ? &m_data->size : nullptr, size(), capacity() };
	}

	
	CudaDeviceBufferRef<const T> deviceRefRo() const
	{
		return CudaDeviceBufferRef<const T> { cudaPtr(), m_data ? &m_data->size : nullptr, size(), capacity() };
	}


	void set(uint8_t value, size_t count = ~size_t(0))
	{
		memset(data(), value, std::min(count, size()) * sizeof(T));
		// TODO: should really do cuda-side set also to ensure consistent views (?)...
		// TODO: add flags to select either or both?
	}

	/**
	 * Transfer data to device
	 * TODO: add support for async & streams.
	 */
	void reflectToDevice(bool async)
	{
		if (m_hostData && m_data)
		{
			if (async)
			{
				cuda_wrapper::MemcpyAsync(m_data, m_hostData, sizeof(MetaData) + m_hostData->size * sizeof(T), cuda_wrapper::MemcpyHostToDevice);
			}
			else
			{
				cuda_wrapper::Memcpy(m_data, m_hostData, sizeof(MetaData) + m_hostData->size * sizeof(T), cuda_wrapper::MemcpyHostToDevice);
			}
		}
	}

	/**
	* Transfer data to host
	* TODO: add support for async.
	*/
	void reflectToHost()
	{
		if (m_hostData && m_data)
		{
			cuda_wrapper::Memcpy(m_hostData, m_data, sizeof(MetaData) + m_hostData->capacity * sizeof(T), cuda_wrapper::MemcpyDeviceToHost);
		}
	}

	/**
	 * Copies data on both host and device sides from 'sourceBuffer'
	 * TODO: add version with host/device side flags?
	 * Automatically clamps to size of source buffer.
	 */
	void copy(const CudaMirrorBuffer<T> &sourceBuffer)//, size_t count = ~size_t(0))
	{
		size_t num = sourceBuffer.size();
		assert(capacity() >= num);

		if (num)
		{
			memcpy(m_hostData, sourceBuffer.m_hostData, sizeof(MetaData) + num * sizeof(T));
			cuda_wrapper::Memcpy(m_data, sourceBuffer.m_data, sizeof(MetaData) + num * sizeof(T), cuda_wrapper::MemcpyDeviceToDevice);

			// it would be weird otherwise
			assert(m_hostData->size == num);
		}
	}

	/**
	* Copies from a host data pointer to the host-side mirror buffer,
	* NOTE: Does not reflect to Device buffer!
	*/
	void copyFromHost(const T *srcData, size_t count)
	{
		assert(capacity() >= count);
		assert(count <= std::numeric_limits<uint32_t>::max());
		//cuda_wrapper::Memcpy(m_data, data, count * sizeof(T), cuda_wrapper::MemcpyHostToDevice);
		memcpy(data(), srcData, count * sizeof(T));
		m_hostData->size = uint32_t(count);
	}

#if 0
	/**
	 * Copies from a device data pointer to the device-side mirror buffer, 
	 * NOTE: Does not reflect to Host buffer!
	 */
	void copyFromDevice(const T *deviceData, size_t count)
	{
		TODO: must be very careful with device side maintained meta data
		assert(m_hostData->capacity >= count);
		cuda_wrapper::Memcpy(m_data, deviceData, count * sizeof(T), cuda_wrapper::MemcpyDeviceToDevice);
		m_hostData->size = count;
	}
#endif
	/**
	 * Ensure async copies have finished.
	 */
	void sync()
	{
		if (m_asyncInProgress)
		{
			cuda_wrapper::EventSynchronize(m_asyncCopyEvent);
			m_asyncInProgress = false;
		}
	}

	void operator=(const std::vector<T> &other)
	{
		reserve(other.size(), true);
		if (!other.empty())
		{
			copyFromHost(other.data(), other.size());
		}
	}


private:
	// These are in their own struct to make replication to and from device easier (and more obvious)
	struct MetaData
	{
		uint32_t size;
		uint32_t capacity;
		// pad to 64 byte boundary
		uint32_t _padding[14];
	};
	static_assert(sizeof(MetaData) == 64U, "Invalid metadata struct size");

	cuda_wrapper::Event_t m_asyncCopyEvent;
	bool m_asyncInProgress;
	// NOTE: the data arrays (m_data / m_hostData) both start with the metadata block
	// (MetaData  T[capacity])
	MetaData *m_data;
	MetaData *m_hostData;
};

#endif // _CudaMirrorBuffer_h_
