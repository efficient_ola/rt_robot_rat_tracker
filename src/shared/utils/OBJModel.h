/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/

#ifndef __OBJModel_h_
#define __OBJModel_h_

// Useful for using just to load data.
#ifndef OBJ_MODEL_ENABLE_RENDERING
	#define OBJ_MODEL_ENABLE_RENDERING 1
#endif // OBJ_MODEL_ENABLE_RENDERING

#if OBJ_MODEL_ENABLE_RENDERING
#include <glad/glad.h>
#include <utils/GlBufferObject.h>
#include <utils/ComboShader.h>
#endif // OBJ_MODEL_ENABLE_RENDERING

#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <linmath/float2.h>
#include <linmath/float3.h>
#include <linmath/float4.h>
#include <linmath/float4x4.h>
#include <linmath/Aabb.h>

class OBJModel
{
public:
	enum RenderFlags
	{
		RF_Opaque = 1, /**< Draw chunks that are fully opaque, i.e. do not have transparency or an opacity map */
		RF_AlphaTested = 1 << 1,  /**< Draw chunks that have an opacity map */
		RF_Transparent = 1 << 2,  /**< Draw chunks that have an alpha value != 1.0f */
		RF_All = RF_Opaque | RF_AlphaTested | RF_Transparent,  /**< Draw everything. */
	};

	OBJModel(std::string fileName = std::string());
	~OBJModel(void);
	/**
	 */
	bool load(std::string fileName); 
  /**
   */
  const chag::Aabb &getAabb() const { return m_aabb; }

#if OBJ_MODEL_ENABLE_RENDERING
	/**
	 */
	void render(chag::ComboShader *shader, uint32_t renderFlags, const chag::float4x4 &viewMatrix);
	void render(chag::ComboShader *shader, uint32_t renderFlags = RF_All) { render(shader, renderFlags, chag::make_identity<chag::float4x4>()); }

  // used open GL texture units, ensure they are bound appropriately for the shaders.
  enum TextureUnits
  {
    TU_Diffuse = 0,
    TU_Opacity,
    TU_Specular,
    TU_Normal,
    TU_Max,
  };
  // used in open gl, ensure they are bound appropriately for the shaders.
  enum AttributeArrays
  {
    AA_Position = 0,
    AA_Normal,
    AA_TexCoord,
    AA_Tangent,
    AA_Bitangent,
    AA_Max,
  };

  /**
   * Matrial properties are packed into an uniform buffer, should be declared thus (in the shader):
   *  layout(std140) uniform MaterialProperties
   *  {
   *    vec3 material_diffuse_color; 
   *    float material_alpha;
   *    vec3 material_specular_color; 
   *    vec3 material_emissive_color; 
	 *		float material_specular_exponent;
   *  };
   */
  enum UniformBufferSlots
  {
    UBS_MaterialProperties = 0,
    UBS_Max,
  };
#endif // OBJ_MODEL_ENABLE_RENDERING

	/**
	 */
	std::vector<std::string> getShadingModels() const;

public:

	size_t getNumVerts();

	bool loadOBJ(std::ifstream &file, std::string basePath);
	bool loadMaterials(std::string fileName, std::string basePath);
	int loadTexture(std::string fileName, std::string basePath, bool srgb);

	struct Material
	{
    struct Color
    {
    chag::float3 diffuse;
		chag::float3 ambient;
		chag::float3 specular;
		chag::float3 emissive;
    } color;
    float specularExponent;
    struct TextureId
    {
      int diffuse;
      int opacity;
      int specular;
      int normal;
    } textureId;

		float alpha;
    size_t offset;
		std::string shadingModel;
	};


  typedef std::map<std::string, Material> MatrialMap;
	MatrialMap m_materials;

#if OBJ_MODEL_ENABLE_RENDERING
  // maps to layout of uniforms under std140 layout.
  struct MaterialProperties_Std140
  {
    chag::float3 diffuse_color; 
    float alpha;
    chag::float3 specular_color; 
    float pad1;
    chag::float3 emissive_color; 
    float specular_exponent;
    // this meets the alignment required for uniform buffer offsets on NVidia GTX280/480, also 
    // compatible with AMD integrated Radeon HD 3100.
    float alignPad[52];
  };
  GlBufferObject<MaterialProperties_Std140> m_materialPropertiesBuffer;
#endif // OBJ_MODEL_ENABLE_RENDERING
	struct Chunk
	{
		chag::Aabb aabb;
		Material *material;
    uint32_t offset;
    uint32_t count;
		uint32_t renderFlags;
	};

  size_t m_numVerts;
	// Data on host
	std::vector<chag::float3> m_positions;
	std::vector<chag::float3> m_normals;
	std::vector<chag::float2> m_uvs; 
	std::vector<chag::float3> m_tangents;
	std::vector<chag::float3> m_bitangents;

#if OBJ_MODEL_ENABLE_RENDERING
  // Data on GPU
	GLuint	m_positions_bo; 
	GLuint	m_normals_bo; 
	GLuint	m_uvs_bo; 
	GLuint	m_tangents_bo; 
	GLuint	m_bitangents_bo; 
	// Vertex Array Object
	GLuint	m_vaob;
  GLuint m_defaultTextureOne; /**< all 1, single pixel texture to use when no texture is loaded. */
  GLuint m_defaultNormalTexture;  /**< { 0.5, 0.5, 1, 1 }, single pixel float texture to use when no normal texture is loaded. */
#endif // OBJ_MODEL_ENABLE_RENDERING
	std::vector<Chunk> m_chunks;
	std::set<std::string> m_shadingModels;

  chag::Aabb m_aabb;

	friend struct SortAlphaChunksPred;



	// aabb tree stuff
	struct AabbTreeNode
	{
		chag::Aabb aabb;
	};

	struct Batch
	{
		chag::Aabb aabb;
		uint32_t start;
		uint32_t count;
	};


	chag::Aabb m_treeAabb;
	std::vector<Batch> m_batches;
	//std::vector<AabbTreeNode> m_nodes;
#if OBJ_MODEL_ENABLE_RENDERING
	//GlBufferObject<chag::float4> m_batchPositions; 
	GlBufferObject<uint32_t> m_batchIndices;
#endif // OBJ_MODEL_ENABLE_RENDERING

	void getBatches(const chag::Aabb &aabb, std::vector<uint32_t> &batchIndices, uint32_t index = 1);
	void getBatches(const chag::float3 &spherePos, const float sphereRadius, std::vector<uint32_t> &batchIndices, uint32_t index = 1);

	bool traceRay(const chag::float3 &origin, const chag::float3 &direction, int &hitBatch);
	bool traceRay(const chag::float3 &origin, const chag::float3 &direction)
	{
		int hitBatch = 0;
		return traceRay(origin, direction, hitBatch);
	}

	Batch buildBatch(const std::vector<uint32_t> &triangles, std::vector<uint32_t> &indices);

	void buildOctTreeNode(const chag::Aabb &nodeAABB, std::vector<uint32_t> triangles,
		const uint32_t minTriangles, const float minNodeSize,
		std::vector<Batch> &batches,
		std::vector<uint32_t> &indices);
	void buildBatches();
};

#endif // __OBJModel_h_
