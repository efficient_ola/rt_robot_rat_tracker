/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#ifndef _DebugRenderer_h_
#define _DebugRenderer_h_

#include "IntTypes.h"
#include <map>
#include <vector>
#include <memory>
#include <string>
#include <functional>
#include <thread>
#include <mutex>

#include <linmath/float3.h>
#include <linmath/float4.h>
#include <linmath/float4x4.h>
#include <linmath/Aabb.h>

#include "HashVector.h"

#include "GlBufferObject.h"

namespace chag
{

class PrimitiveRenderer;
class SimpleShader;

};

/**
 */
class DebugRenderer
{
public:
	static const chag::float4 red;
	static const chag::float4 blue;
	static const chag::float4 green;
	static const chag::float4 yellow;
	static const chag::float4 purple;
	static const chag::float4 cyan;
	static const chag::float4 orange;
	static const chag::float4 brown;
	static const chag::float4 pink;
	static const chag::float4 olive;
	static const chag::float4 white;
	static const chag::float4 black;
	static const chag::float4 random;

	using float4x4 = chag::float4x4;
	using float4 = chag::float4;
	using float3 = chag::float3;
	using Aabb = chag::Aabb;

	static chag::float4 randomColor(size_t seed, float alpha = 1.0f);
	static chag::float4 heatMap(const float _value, const float minVal, const float maxVal, float alpha = 1.0f);


	// retrieve singleon, which is accessible from all threads.
	static DebugRenderer& instance();
	// Used to set a more informative thread ID than the auto-generated one.
	void setThreadId(const std::string &id);

	// See below for details.
	class Backend;

	/**
	 * A draw command is simply some function that takes a Backend and performs some debug rendering.
	 * It is important to understand that the command will propbably be invoked in another thread context and 
	 * is stored (so never use pointers/references to local data).
	 */
	using DrawCmd = std::function<void(Backend&)>;

	/**
	 * Add an object, typically you should use the macro: DBG_RENDERER_OBJECT as this makes it easier to remove.
	 */
	template <typename DRAW_FN_T>
	void addObject(const std::string &id, DRAW_FN_T fn)
	{
		// an 'object' is just a namespace with a single command, this gives the command a name.
		// could shortcut this...
		push(id, fn);
		pop(id);
	}

	void addCommand(DrawCmd cmd);

	/**
	 * Push / Pop a namespace. All commands and objects between these will be children of this namespace.
	 * Typically use the macro: DBG_RENDERER_OBJECT_SCOPE which automates the 'pop'.
	 */
	void push(const std::string &id, DrawCmd prologue = DrawCmd());
	void pop(const std::string &id, DrawCmd epilogue = DrawCmd());


#if 0
	// These are a bunch of templated overloads, because on some platforms the lambdas may not auto-convert to the std::function, I think.
	// Don't seem to be needed on MSVC 2015
	template <typename DRAW_FN_T>
	void addCommand(DRAW_FN_T fn)
	{
		// a comman is added to the current namespace and has no ID, the draw commands within may be tagged though
		// could shortcut this...
		addCommand(DrawCmd(fn));
	}
	template <typename PROLOGUE_FN_T>
	void push(const std::string &id, PROLOGUE_FN_T prologue)
	{
		push(id, DrawCmd(prologue));
	}

	template <typename EPILOGUE_FN_T>
	void pop(const std::string &id, EPILOGUE_FN_T epilogue)
	{
		pop(id, DrawCmd(epilogue));
	}
#endif


	struct NameSpaceData;
	using NameSpaceDataPtr = std::shared_ptr<NameSpaceData>;

	/**
	 * A namespace is a named container of draw commands and sub-namespaces.
	 * There may be several sub-spaces with the same ID, so the sub-space data structure is a map of vectors.
	 */
	struct NameSpaceData
	{
		NameSpaceData(const std::string &_id, DrawCmd _prologue) : id(_id), prologue(_prologue), m_outerPreStackDepth(-1){ }

		std::string id;
		std::map<std::string, std::vector<NameSpaceDataPtr>> subSpaces;

		// Call these before and after drawing any child nodes, as they are inteded to inherit the transformation.
		void beginDraw(DebugRenderer::Backend & be);
		// endDraw causes the commands, and the epilogue to be executed.
		// also restores the transform stack to the state before calling beginDraw.
		void endDraw(DebugRenderer::Backend & be);

	protected:
		friend class DebugRenderer;

		std::vector<DrawCmd> drawCommands;

		DrawCmd prologue;
		DrawCmd epilogue;
		int m_outerPreStackDepth; // used to restore stack depth upon calling endDraw is -1 if not in begin/end pair
	};

	using MasterNamespace = std::map<std::string, std::map<std::string, NameSpaceDataPtr>>;
	// Returns a COPY of the master namespace, which is just a bunch of smart pointers in a map, so not too expensive.
	// copying the thing enables low-contention access from threads.
	MasterNamespace getMasterNamespace()
	{
		std::unique_lock<std::mutex> l(m_masterMutex);
		return m_masterNamespace; 
	}


	// TODO: should this be an interface? Performance overhead, sure but flexibility, also 
	// with new model this overhead is moved to rendering thread.
	class Backend
	{
	public:
		Backend(chag::PrimitiveRenderer *primitiveRenderer, chag::SimpleShader *shader);
		void pushTransform(const chag::float4x4 &tfm);
		void popTransform();

		void drawAabb(const Aabb &aabb, const float4 &color, const std::string &subId = std::string());
		void drawSphere(const float3 &pos, float rad, const float4 &color, const std::string &subId = std::string());
		void drawLine(const float3 &pos0, const float3 &pos1, const float4 &color, const std::string &subId = std::string());
		void drawCone(const float3 &pos0, const float3 &dir, float height, float angleRad, const float4 &color, const std::string &subId = std::string());
		void drawDisc(const float3 &pos, const float3 &dir, float radius, const float4 &color, const std::string &subId = std::string());

		void setTransformStackDepth(int depth);
		int getTransformStackDepth() { return int(m_transformStack.size()); }
		void setSubIdFilter(std::map<std::string, bool> *subIdFilter = nullptr, bool defaultState = false) { m_subIdFilter = subIdFilter; m_subIdDefaultVis = defaultState; }

	protected:
		void drawSetup(const float4 & color);
		bool testSubId(const std::string &subId)
		{
			if(subId.empty() || !m_subIdFilter)
			{ 
				return true;
			}
			auto &sf = *m_subIdFilter;
			if (!sf.count(subId))
			{
				sf[subId] = m_subIdDefaultVis;
			}
			return sf[subId];
		}

		std::vector<float4x4> m_transformStack;
		float4x4 m_transform;

		chag::PrimitiveRenderer *m_primitiveRenderer;
		chag::SimpleShader *m_shader;
		std::map<std::string, bool> *m_subIdFilter;
		bool m_subIdDefaultVis;
	};

private:
	// Sub-data that is stored for each thread using a static thread_local variable
	struct PerThreadData
	{
		std::string threadId;
		std::vector<NameSpaceDataPtr> m_namespaceStack;
	};

	PerThreadData &getPerThreadData();

	int m_threadIdCounter;

	MasterNamespace m_masterNamespace;
	std::mutex m_masterMutex;
  DebugRenderer();
};


namespace detail
{
	struct DebugRendererNameSpaceHelper
	{
		DebugRendererNameSpaceHelper(const std::string& id) : m_id(id)
		{
			DebugRenderer::instance().push(m_id);
		}

		template <typename PROLOGUE_FN_T>
		DebugRendererNameSpaceHelper(const std::string& id, PROLOGUE_FN_T prologue) : m_id(id)
		{
			DebugRenderer::instance().push(m_id, prologue);
		}

		template <typename PROLOGUE_FN_T, typename EPILOGUE_FN_T>
		DebugRendererNameSpaceHelper(const std::string& id, PROLOGUE_FN_T prologue, EPILOGUE_FN_T epilogue) : m_id(id), m_epliogue(epliogue)
		{
			DebugRenderer::instance().push(m_id, prologue);
		}
		~DebugRendererNameSpaceHelper()
		{
			DebugRenderer::instance().pop(m_id, m_epliogue);
		}

		std::string m_id;
		DebugRenderer::DrawCmd m_epliogue;
	};
}

// These macros are for instrumenting the code, they can then be compiled out when instrumentation is not desired.

#define DBG_RENDER_CAT_ID2(_id1_, _id2_) _id1_##_id2_
#define DBG_RENDER_CAT_ID(_id1_, _id2_) DBG_RENDER_CAT_ID2(_id1_, _id2_)

#define DBG_RENDERER_OBJECT(_name_, _draw_func_) DebugRenderer::instance().addObject(_name_, _draw_func_)
#define DBG_RENDERER_CMD(_draw_func_) DebugRenderer::instance().addCommand(_draw_func_)

// This declares a class that pushes the namespace and automatically pops is when the current scope ends
// Any objects created will inherit the namespaec name, as a prefix. 
// Another feature is that while within a namespace, any object that appears more than once will be interpreted as being different instances.
// When a namespace is begun any object within is removed. A consequence is that those in global namespace will live until replaced, 
// and there may only be one of each name in this namespace.
// Namespaces act on a per-thread basis, and if the same namespace is present on more than one thread, the objects are merged (drawn together), but life-times
// are managed separately for each thread.
#define DBG_RENDERER_OBJECT_SCOPE(...) detail::DebugRendererNameSpaceHelper DBG_RENDER_CAT_ID(_debug_renderer_scope_ns_helper, __LINE__)(__VA_ARGS__)

#endif // _DebugRenderer_h_
