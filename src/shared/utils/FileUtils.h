/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#ifndef _FileUtils_h_
#define _FileUtils_h_

#include <inttypes.h>
#include <stdio.h>
#include <string>
#include <vector>

namespace file_utils
{
void seek64Bit(FILE *f, int64_t offset, int origin);
int64_t tell64Bit(FILE *f);


namespace detail
{

template <typename T, typename TT = std::enable_if_t<std::is_trivial<T>::value>>
inline bool read(FILE *f, T &tmp)
{
	return fread(&tmp, sizeof(T), 1, f) == 1;	
}

inline bool read(FILE *f, std::string &s)
{
	uint64_t strSize;
	bool ok = read(f, strSize);
	s.resize(strSize);
	return ok && fread(&s[0], 1, s.size(), f) == s.size();
}

}


template <typename T>
T read(FILE *f)
{
	T tmp;
	detail::read(f, tmp);
	return tmp;
}

template <typename T>
T read(FILE *f, bool &status)
{
	T tmp;
	if (status)
	{
		status = detail::read(f, tmp);
	}
	return tmp;
}


template <typename T, typename = std::enable_if_t<std::is_trivial<T>::value>>
inline void read(FILE *f, std::vector<T> &data)
{
	uint64_t size = read<uint64_t>(f);
	data.resize(size);
	if (size > 0)
	{
		fread(data.data(), sizeof(T), size, f);
	}
}

template <typename T, typename = std::enable_if_t<std::is_trivial<T>::value>>
void write(FILE *f, const T &value)
{
	T tmp;
	fwrite(&value, sizeof(tmp), 1, f);
}


} // namespace file_utils

#endif // _FileUtils_h_
