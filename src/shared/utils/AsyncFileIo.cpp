/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#include "AsyncFileIo.h"
#include <thread>

#define LOGF(...)

namespace async_file_io
{
namespace detail
{


class BinaryOutFileImpl
{
public:
	BinaryOutFileImpl() : 
		m_fileState(FS_OpenPending), 
		m_queuedBytes(0), 
		m_file(nullptr),
		m_ioManager(IoManager::instance())
	{
	}



	~BinaryOutFileImpl()
	{
		LOGF("[h]~BinaryOutFileImpl, %s\n", m_name.c_str());
	}


	enum FileState
	{
		FS_Open,
		FS_OpenPending,
		FS_Closed,
		FS_Max
	};



	inline bool isOpen()
	{
		return m_file || m_fileState == FS_Open || m_fileState == FS_OpenPending;
	}

	IoManagerPtr m_ioManager;
	std::atomic<FileState> m_fileState;
	size_t m_queuedBytes;
	std::mutex m_mutex;
	FILE *m_file;
	std::string m_name;
};



namespace
{
	// This little guy helps ensuring the manager is flushed at exit.
	// if anyone tries to write after this point they'll get an invalid pointer to the manager anyway and it should not be possible.
	struct ExitWrapper
	{
		~ExitWrapper()
		{
			ptr->flush();
			ptr->shutdown();
		}
		std::shared_ptr<IoManager> ptr;
	};
}



std::shared_ptr<IoManager> IoManager::instance()
{
	static ExitWrapper wrappo{ detail::IoManagerPtr(new IoManager) };
	return wrappo.ptr;
}



void IoManager::flush()
{
	std::mutex m;
	std::unique_lock<std::mutex> lock(m);
	std::condition_variable flushed;
	
	LOGF("[h]flush, push\n");
	push([&]()
	{
		LOGF("[x]flush\n");
		flushed.notify_one();
	});
	LOGF("[h]flush, wait()\n");
	flushed.wait(lock);
	LOGF("[h]flush, done\n");

	// queue up a command that just unlocks the mutex
	
	// wait...
}



IoManager::IoManager() : m_exit(false)
{
	m_processingThread = std::thread(&IoManager::processingFunc, this);
}



void IoManager::shutdown()
{
	flush();
	m_exit = true;
	push([] {
		LOGF("[x] ciao\n");
	});
	m_processingThread.join();
}



IoManager::~IoManager()
{
}



void IoManager::processingFunc()
{
	while(!m_exit)
	{
		Cmd cmd;
		if (m_cmdQueue.pop_front(cmd))
		{
			cmd();
		}
	}
}



} // namespace detail



BinaryOutFile::BinaryOutFile(const std::string &filenNme)
{
	if (!filenNme.empty())
	{
		open(filenNme);
	}
}



BinaryOutFile::~BinaryOutFile()
{
	close();
}



bool BinaryOutFile::open(const std::string & filenNme)
{
	m_implementation = ImplPtr(new detail::BinaryOutFileImpl);

	// Note: the impl local is needed to ensure lambda captures current pointer and not 'this' pointer.
	ImplPtr impl = m_implementation;
	LOGF("[h]open: %s\n", filenNme.c_str());
	impl->m_ioManager->push([=]()
	{
		assert(!impl->m_file);
		assert(impl->m_fileState == detail::BinaryOutFileImpl::FS_OpenPending);

		impl->m_file = fopen(filenNme.c_str(), "wb");
		impl->m_name = filenNme;

		if (impl->m_file)
		{
			LOGF("[x]opened: %s\n", impl->m_name.c_str());
			impl->m_fileState = detail::BinaryOutFileImpl::FS_Open;
		}
		else
		{
			LOGF("[x]failed to open: %s\n", impl->m_name.c_str());
			impl->m_fileState = detail::BinaryOutFileImpl::FS_Closed;
			LOGF(WARNING, "FAILED TO OPEN '%s'\n", cmd->fileName.c_str());
		}
	});
	return true;
}



void BinaryOutFile::close(bool wait)
{
	// Note: the impl local is needed to ensure lambda captures current pointer and not 'this' pointer.
	ImplPtr impl = m_implementation;

	if (impl)
	{
		LOGF("[h]close: %s\n", impl->m_name.c_str());
		impl->m_ioManager->push([=]()
		{
			assert(impl->m_file);
			assert(impl->m_queuedBytes == 0);
			fclose(impl->m_file);
			// NOTE: this should not be needed as the implementation should go out of scope and self-destroy at this point.
			impl->m_file = nullptr;
			LOGF("[x]closed: %s\n", impl->m_name.c_str());
		});
		if (wait)
		{
			impl->m_ioManager->flush();
		}
		// release hold of pointer.
		m_implementation = ImplPtr();
	}
}



bool BinaryOutFile::isOpen() const
{
	// it is open assuming the file has been opened (as indicated by the implementation pointer) and the open is pending or has completed (m_implementation->m_fileState).
	return m_implementation && m_implementation->isOpen();
}



size_t BinaryOutFile::queueDepthBytes() const
{
	return m_implementation ? m_implementation->m_queuedBytes : 0;
}



void BinaryOutFile::write(const std::vector<uint8_t>& chunk)
{
	if (chunk.size())
	{
		// Note: the impl local is needed to ensure lambda captures current pointer and not 'this' pointer.
		ImplPtr impl = m_implementation;
		LOGF("[h]write: %s (%d)\n", impl->m_name.c_str(), int(chunk.size()));
		// 1. update queue size (since we dont store it explicitly, we must keep it updated).
		//    Since it seems size_t is not supported with std::atomic, we'll just use a mutex.
		//    It'll only be locked for very short intervals anyway.
		{
			std::lock_guard<std::mutex> g(impl->m_mutex);
			impl->m_queuedBytes += chunk.size();
		}

		// use lambda capture to copy the data, crucially also copies ptr to implementation and so keeps it alive.
		impl->m_ioManager->push([=]()
		{
			//std::this_thread::sleep_for(std::chrono::milliseconds(3000));
			if (impl->m_file)
			{
				fwrite(chunk.data(), 1, chunk.size(), impl->m_file);
				{
					std::lock_guard<std::mutex> g(impl->m_mutex);
					impl->m_queuedBytes -= chunk.size();
				}
			}
			LOGF("[x]wrote: %s (%d)\n", impl->m_name.c_str(), int(chunk.size()));
		});
	}
}



}