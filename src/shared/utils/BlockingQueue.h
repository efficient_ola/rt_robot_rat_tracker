/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#ifndef _BlockingQueue_h_
#define _BlockingQueue_h_

#include <cassert>
#include <iostream>
#include <list>
#include <mutex>
#include <condition_variable>

#include "Assert.h"


// 
template <typename T>
class BlockingQueue
{
public:
	BlockingQueue(int maxQueueDepth = -1) : m_maxQueueDepth(maxQueueDepth) { }

	void push_back(const T &v)
	{
		{
			std::lock_guard<std::mutex> lock(m_mutex);
			m_queue.push_back(v);
			// Ensure queue depth is enforced by dropping items... maybe it should be called 'dropping queue'...
			if (m_maxQueueDepth > 0)
			{
				while (int(m_queue.size() > m_maxQueueDepth))
				{
					m_queue.pop_front();
				}
			}
		}
		m_dataAvailable.notify_one();
	}

	/**
	 * Blocking read from front of queue, sleeps peacefully until condition varaible singals wakeup time.
	 */
	bool pop_front(T &v)
	{
		std::unique_lock<std::mutex> lock(m_mutex);

		m_dataAvailable.wait(lock, [&]() { return !m_queue.empty(); });
		if (!m_queue.empty())
		{
			v = m_queue.front();
			m_queue.pop_front();
			return true;
		}
		// Somehow, we ended up not getting anything!
		return false;
	}

	void pop_front_batch(std::list<T> &v, int maxNum = -1)
	{
		std::unique_lock<std::mutex> lock(m_mutex);
		m_dataAvailable.wait(lock, [&]() { return !m_queue.empty(); });
		if (maxNum > 0)
		{
			v.clear();
			for (; maxNum > 0 && !m_queue.empty(); --maxNum)
			{
				v.push_back(m_queue.front());
				m_queue.pop_front();
			}
		}
		else
		{
			v = m_queue;
			m_queue.clear();
		}
	}

	/**
	 * Non-Blocking read from front of queue
	 */
	bool pop_front_no_wait(T &v)
	{
		std::unique_lock<std::mutex> lock(m_mutex);

		if (!m_queue.empty())
		{
			v = m_queue.front();
			m_queue.pop_front();
			return true;
		}
		// Somehow, we ended up not getting anything!
		return false;
	}


	void pop_front_batch_no_wait(std::list<T> &v, int maxNum = -1)
	{
		std::unique_lock<std::mutex> lock(m_mutex);
		if (maxNum > 0)
		{
			v.clear();
			for (; maxNum > 0 && !m_queue.empty(); --maxNum)
			{
				v.push_back(m_queue.front());
				m_queue.pop_front();
			}
		}
		else
		{
			v = m_queue;
			m_queue.clear();
		}
	}

	void clear()
	{
		std::unique_lock<std::mutex> lock(m_mutex);
		m_queue.clear();
	}

	// NOTE: in a threaded world you cant ever be sure the size does not change after you check...
	size_t size() const
	{
		std::lock_guard<std::mutex> lock(m_mutex);
		return m_queue.size();
	}

	// NOTE: in a threaded world you cant ever be sure the size does not change after you check...
	bool empty() const
	{
		std::lock_guard<std::mutex> lock(m_mutex);
		return m_queue.empty();
	}


	std::list<T> m_queue;
	mutable std::mutex m_mutex;
	std::condition_variable m_dataAvailable;
	int m_maxQueueDepth;
};

#endif // _BlockingQueue_h_
