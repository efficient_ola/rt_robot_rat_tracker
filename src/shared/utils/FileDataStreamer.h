#ifndef _FileDataStreamer_h_
#define _FileDataStreamer_h_

#include "AsyncFileIo.h"
#include <map>

/**
 * Output file streamer that uses a structured file format with blocks of some size. Probably re-implementation of something that 
 * already exists, but what can I say, I think re-doing this wheel will actually save time. 
 * The assumptions is that there are relatively small blocks of data, and that they are given some tag.
 */
class ChunkOutStreamer
{
public:
	ChunkOutStreamer(std::function<double(void)> timeSource) : 
		m_timeSource(timeSource)
	{
	}

	void open(const std::string &fileName)
	{
		std::lock_guard<std::mutex> lock(m_mutex);

		m_outFile.open(fileName);
		m_outFile.write(std::vector<uint8_t>{'b', 'd', 's', 'x'});
	}

	void close()
	{
		std::lock_guard<std::mutex> lock(m_mutex);

		m_outFile.close();
	}

	bool isOpen() const
	{
		std::lock_guard<std::mutex> lock(m_mutex);

		return m_outFile.isOpen();
	}


	size_t queueDepthBytes() const
	{
		std::lock_guard<std::mutex> lock(m_mutex);

		return m_outFile.queueDepthBytes();
	}
	/**
	 * Helper that serves to provide both fuctions to write trivial data
	 */
	struct BlockWriter
	{
		static constexpr size_t s_endOffset = ~size_t(0);

		BlockWriter(const std::string &blockType, const std::string &blockVersion, double timeStamp) : 
			m_blockType(blockType),
			m_blockVersion(blockVersion),
			m_timeStamp(timeStamp)
		{
			write(m_blockType);
			write(m_blockVersion);
			write(m_timeStamp);
		}
		
		template <typename T, typename = std::enable_if_t<std::is_trivial<T>::value>>
		void write(const T &value, size_t offset = s_endOffset)
		{
			const uint8_t *b = reinterpret_cast<const uint8_t *>(&value);
			const uint8_t *e = b + sizeof(value);
			if (offset == s_endOffset)
			{
				m_data.insert(m_data.end(), b, e);
			}
			else
			{
				assert(offset <= m_data.size());
				m_data.insert(m_data.begin() + offset, b, e);
			}
		}

		template <typename T, typename = std::enable_if_t<std::is_trivial<T>::value>>
		void write(const std::vector<T> &value)
		{
			uint64_t size = value.size();
			write(size);
			const uint8_t *b = reinterpret_cast<const uint8_t *>(value.data());
			const uint8_t *e = b + size * sizeof(value[0]);
			m_data.insert(m_data.end(), b, e);
		}

		void write(const std::string &value) 
		{ 
			uint64_t size = value.size();
			write(size);
			const uint8_t *b = reinterpret_cast<const uint8_t *>(value.data());
			const uint8_t *e = b + size * sizeof(value[0]);
			m_data.insert(m_data.end(), b, e);
		}

		uint64_t size() const
		{
			return m_data.size();
		}

		std::string m_blockType;
		std::string m_blockVersion;
		double m_timeStamp;
		std::vector<uint8_t> m_data;
	};

	/**
	 * This function is thread safe, however, the returend block writer is not and access control is up to the user.
	 */
	inline BlockWriter beginBlock(const std::string &blockType, const std::string &blockVersion, double timeStamp = -1.0)
	{
		std::lock_guard<std::mutex> lock(m_mutex);
		return BlockWriter(blockType, blockVersion, timeStamp < 0.0 ? m_timeSource() : timeStamp);
	}
	/**
	 * Again thread-safe operation, pushes the data to the out file.
	 */
	inline void endBlock(BlockWriter &b)
	{
		// write the size (note, this will use insert to write the value first in the vector, which will be somewhat inefficient, causing a copy/move of the memory block etc.
		uint64_t size = b.size();
		b.write(size, 0);

		std::lock_guard<std::mutex> lock(m_mutex);
		if (m_outFile.isOpen())
		{
			m_outFile.write(b.m_data);
		}
	}


protected:
	mutable std::mutex m_mutex;
	std::function<double(void)> m_timeSource;
	async_file_io::BinaryOutFile m_outFile;
};

// 
class ChunkInStreamer
{
public:

	struct BlockHeader
	{
		std::string type;
		std::string version;
		double timeStamp;
		size_t size;
		size_t headerStartOffset;
		size_t dataStartOffset;
	};
	//using BlockHeaderPtr = std::shared_ptr<BlockHeader>;
	using iterator = std::vector<BlockHeader>::iterator;
	using const_iterator = std::vector<BlockHeader>::const_iterator;

	ChunkInStreamer(std::function<double(void)> timeSource) :
		m_timeSource(timeSource),
		m_file(nullptr)
	{
	}

	struct BlockReader
	{
		static constexpr size_t s_endOffset = ~size_t(0);

		BlockReader(const std::vector<uint8_t> &data) :
			m_data(data),
			m_readOffset(0)
		{
			read(m_blockType);
			read(m_blockVersion);
			read(m_timeStamp);
		}

		template <typename T, typename = std::enable_if_t<std::is_trivial<T>::value>>
		bool read(T &value)
		{
			if (m_readOffset + sizeof(value) > m_data.size())
			{
				return false;
			}
			uint8_t *b = reinterpret_cast<uint8_t *>(&value);
			memcpy(b, m_data.data() + m_readOffset, sizeof(value));
			m_readOffset += sizeof(value);
			return true;
		}

		template <typename T, typename = std::enable_if_t<std::is_trivial<T>::value>>
		bool read(std::vector<T> &value)
		{
			uint64_t size = 0ULL;
			if (read(size))
			{
				if (m_readOffset + sizeof(T) * size <= m_data.size())
				{
					value.resize(size);
					uint8_t *b = reinterpret_cast<uint8_t *>(value.data());
					memcpy(b, m_data.data() + m_readOffset, sizeof(T) * size);
					m_readOffset += sizeof(T) * size;

					return true;
				}
			}
			return false;
		}

		bool read(std::string &value)
		{
			uint64_t size = 0ULL;
			if (read(size))
			{
				if (m_readOffset + size <= m_data.size())
				{
					value.resize(size);
					if (size)
					{
						uint8_t *b = reinterpret_cast<uint8_t *>(&value[0]);
						memcpy(b, m_data.data() + m_readOffset, size);
						m_readOffset += size;
					}
					return true;
				}
			}
			return false;
		}

		uint64_t size() const
		{
			return m_data.size();
		}

		std::string m_blockType;
		std::string m_blockVersion;
		double m_timeStamp;
		std::vector<uint8_t> m_data;
		size_t m_readOffset;
	};

	BlockReader readBlock(const BlockHeader &bh);

	bool open(const std::string &fileName);
	bool isOpen() 
	{ 
		std::lock_guard<std::mutex> lock(m_mutex);
		return m_file != nullptr;
	}
	void close()
	{
		std::lock_guard<std::mutex> lock(m_mutex);
		if (m_file)
		{
			fclose(m_file);
			m_file = nullptr;
		}
	}

	const BlockHeader *findNearest(const std::string &tag, double timeStamp);

	const_iterator begin(const std::string &tag);
	const_iterator begin(const std::string &tag, double timeStamp, bool precedessor = true);
	const_iterator end(const std::string &tag);

	std::string getFileName() const
	{
		std::lock_guard<std::mutex> lock(m_mutex);
		return m_fileName;
	}

	BlockHeader readBlockHeader(FILE *f);

	double getStartTime() const { return m_startTime; }
	double getEndTime() const { return m_endTime; }

	std::map<std::string, std::vector<BlockHeader> > m_tagIndex;
	std::vector<BlockHeader> m_index;
	mutable std::mutex m_mutex;
	std::function<double(void)> m_timeSource;
	FILE *m_file;
	std::string m_fileName;
	double m_startTime;
	double m_endTime;
};

using ChunkInStreamerPtr = std::shared_ptr<ChunkInStreamer>;

/**
 * Manages playback, time-based or sequential of a chunk stream. This is separate from the chunk stream since it will have state...
 */
class ChunkInStreamPlayer
{
public:

	/**
	 * create a stream player, using the given chunk in stream.
	 */
	ChunkInStreamPlayer(const ChunkInStreamerPtr &instr, double defaultTimeInterval);

	// seek to the given time, resets t0,t1 
	void seekTime(double currT, double prevT = -1.0);
	// advance to give time, updates the time interval, such that t0 = t1, t1 = t
	void advance(double currT);
	//void seekFrame(size_t index);
	void step(const std::string &tag, int direction);
	bool paused() const { return m_paused; }
	void setPaused(bool paused);
	void play(double speed = 1.0 / 30.0);
	//void reset();
	double getStartTime() const { return m_in->getStartTime(); }
	double getEndTime() const { return m_in->getEndTime(); }

	double getPrevT() const { return m_prevT;	}
	double getCurrT() const { return m_currT; }

	std::string getFileName() const { return m_in->getFileName(); }

	ChunkInStreamerPtr getStream() const { return m_in; }

	ChunkInStreamerPtr m_in;
	double m_defaultTimeInterval;
	double m_prevT;
	double m_currT;
	bool m_paused;
};

using ChunkInStreamPlayerPtr = std::shared_ptr<ChunkInStreamPlayer>;

#endif // _FileDataStreamer_h_
