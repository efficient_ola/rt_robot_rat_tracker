/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#ifndef _chag_Math_h_
#define _chag_Math_h_

#include "IntTypes.h"

#include <utils/CudaCompat.h>

namespace chag
{
const float g_pi = 3.1415926535897932384626433832795f;

  
template <typename T>
FUNC_CUDA_HD T min(const T &a, const T &b)
{
  return a < b ? a : b;
}



template <typename T>
FUNC_CUDA_HD T max(const T &a, const T &b)
{
  return a > b ? a : b;
}


/*
template <typename T>
void swap(T &a, T &b)
{
  T tmp(a);
  a = b;
  b = tmp;
}*/



template <typename T>
FUNC_CUDA_HD const T clamp(const T &value, const T &lowerInc, const T &upperInc)
{
  return min(max(value, lowerInc), upperInc);
}



template <typename T>
FUNC_CUDA_HD const T square(T a)
{
  return a * a;
}



template <typename T>
FUNC_CUDA_HD T getNearestHigherMultiple(const T count, const T multipleOf)
{
  return ((count + multipleOf - T(1)) / multipleOf) * multipleOf;
}


FUNC_CUDA_HD float toRadians(float degrees)
{
	return degrees * g_pi / 180.0f;
}



}; // namespace chag

#endif // _chag_Math_h_
