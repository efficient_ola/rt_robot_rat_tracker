/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#ifndef _CudaPinnedHostBuffer_h_
#define _CudaPinnedHostBuffer_h_

#include <cstdio>
#include <cassert>
#include <iostream>

#include "Assert.h"

#if ENABLE_CUDA
#include "CudaSafeCall.h"


template <typename T>
class CudaPinnedHostBuffer
{
public:
	CudaPinnedHostBuffer(size_t size = 0) : 
		m_size(0), 
		m_data(0),
		m_asyncInProgress(false)
	{ 
		cudaEventCreate(&m_asyncCopyEvent);
		init(size);
	} 

	CudaPinnedHostBuffer(const T *hostData, size_t size) : 
		m_size(0), 
		m_data(0),
		m_asyncInProgress(false)
	{ 
		cudaEventCreate(&m_asyncCopyEvent);
		init(size);
		if (hostData)
		{
			copyFromHost(hostData, uint32_t(size));
		}
	} 


	CudaPinnedHostBuffer(const CudaPinnedHostBuffer &other) : 
		m_size(0), 
		m_data(0),
		m_asyncInProgress(false)
	{
		cudaEventCreate(&m_asyncCopyEvent);

		init(other.size());
		copy(other, other.size());
	}
	~CudaPinnedHostBuffer()
	{
		cudaEventDestroy(m_asyncCopyEvent);
		clear();
	}

	void clear()
	{
		ASSERT(!m_asyncInProgress);
		if (m_data)
		{
			CUDA_SAFE_CALL(cudaFreeHost(m_data));
		}
		m_size = 0; 
		m_data = 0;
	}

	void init(size_t size)
	{
		resize(size);
	}


	T &operator[](size_t index)
	{
		ASSERT(!m_asyncInProgress);
		ASSERT(index < m_size);
		return m_data[index];
	}


	const T &operator[](size_t index) const
	{
		ASSERT(!m_asyncInProgress);
		ASSERT(index < m_size);
		return m_data[index];
	}


	void resize(size_t size, bool growOnly = false, bool keepData = false)
	{
		if (!growOnly || size > m_size)
		{
			ASSERT(!m_asyncInProgress);

			T *oldData = 0;
			size_t oldSize = 0;
			if (m_data)
			{
				// defer freeing of data...
				if (keepData && size > 0)
				{
					oldData = m_data;
					oldSize = m_size;
				}
				else
				{
					CUDA_SAFE_CALL(cudaFreeHost(m_data));
				}
				m_data = 0;
			}
			m_size = size;
			if (m_size)
			{
				void *tmp = 0;
				CUDA_SAFE_CALL(cudaHostAlloc(&tmp, m_size * sizeof(T), cudaHostAllocDefault));
				m_data = reinterpret_cast<T*>(tmp);
			}

			if (oldData)
			{
				CUDA_SAFE_CALL(cudaMemcpy(m_data, oldData, ::min(uint32_t(oldSize), uint32_t(m_size)) * sizeof(T), cudaMemcpyHostToHost));
				CUDA_SAFE_CALL(cudaFreeHost(oldData));
			}
		}
	}

	size_t size() const
	{
		return m_size;
	}

	size_t byteSize() const
	{
		return m_size * sizeof(T);
	}

	T* ptr()
	{ 
		ASSERT(m_size > 0);
		return m_data; 
	}

	const T* ptr() const
	{ 
		ASSERT(m_size > 0);
		return m_data; 
	}


	void set(uint8_t value, size_t count = ~size_t(0))
	{
		memset(m_data, value, min(uint32_t(count), uint32_t(m_size)) * sizeof(T));
	}

	void copy(const CudaPinnedHostBuffer<T> &b, size_t count)
	{
		ASSERT(m_size >= count);
		ASSERT(b.size() >= count);

		if (count)
		{
			cudaMemcpy(m_data, b.cudaPtr(), count * sizeof(T), cudaMemcpyHostToHost);
		}
	}

	void copyFromHost(const T *hostPtr, size_t count)
	{
		ASSERT(m_size >= count);
		if (count)
		{
			cudaMemcpy(m_data, hostPtr, count * sizeof(T), cudaMemcpyHostToHost);
		}
	}

	void copy(const CudaBuffer<T> &b, size_t count, bool async = false, cudaStream_t stream = 0)
	{
		ASSERT(m_size >= count);
		ASSERT(b.size() >= count);
		ASSERT(!m_asyncInProgress);

		if (async)
		{
			m_asyncInProgress = true;
			cudaMemcpyAsync(m_data, b.cudaPtr(), count * sizeof(T), cudaMemcpyDeviceToHost, stream);
			cudaEventRecord(m_asyncCopyEvent, stream);
		}
		else
		{
			cudaMemcpy(m_data, b.cudaPtr(), count * sizeof(T), cudaMemcpyDeviceToHost);
		}
	}

	/**
	 * Ensure async copies have finished.
	 */
	void sync()
	{
		if (m_asyncInProgress)
		{
			cudaEventSynchronize(m_asyncCopyEvent);
			m_asyncInProgress = false;
		}
	}

	void operator=(const CudaPinnedHostBuffer &other)
	{
		resize(other.size(), true);
		copy(other, other.size());
	}

private:
	size_t m_size;
	cudaEvent_t m_asyncCopyEvent;
	bool m_asyncInProgress;
	T *m_data;
};

#endif // ENABLE_CUDA


#endif // _CudaPinnedHostBuffer_h_
