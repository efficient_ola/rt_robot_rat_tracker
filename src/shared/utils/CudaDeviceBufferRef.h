/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#ifndef _CudaDeviceBufferRef_h_
#define _CudaDeviceBufferRef_h_

#include "CudaCompat.h"
#include <assert.h>
/**
 * Simple wrapper to pass a buffer ref to a kernel, since we cant / ought not not pass a whole
 * buffer object, for example with pointers to mirror and copy constructors and whatnot.
 */

template <typename T>
class CudaDeviceBufferRef
{
public:
	//CudaDeviceBufferRef() : ptr(nullptr), capacity(0), len(0), sizePtr(nullptr) {}

	T * CUDA_RESTRICT ptr;
	uint32_t *sizePtr;
	size_t len;
	size_t capacity;

	FUNC_CUDA_D T &operator[](int index) const
	{
		assert(index >= 0);
		assert(index < int(size()));
		return ptr[index];
	}

	FUNC_CUDA_HD T *CUDA_RESTRICT begin() const
	{
		return ptr;
	}
	FUNC_CUDA_HD T *CUDA_RESTRICT end() const
	{
		return ptr + size();
	}

	// NOTE: returns the dynamic size if present!
	FUNC_CUDA_D size_t size() const { return sizePtr ? *sizePtr : len; }

	// NOTE: sub-section cannot access dynamic size!!
	FUNC_CUDA_D CudaDeviceBufferRef<T> sub(size_t offset, size_t count)
	{
		assert(offset < size());
		assert(offset + count <= size());
		return CudaDeviceBufferRef<T>{ ptr + offset, nullptr, count, count };
	}

	FUNC_CUDA_D void push_back(const T &v) const
	{
		assert(sizePtr);
		assert(size() < capacity);
		uint32_t offset = atomicAdd(sizePtr, 1);
		ptr[offset] = v;
	}

	FUNC_CUDA_D uint32_t allocateRange(uint32_t count) const
	{
		assert(sizePtr);
		assert(size() + count <= capacity);
		return atomicAdd(sizePtr, count);
	}

	// NOTE: non-atomic user is responsible for that bit...
	FUNC_CUDA_D void setSize(uint32_t newSize) const
	{
		assert(sizePtr);
		assert(newSize <= capacity);
		*sizePtr = newSize;
	}

#ifdef __CUDACC__

	FUNC_CUDA_D T loadAligned(int ind) const
	{
		assert(ind >= 0);
		assert(ind < int(size()));

		const ::float4 * CUDA_RESTRICT ptr_x = reinterpret_cast<const ::float4 *>(ptr);
		static_assert((sizeof(T) % sizeof(::float4)) == 0, "ARGH!");
		const int numFloat4 = sizeof(T) / sizeof(float4);

		using base_type = typename std::remove_cv<T>::type;

		base_type tmp;
		::float4 * CUDA_RESTRICT dstPtr = reinterpret_cast<::float4 *>(&tmp);
		for (int i = 0; i < numFloat4; ++i)
		{
			dstPtr[i] = ptr_x[ind * numFloat4 + i];
		}
		return tmp;
	}
#endif // __CUDACC__
};

#endif // _CudaDeviceBufferRef_h_
