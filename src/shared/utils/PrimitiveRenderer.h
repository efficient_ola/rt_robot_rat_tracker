/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#ifndef _PrimitiveRenderer_h_
#define _PrimitiveRenderer_h_

#include <linmath/float3.h>
#include <linmath/float2.h>
#include <linmath/float4x4.h>
#include <linmath/Aabb.h>
#include <glad/glad.h>
#include <utils/GlBufferObject.h>
#include <map>
#include <string>

namespace chag
{

class SimpleShader;

class PrimitiveRenderer
{
public:
	void init(SimpleShader *defaultShader = nullptr);

	SimpleShader *getDefautShader() { return m_defaultShader; }

	void drawSphere(const float3 &pos, float size, SimpleShader *shader, bool wireFrame = false);
	void drawHemiSphere(const float3 &pos, float size, SimpleShader *shader, bool wireFrame = false);
	void drawAabb(const chag::Aabb &aabb, SimpleShader *shader, bool wireFrame = false);
	void drawPlane(chag::float3 normal, chag::float3 point, chag::float3 alignToDir, float planeSize, SimpleShader *shader);
	void drawPlane(chag::float3 normal, chag::float3 point, float planeSize, SimpleShader *shader);
	void drawQuadGrid(chag::float3 normal, chag::float3 point, chag::float3 alignToDir, float planeSize, SimpleShader *shader);
	void drawQuadGrid();
	void drawLine(chag::float3 pos0, chag::float3 pos1, SimpleShader *shader);
	void drawCone(const float3 &pos, const float3 &axis, float height, float angleRad, SimpleShader *shader, bool wireFrame);
	void drawDisc(const float3 & pos, const float3 & axis, float radius, SimpleShader * shader, bool wireFrame);
	void drawQuad(chag::float3 minVert, chag::float3 maxVert);

	void drawPoints(int numPoints, const chag::float3 * positions, const chag::float3 *colours);

	enum AttributeArrays
  {
    AA_Position = 0,
    AA_Normal,
    AA_TexCoord,
		AA_Colour,
    AA_Max,
  };

	/**
	 * Bit of a hack (and not very efficient either...).
	 */
	void setTransform(const chag::float4x4 &tfm) { m_transform = tfm; }

	std::map<std::string, int> getAttributeLocations() const
	{
		return{
			{ "positionAttrib", AA_Position },
			{ "normalAttrib", AA_Normal },
			{ "texCoordAttrib", AA_TexCoord },
			{ "colourAttrib", AA_Colour },
		};
	}

private:

	SimpleShader *m_defaultShader;

	void createSphereModel();
	void buildAabb();
	void buildPlane();
	void buildQuadGrid();
	void buildLine();
	void buildCone();
	void buildDisc();


	GlBufferObject<chag::float3> g_aabbVerts;
	GlBufferObject<chag::float3> g_aabbNormals;
	GlBufferObject<chag::float2> g_aabbUvs;
	GlBufferObject<int> g_aabbInds;
	GLuint g_aabbVaob;

	GLuint m_sphereVaob;
	GlBufferObject<chag::float3> g_sphereVertexBuffer;

	GlBufferObject<chag::float3> g_planeVerts;
	GlBufferObject<chag::float3> g_planeNormals;
	GlBufferObject<chag::float2> g_planeUvs;
	GLuint g_planeVaob;


	GlBufferObject<chag::float3> g_quadGridVerts;
	GlBufferObject<chag::float3> g_quadGridNormals;
	GlBufferObject<chag::float2> g_quadGridUvs;
	GlBufferObject<int> g_quadGridInds;
	GLuint g_quadGridVaob;

	GlBufferObject<chag::float3> m_coneVerts;
	GlBufferObject<chag::float3> m_coneNormals;
	//GlBufferObject<chag::float2> m_coneUvs;
	GlBufferObject<int> m_coneInds;
	GLuint m_coneVao;

	GlBufferObject<chag::float3> m_discVerts;
	GlBufferObject<chag::float3> m_discNormals;
	GlBufferObject<chag::float2> m_discUvs;
	GLuint m_discVao;

	chag::float4x4 m_transform;


	GlBufferObject<chag::float3> m_linePointsBuffer;
	GLuint m_lineVaob;

	GlBufferObject<chag::float3> m_pointsPosBuffer;
	GlBufferObject<chag::float3> m_pointsColBuffer;
	//GlBufferObject<float> m_pointsSizeBuffer;
	GLuint m_pointsVao;

	GlBufferObject<chag::float3> m_quadVerts;
	//GlBufferObject<chag::float3> g_quadNormals;
	GlBufferObject<chag::float2> m_quadUvs;
	GlBufferObject<int> m_quadInds;
	GLuint m_quadVao;

	const int s_maxQuadGrid = 16;
};


}; // namespace chag


#endif // _PrimitiveRenderer_h_
