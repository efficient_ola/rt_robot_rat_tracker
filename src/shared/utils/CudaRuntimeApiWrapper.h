/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#ifndef _CudaRuntimeApiWrapper_h_
#define _CudaRuntimeApiWrapper_h_


#include <string>
// The main purpose of this namespace and functions is to replicate the cuda api functions that I happen to need
// but without exposing the global namespace to the whole of the cuda runtime API, which notably includes 'float3' for
// name-clash galore (@nvidia how about a 'cuda' namespace??)!
//
// In also provides the host-side APIs when there is no CUDA present (at compile time, or if detectCudaDevice() is called and returned false also at runtime)
// , to make it easy to use for example CudaMirrorBuffer
//

namespace cuda_wrapper
{
bool detectCudaDevice(std::string *deviceInfo);

struct EventSt;
using Event_t = EventSt*;

void EventCreate(Event_t *e);
void EventDestroy(Event_t e);
void EventSynchronize(Event_t e);

void Free(void *ptr);
void FreeHost(void *ptr);

enum MemcpyKind
{
	MemcpyHostToHost = 0,      /**< Host   -> Host */
	MemcpyHostToDevice = 1,      /**< Host   -> Device */
	MemcpyDeviceToHost = 2,      /**< Device -> Host */
	MemcpyDeviceToDevice = 3,      /**< Device -> Device */
	MemcpyDefault = 4       /**< Direction of the transfer is inferred from the pointer values. Requires unified virtual addressing */
};

void Memcpy(void *dst, const void *src, size_t count, enum MemcpyKind kind);
void MemcpyAsync(void *dst, const void *src, size_t count, enum MemcpyKind kind);
void Malloc(void **p, size_t s);


constexpr unsigned int HostAllocDefault = 0x00;  /**< Default page-locked allocation flag */
constexpr unsigned int HostAllocPortable = 0x01;  /**< Default page-locked allocation flag */
constexpr unsigned int HostAllocMapped = 0x02;  /**< Default page-locked allocation flag */
constexpr unsigned int HostAllocWriteCombined = 0x04;  /**< Default page-locked allocation flag */

void HostAlloc(void **pHost, size_t size, unsigned int flags = HostAllocDefault);

}; // namespace cuda_wrapper

#endif // _CudaRuntimeApiWrapper_h_
