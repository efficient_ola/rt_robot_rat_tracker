#include "FileDataStreamer.h"
#include "FileUtils.h"
#include <algorithm>

ChunkInStreamer::BlockReader ChunkInStreamer::readBlock(const BlockHeader & bh)
{
	std::lock_guard<std::mutex> lock(m_mutex);

	using namespace file_utils;
	std::vector<uint8_t> data;
	seek64Bit(m_file, bh.headerStartOffset, SEEK_SET);
	read(m_file, data);
	return BlockReader(data);
}

bool ChunkInStreamer::open(const std::string & fileName)
{
	std::lock_guard<std::mutex> lock(m_mutex);

	// TODO: at this point we could check if there is a pre-built index for this file
	//       and that the file date is more recent than the file and load that instead of re-building the index.

	m_fileName = fileName;
	m_file = fopen(fileName.c_str(), "rb");

	m_startTime = std::numeric_limits<double>::max();
	m_endTime = -std::numeric_limits<double>::max();

	if (m_file)
	{
		char fileTag[5];
		fread(fileTag, 1, 4, m_file);
		fileTag[4] = 0;
		if (std::string(fileTag) != "bdsx")
		{
			fclose(m_file);
			printf("File tag mismatch in file '%s', tag '%s' != 'bdsx'\n", fileName.c_str(), fileTag);
			return false;
		}
		using namespace file_utils;
		seek64Bit(m_file, 0, SEEK_END);
		uint64_t fileEnd = tell64Bit(m_file);

		// land just after 4 byte tag
		seek64Bit(m_file, 4, SEEK_SET);


		while (tell64Bit(m_file) != fileEnd)
		{
			BlockHeader bh = readBlockHeader(m_file);

			m_startTime = std::min(m_startTime, bh.timeStamp);
			m_endTime = std::max(m_endTime, bh.timeStamp);

			m_index.push_back(bh);
			m_tagIndex[bh.type].push_back(bh);

			// skip right past the block
			seek64Bit(m_file, bh.headerStartOffset + bh.size + 8, SEEK_SET);
		}

		// TODO: at this point we ought to validate that all the blocks are in monotonic order.
	}

	return m_file != nullptr;
}


const ChunkInStreamer::BlockHeader * ChunkInStreamer::findNearest(const std::string & tag, double timeStamp)
{
	if (m_tagIndex.count(tag) > 0)
	{
		auto it = begin(tag, timeStamp);
		return it != m_tagIndex[tag].end() ? &(*it) : nullptr;
	}
	return nullptr;
}

ChunkInStreamer::const_iterator ChunkInStreamer::begin(const std::string & tag)
{
	if (m_tagIndex.count(tag) > 0)
	{
		return m_tagIndex[tag].begin();
	}
	return const_iterator();
}

ChunkInStreamer::const_iterator ChunkInStreamer::begin(const std::string & tag, double timeStamp, bool precedessor)
{
	if (m_tagIndex.count(tag) > 0)
	{
		BlockHeader dummy;
		dummy.timeStamp = timeStamp;

		const std::vector<BlockHeader> &hs = m_tagIndex[tag];

		auto it = std::lower_bound(hs.begin(), hs.end(), dummy, [&](const BlockHeader &a, const BlockHeader &b) { return a.timeStamp < b.timeStamp; });

		if (!precedessor)
		{
			return it;
		}
		else
		{
			// if it is (epsilon) equal, then return the iterator,
			if (std::abs(timeStamp - (*it).timeStamp) < 0.00001)
			{
				return it;
			}
			// otherwise, we should return the most recent past item
			if (it != hs.end() && it != hs.begin())
			{
				--it;
				return it;
			}
			return hs.end();
		}
	}
	return const_iterator();
}

ChunkInStreamer::const_iterator ChunkInStreamer::end(const std::string & tag)
{
	if (m_tagIndex.count(tag) > 0)
	{
		return m_tagIndex[tag].end();
	}
	return const_iterator();
}

ChunkInStreamer::BlockHeader ChunkInStreamer::readBlockHeader(FILE *f)
{
	using namespace file_utils;

	BlockHeader bh;
	bh.headerStartOffset = tell64Bit(f);
	bh.size = read<uint64_t>(f);
	bh.type = read<std::string>(f);
	bh.version = read<std::string>(f);
	bh.timeStamp = read<double>(f);
	bh.dataStartOffset = tell64Bit(f);

	return bh;
}

ChunkInStreamPlayer::ChunkInStreamPlayer(const ChunkInStreamerPtr &instr, double defaultTimeInterval)
	: m_in(instr),
	m_defaultTimeInterval(defaultTimeInterval),	
	m_prevT(-1.0),
	m_currT(-1.0),
	m_paused(false)
{
}

void ChunkInStreamPlayer::seekTime(double currT, double prevT)
{
	m_currT = std::min(std::max(m_in->getStartTime(), currT), m_in->getEndTime());
	if (prevT >= 0.0)
	{
		m_prevT = std::min(std::max(m_in->getStartTime(), prevT), m_in->getEndTime());
	}
	else
	{
		m_prevT = std::max(m_in->getStartTime(), m_currT - m_defaultTimeInterval);
	}
	assert(m_currT >= m_prevT);
}

void ChunkInStreamPlayer::advance(double currT)
{
	m_prevT = m_currT;
	m_currT = std::min(std::max(m_in->getStartTime(), currT), m_in->getEndTime());
}

void ChunkInStreamPlayer::step(const std::string &tag, int direction)
{
	// get current item
	auto it = m_in->begin(tag, m_currT);
	if (direction > 0)
	{
		if (it != m_in->end(tag))
		{
			++it;
		}
		if (it != m_in->end(tag))
		{
			advance((*it).timeStamp);
		}
	}
	else if (direction < 0)
	{
		if (it != m_in->begin(tag))
		{
			--it;
		}
		if (it != m_in->end(tag))
		{
			seekTime((*it).timeStamp, (it != m_in->begin(tag)) ? (*(it - 1)).timeStamp : -1.0);
		}
	}
}

void ChunkInStreamPlayer::setPaused(bool paused)
{
	m_paused = paused;
}

void ChunkInStreamPlayer::play(double speed)
{
}
