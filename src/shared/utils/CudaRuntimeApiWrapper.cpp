/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#include "CudaRuntimeApiWrapper.h"
#include "IntTypes.h"
#include <assert.h>
#include <stdlib.h>
#include <memory.h>

#if ENABLE_CUDA
#include <cuda_runtime_api.h>
#endif // ENABLE_CUDA

#define CHECK_CONSTANT(X) static_assert(uint32_t(X)==uint32_t(cuda##X), "Enums out of sync")
#define CHECK_SIZE(X) static_assert(sizeof(X)==sizeof(cuda##X), "Size out of sync")

namespace cuda_wrapper
{

#if ENABLE_CUDA
static bool g_cudaDevicePresent = true;
#else // !ENABLE_CUDA
	static bool g_cudaDevicePresent = false;
#endif // ENABLE_CUDA

bool detectCudaDevice(std::string *deviceInfo)
{
#if ENABLE_CUDA
	int deviceCount = 0;
	g_cudaDevicePresent = cudaGetDeviceCount(&deviceCount) == cudaSuccess && deviceCount > 0;

	if (g_cudaDevicePresent && deviceInfo)
	{
		cudaDeviceProp devProps;
		cudaGetDeviceProperties(&devProps, 0);
		*deviceInfo = devProps.name;
	}
#endif // ENABLE_CUDA
	return g_cudaDevicePresent;
}
	
	
void EventCreate(Event_t *e)
{
#if ENABLE_CUDA
	if (g_cudaDevicePresent)
	{
		CHECK_SIZE(Event_t);

		cudaEvent_t *ee = reinterpret_cast<cudaEvent_t*>(e);
		cudaEventCreate(ee);
	}
	else
#endif // ENABLE_CUDA
	{
		e = nullptr;
	}
}

void EventDestroy(Event_t e)
{
#if ENABLE_CUDA
	if (g_cudaDevicePresent)
	{
		CHECK_SIZE(Event_t);

		cudaEvent_t ee = reinterpret_cast<cudaEvent_t>(e);
		cudaEventDestroy(ee);
	}
#endif // ENABLE_CUDA
}

void EventSynchronize(Event_t e)
{
#if ENABLE_CUDA
	if (g_cudaDevicePresent)
	{
		CHECK_SIZE(Event_t);

		cudaEvent_t ee = reinterpret_cast<cudaEvent_t>(e);
		cudaEventSynchronize(ee);
	}
#endif // ENABLE_CUDA
}


void Free(void *ptr)
{
#if ENABLE_CUDA
	if (g_cudaDevicePresent)
	{
		cudaFree(ptr);
	}
	else
#endif // ENABLE_CUDA
	{
		assert(!ptr);
	}
}

void FreeHost(void *ptr)
{
#if ENABLE_CUDA
	if (g_cudaDevicePresent)
	{
		cudaFreeHost(ptr);
	}
	else
#endif // ENABLE_CUDA
	{
		free(ptr);
	}
}


void Memcpy(void *dst, const void *src, size_t count, enum MemcpyKind kind) 
{
#if ENABLE_CUDA
	CHECK_CONSTANT(MemcpyHostToHost);
	CHECK_CONSTANT(MemcpyHostToDevice);
	CHECK_CONSTANT(MemcpyDeviceToHost);
	CHECK_CONSTANT(MemcpyDeviceToDevice);
	CHECK_CONSTANT(MemcpyDefault);
	if (g_cudaDevicePresent)
	{

		cudaMemcpy(dst, src, count, cudaMemcpyKind(kind));
	}
	else
#endif // ENABLE_CUDA
	{
		if (kind == MemcpyHostToHost || kind == MemcpyDefault)
		{
			memcpy(dst, src, count);
		}
		else
		{
			assert(!dst || !src); // one or the other or both should be NULL if in CUDA-free mode (and they are device pointers).
		}
	}
}

void MemcpyAsync(void *dst, const void *src, size_t count, enum MemcpyKind kind)
{
#if ENABLE_CUDA
	CHECK_CONSTANT(MemcpyHostToHost);
	CHECK_CONSTANT(MemcpyHostToDevice);
	CHECK_CONSTANT(MemcpyDeviceToHost);
	CHECK_CONSTANT(MemcpyDeviceToDevice);
	CHECK_CONSTANT(MemcpyDefault);
	if (g_cudaDevicePresent)
	{
		cudaMemcpyAsync(dst, src, count, cudaMemcpyKind(kind));
	}
	else
#endif // ENABLE_CUDA
	{
		if (kind == MemcpyHostToHost || kind == MemcpyDefault)
		{
			memcpy(dst, src, count);
		}
		else
		{
			assert(!dst || !src); // one or the other or both should be NULL if in CUDA-free mode (and they are device pointers).
		}
	}
}

void HostAlloc(void **pHost, size_t size, unsigned int flags)
{
#if ENABLE_CUDA
	CHECK_CONSTANT(HostAllocDefault);
	CHECK_CONSTANT(HostAllocPortable);
	CHECK_CONSTANT(HostAllocMapped);
	CHECK_CONSTANT(HostAllocWriteCombined);

	if (g_cudaDevicePresent)
	{
		cudaHostAlloc(pHost, size, flags);
	}
	else
#endif // ENABLE_CUDA
	{
		*pHost = malloc(size);
	}
}

void Malloc(void **p, size_t s)
{
#if ENABLE_CUDA
	if (g_cudaDevicePresent)
	{
		cudaMalloc(p, s);
	}
	else
#endif // ENABLE_CUDA
	{
		*p = nullptr;
	}
}

}; // namespace cuda_wrapper

