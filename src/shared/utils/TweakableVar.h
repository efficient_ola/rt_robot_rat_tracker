/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#ifndef _TweakableVar_h_
#define _TweakableVar_h_

#include <map>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include <mutex>

// Can be used if, for some reason, thread safety for variable value access is not needed (and deemed worth turning off)
#ifndef TWEAKABLE_VAR_ENABLE_VAR_MUTEX
	#define TWEAKABLE_VAR_ENABLE_VAR_MUTEX 1
#endif // TWEAKABLE_VAR_ENABLE_VAR_MUTEX

// Helper macro to loose the g_ part of the names.
#define DECLARE_TWEAKABLEVAR_G(_type_, _name_, _value_) TweakableVar::Variable<_type_> _name_(_value_, #_name_ + 2)
#define DECLARE_TWEAKABLEVAR_G_NS(_type_, _name_, _value_) TweakableVar::Variable<_type_> _name_(_value_, #_name_ + 2, false)

//#define DYNAMIC_TWEAK_VAR(_type_, _name_, _value_) TweakableVar::Variable<_type_> _name_(_value_, #_name_ + 2, false)

namespace TweakableVar
{

struct VariableBase;

template <typename T>
inline T clamp(const T& v, const T& lo, const T& hi);


class Stream
{
public:
	virtual bool isInput()
	{
		return false;
	}
	virtual bool eof()
	{
		return false;
	}
	virtual void write(const std::string &key, const std::string &value) { };
	virtual std::pair<std::string, std::string> read() 
	{
		return make_pair(std::string(), std::string());
	};
};

struct IfOfStream : public Stream
{
	IfOfStream(std::ostream &o) : output(&o), input(0) { }
	IfOfStream(std::istream &i) : output(0), input(&i) { }
	bool isInput()
	{
		return output == 0;
	}
	virtual bool eof()
	{
		return input->eof();
	}
	virtual void write(const std::string &key, const std::string &value)
	{
		(*output) << key << " " << value << std::endl;
	}
	virtual std::pair<std::string, std::string> read()
	{
		std::string tmp;
		std::getline(*input, tmp);
		size_t firstSpace = tmp.find_first_of(" \t");
		size_t firstSpaceEnd = tmp.find_first_not_of(" \t", firstSpace);
		if (firstSpace != std::string::npos && firstSpaceEnd != std::string::npos)
		{
			std::string key = tmp.substr(0, firstSpace);
			std::string value = tmp.substr(firstSpaceEnd);

			return make_pair(key, value);
		}
		return make_pair(std::string(), std::string());
	}

	/*bool is_open() const
	{
		if (output != 0)
		{
			return output->is_open();
		}
		if (input != 0)
		{
			return input->is_open();
		}
		return false;
	}*/

	std::ostream *output;
	std::istream *input;
};


struct TweakVarUiBase
{
	TweakVarUiBase(const std::string &info, bool modifiable) :
		m_info(info), m_modifiable(modifiable)
	{
	}

	virtual std::string getInfo() {	return m_info;	}

	virtual const std::string &getId() = 0;// { return m_var->m_name; }
	// may be overridded by specialized implementation class to provide some useful rendering
	virtual void render() = 0;
	virtual void onKey(int ch) = 0;

	//virtual bool onKeyPress(int ch) = 0;
	//TweakableVar::VariableBase *m_var;
	std::string m_info;
	bool m_modifiable;
};


// Singleton class looking after the variables...
class VariableManager
{
public:
	static VariableManager &get();

	void serialize(Stream &stream);

	using UiList = std::vector<TweakVarUiBase *>;

	template <typename CREATE_CALLBACK_FN_T>
	VariableBase *atomicGetCreate(const char *name, CREATE_CALLBACK_FN_T createCallback)
	{
		std::lock_guard<std::recursive_mutex> lock(m_mutex);
		if (!m_variables.count(name))
		{
			createCallback();
		}

		return getVar(name);
	}

	VariableBase *getVar(const std::string &name)
	{
		std::lock_guard<std::recursive_mutex> lock(m_mutex);
		auto it = m_variables.find(name);
		if (it != m_variables.end())
		{
			return (*it).second;
		}

		return nullptr;
	}

	UiList getUis() 
	{ 
		std::lock_guard<std::recursive_mutex> lock(m_mutex);
		return m_uis; 
	}

	void addUi(TweakVarUiBase *ui)
	{ 
		std::lock_guard<std::recursive_mutex> lock(m_mutex);
		m_uis.push_back(ui);
	}

private:
	friend struct VariableBase;

	void registerVar(const std::string &name, VariableBase *var);
	void loadFromConfig(const std::string &name, VariableBase *var);

	std::map<std::string, VariableBase *> m_variables;
	std::map<std::string, std::string> m_configStrings; // Used for variables that appear later...
	UiList m_uis;

	VariableManager();
	~VariableManager();

	std::recursive_mutex m_mutex;
};


struct VariableBase
{
	VariableBase(const std::string &name, bool serialize) : m_name(name), m_serialize(serialize)
	{
		VariableManager::get().registerVar(name, this);
	}

	virtual void writeValue(std::stringstream &s) = 0;
	virtual void readValue(std::istringstream &s) = 0;


	void loadFromConfig()
	{
		if (m_serialize)
		{
			VariableManager::get().loadFromConfig(m_name, this);
		}
	}

	bool m_serialize;
	std::string m_name;
};

template <typename T>
struct Variable : public VariableBase
{
	Variable(const T &value, const std::string &name, bool serialize = true) :
		VariableBase(name, serialize),
		m_value(value)
	{
	}

	operator T() const
	{
#if TWEAKABLE_VAR_ENABLE_VAR_MUTEX
		std::lock_guard<std::mutex> lock(m_mutex);
#endif // TWEAKABLE_VAR_ENABLE_VAR_MUTEX
		return m_value;
	}

	const T value() const
	{
#if TWEAKABLE_VAR_ENABLE_VAR_MUTEX
		std::lock_guard<std::mutex> lock(m_mutex);
#endif // TWEAKABLE_VAR_ENABLE_VAR_MUTEX
		return m_value;
	}

	void setValue(const T &value)
	{
#if TWEAKABLE_VAR_ENABLE_VAR_MUTEX
		std::lock_guard<std::mutex> lock(m_mutex);
#endif // TWEAKABLE_VAR_ENABLE_VAR_MUTEX
		m_value = value;
	}	

	void operator = (const T &v)
	{
#if TWEAKABLE_VAR_ENABLE_VAR_MUTEX
		std::lock_guard<std::mutex> lock(m_mutex);
#endif // TWEAKABLE_VAR_ENABLE_VAR_MUTEX
		m_value = v;
	}

	virtual void writeValue(std::stringstream &stream)
	{
#if TWEAKABLE_VAR_ENABLE_VAR_MUTEX
		std::lock_guard<std::mutex> lock(m_mutex);
#endif // TWEAKABLE_VAR_ENABLE_VAR_MUTEX
		stream << m_value;
	}
	virtual void readValue(std::istringstream &stream)
	{
#if TWEAKABLE_VAR_ENABLE_VAR_MUTEX
		std::lock_guard<std::mutex> lock(m_mutex);
#endif // TWEAKABLE_VAR_ENABLE_VAR_MUTEX
		stream >> m_value;
	}
private:
#if TWEAKABLE_VAR_ENABLE_VAR_MUTEX
	mutable std::mutex m_mutex;
#endif // TWEAKABLE_VAR_ENABLE_VAR_MUTEX
	T m_value;
};

#define DECLARE_TWEAKABLEVAR(_type_, _name_, _value_) TweakableVar::Variable<_type_> _name_(_value_, #_name_)


/**
 * This function must be implemented by the user to convert key codes (application defined, just seen as integers by the library)
 * to strings, to be able to print the help text. Could probably be done in a neater way...
 */
std::string keyToString(int key);



namespace TweakVarUiWidgetTypes
{


template <typename T>
struct RangedWidget
{
	RangedWidget(T _min, T _max, T _step, int _decKey, int _incKey) :
		min(_min),
		max(_max),
		step(_step),
		decKey(_decKey),
		incKey(_incKey)
	{
		std::stringstream ss;
		if (incKey)
		{
			ss << " inc: '" << keyToString(incKey) << "'";
		}
		if (decKey)
		{
			ss << " dec: '" << keyToString(decKey) << "'";
		}
		if (step != T(1))
		{
			ss << " step: '" << step << "'";
		}
		if (min > std::numeric_limits<T>::lowest())
		{
			ss << " min: '" << min << "'";
		}
		if (max < std::numeric_limits<T>::max())
		{
			ss << " max: '" << max << "'";
		}

		info = ss.str();
	}

	const std::string &getInfo() const
	{
		return info;
	}

	void onKey(int ch, T &value)
	{
		if (ch == incKey)
		{
			value += step;
		}
		if (ch ==  decKey)
		{
			value -= step;
		}
		value = clamp(value, min, max);
	}


	T min;
	T max;
	T step;
	int decKey;
	int incKey;
	std::string info;
};

template <typename T>
struct SliderT : public RangedWidget<T>
{
	SliderT(T _min = std::numeric_limits<T>::lowest(), T _max = std::numeric_limits<T>::max(), T _step = T(1), int _decKey = 0, int _incKey = 0) :
		RangedWidget<T>(_min, _max, _step, _decKey,  _incKey)
	{
	}
};


template <typename T>
struct InputT : public RangedWidget<T>
{
	InputT(T _min = std::numeric_limits<T>::lowest(), T _max = std::numeric_limits<T>::max(), T _step = T(1), int _decKey = 0, int _incKey = 0) :
		RangedWidget<T>(_min, _max, _step, _decKey, _incKey)
	{
	}
};

// Helper to pick which of the implementations to use based on type of the variable
template <typename T>
struct Selector
{
};


template <>
struct Selector<float>
{
	using Slider = SliderT<float>;
	using Input = InputT<float>;
};

template <>
struct Selector<int>
{
	using Slider = SliderT<int>;
	using Input = InputT<int>;

	struct Enum
	{
		Enum(const std::vector<std::pair<const char *, int>> &_items = std::vector<std::pair<const char *, int>>(), int _decKey = 0, int _incKey = 0) :
			items(_items),
			decKey(_decKey),
			incKey(_incKey)
		{
			int ind = 0;
			for (auto kv : items)
			{
				values[kv.first] = kv.second;
				invValues[kv.second] = kv.first;
				valueToInd[kv.second] = ind++;
			}
			genInfo();
		}
		Enum(const char **names, int num, int _decKey = 0, int _incKey = 0) :
			decKey(_decKey),
			incKey(_incKey)
		{
			for (int i = 0; i < num; ++i)
			{
				items.push_back(std::make_pair(names[i], i));
				values[names[i]] = i;
				invValues[i] = names[i];
				valueToInd[i] = i;
			}
			genInfo();
		}
		void genInfo()
		{
			std::stringstream ss;
			ss << "{";
			for (auto kv = items.begin(); kv != items.end(); ++kv)
			{
				if (kv != items.begin())
				{
					ss << "|";
				}
				ss << (*kv).first;
			}
			ss << "}";
			if (incKey)
			{
				ss << " inc: '" << keyToString(incKey) << "'";
			}
			if (decKey)
			{
				ss << " dec: '" << keyToString(decKey) << "'";
			}

			info = ss.str();
		}

		const std::string &getInfo() const
		{
			return info;
		}

		void onKey(int ch, int &value)
		{
			// This should always be true unless values have been changed elsewhere...
			auto it = valueToInd.find(value);
			if (it != valueToInd.end())
			{
				int index = (*it).second;
				int num = int(items.size());
				if (ch == incKey)
				{
					value = items[(index + 1) % num].second;
				}
				if (ch == decKey)
				{
					value = items[(index + num - 1) % num].second;
				}
			}
		}


		std::vector<std::pair<const char *, int>> items;
		std::map<const char *, int> values;
		std::map<int, const char *> invValues;
		std::map<int, int> valueToInd;
		int decKey;
		int incKey;
		std::string info;
	};
};

template <>
struct Selector<bool>
{
	struct Toggle
	{
		Toggle(int _key = 0, const std::string &desc = std::string()) :
			key(_key)
		{
			std::stringstream ss;
			ss << desc;
			if (key)
			{
				ss << " key: '" << keyToString(key) << "'";
			}
			info = ss.str();
		}

		const std::string &getInfo() const
		{
			return info;
		}

		void onKey(int ch, bool &value)
		{
			if (ch == key)
			{
				value = !value;
			}
		}

		std::string info;
		int key;
	};
};


}; // namespace TweakVarUiWidgetTypes


} // namespace TweakableVar

#endif // _TweakableVar_h_
