/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#include <map>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>

#include "TweakableVar.h"

namespace TweakableVar
{

// Singleton class looking after the variables...
VariableManager &VariableManager::get()
{
	static VariableManager vm;
	return vm;
};

void VariableManager::serialize(Stream &stream) 
{
	std::lock_guard<std::recursive_mutex> lock(m_mutex);

	if (stream.isInput())
	{

		while (!stream.eof())
		{
			std::string key;
			std::string value;
			std::tie(key, value) = stream.read();
			if (!key.empty())
			{
				m_configStrings[key] = value;
			}
		}
		for (auto & v : m_variables)
		{
			if (v.second->m_serialize)
			{
				v.second->loadFromConfig();
			}
		}
	}
	else
	{
		for (auto & v : m_variables)
		{
			if (v.second->m_serialize)
			{
				std::stringstream tmp;
				v.second->writeValue(tmp);
				m_configStrings[v.first] = tmp.str();
			}
		}
		for (auto kv : m_configStrings)
		{
			stream.write(kv.first, kv.second);
		}
	}
};

void VariableManager::registerVar(const std::string &name, VariableBase *var) 
{ 
	std::lock_guard<std::recursive_mutex> lock(m_mutex);

	auto it = m_variables.find(name);
	if (it == m_variables.end())
	{
		m_variables[name] = var;
		// note load from config might seem like a good idea, but will fail due to inheritance and being called by ctor
	}
	else
	{
		// ERROR!
	}
};



void VariableManager::loadFromConfig(const std::string &name, VariableBase *var) 
{ 
	std::lock_guard<std::recursive_mutex> lock(m_mutex);

	if (var->m_serialize)
	{
		auto it = m_configStrings.find(name);
		if (it != m_configStrings.end())
		{
			std::istringstream tmp((*it).second);
			var->readValue(tmp);
		}
	}
};

VariableManager::VariableManager() { };
VariableManager::~VariableManager() { };


} // namespace TweakableVar
