/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#include "DebugRenderer.h"
#include "PrimitiveRenderer.h"
#include "SimpleShader.h"
#include "Assert.h"
#include <glad/glad.h>
#include <linmath/int2.h>
#include <linmath/int3.h>
#include <linmath/float3x3.h>
#include <linmath/float4x4.h>
#include <linmath/float3.h>
#include <linmath/float2.h>
#include <utils/HashFunction.h>
#include <sstream>
#include <cstdio>



const chag::float4 DebugRenderer::red    = {1.0f, 0.0f, 0.0f, 1.0f};
const chag::float4 DebugRenderer::blue   = {0.0f, 0.0f, 1.0f, 1.0f};
const chag::float4 DebugRenderer::green  = {0.0f, 1.0f, 0.0f, 1.0f};
const chag::float4 DebugRenderer::yellow = {1.0f, 1.0f, 0.1f, 1.0f};
const chag::float4 DebugRenderer::purple = {0.4f, 0.1f, 0.4f, 1.0f};
const chag::float4 DebugRenderer::cyan   = {0.0f, 1.0f, 1.0f, 1.0f};
const chag::float4 DebugRenderer::orange = {1.0f, 0.5f, 0.0f, 1.0f};
const chag::float4 DebugRenderer::brown  = {0.55f, 0.27f, 0.07f, 1.0f};
const chag::float4 DebugRenderer::pink   = {0.78f, 0.44f, 0.44f, 1.0f};
const chag::float4 DebugRenderer::olive  = {0.56f, 0.56f, 0.22f, 1.0f};
const chag::float4 DebugRenderer::white = { 1.0f, 1.0f, 1.0f, 1.0f };
const chag::float4 DebugRenderer::black = { 0.0f, 0.0f, 0.0f, 1.0f };
const chag::float4 DebugRenderer::random = { -1.0f, -1.0f, -1.0f, 1.0f };

DebugRenderer& DebugRenderer::instance()
{
	static DebugRenderer inst;
  return inst;
}



void DebugRenderer::setThreadId(const std::string & id)
{
	PerThreadData &td = getPerThreadData();

	// ensure any thread data under old name is renamed, should happend very very rarely...
	if (id != td.threadId)
	{
		std::unique_lock<std::mutex> l(m_masterMutex);
		if (m_masterNamespace.count(td.threadId))
		{
			m_masterNamespace[id] = m_masterNamespace[td.threadId];
			m_masterNamespace.erase(td.threadId);
		}
	}
	getPerThreadData().threadId = id;
}



chag::float4 DebugRenderer::randomColor(size_t i, float alpha)
{
	static const chag::float4 colors[] =
	{
		red,
		green,
		blue,
		yellow,
		cyan,
		purple,
		orange,
		pink,
		brown,
//		olive,
	};
	float4 c = colors[i % (sizeof(colors) / sizeof(colors[0]))];
	c.w = alpha;
	return c;
}

chag::float4 DebugRenderer::heatMap(const float _value, const float minVal, const float maxVal, float alpha)
{
	chag::float4 color = { 0.0f, 0.0f, 0.0f, alpha };
	float range = maxVal - minVal;
	float value = chag::clamp(_value - minVal, 0.0f, range);
	float step = range / 6.0f;
	if (value < step)
	{
		color.z = value / step;
	}
	else if (value < 2.0f * step)
	{
		color.y = (value-step) / step;
		color.z = 1.0f;
	}
	else if (value < 3.0f * step)
	{
		color.y = 1.0f;
		color.z = 1.0f - (value-2.0f * step) / step;
	}
	else if (value < 4.0f * step)
	{
		color.x = (value-3.0f * step) / step;
		color.y = 1.0f;
	}
	else if (value < 5.0f * step)
	{
		color.x = 1.0f;
		color.y = 1.0f - (value-4.0f * step) / step;
	}
	else
	{
		color.x = 1.0f;
		color.y = (value - 5.0f * step) / step;
		color.z = (value - 5.0f * step) / step;
	}

	return color;
}



void DebugRenderer::addCommand(DrawCmd cmd)
{
	PerThreadData &td = getPerThreadData();
	td.m_namespaceStack.back()->drawCommands.push_back(cmd);
}



void DebugRenderer::push(const std::string & id, DrawCmd prologue)
{
	PerThreadData &td = getPerThreadData();
	td.m_namespaceStack.push_back(NameSpaceDataPtr(new NameSpaceData(id, prologue)));
}



void DebugRenderer::pop(const std::string & id, DrawCmd epilogue)
{
	PerThreadData &td = getPerThreadData();

	NameSpaceDataPtr nsData = td.m_namespaceStack.back();
	ASSERT(id == nsData->id);

	if (epilogue)
	{
		nsData->epilogue = epilogue;
	}

	td.m_namespaceStack.pop_back();

	// If we're back at the root, parcel off to the main thread.
	if (td.m_namespaceStack.empty())
	{
		std::unique_lock<std::mutex> l(m_masterMutex);
		m_masterNamespace[td.threadId][nsData->id] = nsData;
	}
	else
	{
		NameSpaceDataPtr parent = td.m_namespaceStack.back();
		parent->subSpaces[id].push_back(nsData);
	}
}



DebugRenderer::PerThreadData & DebugRenderer::getPerThreadData()
{
	static thread_local PerThreadData perThreadData;

	// generate unique thread ID, may be changed later to something more menaingful
	if (perThreadData.threadId.empty())
	{
		std::stringstream ss;
		ss << "th_";
		{
			std::unique_lock<std::mutex> l(m_masterMutex);
			ss << m_threadIdCounter++;
		}
		perThreadData.threadId = ss.str();
	}

	return perThreadData;
}



DebugRenderer::DebugRenderer()	: m_threadIdCounter(0)
{
}



DebugRenderer::Backend::Backend(chag::PrimitiveRenderer *primitiveRenderer, chag::SimpleShader *shader) :
	m_primitiveRenderer(primitiveRenderer),
	m_shader(shader),
	m_transform(chag::make_identity<float4x4>()),
	m_subIdFilter(nullptr),
	m_subIdDefaultVis(true)
{
}



void DebugRenderer::Backend::pushTransform(const chag::float4x4 & tfm)
{
	m_transform = m_transform * tfm;
	m_transformStack.push_back(tfm);
}



void DebugRenderer::Backend::popTransform()
{
	m_transformStack.pop_back();
	m_transform = chag::make_identity<float4x4>();
	for (const auto &tfm : m_transformStack)
	{
		m_transform = m_transform * tfm;
	}
}



void DebugRenderer::Backend::drawSetup(const float4 &color)
{
	m_primitiveRenderer->setTransform(m_transform);
	m_shader->setUniform("color", color);
	if (color.w < 1.0f)
	{
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}
	else
	{
		glDisable(GL_BLEND);
	}
}



void DebugRenderer::Backend::drawAabb(const Aabb & aabb, const float4 & color, const std::string & subId)
{
	if (testSubId(subId))
	{
		drawSetup(color);
		m_primitiveRenderer->drawAabb(aabb, m_shader, false);// true);
	}
}



void DebugRenderer::Backend::drawSphere(const float3 & position, float radius, const float4 & color, const std::string & subId)
{
	if (testSubId(subId))
	{
		drawSetup(color);
		m_primitiveRenderer->drawSphere(position, radius, m_shader, false);
	}
}



void DebugRenderer::Backend::drawLine(const float3 & pos0, const float3 & pos1, const float4 & color, const std::string & subId)
{
	if (testSubId(subId))
	{
		drawSetup(color);
		m_primitiveRenderer->drawLine(pos0, pos1, m_shader);
	}
}



void DebugRenderer::Backend::drawCone(const float3 & pos0, const float3 & dir, float height, float angleRad, const float4 & color, const std::string & subId)
{
	if (testSubId(subId))
	{
		drawSetup(color);
		m_primitiveRenderer->drawCone(pos0, dir, height, angleRad, m_shader, false);
	}
}



void DebugRenderer::Backend::drawDisc(const float3 & position, const float3 & direction, float radius, const float4 & color, const std::string & subId)
{
	if (testSubId(subId))
	{
		drawSetup(color);
		m_primitiveRenderer->drawDisc(position, direction, radius, m_shader, false);// true);
	}
}



void DebugRenderer::Backend::setTransformStackDepth(int depth)
{
	ASSERT(depth <= getTransformStackDepth());

	if (depth < getTransformStackDepth())
	{
		m_transformStack.resize(depth);
		m_transform = chag::make_identity<float4x4>();
		for (const auto &tfm : m_transformStack)
		{
			m_transform = m_transform * tfm;
		}
	}
}



void DebugRenderer::NameSpaceData::beginDraw(DebugRenderer::Backend & be)
{
	ASSERT(m_outerPreStackDepth == -1);
	m_outerPreStackDepth = be.getTransformStackDepth();

	if (prologue)
	{
		prologue(be);
	}
}



void DebugRenderer::NameSpaceData::endDraw(DebugRenderer::Backend & be)
{
	// execute draw commands
	for (const auto & cmd : drawCommands)
	{
		int preStackDepth = be.getTransformStackDepth();
		cmd(be);
		be.setTransformStackDepth(preStackDepth);
	}

	if (epilogue)
	{
		epilogue(be);
	}

	// ensure correct
	ASSERT(m_outerPreStackDepth != -1);
	be.setTransformStackDepth(m_outerPreStackDepth);
	m_outerPreStackDepth = -1;
}
