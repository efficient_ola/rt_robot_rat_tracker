/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#ifndef _AsyncFileIo_h_
#define _AsyncFileIo_h_

#include "BlockingQueue.h"
#include <string>
#include <vector>
#include <atomic>
#include <inttypes.h>

namespace async_file_io
{
	class BinaryOutFile;

namespace detail
{
	class BinaryOutFileImpl;


	// Singleton class, that processes the async writing tasks. They are all globally ordered. It is only used internally, and will ensure that all writes are completed when
	// a program shuts down. A program that wishes to know when this happens should end with a flush operation (otherwise the program may just seem to hang at the end).
	class IoManager
	{
	public:
		static std::shared_ptr<IoManager> instance();

		// waits for all outstanding commands to finish before returning.
		void flush();

		// terminates the command processing thread and waits for it to exit.
		void shutdown();

		~IoManager();
	protected:
		IoManager();


		friend class BinaryOutFile;
		std::thread m_processingThread;
		
		using Cmd = std::function<void(void)>;
		template <typename FN_T>
		void push(FN_T fn)
		{
			m_cmdQueue.push_back(Cmd(fn));
		}

		void processingFunc();

		BlockingQueue<Cmd> m_cmdQueue;
		std::atomic<bool> m_exit;
	};

	using IoManagerPtr = std::shared_ptr<IoManager>;
}


/**
	* Asynchronous binary file writer. Simple implementation that just enqueues any chunk in a queue and uses a thread to serve that.
	* One thread is created for all processing, thus it is not optimal for writing to different hardware devices, assuming very different performance
	* the slower device may then throttle the others.
	*/
class BinaryOutFile
{
public:
	BinaryOutFile(const std::string &filenNme = std::string());
	~BinaryOutFile();

	bool open(const std::string &filenNme);
	void close(bool wait = false);

	/** 
	 * returns true if a) the file open funcion has been called, and b) the actual file open operation has not completed yet, OR been successful.
	 */
	bool isOpen() const;

	size_t queueDepthBytes() const;	
	void write(const std::vector<uint8_t> &chunk);

protected:
	using ImplPtr = std::shared_ptr<detail::BinaryOutFileImpl>;
	ImplPtr m_implementation;
};


};//

#endif // _AsyncFileIo_h_
