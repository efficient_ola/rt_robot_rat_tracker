/****************************************************************************/
/* Copyright (c) 2011, Markus Billeter, Ola Olsson, Erik Sintorn and Ulf Assarsson
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
/****************************************************************************/
#ifndef _chag_SmallVector2_inl
#define _chag_SmallVector2_inl


#include "SmallVector2.h"
#include <utils/Math.h>
#include <cmath>
#include <type_traits>

// 
namespace chag
{


template <typename T>
FUNC_CUDA_HD T& SmallVector2<T>::operator [] (size_t i)
{  
  return *(&x + i); 
}



template <typename T>
FUNC_CUDA_HD const T& SmallVector2<T>::operator [] (size_t i) const
{  
  return *(&x + i); 
}



template <typename T>
FUNC_CUDA_HD bool SmallVector2<T>::operator == (const SmallVector2<T>& v) const
{ 
  return (v.x == x) && (v.y == y); 
}



template <typename T>
FUNC_CUDA_HD bool SmallVector2<T>::operator != (const SmallVector2<T>& v) const 
{ 
  return !(v == *this); 
}



template <typename T>
FUNC_CUDA_HD SmallVector2<T> SmallVector2<T>::operator - () const
{ 
  return make_vector<T>(-x, -y); 
}



template <typename T>
FUNC_CUDA_HD const SmallVector2<T>& SmallVector2<T>::operator += (const SmallVector2<T>& v)
{
  x += v.x; 
  y += v.y;
  return *this;
}



template <typename T>
FUNC_CUDA_HD const SmallVector2<T>& SmallVector2<T>::operator -= (const SmallVector2<T>& v)
{
  x -= v.x; 
  y -= v.y;
  return *this;
}



template <typename T>
template <typename S>
FUNC_CUDA_HD const SmallVector2<T>& SmallVector2<T>::operator *= (const S &s)
{
  x *= s; 
  y *= s;
  return *this;
}



template <typename T>
template <typename S>
FUNC_CUDA_HD const SmallVector2<T>& SmallVector2<T>::operator /= (const S& s)
{
  x /= s; 
  y /= s;
  return *this;
}



template <typename T>
FUNC_CUDA_HD SmallVector2<T> SmallVector2<T>::operator + (const SmallVector2<T>& v) const 
{
  return make_vector<T>(x + v.x, y + v.y);
}




template <typename T>
FUNC_CUDA_HD SmallVector2<T> SmallVector2<T>::operator + (const T& s) const 
{
  return make_vector<T>(x + s, y + s);
}



template <typename T>
FUNC_CUDA_HD SmallVector2<T> SmallVector2<T>::operator - (const SmallVector2<T>& v) const 
{
  return make_vector<T>(x - v.x, y - v.y);
}


template <typename T>
FUNC_CUDA_HD SmallVector2<T> SmallVector2<T>::operator - (const T& s) const 
{
  return make_vector<T>(x - s, y - s);
}



template <typename T>
template <typename S>
FUNC_CUDA_HD SmallVector2<T> SmallVector2<T>::operator * (const S& s) const
{
  return make_vector<T>(x * s, y * s);
}



template <typename T>
FUNC_CUDA_HD SmallVector2<T> SmallVector2<T>::operator * (const SmallVector2<T>& v) const
{
  return make_vector<T>(x * v.x, y * v.y);
}


template <typename T, typename = std::enable_if_t<std::is_floating_point<T>::value>>
FUNC_CUDA_HD SmallVector2<T> operator / (const SmallVector2<T> &v, const float& s)
{
	T r = 1.0f / s;
	return make_vector<T>(v.x * r, v.y * r);
}

template <typename T, typename S, typename = std::enable_if_t<!std::is_floating_point<S>::value && std::is_scalar<S>::value>>
FUNC_CUDA_HD SmallVector2<T> operator / (const SmallVector2<T> &v, const S& s)
{
	return make_vector<T>(v.x / s, v.y / s);
}


template <typename T>
FUNC_CUDA_HD SmallVector2<T> SmallVector2<T>::operator / (const SmallVector2<T>& v) const 
{
  return make_vector<T>(x / v.x, y / v.y);
}

template <typename T>
FUNC_CUDA_HD T dot(const SmallVector2<T>& a, const SmallVector2<T>& b)
{
  return a.x * b.x + a.y * b.y;
}



template <typename T>
FUNC_CUDA_HD T length(const SmallVector2<T> &v)
{
  return (T)sqrt(dot(v,v));
}



template <typename T>
FUNC_CUDA_HD T lengthSquared(const SmallVector2<T> &v)
{
  return dot(v,v);
}



template <typename T>
FUNC_CUDA_HD SmallVector2<T> perpendicular(const SmallVector2<T> &v)
{
  return make_vector(-v.y, v.x);
}



template <typename T>
FUNC_CUDA_HD SmallVector2<T> normalize(const SmallVector2<T>& v)
{
  return v / length(v);
}



template <typename T>
FUNC_CUDA_HD SmallVector2<T> pow(const SmallVector2<T>& v, const T &exp)
{
  return make_vector(std::pow(v.x, exp), std::pow(v.y, exp));
}



template <typename T>
FUNC_CUDA_HD SmallVector2<T> ceil(const SmallVector2<T>& v)
{
  return make_vector(std::ceil(v.x), std::ceil(v.y));
}



template <typename S, typename T>
FUNC_CUDA_HD SmallVector2<T> operator * (const S &s, const SmallVector2<T>& v)
{
  return v * s;
}



template <typename T>
FUNC_CUDA_HD SmallVector2<T> make_vector(const T& x, const T& y)
{
  SmallVector2<T> r = { x, y };
  return r;
};

template <typename T, typename U, typename std::enable_if<!std::is_same<T, U>::value, int>::type = 0>
FUNC_CUDA_HD SmallVector2<T> make_vector(const U& x, const U& y)
{
	SmallVector2<T> r = { T(x), T(y) };
	return r;
};


template <typename T, typename S>
FUNC_CUDA_HD SmallVector2<T> make_vector(const SmallVector2<S> &v)
{
  SmallVector2<T> r = { T(v.x), T(v.y) };
  return r;
}



template <typename T>
FUNC_CUDA_HD SmallVector2<T> make_vector2(const T *v)
{
  SmallVector2<T> r = { v[0], v[1] };
  return r;
};

template <typename T>
FUNC_CUDA_HD SmallVector2<T> make_vector2(const T &v)
{
	SmallVector2<T> r = { v, v };
	return r;
};


template <typename T>
FUNC_CUDA_HD SmallVector2<T> min(const SmallVector2<T> &a, const SmallVector2<T> &b)
{
  return make_vector(chag::min(a.x, b.x), chag::min(a.y, b.y));
}



template <typename T>
FUNC_CUDA_HD SmallVector2<T> max(const SmallVector2<T> &a, const SmallVector2<T> &b)
{
  return make_vector(chag::max(a.x, b.x), chag::max(a.y, b.y));
}

template <typename T>
FUNC_CUDA_HD SmallVector2<T> clamp(const SmallVector2<T> &value, const SmallVector2<T> &lowerInc, const SmallVector2<T> &upperInc)
{
  return min(max(value, lowerInc), upperInc);
}

template <typename T>
FUNC_CUDA_HD const SmallVector2<T> abs(const SmallVector2<T>& v)
{
	return make_vector(abs(v.x), abs(v.y));
}

template <>
FUNC_CUDA_HD const SmallVector2<float> abs(const SmallVector2<float>& v)
{
	return make_vector(fabsf(v.x), fabsf(v.y));
}


} // namespace chag


#endif // _chag_SmallVector2_inl
