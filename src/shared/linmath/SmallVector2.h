/****************************************************************************/
/* Copyright (c) 2011, Markus Billeter, Ola Olsson, Erik Sintorn and Ulf Assarsson
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
/****************************************************************************/
#ifndef _chag_SmallVector2_h
#define _chag_SmallVector2_h

#include <utils/CudaCompat.h>
#include <math.h>
#include <cstddef>

#ifdef min
  #undef min
#endif
#ifdef max
  #undef max
#endif

// 
namespace chag
{

template <typename T>
class SmallVector2
{
public:
  T x;
  T y;

  // Indexation
	FUNC_CUDA_HD T& operator [] (const size_t i);
	FUNC_CUDA_HD const T& operator [] (const size_t i) const;

	FUNC_CUDA_HD bool operator == (const SmallVector2& v) const;
	FUNC_CUDA_HD bool operator != (const SmallVector2& v) const;

	// Negation
	FUNC_CUDA_HD SmallVector2 operator - () const;

	FUNC_CUDA_HD const SmallVector2& operator += (const SmallVector2& v);
	FUNC_CUDA_HD const SmallVector2& operator -= (const SmallVector2& v);

	template <typename S>
	FUNC_CUDA_HD const SmallVector2& operator *= (const S& s);

	template <typename S>
	FUNC_CUDA_HD const SmallVector2& operator /= (const S& s);

	FUNC_CUDA_HD SmallVector2 operator + (const SmallVector2& v) const;
	FUNC_CUDA_HD SmallVector2 operator + (const T& s) const;
	FUNC_CUDA_HD SmallVector2 operator - (const SmallVector2& v) const;
	FUNC_CUDA_HD SmallVector2 operator - (const T& s) const;

	template <typename S>
	FUNC_CUDA_HD SmallVector2 operator * (const S& s) const;

	FUNC_CUDA_HD SmallVector2 operator * (const SmallVector2& v) const;

	FUNC_CUDA_HD SmallVector2 operator / (const SmallVector2& v) const;
};


template <typename T, typename S>
FUNC_CUDA_HD SmallVector2<T> operator / (const SmallVector2<T> &v, const float& s);


template <typename T>
FUNC_CUDA_HD T dot(const SmallVector2<T>& a, const SmallVector2<T>& b);



template <typename T>
FUNC_CUDA_HD T length(const SmallVector2<T>& v);



template <typename T>
FUNC_CUDA_HD T lengthSquared(const SmallVector2<T> &v);


template <typename T>
FUNC_CUDA_HD SmallVector2<T> perpendicular(const SmallVector2<T> &v);


template <typename T>
FUNC_CUDA_HD SmallVector2<T> normalize(const SmallVector2<T>& v);



template <typename T>
FUNC_CUDA_HD SmallVector2<T> pow(const SmallVector2<T>& v, const T &exp);



template <typename T>
FUNC_CUDA_HD SmallVector2<T> ceil(const SmallVector2<T>& v);



template <typename S, typename T>
FUNC_CUDA_HD SmallVector2<T> operator * (const S &s, const SmallVector2<T>& v);



template <typename T>
FUNC_CUDA_HD SmallVector2<T> make_vector(const T& x, const T& y);



template <typename T, typename S>
FUNC_CUDA_HD SmallVector2<T> make_vector(const SmallVector2<S> &v);



template <typename T>
FUNC_CUDA_HD SmallVector2<T> make_vector2(const T *v);



template <typename T>
FUNC_CUDA_HD SmallVector2<T> min(const SmallVector2<T> &a, const SmallVector2<T> &b);



template <typename T>
FUNC_CUDA_HD SmallVector2<T> max(const SmallVector2<T> &a, const SmallVector2<T> &b);



template <typename T>
FUNC_CUDA_HD SmallVector2<T> clamp(const SmallVector2<T> &value, const SmallVector2<T> &lowerInc, const SmallVector2<T> &upperInc);

} // namespace chag

#endif // _chag_SmallVector2_h

#include "SmallVector2.inl"
