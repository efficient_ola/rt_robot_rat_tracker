/****************************************************************************/
/* Copyright (c) 2018 Ola Olsson
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
/****************************************************************************/
#ifndef _chag_type_conversions_h
#define _chag_type_conversions_h

#include "float2.h"
#include "float3.h"
#include "float4.h"

namespace chag
{
namespace type_conversions
{



FUNC_CUDA_HD float2 f2(const float3 &v) { return make_vector(v.x, v.y); }
FUNC_CUDA_HD float3 f3(const float4 &v) { return make_vector(v.x, v.y, v.z); }
FUNC_CUDA_HD float4 f4(const float3 &v, float w) { return make_vector(v.x, v.y, v.z, w); }
FUNC_CUDA_HD float3 f3(const float2 &v, float z) { return make_vector(v.x, v.y, z); }

} // namespace type_conversions
} // namespace chag

#endif // _chag_type_conversions_h
