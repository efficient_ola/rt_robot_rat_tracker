/****************************************************************************/
/* Copyright (c) 2011, Markus Billeter, Ola Olsson, Erik Sintorn and Ulf Assarsson
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
/****************************************************************************/
#ifndef _chag_linmath_IOStreamUtils_h
#define _chag_linmath_IOStreamUtils_h

#include "SmallVector2.h"
#include "SmallVector3.h"
#include "SmallVector4.h"
#include <iostream>

namespace chag
{

template <typename T>
inline std::istream &operator>>(std::istream &ss, SmallVector4<T> &v)
{
	ss >> v.x >> v.y >> v.z >> v.w;
	return ss;
}



template <typename T>
inline std::ostream &operator<<(std::ostream &ss, const SmallVector4<T> &v)
{
	ss << v.x << " " << v.y << " " << v.z << " " << v.w;
	return ss;
}



template <typename T>
inline std::istream &operator>>(std::istream &ss, SmallVector3<T> &v)
{
	ss >> v.x >> v.y >> v.z;
	return ss;
}



template <typename T>
inline std::ostream &operator<<(std::ostream &ss, const SmallVector3<T> &v)
{
	ss << v.x << " " << v.y << " " << v.z;
	return ss;
}


template <typename T>
inline std::istream &operator>>(std::istream &ss, SmallVector2<T> &v)
{
	ss >> v.x >> v.y;
	return ss;
}



template <typename T>
inline std::ostream &operator<<(std::ostream &ss, const SmallVector2<T> &v)
{
	ss << v.x << " " << v.y;
	return ss;
}
} // namespace chag

#endif // _chag_linmath_IOStreamUtils_h
