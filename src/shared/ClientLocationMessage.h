#ifndef _ClientLocationMessage_h_
#define _ClientLocationMessage_h_

namespace ClientLocationMessage
{
	static constexpr int s_maxIdChars = 24;
	static constexpr int s_maxAnnotaions = 4;

	// local
	struct Annoation
	{
		float x;
		float y;
		float z;
		float orientationAngle;
		char id[s_maxIdChars];
	};

	struct Location
	{
		float x;
		float y;
		float z;
		float orientationAngle;
		char id[s_maxIdChars];
		char objectClass[s_maxIdChars];
		Annoation annotations[s_maxAnnotaions];
	};

} // namespace ClientLocationMessage

#endif // _ClientLocationMessage_h_