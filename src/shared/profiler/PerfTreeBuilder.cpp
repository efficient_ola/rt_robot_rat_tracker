/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#include "PerfTreeBuilder.h"
#include <algorithm>
#include <utility>


std::vector<PerfTreeNode*> PerfTreeBuilder::build(Profiler &profiler)
{
	for (PathItemMap::iterator it = m_pathItemMap.begin(); it != m_pathItemMap.end(); ++it)
	{
		PerfTreeNode *n = (*it).second;

		n->averageSum -= n->averageBuffer[n->currentAverageSlot % PerfTreeNode::s_numAvgSlots];
		// store new value to be part of average.
		n->averageBuffer[n->currentAverageSlot % PerfTreeNode::s_numAvgSlots] = n->time;
		// add new to sum to be averaged
		n->averageSum += n->time;
		n->average = n->averageSum / double(std::min<uint32_t>(n->currentAverageSlot + 1, PerfTreeNode::s_numAvgSlots));
		n->currentAverageSlot++;
		n->count = 0ULL;
		n->time = 0.0;
	}

	Profiler &p = profiler;
	Profiler::const_iterator it = p.begin();
	std::vector<int> path;

	return buildItemTree(it, p, path);
}



std::vector<PerfTreeNode*> PerfTreeBuilder::buildItemTree(Profiler::const_iterator &it, Profiler &profiler, std::vector<int> &path)
{
  std::vector<PerfTreeNode*> items;

  double startTime = it != profiler.end() ? (*it).m_timeStamp : 0.0;

  for (; it != profiler.end(); ++it)
  {
    bool stop = false;

    switch((*it).m_id)
    {
    case Profiler::CID_Message:
			{
				path.push_back((*it).m_tokenId);

				PerfTreeNode *item = m_pathItemMap[path];
				if (!item)
				{
					item = new PerfTreeNode(profiler.getTokenString((*it).m_tokenId));
					m_pathItemMap[path] = item;
				}
				std::string tmp(reinterpret_cast<const char *>((*it).extraData()), (*it).m_sizeOfMessage);
				item->message = tmp;
				item->count += 1;
				path.pop_back();

				if (std::find(items.begin(), items.end(), item) == items.end())
				{
					items.push_back(item);
				}
			}
			break;
    case Profiler::CID_Counter:
      {
        path.push_back((*it).m_tokenId);

				PerfTreeNode *item = m_pathItemMap[path];
				if (!item)
				{
					item = new PerfTreeNode(profiler.getTokenString((*it).m_tokenId));
					m_pathItemMap[path] = item;
				}

        item->count += uint64_t((*it).m_value);
        path.pop_back();

				if (std::find(items.begin(), items.end(), item) == items.end())
        {
					items.push_back(item);
        }
      }
      break;
    case Profiler::CID_BeginBlock:
      {
        PerfTreeNode *item = handleBlock(it, profiler, path);
				if (std::find(items.begin(), items.end(), item) == items.end())
        {
					items.push_back(item);
        }
      }
      break;
    case Profiler::CID_EndBlock:
      return items;
    case Profiler::CID_OneOff:
      break;
    };
  }

  return items;
}




PerfTreeNode *PerfTreeBuilder::handleBlock(Profiler::const_iterator &it, Profiler &profiler, std::vector<int> &path)
{
  double startTime = (*it).m_timeStamp;
  ASSERT((*it).m_id == Profiler::CID_BeginBlock);
	path.push_back((*it).m_tokenId);
  PerfTreeNode *result = m_pathItemMap[path];
  if (!result)
  {
    result = new PerfTreeNode(profiler.getTokenString((*it).m_tokenId));
    m_pathItemMap[path] = result;
  }
  result->count += 1;
  ++it;

	result->children = buildItemTree(it, profiler, path);
  path.pop_back();
  result->time += (*it).m_timeStamp - startTime;
  ASSERT((*it).m_id == Profiler::CID_EndBlock);

  return result;
}
