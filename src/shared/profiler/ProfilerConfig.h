/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#ifndef _ProfilerConfig_h_
#define _ProfilerConfig_h_

#if !ENABLE_CUDA
	#define PROFILER_DISABLE_CUDA
#endif // !ENABLE_CUDA

// #define PROFILER_NO_CUDA_GL_TIMERS
#define PROFILER_SEND_IN_THREAD 0


#if defined(_WIN32)
#	if !defined(PROFILER_USE_TCP_SOCKETS)
#		define PROFILER_USE_WIN32_NAMED_PIPES 1
#	endif // PROFILER_USE_TCP_SOCKETS

/* XXX- Untested on windows. Let's not aggravate Ola. Disable for the moment
 * on windows. :-)
 */
#	undef PROFILER_SEND_IN_THREAD

#else
#	if !defined(PROFILER_USE_TCP_SOCKETS)
#		define PROFILER_USE_TCP_SOCKETS 0
#	endif // PROFILER_USE_TCP_SOCKETS

#endif // platform dependent

#endif // _ProfilerConfig_h_

