/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#define _USE_MATH_DEFINES 1
#include <iostream>
#include <algorithm>
#include <numeric>
#include "utils/PerformanceTimer.h"
#include <assert.h>
#ifdef _WIN32
#include <intrin.h>
#endif
#include <ctime>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <imgui/imgui.h>
#include <imgui/imgui_impl_glfw_gl3.h>
#include <utils/ImGuiX.h>

#include <utils/CheckGLError.h>
#include <utils/GlBufferObject.h>
#include <utils/Assert.h>
#include "utils/SimpleCamera.h"
#include "utils/Rendering.h"
#include "utils/CameraPoints.h"
#include "utils/SimpleShader.h"
#include "utils/Random.h"
#include "linmath/Aabb.h"
#include "utils/DebugRenderer.h"
#include "utils/PrimitiveRenderer.h"
#include "utils/ChagIntersection.h"
#include "profiler/Profiler.h"
#include "profiler/PerfTreeBuilder.h"
#include "ClientOutput.h"
#include <utils/OBJModel.h>
#include "linmath/IOStreamUtils.h"
#include "TemplateTracker.h"
#include "TemplateLibrary.h"
#include "TemplateEditor.h"
#include "Config.h"
#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>    
#include <boost/exception/diagnostic_information.hpp> 

#include "CrashHandlerWrapper.h"

#include <future>

#include "utils/TweakableVarImGui.h"
#include "utils/ChagJsonUtils.h"
#include <nlohmann/json.hpp>
#include "BackendSharedImplementation.h"
#include "ImageStreamOutput.h"

#include <utils/BlockingQueue.h>

using json = nlohmann::json;


#if USE_TRACKING_THREAD
#include <mutex>
#include <thread>
#endif // USE_TRACKING_THREAD


#include "utils/TweakableVar.h"
#include <boost/filesystem.hpp>

#include <unordered_map>
#include "ColorDepthSource.h"

namespace bfs = boost::filesystem;

static bfs::path g_envRootPath;

static std::string g_imageOutputSubPath = "shots";
static std::string g_arenaSetupsSubPath = "arena_setups";
static std::string g_trackedObjectsSetupsSubPath = "tracked_object_setups";
static std::string g_recordingsOutputSubPath = "recordings";

static bfs::path g_imageOutputPath;
static bfs::path  g_arenaSetupsPath;
static bfs::path  g_trackedObjectsSetupsPath;

static std::string g_outputTagBase = "";

using namespace chag;
namespace tv = TweakableVar;

const float g_nInf = -std::numeric_limits<float>::infinity();

ClientOutput *g_output = nullptr;
ImageStreamOutput *g_imageOuput = nullptr;

PrimitiveRenderer *g_primitiveRenderer = nullptr;

static std::map<std::string, TemplateLibraryPtr> g_templateLibs;

// collects info needed to set up a type of object to track
struct TrackedObjectType
{
	//std::string id; // name this descriptor - e.g., "basic_rat" used to refer to from instance
	std::string templateLibraryName;
	std::string objectClass;  // e.g., robot/animal
};

// makes it easy to have several instances of the same type, e.g., several robots with differnt names
struct TrackedObjectTypeInstance
{
	std::string name; // irat-blue, irat-red, etc, maybe even use for animals?
	std::string typeId; //TrackedObjectType above
	chag::float3 displayColour;  // 
};

static std::map<std::string, TrackedObjectType> g_objectTypes;
static std::vector<TrackedObjectTypeInstance> g_objectInstances;

std::map<std::string, TrackedObjectType> loadTrackedObjectTypes(const bfs::path &fileName);
std::vector<TrackedObjectTypeInstance> loadTrackedObjectInstances(const bfs::path &fileName);

static std::map<const std::string, chag::float4> g_modelColours=
{
	{ "irat-new", DebugRenderer::red },
	{ "real-rat", DebugRenderer::green },
	{ "irat-old", DebugRenderer::blue },
};


// Special keys are offset since GLUT has the awseome idea of mapping them over the alpha numeric range...
// This remaps them so we can get useful distinct codes to use with the GUI
const int g_specialKeyOffset = 0;// 1024;

enum SpecialKey
{
	SK_F1 = GLFW_KEY_F1 + g_specialKeyOffset,
	SK_F2 = GLFW_KEY_F2 + g_specialKeyOffset,
	SK_F3 = GLFW_KEY_F3 + g_specialKeyOffset,
	SK_F4 = GLFW_KEY_F4 + g_specialKeyOffset,
	SK_F5 = GLFW_KEY_F5 + g_specialKeyOffset,
	SK_F6 = GLFW_KEY_F6 + g_specialKeyOffset,
	SK_F7 = GLFW_KEY_F7 + g_specialKeyOffset,
	SK_F8 = GLFW_KEY_F8 + g_specialKeyOffset,
	SK_F9 = GLFW_KEY_F9 + g_specialKeyOffset,
	SK_F10 = GLFW_KEY_F10	+ g_specialKeyOffset,
	SK_F11 = GLFW_KEY_F11	+ g_specialKeyOffset,
	SK_F12 = GLFW_KEY_F12	+ g_specialKeyOffset,
	SK_Left = GLFW_KEY_LEFT + g_specialKeyOffset,
	SK_Right = GLFW_KEY_RIGHT + g_specialKeyOffset,
	SK_Up = GLFW_KEY_UP + g_specialKeyOffset,
	SK_Down = GLFW_KEY_DOWN + g_specialKeyOffset,
	SK_PageUp = GLFW_KEY_PAGE_UP + g_specialKeyOffset,
	SK_PageDown = GLFW_KEY_PAGE_DOWN + g_specialKeyOffset,
	SK_Home = GLFW_KEY_HOME + g_specialKeyOffset,
	SK_End = GLFW_KEY_END + g_specialKeyOffset,
	SK_Insert = GLFW_KEY_INSERT + g_specialKeyOffset,
	// Some Special keys are NOT offset since they are not treated as special keys, very arbitrary
	SK_Del = GLFW_KEY_DELETE,
	SK_Enter = GLFW_KEY_ENTER,
	SK_Escape = GLFW_KEY_ESCAPE,
	SK_Tab = GLFW_KEY_TAB,
	SK_Divide = GLFW_KEY_KP_DIVIDE,
	SK_Multiply = GLFW_KEY_KP_MULTIPLY,
	SK_Subtract = GLFW_KEY_KP_SUBTRACT,
	SK_Add = GLFW_KEY_KP_ADD,
};

bool g_showInfo = false;
bool g_showDebugRenderer = false;
bool g_showMainUi = true;
bool g_showProfilerInfo = false;
bool g_frameIndexSet = false; // used to indicate when the frame has been changed by the UI and which overides the g_paused flag for one frame.

enum ArenaSetupControl
{
	ASC_Idle,
	ASC_WaitingForClicks,
	ASC_HasSetup,
	ASC_Max,
};
static ArenaSetupControl g_setupArena = ASC_Idle;



DECLARE_TWEAKABLEVAR_UI_G(bool, g_showTemplates, true, Toggle(SK_F8, "Toggle visibility of the cursor for the currently selected pose in the 3D view."));
DECLARE_TWEAKABLEVAR_UI_G(bool, g_paused, false, Toggle(' '));
DECLARE_TWEAKABLEVAR_UI_G(int, g_pauseFrameCount, -1, Input(-1));
DECLARE_TWEAKABLEVAR_UI_G_RO(int, g_currentFrame, -1, Input(-1));
DECLARE_TWEAKABLEVAR_UI_G(bool, g_showRgb, false, Toggle('d', "Toggle 2D view between showing color-coded depth and the data from the RGB camera."));
DECLARE_TWEAKABLEVAR_UI_G(float, g_fov, 60.0f, Slider(1.0f, 90.0f));
DECLARE_TWEAKABLEVAR_UI_G(bool, g_constrainTfm, true, Toggle(-1, "If set to true, the transform of the tracked objects is constrained to some tolerance of the arena plane"));
DECLARE_TWEAKABLEVAR_UI_G(float, g_minDepth, 0.0f, Slider(0.0f, 10.0f, 0.1f));
DECLARE_TWEAKABLEVAR_UI_G(float, g_maxDepth, 4.5f, Slider(0.0f, 10.0f, 0.1f));
DECLARE_TWEAKABLEVAR_UI_G(float, g_defaultEllipsoidTemplateRadius, 0.1f, Slider(0.001f, 0.5f, 0.001f));
DECLARE_TWEAKABLEVAR_UI_G(float, g_leftWidthProp, 0.5f, Slider(0.0f, 1.0f));
DECLARE_TWEAKABLEVAR_UI_G(bool, g_poseCaptureMode, false, Toggle(-1, "Enables capture mode which forces constraining to be on and set to zero tolerance."));

DECLARE_TWEAKABLEVAR_UI_G(float, g_onPlaneTolerance, 0.01f, Input(0.001f, 100.0f, 0.01f));

DECLARE_TWEAKABLEVAR_UI_G_NS(int, g_framesToShoot, -1, Input(-1));
DECLARE_TWEAKABLEVAR_UI_G_RO(int, g_frameNr, 0, Input(0));

DECLARE_TWEAKABLEVAR_UI_G_NS(int, g_takeNumber, 0, Input(0));
DECLARE_TWEAKABLEVAR_UI_G_NS(bool, g_outputFullFrame, false, Toggle('x', "If enabled the frame capture (i.e., to video or image stream) includes the UI."));
DECLARE_TWEAKABLEVAR_UI_G_NS(bool, g_dumpToFile, false, Toggle('f', "Controls streaming input to a binary recording file. New files are given the tag and a date and are stored in the folder '" + g_recordingsOutputSubPath + "'")); /**< Toggles dumping stream to a file. */
DECLARE_TWEAKABLEVAR_UI_G(float, g_depthClusterThreshold, 0.05f, Input(0.001f, 1.0f, 0.001f));

// These are declared without UI, so they get stored and reloaded.
DECLARE_TWEAKABLEVAR_G(std::string, g_arenaSetupName, "");
DECLARE_TWEAKABLEVAR_G(std::string, g_selectedObjectName, "");
DECLARE_TWEAKABLEVAR_G(std::string, g_trackedObjectsSetup, "default.json");

DECLARE_TWEAKABLEVAR_UI_G(float, g_arenaRadius, 0.6f, Input(0.001f, 10.0f, 0.01f));
static chag::float4 g_arenaPlaneEq = make_vector(0.0f, 0.0f, 0.0f, 0.0f);
static chag::float3 g_arenaCentre = make_vector(0.0f, 0.0f, 0.0f);


enum ViewMode
{
	VM_Both,
	VM_2D,
	VM_3D
};

DECLARE_TWEAKABLEVAR_UI_G(int, g_viewMode, VM_Both, Enum({ { "Both", VM_Both },{ "2D", VM_2D },{ "3D", VM_3D } }, SK_Up, SK_Down));


static bool g_showTemplatesGui = false;

static float g_pointSize = 2.0f;
static int g_optionsFileIndex = 0;
static float g_realTime = 0.0f;
// Transform from and to local arena space with center of arena at origin and positive Z axis pointing up
chag::float4x4 g_arenaToWorldTfm = chag::make_identity<chag::float4x4>();
chag::float4x4 g_worldToArenaTfm = chag::make_identity<chag::float4x4>();

static ChunkInStreamPlayerPtr g_blockStreamPlayer = nullptr;
static FileOutputStreamerDepthSource *g_source = 0;

static SimpleShader *g_debugShader = 0;
static SimpleShader *g_pointsShader = 0;
static SimpleShader *g_texturedQuadShader = 0;
static SimpleShader *g_objModelShader = 0;

using TemplateTracker = TemplateTracker;
static TemplateTracker *g_tracker = nullptr;
static OBJModel *g_cursorModel = nullptr;

class JsonDataLoggerBackend : public DataLoggerBackend
{
public:
	std::string m_fileName;
	std::vector<std::pair<std::string, json>> m_stack;
	JsonDataLoggerBackend(const bfs::path &fileName) : m_fileName(fileName.string())
	{
		json j;
		m_stack.push_back(std::pair<std::string, json>("root", j));
	}
	~JsonDataLoggerBackend()
	{
		assert(m_stack.size() == 1);
		std::ofstream o(m_fileName);
		if (o.is_open())
		{
			o << std::setw(4) << m_stack[0].second;
		}

	}
	virtual void begin(const std::string &section)
	{
		json j;
		m_stack.push_back(make_pair(section, j));
	}
	virtual void end()
	{
		assert(m_stack.size() > 1);
		std::pair<std::string, json> &sect = m_stack[m_stack.size() - 1];
		json &j = m_stack[m_stack.size() - 2].second;
		if (!j.count(sect.first))
		{
			j[sect.first] = std::vector<json>{ sect.second };
		}
		else
		{
			j[sect.first].push_back(sect.second);
		}
		m_stack.pop_back();
	}

	template <typename T>
	void writeJson(const std::string &key, const T &value)
	{
		std::pair<std::string, json> &sect = m_stack[m_stack.size() - 1];
		sect.second[key] = value;
	}
	virtual void write(const std::string &key, float value)
	{
		writeJson(key, value);
	}
	virtual void write(const std::string &key, double value)
	{
		writeJson(key, value);
	}
	virtual void write(const std::string &key, const chag::float3 &value)
	{
		writeJson(key, value);
	}

	virtual void write(const std::string &key, const chag::float4x4 &value)
	{
		writeJson(key, value);
	}

	virtual void write(const std::string &key, const chag::Aabb &value)
	{
		writeJson(key, value);
	}

	virtual void write(const std::string &key, bool value)
	{
		writeJson(key, value);
	}

	virtual void write(const std::string &key, const std::string &value)
	{
		writeJson(key, value);
	}

	virtual void write(const std::string &key, int value)
	{
		writeJson(key, value);
	}

	virtual void write(const std::string &key, const std::vector<float> &value) 
	{
		writeJson(key, value);
	}
	virtual void write(const std::string &key, const std::vector<std::string> &value)
	{
		writeJson(key, value);
	}
};



struct DataLoggerLogStream : public TweakableVar::Stream
{
	DataLoggerLogStream(DataLogger *l) : m_l(l) { }

	virtual void write(const std::string &key, const std::string &value)
	{
		m_l->write(key, value);
	}
	DataLogger *m_l;
};


void readVars(const bfs::path &fileName)
{
	std::ifstream i(fileName.string());

	if (i.is_open())
	{
		tv::IfOfStream stream(i);
		TweakableVar::VariableManager::get().serialize(stream);
		printf("INFO: Loaded options from '%s'\n", fileName.string().c_str());
	}
	else
	{
		printf("INFO: Failed to load options from '%s'\n", fileName.string().c_str());
	}
}

static void printInfo();

void writeVars(const bfs::path &fileName)
{
	std::ofstream o(fileName.string());
	if (o.is_open())
	{
		tv::IfOfStream stream(o);
		TweakableVar::VariableManager::get().serialize(stream);
	}
}

/**
* This really has nothing to do with the tweak vars or the tweak var GUI except being rendered together, and handled in a similar way.
* TODO: Think about migrating somewhere? Keybindings could be abstracted on their own, but UI should show info about it so...
*/
struct TweakVarUIButton
{
	TweakVarUIButton(const std::string &text, int key, std::function<void()> cb, const std::string &desc) :
		m_text(text), m_key(key), m_callback(cb), m_desc(desc)
	{
	}

	std::string m_text;
	int m_key;
	std::function<void()> m_callback;
	std::string m_desc;

	void render()
	{
		if (ImGui::Button(m_text.c_str(), { -1, 0 }))
		{
			m_callback();
		}
	}

	void onKey(int ch)
	{
		if (ch == m_key)
		{
			m_callback();
		}
	}

	std::string getInfo()
	{
		std::stringstream ss;
		if (m_key)
		{
			ss << " key: '" << tv::keyToString(m_key) << "'" << std::endl << m_desc;
		}
		return ss.str();
	}
};

static std::vector<TweakVarUIButton*> g_tweakVarButtons;

static void tweakVarUiAddButton(const char *text, int key, std::function<void()> cb, const std::string &desc)
{
	g_tweakVarButtons.push_back(new TweakVarUIButton(text, key, cb, desc));
}

// dispatches key code to all variable UI elements and buttons.
static void tweakVarUiOnKey(int key)
{
	tv::VariableManager::UiList uis = tv::VariableManager::get().getUis();
	for (auto twr : uis)
	{
		twr->onKey(key);
	}
	for (auto b : g_tweakVarButtons)
	{
		b->onKey(key);
	}
}


inline float degreesToRadians(float degs)
{
	return degs * g_pi / 180.0f;
}

const int g_startWidth  = 1280; //1920; // // 1920; // 
const int g_startHeight = 720; // 480;  // 1080; // 

static int g_width = g_startWidth;
static int g_height = g_startHeight;
static PerformanceTimer g_timer;

static chag::SimpleCamera g_camera;
static CameraPoints g_cameraPoints;

static bool g_mouseLookActive = false;
static std::vector<chag::int2> g_clickCoords;
static std::vector<chag::float2> g_imageSpaceClickCoords;

static int2 g_mousePos = { 0, 0 };
static int2 g_mouseLeftClickDownPos = { 0, 0 };

inline bool inLeftWindow(const int2 &mousePos)
{
	// TODO: plug in the horizontal splitter thing here.
	int leftOffset = int(g_leftWidthProp * float(g_width));
	if (g_viewMode == VM_2D)
	{
		leftOffset = g_width;
	}
	else if (g_viewMode == VM_3D)
	{
		leftOffset = 0;
	}
	return mousePos.x < leftOffset;
}



template <typename T>
inline T *safe_ptr(std::vector<T> &v)
{
	return v.empty() ? (T*)0 : &v[0];
}



template <typename T>
inline const T *safe_ptr(const std::vector<T> &v)
{
	return v.empty() ? (const T*)0 : &v[0];
}



template <typename T>
static void deleteIfThere(T *&shader)
{
	if (shader)
	{
		delete shader;
		shader = 0;
	}
}

TemplateEditor *g_templateGui = nullptr;

static std::string getTimeStampStr(bool fileNameFmt = true)
{
	time_t rawtime;
	struct tm * timeinfo;
	char buffer[80];

	time(&rawtime);
	timeinfo = localtime(&rawtime);

	if (fileNameFmt)
	{
		strftime(buffer, sizeof(buffer), "%Y%m%d-%I_%M_%S", timeinfo);
	}
	else
	{
		strftime(buffer, sizeof(buffer), "%Y-%m-%d %I:%M:%S", timeinfo);
	}
	return std::string(buffer);
}



uint32_t toFixed(float v, uint32_t max)
{
	return uint32_t(v * float(max));
}



uint32_t packRgba(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
	return	a << 24U | b << 16U | g << 8U | r;
}



chag::float4 rgbaToFloat(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
	return chag::make_vector(float(r), float(g), float(b), float(a)) * (1.0f / 255.0f);
}



uint32_t packRgba(uint32_t r, uint32_t g, uint32_t b, uint32_t a)
{
	return	(a & 255U) << 24 | (b & 255U) << 16 | (g & 255U) << 8 | (r & 255U);
}



uint32_t packRgba(float r, float g, float b, float a)
{
	return toFixed(a, 255) << 24 |
		toFixed(b, 255) << 16 |
		toFixed(g, 255) << 8 |
		toFixed(r, 255);
}



uint32_t packRgba(const chag::float3 rgb, float a)
{
	return packRgba(rgb.x, rgb.y, rgb.z, a);
}



uint32_t packRgba(const chag::float4 &rgb)
{
	return packRgba(rgb.x, rgb.y, rgb.z, rgb.w);
}


void packRgba(uint8_t *c, float r, float g, float b, float a)
{
	c[0] = toFixed(a, 255);
	c[1] = toFixed(b, 255);
	c[2] = toFixed(g, 255);
	c[3] = toFixed(r, 255);
}



struct DynamicTextureDisplayThing
{
	int2 size;
	GLuint textureId;

	DynamicTextureDisplayThing() : size(make_vector(0, 0)), textureId(0) {}
	/**
	 * Uploads and renders the texture using old school openGL commands... re-creates the texture if the size has changed.
	 */
	// uint32_t is assumed to mean rgba
	void upload(const uint32_t *data)
	{
		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, size.x, size.y, GL_RGBA, GL_UNSIGNED_BYTE, data);
	}
	// uint8_t is assumed to mean bgra
	void upload(const uint8_t *data)
	{
		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, size.x, size.y, GL_BGRA, GL_UNSIGNED_BYTE, data);
	}

	template <typename IMAGE_DATA_T>
	void draw(const float4x4 &transform, const int2 &newSize, const IMAGE_DATA_T *data, const int2 &viewportSize, bool flipX = false)
	{
		if (size.x != newSize.x || size.y != newSize.y)
		{
			size = newSize;
			CHECK_GL_ERROR();
			if (textureId)
			{
				glDeleteTextures(1, &textureId);
				textureId = 0;
			}
			CHECK_GL_ERROR();
			glGenTextures(1, &textureId);
			glBindTexture(GL_TEXTURE_2D, textureId);
			glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGB8, size.x, size.y);
			CHECK_GL_ERROR();
			//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, sw, sh, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
			CHECK_GL_ERROR();
		}
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, textureId);

		upload(data);

		CHECK_GL_ERROR();
		glEnable(GL_TEXTURE_2D);
		glDisable(GL_BLEND);

		float scale = std::min(float(viewportSize.x) / float(size.x), float(viewportSize.y) / float(size.y));

		g_texturedQuadShader->begin();
		g_texturedQuadShader->setUniform("transform", transform);
		g_texturedQuadShader->setUniform("flipX", flipX);
		g_texturedQuadShader->setUniform("tex", 0);
		g_primitiveRenderer->drawQuad(make_vector(0.0f, 0.0f, 0.0f), make_vector(float(size.x), float(size.y), 0.0f) * scale);
		g_texturedQuadShader->end();
		glBindTexture(GL_TEXTURE_2D, 0);
	}
};

DynamicTextureDisplayThing g_depthVisTexture;
DynamicTextureDisplayThing g_colorVisTexture;

template <typename T>
struct Sampler2D
{
	Sampler2D(const T *data, int w, int h) : m_data(data), m_w(w), m_h(h) {} 

	const T &operator()(int x, int y) const
	{
		assert(x >= 0);
		assert(y >= 0);
		assert(x < m_w);
		assert(y < m_h);

		int ind = y * m_w + x;
		return m_data[ind];
	}

	const T *m_data;
	int m_w;
	int m_h;
};


static void saveCurrentCameraLocation()
{
	CameraPoints::Snap s;
	s.fwd = g_camera.getDirection();
	s.up = g_camera.getUp();
	s.pos = g_camera.getPosition();
	s.alternativeControls = g_camera.getAlternativeControls();
	s.moveVel = g_camera.getMoveVel();
	s.moveVelMult = g_camera.getMoveVelMult();

	g_cameraPoints.addSnap(s);
	g_cameraPoints.save();
}



static void useCurrentCameraLocation(float transitionTime = 0.0f)
{
	if (!g_cameraPoints.empty())
	{
		CameraPoints::Snap s = g_cameraPoints.getCurrentSnap();
		g_camera.setTransform(make_matrix(make_matrix(cross(s.up, s.fwd), s.up, s.fwd), s.pos), transitionTime);
	}
}


template <typename T>
std::vector<T> normalize(const std::vector<T> &src)
{
	std::vector<T> res(src.size());
	const T maxVal = *std::max_element(src.begin(), src.end());
	for (size_t i = 0; i < src.size(); ++i)
	{
		res[i] = src[i] / maxVal;
	}
	return res;
}


template <typename IND_T>
inline float3 getPosition(IND_T _i, IND_T _j, float depth, FrameData &frame)
{
	int i = int(_i);
	int j = int(_j);

	const float ninf = -std::numeric_limits<float>::infinity();

	if (i >= frame.depthWidth || i < 0 || j >= frame.depthHeight || j < 0)
	{
		return make_vector(ninf, ninf, ninf);
	}

	int ind = j * frame.depthWidth + i;
	chag::float2 trans = frame.depthToCameraSpaceCoeffs[ind];
	if (isfinite(trans.x) && isfinite(trans.y))
	{
		// scale to metres and camera space...
		return chag::make_vector(trans.x * depth, trans.y * depth, depth) * (1.0f / 1000.0f);
	}

	return make_vector(ninf, ninf, ninf);
}


template <typename IND_T>
inline float3 getPosition(IND_T _i, IND_T _j, FrameData &frame, const float defaultValue = -std::numeric_limits<float>::infinity())
{
	int i = int(_i);
	int j = int(_j);

	if (i >= frame.depthWidth || i < 0 || j >= frame.depthHeight || j < 0)
	{
		return make_vector(defaultValue, defaultValue, defaultValue);
	}

	int ind = j * frame.depthWidth + i;
	uint16_t depthData = frame.depthData[ind];
	float depth = float(frame.depthData[ind]);
	chag::float2 trans = frame.depthToCameraSpaceCoeffs[ind];
	if (depthData > 0 && isfinite(trans.x) && isfinite(trans.y))
	{
		// scale to metres and camera space...
		return chag::make_vector(trans.x * depth, trans.y * depth, depth) * (1.0f / 1000.0f);
	}

	return make_vector(defaultValue, defaultValue, defaultValue);
}



template <typename IND_T>
inline float3 getPosition2(IND_T _i, IND_T _j, const FrameData &frame, const float defaultValue = -std::numeric_limits<float>::infinity())
{
	int i = int(_i);
	int j = int(_j);

	int ind = j * frame.depthWidth + i;
	uint16_t depthData = frame.depthData[ind];
	float depth = float(depthData);
	chag::float2 trans = frame.depthToCameraSpaceCoeffs[ind];
	if (depthData > 0 && trans.x != -std::numeric_limits<float>::infinity())//&& isfinite(trans.x) && isfinite(trans.y))
	{
		// scale to metres and camera space...
		return chag::make_vector(trans.x * depth, trans.y * depth, depth) * (1.0f / 1000.0f);
	}

	return make_vector(defaultValue, defaultValue, defaultValue);
}

inline float3 getPosition(const float3 &p, FrameData &frame)
{
	return getPosition(int(p.x), int(p.y), frame);
}


// Queue of function invocations that are processed on the tracking thread side of things. The intended use is by adding lambdas, with which one has to be super-careful about using 
// references within... The lambdas / functions are executed within the tracking thread context (which can then result in async access to the main thread if care is not taken.
using TrackingThreadCmd = std::function<void(void)>;

static BlockingQueue<TrackingThreadCmd> g_tackingThreadCommandQueue;

template <typename FN_T>
void postTrackingThreadCmd(FN_T fn)
{
	g_tackingThreadCommandQueue.push_back(TrackingThreadCmd(fn));
}

template <typename FN_T>
auto postTrackingThreadTask(FN_T taskFn)
{
	using RT = decltype(taskFn());
	// 1. wrap in packaged task, so we can get a future, allocate on heap so it doesn't get moved around as it is captured by the lambda...
	auto taskPtr = std::shared_ptr<std::packaged_task<RT()>>(new std::packaged_task<RT()>(taskFn));
	// 2. get the future so we can return it.
	auto result = taskPtr->get_future();
	// 3. queue up a void(void) lambda that exectures the task (in the context of the tracking thread)
#if USE_TRACKING_THREAD
	postTrackingThreadCmd([=]()
	{
		//auto t = std::move(task);
		// execute the task
		(*taskPtr)();
	});
#else // !USE_TRACKING_THREAD
	// if not threaded execute at once, to avoid deadlocks if the caller waits before the command is processed.
	(*taskPtr)();
#endif // USE_TRACKING_THREAD
	// Finally, return the future, such that it can be waited on.
	return result;
}

struct TrackingThreadResult
{
	RawFrameData frame;

	struct ObjectState
	{
		std::string id;
		chag::float4x4 objectToWorldTfm;
		TemplateLibrary::Template templ;
	};

	std::vector<ObjectState> objectStates;

	std::vector<uint8_t> profilerCommands;
};

static BlockingQueue<TrackingThreadResult> g_trackingResult;

#if USE_TRACKING_THREAD
std::thread g_trackingThread;
std::atomic<bool> g_exitTracking = false;
#endif // USE_TRACKING_THREAD


inline float evalPlaneEq(const float4 &planeEq, float x, float y)
{
	float planeDepth = -(planeEq.x * x + planeEq.y * y + planeEq.w) / planeEq.z;
	return planeDepth;
}

DataLogger *g_logger = nullptr;


void loadAndSetupTrackedObjectTypes()
{
	DataLoggerScopeHelper dlsh(g_logger, "TrackedObjectTypes");

	boost::filesystem::path templateLibsPath(g_envRootPath / "template_libs");

	// 1. get previous transforms of tracked objects.
	std::map<std::string, float4x4> prevTransforms;
	for (auto it = g_tracker->begin(); it != g_tracker->end(); ++it)
	{
		std::string name = (*it).first;
		TemplateTracker::TrackedObjectPtr o = (*it).second;
		prevTransforms[name] = o->getObjectToWorldTfm();
	}
	g_tracker->clearObjects();

	g_objectTypes = loadTrackedObjectTypes(g_envRootPath / "tracked_object_types.json");
	g_logger->write("tracked_object_types_path", "tracked_object_types.json");
	// load all template libraries
	// TODO: load on demand probably
	for (auto t : g_objectTypes)
	{
		if (g_templateLibs.count(t.second.templateLibraryName) == 0)
		{
			TemplateLibraryPtr lib(new TemplateLibrary);
			lib->load(templateLibsPath, t.second.templateLibraryName);
			g_templateLibs[t.second.templateLibraryName] = lib;
		}
	}

	// option 1: load  at startup
	g_objectInstances = loadTrackedObjectInstances((g_trackedObjectsSetupsPath / g_trackedObjectsSetup.value()).string());
	g_logger->write("tracked_objects_setup", g_trackedObjectsSetupsSubPath + "/" + g_trackedObjectsSetup.value());

	if (std::find_if(g_objectInstances.begin(), g_objectInstances.end(), [&](const auto &inst) { return inst.name == g_selectedObjectName.value(); }) == g_objectInstances.end() && !g_objectInstances.empty())
	{
		g_selectedObjectName = g_objectInstances[0].name;
	}
	// create tracked objects for these
	for (auto i : g_objectInstances)
	{
		if (g_objectTypes.count(i.typeId) == 0)
		{
			fprintf(stderr, "Invalid tracked object type id '%s' in object def '%s' from file '%s'!", i.typeId.c_str(), i.name.c_str(), g_trackedObjectsSetup.value().c_str());
		}
		assert(g_objectTypes.count(i.typeId) != 0);

		const TrackedObjectType &t = g_objectTypes[i.typeId];
		TemplateLibraryPtr lib = g_templateLibs[t.templateLibraryName];

		std::vector<TemplateTracker::TrackedMultiTemplateObject::DiscTemplate> templates;
		// Insert the default ellipsoid if empty, it is flagged as a default and not added to the library itself.
		if (lib->templates.empty())
		{
			templates.push_back(TemplateTracker::TrackedMultiTemplateObject::generateEllipsoidTemplate(chag::make_vector(g_defaultEllipsoidTemplateRadius * 1.0f, g_defaultEllipsoidTemplateRadius * 0.35f, g_defaultEllipsoidTemplateRadius * 0.75f), 2));
		}

		for (const auto &st : lib->templates)
		{
			TemplateTracker::TrackedMultiTemplateObject::DiscTemplate dt;
			dt.isDefaultPose = false;
			dt.m_sampleDiscs.resize(0);
			dt.name = st.poseName;

			float averageConeAngle = acosf(1.0f - 1.0f / float(st.discs.size()));

			for (auto sd : st.discs)
			{
				TemplateTracker::TrackedMultiTemplateObject::TemplateDisc td;
				td.radius = sd.radius;
				td.direction = sd.normal;
				td.position = sd.position;
				td.coneAngle = averageConeAngle;
				td.offset = td.radius  / tanf(averageConeAngle);
				dt.m_sampleDiscs.push_back(td);
			}

			dt.m_aabb = make_aabb(int(dt.m_sampleDiscs.size()), [&](int i) { return dt.m_sampleDiscs[i].position; });
			templates.push_back(dt);
		}

		g_tracker->addObject(TemplateTracker::TrackedObjectPtr(new TemplateTracker::TrackedMultiTemplateObject(i.name, t.objectClass, templates, prevTransforms.count(i.name) ? prevTransforms[i.name] : make_identity<float4x4>(), lib)));

		g_modelColours[i.name] = type_conversions::f4(i.displayColour, 1.0f);
	}
}

/**
 * This is the main thread function that drives the tracking loop, i.e., reading from the input source and pushing updates to the
 * output thingo.
 * If USE_TRACKING_THREAD is one, then this is its own thread, otherwise it is called from the rendering function.
 */
void trackingThreadFunc()
{
	try
	{
		DebugRenderer::instance().setThreadId("tracking");

		// NOTE!! static to retain data between calls
		static RawFrameData frame;
#if USE_TRACKING_THREAD
		for (;;)
#endif // USE_TRACKING_THREAD
		{

			{
				PROFILE_SCOPE("TrackingFrame", TT_Cpu);
				// note: we'll check this further down also, since we may have been asleep at the input source.
#if USE_TRACKING_THREAD
				if (g_exitTracking)
				{
					return;
				}
#endif // USE_TRACKING_THREAD

				{
					PROFILE_SCOPE("Commands", TT_Cpu);
					// consume all commands sent since last iteration...
					TrackingThreadCmd cmd;
					while (g_tackingThreadCommandQueue.pop_front_no_wait(cmd))
					{
						cmd();
					}
				}
				// TODO: move to using commands.
				// Check for template library modifications
				// re-init templates..
				{
					PROFILE_SCOPE("TemplateReinit", TT_Cpu);

					for (auto oi : *g_tracker)
					{
						if (TemplateTracker::TrackedMultiTemplateObject *tmuo = dynamic_cast<TemplateTracker::TrackedMultiTemplateObject*>(oi.second.get()))
						{
							if (tmuo->m_templateLibrary && tmuo->m_templateLibrary->m_modified)
							{
								std::vector<TemplateTracker::TrackedMultiTemplateObject::DiscTemplate> templates;
								// keep default poses.
								for (const auto &ot : tmuo->m_templates)
								{
									if (ot.isDefaultPose)
									{
										templates.push_back(ot);
									}
								}
								// copy everything from library.
								for (const auto &st : tmuo->m_templateLibrary->templates)
								{
									TemplateTracker::TrackedMultiTemplateObject::DiscTemplate dt;
									dt.isDefaultPose = false;
									dt.m_sampleDiscs.resize(0);
									dt.name = st.poseName;

									float averageConeAngle = acosf(1.0f - 1.0f / float(st.discs.size()));

									for (auto sd : st.discs)
									{
										TemplateTracker::TrackedMultiTemplateObject::TemplateDisc td;
										td.radius = sd.radius;
										td.direction = sd.normal;
										td.position = sd.position;
										td.coneAngle = averageConeAngle;
										td.offset = td.radius  / tanf(averageConeAngle);
										dt.m_sampleDiscs.push_back(td);
									}

									dt.m_aabb = make_aabb(int(dt.m_sampleDiscs.size()), [&](int i) { return dt.m_sampleDiscs[i].position; });
									templates.push_back(dt);
								}
								tmuo->m_templates = templates;
							}
						}
					}
					// clear in a separate pass as more than one object can use the same library
					for (auto oi : *g_tracker)
					{
						if (TemplateTracker::TrackedMultiTemplateObject *tmuo = dynamic_cast<TemplateTracker::TrackedMultiTemplateObject*>(oi.second.get()))
						{
							if (tmuo->m_templateLibrary)
							{
								tmuo->m_templateLibrary->m_modified = false;
							}
						}
					}
				}



				bool wasReset = false;

				if (g_pauseFrameCount >= 0)
				{
					g_paused = false;
					if (g_pauseFrameCount == 0)
					{
						g_paused = true;
					}

					g_pauseFrameCount -= 1;
				}

				if (!g_paused || g_frameIndexSet)
				{
					// Always reset to false
					g_frameIndexSet = false;
					PROFILE_SCOPE("FrameInput", TT_Cpu);
					//
					if (!g_source->readFrameRaw(frame))
					{
#if USE_TRACKING_THREAD
						if (g_exitTracking || g_source->isStopped())
						{
							return;
						}
#endif// USE_TRACKING_THREAD
						wasReset = true;
						g_source->reset();

						// failed again, probably broken somehow
						if (!g_source->readFrameRaw(frame))
						{
#if USE_TRACKING_THREAD
							if (g_exitTracking || g_source->isStopped())
							{
								return;
							}
#endif// USE_TRACKING_THREAD
							printf("repeated read failure! Exiting.\n");
							assert(false);
							exit(1);
						}
					}

					if (wasReset && DYNAMIC_TWEAKABLEVAR_UI(bool, "auto_pause_on_wrap", false, Toggle()))
					{
						g_paused = true;
					}

					// File streaming output is handled here,
					if (g_dumpToFile && !g_source->isOutStreamOpen())
					{
						bfs::path fileName = g_envRootPath / g_recordingsOutputSubPath / (g_outputTagBase + "_" + getTimeStampStr() + ".bdsx");
						g_source->openOutStream(fileName.string());
					}
					if (!g_dumpToFile && g_source->isOutStreamOpen())
					{
						g_source->closeOutStream();
					}
				}
				else
				{
#if USE_TRACKING_THREAD
					// Bit of a hack to avoid running wild when paused, more neat solutions can absolutely be found
					//std::this_thread::sleep_for(std::chrono::milliseconds(10));
#endif // USE_TRACKING_THREAD
				}

				if (!frame.depthData.empty())
				{
					PROFILE_SCOPE("tracking", TT_Cpu);
					{
						PROFILE_SCOPE("g_tracker->update", TT_Cpu);

						TemplateTracker::Params params;
						params.distanceWeightRangeScale = DYNAMIC_TWEAKABLEVAR_UI(float, "distanceWeightRangeScale", 0.325f, Slider(0.0f, 2.0f));

						params.trackingIterations = DYNAMIC_TWEAKABLEVAR_UI(int, "tracking_iterations", 20, Slider(1, 128));
						// Ensure fully constrained
						if (g_poseCaptureMode)
						{
							params.constrainTransform = true;
							params.minMaxRotXZ = 0.0f;
							params.minMaxTransZ = 0.0f;
							// up the tracking iterations to ensure stable convergence...
							params.trackingIterations = std::max(40, params.trackingIterations);
						}
						else
						{
							params.constrainTransform = g_constrainTfm;
							// Allow rotation to deviate by acos(0.1) from the floor plane I think, but it's probably slightly off anyway
							params.minMaxRotXZ = DYNAMIC_TWEAKABLEVAR_UI(float, "minMaxRotXZ", 0.1f, Slider(0.0f, 0.9f));
							// Allow translation to deviate by 5% from the floor
							params.minMaxTransZ = DYNAMIC_TWEAKABLEVAR_UI(float, "minMaxTransZ", 0.05f, Slider(0.0f, 2.0f)); ;
						}
						params.arenaRadius = g_arenaRadius;
						params.arenaToWorldTfm = g_arenaToWorldTfm;
						params.worldToArenaTfm = g_worldToArenaTfm;

						// TODO! params.depthClusterThreshold = tracker->m_depthClusterThreshold;
						params.depthClusterThreshold = g_depthClusterThreshold;
						params.normalsDepthThreshold = g_depthClusterThreshold.value() * 50.0f;
						params.hClusterSubDivs = DYNAMIC_TWEAKABLEVAR_UI(int, "hClusterSubDivs", 6, Slider(2, 32));

						// TODO: only do setDepthToCameraCoeffs when actually needed (i.e., at start or if re-opening source)
						// NOTE: this was done here because the coefficients are not neccessarily available before we have read the first frame
						g_tracker->setDepthToCameraCoeffs(g_source->getDepthToCameraSpaceCoeffs(), false);
						g_tracker->update(frame, params, g_source->getCurrentFrame());
						g_currentFrame = g_source->getCurrentFrame();
					}
				}

				// send data to client
				// NOTE: We assume only the tracking thread will send data to the client, so no locks are needed
				{
					PROFILE_SCOPE("ClientOutput", TT_Cpu);

					std::vector<ClientOutput::Location> ls;
					for (auto oi : *g_tracker)
					{
						TemplateTracker::TrackedObjectPtr o = oi.second;
						std::string name = oi.first;
						
						if (o->isActive())
						{
							ClientOutput::Location l;
							memset(&l, 0, sizeof(l));
							strcpy(l.id, name.c_str());
							strcpy(l.objectClass, o->getObjectClass().c_str());

							float4x4 objectToArenaTfm = g_worldToArenaTfm * o->getObjectToWorldTfm();

							l.x = objectToArenaTfm.getTranslation().x;
							l.y = objectToArenaTfm.getTranslation().y;
							l.z = objectToArenaTfm.getTranslation().z;
							// We use the xy orientation as rotation from the x axis in radians
							l.orientationAngle = atan2f(objectToArenaTfm.getXAxis().y, objectToArenaTfm.getXAxis().x);

							if (TemplateTracker::TrackedMultiTemplateObject *tmuo = dynamic_cast<TemplateTracker::TrackedMultiTemplateObject*>(o.get()))
							{
								if (tmuo->m_prevBestPose >= 0)
								{
									const TemplateTracker::TrackedMultiTemplateObject::DiscTemplate &t = tmuo->m_templates[tmuo->m_prevBestPose];

									if (TemplateLibrary::Template *lt = tmuo->m_templateLibrary ? tmuo->m_templateLibrary->find(t.name) : nullptr)
									{
										int numAnnotations = 0;
										for (const auto &a : lt->annotations)
										{
											ClientOutput::Annoation oa;
											// transform position and direction of annotation to arena space
											float3 ap = transformPoint(objectToArenaTfm, a.position);
											float3 ad = transformDirection(objectToArenaTfm, a.direction);

											oa.x = ap.x;
											oa.y = ap.y;
											oa.z = ap.z;
											// We use the xy orientation as rotation from the x axis in radians
											oa.orientationAngle = atan2f(ad.y, ad.x);
											strcpy(oa.id, a.id.c_str());
											l.annotations[numAnnotations++] = oa;

											if (numAnnotations >= ClientOutput::s_maxAnnotaions)
											{
												break;
											}
										}
									}
								}
							}
							ls.push_back(l);
						}
					}
					if (!ls.empty())
					{
						g_output->send(ls);
					}
					if (!g_paused)
					{
						PROFILE_SCOPE("Logging", TT_Cpu);

						// Only output if any are active.
						bool anyActive = false;
						for (auto oi : *g_tracker)
						{
							anyActive = anyActive || oi.second->isActive();
						}
						if (anyActive)
						{
							DataLoggerScopeHelper lsh(g_logger, "tracking_frame");
							g_logger->write("source_frame_number", g_source->getCurrentFrame());
							g_logger->write("time_stamp", g_timer.getElapsedTime());

							// log to logger
							g_logger->begin("tracked_objects");
							for (auto oi : *g_tracker)
							{
								TemplateTracker::TrackedObjectPtr o = oi.second;
								if (o->isActive())
								{
									g_logger->begin("object");
									g_logger->write("name", oi.first);
									o->outputLogInfo(*g_logger);
									g_logger->end();
								}
							}
							g_logger->end();
						}
					}
				}
			}
			// at this point, we publish the info for use in the rendering too (when threaded)
			{
				TrackingThreadResult tr;
				tr.frame = frame;
				for (auto it = g_tracker->begin(); it != g_tracker->end(); ++it)
				{
					TemplateTracker::TrackedObjectPtr o = (*it).second;
					TrackingThreadResult::ObjectState os;
					os.id = (*it).first;

					if (o->isActive())
					{
						os.objectToWorldTfm = o->getObjectToWorldTfm();

						if (TemplateTracker::TrackedMultiTemplateObject *tmuo = dynamic_cast<TemplateTracker::TrackedMultiTemplateObject*>(o.get()))
						{
							if (tmuo->m_prevBestPose >= 0)
							{
								const TemplateTracker::TrackedMultiTemplateObject::DiscTemplate &t = tmuo->m_templates[tmuo->m_prevBestPose];

								if (TemplateLibrary::Template *lt = tmuo->m_templateLibrary->find(t.name))
								{
									os.templ = *lt;
								}
								else
								{
									os.templ.poseName = t.name;
									for (const auto &d : t.m_sampleDiscs)
									{
										os.templ.discs.push_back(TemplateLibrary::Disc{ d.position, d.direction, d.radius });
									}
								}
							}
						}

						tr.objectStates.push_back(os);
					}
				}

#if USE_TRACKING_THREAD
				tr.profilerCommands = Profiler::instance().getCommandBuffer();
				Profiler::instance().clearCommandBuffer();
#endif // USE_TRACKING_THREAD
				g_trackingResult.push_back(tr);
			}
		}
	}
	catch (...)
	{
		std::clog << "TRACKING THREAD EXCEPTION: " << boost::current_exception_diagnostic_information() << std::endl;
		std::clog.flush();
	}
}




std::vector<double> gatherValues(const std::string &idStr, Profiler &prof)
{
	std::vector<double> res;

	uint32_t id = prof.registerToken(idStr.c_str());

	double startTime = -DBL_MAX;
	for (auto n : prof)
	{
		switch (n.m_id)
		{
		case Profiler::CID_Message:
			break;
		case Profiler::CID_Counter:
			break;
		case Profiler::CID_BeginBlock:
		{
			if (id == n.m_tokenId)
			{
				startTime = n.m_timeStamp;
			}
		}
		break;
		case Profiler::CID_EndBlock:
			if (id == n.m_tokenId)
			{
				res.push_back(n.m_timeStamp - startTime);
				startTime = -DBL_MAX;
			}
			break;
		case Profiler::CID_OneOff:
			break;
		};
	}
	return res;
}

double gatherMedian(const std::string &id, Profiler &prof)
{
	std::vector<double> res = gatherValues(id, prof);
	if (!res.empty())
	{
		std::sort(res.begin(), res.end());
		if (res.size() % 2)
		{
			return res[res.size() / 2];
		}
		else
		{
			return res[res.size() / 2] + res[res.size() / 2 + 1];
		}
	}
	return -1.0;
}

double gatherSum(const std::string &id, Profiler &prof)
{
	std::vector<double> res = gatherValues(id, prof);
	return std::accumulate(res.begin(), res.end(), 0.0);
}

static std::string getVarGroup(const std::string &varName)
{
	size_t firstSlash = varName.find_first_of('/');
	return std::string::npos != firstSlash ? varName.substr(0, firstSlash) : std::string();
}


static std::string stripGroupId(const std::string &varName)
{
	size_t firstSlash = varName.find_first_of('/');
	return std::string::npos != firstSlash ? varName.substr(firstSlash + 1) : varName;
}

bool drawTwVar2c(const std::string &label, tv::TweakVarUiBase *twr)
{
	ImGui::PushID(twr); 
	ImGui::AlignTextToFramePadding();
	ImGui::PushStyleColor(ImGuiCol_HeaderHovered, (ImVec4)ImColor(0.2f, 0.2f, 0.3f, 1.0f));
	ImGui::Selectable(label.c_str(), false, ImGuiSelectableFlags_SpanAllColumns);
	ImGui::PopStyleColor();
	ImGui::SetItemAllowOverlap();

	bool hovered = ImGui::IsItemHovered();

	ImGui::NextColumn();
	ImGui::PushItemWidth(-1);
	twr->render();
	ImGui::PopItemWidth();
	ImGui::NextColumn();
	ImGui::PopID();

	return hovered;
}

bool beginTwVarGroup(const std::string &groupId)
{
	ImGui::Columns(1);
	bool asd = ImGui::CollapsingHeader(groupId.c_str(), ImGuiTreeNodeFlags_DefaultOpen);
	ImGui::Columns(2);
	return asd;
	//ImGui::AlignTextToFramePadding();  // Text and Tree nodes are less high than regular widgets, here we add vertical spacing to make the tree lines equal high.
	//bool node_open = ImGui::TreeNodeEx(groupId.c_str(), ImGuiTreeNodeFlags_SpanAllColumns | ImGuiTreeNodeFlags_DefaultOpen);
	//ImGui::NextColumn();
	//ImGui::AlignTextToFramePadding();
	//ImGui::NextColumn();
	//return node_open;
}

void endTwVarGroup()
{
	//ImGui::TreePop();
}

using TweakVarUiMap = std::map<std::string, tv::TweakVarUiBase *>;

static bool drawTwVar2c(const std::string &varId, TweakVarUiMap &uiMap, std::string *infoText)
{
	if (uiMap.count(varId))
	{
		tv::TweakVarUiBase *var = uiMap[varId];
		if (drawTwVar2c(varId, var))
		{
			if (infoText)
			{
				*infoText = var->getInfo();
			}
			return true;
		}
	}
	return false;
}

void drawTweakVarGui()
{
	if (g_showInfo)
	{
		ImGui::SetNextWindowSize(ImVec2(430, 450), ImGuiCond_FirstUseEver);
		if (!ImGui::Begin("Tweak variables", &g_showInfo))
		{
			ImGui::End();
			return;
		}

		// 1. bin by top-level path
		std::map<std::string, std::vector<tv::TweakVarUiBase *>> uisBygroup;
		tv::VariableManager::UiList uis = tv::VariableManager::get().getUis();
		for (auto twr : uis)
		{
			std::string groupId = getVarGroup(twr->getId());
			if (groupId.empty())
			{
				groupId = "default";
			}
			uisBygroup[groupId].push_back(twr);
		}

		ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(2, 0));
		ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(1, 2));
		ImGui::Columns(2);
		//ImGui::Separator();

		std::string text = "";
		for (const auto &groupUis : uisBygroup)
		{

			ImGui::AlignTextToFramePadding();  // Text and Tree nodes are less high than regular widgets, here we add vertical spacing to make the tree lines equal high.
			if (beginTwVarGroup(groupUis.first))
			{
				//for (auto twr : tv::VariableManager::get().getUis())
				for (auto twr : groupUis.second)
				{
					if (drawTwVar2c(stripGroupId(twr->getId()), twr))
					{
						text = twr->getInfo();
						//ImGui::SetTooltip(text.c_str());
					}
				}
				endTwVarGroup();
			}
		}

		ImGui::Columns(1);
		for (auto twr : g_tweakVarButtons)
		{
			ImGui::PushID(twr); // Use field index as identifier.
			ImGui::PushItemWidth(-1);
			twr->render();
			if (ImGui::IsItemHovered())
			{
				text = twr->getInfo();
				//ImGui::SetTooltip(text.c_str());
			}
			ImGui::PopID();
		}
		ImGui::Separator();
		ImGui::PushItemWidth(-1);
		ImGui::TextWrapped(text.c_str());
		ImGui::PopItemWidth();
		//ImGui::Separator();
		ImGui::PopStyleVar(2);

		ImGui::End();
	}
}


std::vector<std::string> getFilenameList(const bfs::path &path, const std::string &ext = std::string())
{
	std::vector<std::string> res;

	if (boost::filesystem::is_directory(path))
	{
		for (auto & p : bfs::directory_iterator(path))
		{
			if (ext.empty() || boost::algorithm::to_lower_copy(p.path().extension().string()) == boost::algorithm::to_lower_copy(ext))
			{
				res.push_back(p.path().filename().string());
			}
		}
	}
	return res;
}

void loadArenaSetup(const bfs::path &name, bool useAsyncSetting = true);
void saveArenaSetup(const bfs::path &name);


// here we store the most recent data from the tracking thread.
static TrackingThreadResult g_mainThreadTrackerState;


template <typename OK_CB_FN>
static void showArenaSetupSaveAsPopup(std::string &tmpArenaSetupName, OK_CB_FN okCb)
{
	if (ImGui::BeginPopup("ArenaNameBox"))
	{
		tmpArenaSetupName = ImGuiX::InputText(tmpArenaSetupName, [&](const std::string &s)
		{
			// string level callback that appends .json to the string (sort of works...).
			bfs::path ap(s);
			if (boost::algorithm::to_lower_copy(ap.extension().string()) != ".json")
			{
				ap.replace_extension(".json");
			}
			return ap.string();
		},
			// character level callback that blocks any non-filename character.
			[&](ImWchar ch)
		{
			char asd[2] = { char(ch), 0 };
			return boost::filesystem::portable_name(asd) ? 0 : 1;
		},
			"Arena Setup Name");

		if (ImGui::Button("Ok"))
		{
			g_arenaSetupName = tmpArenaSetupName;
			saveArenaSetup(g_arenaSetupsPath / g_arenaSetupName.value());
			ImGui::CloseCurrentPopup();
			okCb();
		}

		if (ImGui::Button("Cancel"))
		{
			ImGui::CloseCurrentPopup();
		}

		ImGui::EndPopup();
	}
}
// helper to turn on debug rendering.
static void setDebugUiChildrenVisible(const std::string &path, bool showSubItems = true);

void drawMainUi(TweakVarUiMap &twUis)
{
	if (g_showMainUi)
	{
		ImGui::SetNextWindowSize(ImVec2(430, 450), ImGuiCond_FirstUseEver);
		if (!ImGui::Begin("Main UI", &g_showMainUi))
		{
			ImGui::End();
			return;
		}

		ImGuiX::PropertyListBegin();

		std::string infoText;
		if (beginTwVarGroup("Recording"))
		{
			drawTwVar2c("dumpToFile", twUis, &infoText);
			endTwVarGroup();
		}
		namespace bfs = boost::filesystem;
		if (beginTwVarGroup("Arena Setup"))
		{
			if (ImGuiX::Property("Load", g_arenaSetupName, getFilenameList(g_arenaSetupsPath, ".json")))
			{
				loadArenaSetup(bfs::path(g_arenaSetupsPath).append(g_arenaSetupName.value()).string());
			}

			//
			static std::string tmpArenaSetupName;
			if (ImGuiX::PropertyButton("Save As", "Save the current arena setup under a new name and update selection in current setup.", length3(g_arenaPlaneEq) > 0.9f))
			{
				tmpArenaSetupName = std::string(g_arenaSetupName);
				ImGui::OpenPopup("ArenaNameBox");
			}
			showArenaSetupSaveAsPopup(tmpArenaSetupName, []() {});
			if (ImGuiX::PropertyButton("Create/Modify", "Create a new arena setup by selecting the arena floor in the 2D view."))
			{
				g_setupArena = ASC_WaitingForClicks;
				if (g_viewMode == VM_3D)
				{
					g_viewMode = VM_Both;
				}
				//setDebugUiChildrenVisible("/main/Arena");
				//setDebugUiChildrenVisible("/main/ArenaSetup");
			}
			endTwVarGroup();
		}
		if (beginTwVarGroup("Display"))
		{
			drawTwVar2c("showRgb", twUis, &infoText);
			drawTwVar2c("viewMode", twUis, &infoText);

			endTwVarGroup();
		}
		if (beginTwVarGroup("Tracked Objects"))
		{

			if (ImGuiX::Property("Select Objects Setup", g_trackedObjectsSetup, getFilenameList(g_trackedObjectsSetupsPath, ".json")))
			{
				auto f = postTrackingThreadTask([&]()
				{
					// this must be done in the context of the tracking thread,
					loadAndSetupTrackedObjectTypes();
					return true;
				});
				// wait until tracker has reinitialized, which avoids data races on the data types...
				f.wait();
			}

			std::vector<std::string> objectNames;
			//for (const auto &os : g_mainThreadTrackerState.objectStates)
			for (const auto &os : g_objectInstances)
			{
				objectNames.push_back(os.name);
			}
			ImGuiX::Property("Selected Object", g_selectedObjectName, objectNames, "Use Left/Right arrow keys to change.");
			endTwVarGroup();
		}
		if (beginTwVarGroup("Templates"))
		{
			ImGuiX::Property("Enable Pose Capture", g_poseCaptureMode, "When checked, the buton 'Capture Pose' button will capture a template. It also enables constraining the transformation to the arena floor, overriding other settings.");
			if (ImGuiX::PropertyButton("Capture Pose", "Captures the current pose for the currently selected object, and adds it to the objects template library.", g_poseCaptureMode, 'c'))
			{
				// ensure we capture current value... (not really neccecssary...)
				std::string objName = g_selectedObjectName;
				postTrackingThreadCmd([&,objName]()
				{
					if (TemplateTracker::TrackedObjectPtr to = g_tracker->get(objName))
					{
						to->setCaptureNextFrame();
					}
				});
			}

			float radBefore = g_defaultEllipsoidTemplateRadius;
			drawTwVar2c("defaultEllipsoidTemplateRadius", twUis, &infoText);
			if (radBefore != g_defaultEllipsoidTemplateRadius)
			{
				postTrackingThreadCmd([&]()
				{
					for (auto oi : *g_tracker)
					{
						if (TemplateTracker::TrackedMultiTemplateObject *tmuo = dynamic_cast<TemplateTracker::TrackedMultiTemplateObject*>(oi.second.get()))
						{
							auto it = std::find_if(tmuo->m_templates.begin(), tmuo->m_templates.end(), [&](const auto &t) { return t.isDefaultPose && t.name.find("generateEllipsoidTemplate") != std::string::npos; });
							if (it != tmuo->m_templates.end())
							{
								*it = TemplateTracker::TrackedMultiTemplateObject::generateEllipsoidTemplate(chag::make_vector(g_defaultEllipsoidTemplateRadius * 1.0f, g_defaultEllipsoidTemplateRadius * 0.35f, g_defaultEllipsoidTemplateRadius * 0.75f), 2);
							}
						}
					}
				});
			}

			endTwVarGroup();
		}

		ImGuiX::PropertyListEnd(infoText);

		ImGui::End();
	}

	if (g_setupArena != ASC_Idle)
	{
		float height = 50.0f;

		ImGui::SetNextWindowSize(ImVec2(430, -1), ImGuiCond_FirstUseEver);
		if (ImGui::Begin("Arena Setup UI", 0))
		{
			ImGui::PushItemWidth(-1);
			ImGui::TextWrapped("Click three times on the arena floor to set the ground plane");
			ImGui::PopItemWidth();

			if (ImGuiX::Button("Done", g_setupArena != ASC_WaitingForClicks))
			{
				g_setupArena = ASC_Idle;
				// 
			}
			if (ImGuiX::Button("Save", g_setupArena != ASC_WaitingForClicks && !g_arenaSetupName.value().empty() && length3(g_arenaPlaneEq) > 0.9f))
			{
				saveArenaSetup(g_arenaSetupsPath / g_arenaSetupName.value());
				g_setupArena = ASC_Idle;
			}
			static std::string tmpArenaSetupName;
			if (ImGuiX::Button("Save As", g_setupArena != ASC_WaitingForClicks && length3(g_arenaPlaneEq) > 0.9f))
			{
				tmpArenaSetupName = std::string(g_arenaSetupName);
				ImGui::OpenPopup("ArenaNameBox");
				//g_setupArena = ASC_Idle;
				// 
			}
			showArenaSetupSaveAsPopup(tmpArenaSetupName, [&]()->void {
				g_setupArena = ASC_Idle;
			});
			if (ImGuiX::Button("Cancel"))
			{
				g_setupArena = ASC_Idle;
				// 
			}

			std::string infoText;
			ImGuiX::PropertyListBegin();
			drawTwVar2c("onPlaneTolerance", twUis, &infoText);
			drawTwVar2c("minDepth", twUis, &infoText);
			drawTwVar2c("maxDepth", twUis, &infoText);
			drawTwVar2c("arenaRadius", twUis, &infoText);
			ImGuiX::PropertyListEnd(infoText);

			//ImGui::Separator();
			ImGui::End();
		}
	}
}


void updateArenaTransform()
{
	float3 normal = type_conversions::f3(g_arenaPlaneEq);
	g_arenaToWorldTfm = make_matrix_from_zAxis(g_arenaCentre, normal, make_vector(0.0f, 1.0f, 0.0f));
	g_worldToArenaTfm = inverse(g_arenaToWorldTfm);
	if (g_logger)
	{
		g_logger->begin("ArenaTransform");
		g_logger->write("arenaToWorldTfm", g_arenaToWorldTfm);
		g_logger->write("worldToArenaTfm", g_worldToArenaTfm);
		g_logger->end();
	}
}


void drawDebugRendererUiAndObjects(const float4x4 & viewMatrix, const float4x4 &projectionMatrix);
void loadDebugRendererState(const bfs::path &optionsDir);
void saveDebugRendererState(const bfs::path &optionsDir);

#if USE_TRACKING_THREAD
// Note, not thread safe as this is used to pass these within the main thread...
static std::vector<std::vector<uint8_t>> g_trackerProfilerCommands;
#endif // USE_TRACKING_THREAD


static void renderTrackingFrame(int width, int height)
{
	PROFILE_SCOPE("Tracking", TT_Cpu);

	glDisable(GL_SCISSOR_TEST);
	glViewport(0, 0, g_width, g_height);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	//glDepthMask(GL_FALSE);
	//glEnable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);
	glBlendFunc(GL_ONE, GL_ONE);

	//glEnable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);
	CHECK_GL_ERROR();
	//
	//PROFILE_BEGIN_BLOCK("Tracking", TT_Cpu);

	while (g_trackingResult.pop_front_no_wait(g_mainThreadTrackerState))
	{
#if USE_TRACKING_THREAD
		g_trackerProfilerCommands.push_back(g_mainThreadTrackerState.profilerCommands);
#endif // USE_TRACKING_THREAD
	}

	if (!g_mainThreadTrackerState.frame.depthData.empty())
	{
		PROFILE_SCOPE("Rendering", TT_Cpu);

		FrameData frame;
		static_cast<RawFrameData&>(frame) = g_mainThreadTrackerState.frame;
		frame.depthToCameraSpaceCoeffs = g_source->getDepthToCameraSpaceCoeffs();
		frame.generatePoints();

		int sw = frame.depthWidth;
		int sh = frame.depthHeight;
		float aspectRatio = float(sw) / float(sh);
		float scale = std::min(float(g_width) / float(sw), float(g_height) / float(sh));
		int lw = int(scale * float(sw));
		glViewport(0, 0, lw, int(scale * float(sh)));


		float2 imageSpaceMousePos = make_vector<float>(g_mousePos) / scale;
		if (g_source->isXImageAxisFlipped())
		{
			imageSpaceMousePos.x = float(sw) - imageSpaceMousePos.x;
		}
		//float2 imageSpaceMousePos = clamp(make_vector(0.0f, 0.0f), make_vector(float(sw), float(sh)) - make_vector<float>(g_mousePos) / scale, make_vector(float(sw - 1), float(sh - 1)));

		// X is flipped on kinect, flip image space to match

		float4x4 orthoProj = orthoMatrix(0.0f, float(sw), float(sh), 0.0f, -10.0f, 10.0f);
		if (g_source->isXImageAxisFlipped())
		{
			orthoProj = orthoMatrix(float(sw), 0.0, float(sh), 0.0, -10.0, 10.0);
		}


		if (g_showRgb)
		{
#if 0
			//Debug help to visualize image space.
			for (int i = 0; i < frame.colorHeight; ++i)
			{
				memset(&frame.colorData[i * frame.colorWidth * frame.cBytesPerPixel], int(i * 255) / frame.colorHeight, frame.cBytesPerPixel * 8);
			}

			for (int i = 0; i < frame.colorWidth; ++i)
			{
				for (int j = 0; j < 8; ++j)
				{
					memset(&frame.colorData[(i + j * frame.colorWidth) * frame.cBytesPerPixel], int(i * 255) / frame.colorWidth, frame.cBytesPerPixel);
				}
			}
#endif
			g_colorVisTexture.draw(orthoProj, make_vector(frame.colorWidth, frame.colorHeight), safe_ptr(frame.colorData), make_vector(sw, sh));
		}
		else
		{
			// TODO: make this work using a shader to do the heat map etc.
			std::vector<uint32_t> tmpBuf(sw*sh);

			const std::vector<uint16_t> &depthData = frame.depthData;
			{
				PROFILE_SCOPE("depthData_1", TT_Cpu);
				{
					float minDepthMm = g_minDepth * 1000.0f;
					float maxDepthMm = g_maxDepth * 1000.0f;
					for (int j = 0; j < sh; ++j)
					{
						for (int i = 0; i < sw; ++i)
						{
							uint16_t depth = depthData[j * sw + i];
							tmpBuf[j * sw + i] = packRgba(DebugRenderer::heatMap(float(depth), minDepthMm, maxDepthMm));
						}
					}
				}
			}
			g_depthVisTexture.draw(orthoProj, make_vector(sw, sh), safe_ptr(tmpBuf), make_vector(sw, sh));
		}

		int leftOffset = int(g_leftWidthProp * float(g_width));// g_width / 2;// lw - 200;
		if (g_viewMode == VM_2D)
		{
			leftOffset = g_width;
		}
		else if (g_viewMode == VM_3D)
		{
			leftOffset = 0;
		}

		// Transform click coords to image space
		std::vector<int2> rightWindowClickCoords;
		for (auto cp : g_clickCoords)
		{
			if (cp.x < leftOffset)
			{
				float2 imP = make_vector<float>(cp) / scale;
				if (g_source->isXImageAxisFlipped())
				{
					imP.x = float(sw) - imP.x;
				}
				g_imageSpaceClickCoords.push_back(imP);
			}
			else
			{
				rightWindowClickCoords.push_back(cp);
			}
		}
		g_clickCoords.clear();



		std::vector<chag::float3> pointPositions;
		std::vector<float3> pointColours;

		glPointSize(10.0f);
		pointPositions.push_back(make_vector(0.0f, 0.0f, 0.0f));
		pointColours.push_back(make_vector(1.0f, 0.0f, 0.0f));

		pointPositions.push_back(make_vector(float(sw), float(sh), 0.0f));
		pointColours.push_back(make_vector(0.0f, 1.0f, 0.0f));

		for (auto imP : g_imageSpaceClickCoords)
		{
			pointPositions.push_back(make_vector3(imP, 0.0f));
			pointColours.push_back(make_vector(1.0f, 0.5f, 0.2f));
		}
		g_pointsShader->begin();
		g_pointsShader->setUniform("worldToClipTransform", orthoProj);

		g_primitiveRenderer->drawPoints(int(pointPositions.size()), pointPositions.data(), pointColours.data());

		g_pointsShader->end();

		if (g_setupArena != ASC_Idle)
		{
			// from the starting point of the click coords, do a flood fill and build a plane / circle
			if (g_imageSpaceClickCoords.size() > 2)
			{
				// Change state to indicate that a setup has been found (no knowing if it is good or not...).
				g_setupArena = ASC_HasSetup;
				float3 planePts2[3] =
				{
					getPosition(g_imageSpaceClickCoords[0].x, g_imageSpaceClickCoords[0].y, frame),
					getPosition(g_imageSpaceClickCoords[1].x, g_imageSpaceClickCoords[1].y, frame),
					getPosition(g_imageSpaceClickCoords[2].x, g_imageSpaceClickCoords[2].y, frame),
				};
				float3 e0 = planePts2[1] - planePts2[0];
				float3 e1 = planePts2[2] - planePts2[1];
				float3 normal = normalize(cross(e0, e1));
				if (dot(normal, make_vector(0.0f, 0.0f, -1.0f)) < 0.0f)
				{
					normal = -normal;
				}

				// build temp transform: assume mid of clicks is centre
				float3 cp = (planePts2[0] + planePts2[1] + planePts2[2]) / 3.0f;
				float4x4 arenaTfm = make_matrix_from_zAxis(cp, normal, make_vector(0.0f, 1.0f, 0.0f));
				float4x4 toArenaTfm = inverse(arenaTfm);

				float planeDist = -dot(normal, planePts2[0]);
				g_arenaPlaneEq = make_vector4(normal, planeDist);

				// Perform a breadth first search / flood-fill to find AABB of arena.
				std::vector<int2> todo;
				todo.push_back(make_vector<int>(g_imageSpaceClickCoords[0]));
				todo.push_back(make_vector<int>(g_imageSpaceClickCoords[1]));
				todo.push_back(make_vector<int>(g_imageSpaceClickCoords[2]));
				std::vector<bool> visited(frame.depthWidth * frame.depthHeight, false);

				std::vector<float3> arenaPoints;
				Aabb arenaSpaceAabb = make_inverse_extreme_aabb();
				Aabb arenaWorldAabb = make_inverse_extreme_aabb();
				while (!todo.empty())
				{
					int2 curr = todo.back();
					todo.pop_back();

					int offset = curr.y * frame.depthWidth + curr.x;
					if (curr.x >= 0 && curr.x <= frame.depthWidth && curr.y >= 0 && curr.y < frame.depthHeight && !visited[offset])
					{
						float3 p = getPosition(curr.x, curr.y, frame);
						float dist = dot(g_arenaPlaneEq, make_vector4(p, 1.0f));
						if (isfinite(p.x) && isfinite(p.y) && fabsf(dist) < g_onPlaneTolerance)
						{
							arenaPoints.push_back(p);
							arenaSpaceAabb = combine(arenaSpaceAabb, transformPoint(toArenaTfm, p));
							arenaWorldAabb = combine(arenaWorldAabb, p);
							todo.push_back(curr + make_vector(0, 1));
							todo.push_back(curr + make_vector(0, -1));
							todo.push_back(curr + make_vector(1, 0));
							todo.push_back(curr + make_vector(-1, 0));
						}
						visited[offset] = true;
					}
				}

				DBG_RENDERER_OBJECT("ArenaSetup", [=](DebugRenderer::Backend &be) {
					float rad = 0.3f * sqrtf(arenaSpaceAabb.getDiagonal().x * arenaSpaceAabb.getDiagonal().y / float(arenaPoints.size()));
					for (const float3 &p : arenaPoints)
					{
						be.drawSphere(p, rad, DebugRenderer::green);
					}
				});

				g_arenaRadius = std::max(arenaSpaceAabb.getHalfSize().x, arenaSpaceAabb.getHalfSize().y);
				g_arenaCentre = transformPoint(arenaTfm, arenaSpaceAabb.getCentre());

				updateArenaTransform();

				g_imageSpaceClickCoords.clear();
			}
		}
		else 
		{
			if (!g_imageSpaceClickCoords.empty())
			{
				float4x4 tmp = make_identity<float4x4>();
				float3 pos = getPosition(g_imageSpaceClickCoords[0].x, g_imageSpaceClickCoords[0].y, frame);
				if (isfinite(pos.x) && isfinite(pos.y))
				{
					// snap to arena plane
					pos.z = evalPlaneEq(g_arenaPlaneEq, pos.x, pos.y);
					tmp = make_matrix_from_zAxis(pos, make_vector3(g_arenaPlaneEq), make_vector(0.0f, 1.0f, 0.0f));
					//tmp = make_matrix_from_zAxis(pos, make_vector3(g_arenaPlaneEq), make_vector(1.0f, 0.0f, 0.0f));
					postTrackingThreadCmd([=]() 
					{
						g_tracker->setTransform(g_selectedObjectName, tmp);
					});
				}
			}
			g_imageSpaceClickCoords.clear();
		}

		DBG_RENDERER_OBJECT("Arena", [=](DebugRenderer::Backend &dr)
		{
			// Note, the backend will auto-pop any at the end of the object, this is a feature, I think.
			dr.pushTransform(g_arenaToWorldTfm);
			dr.drawSphere(make_vector(0.0f, 0.0f, 0.0f), 0.05f, DebugRenderer::randomColor(2, 0.4f), "centre");
			for (int i = 0; i < 64; ++i)
			{
				float ang = 2.0f * g_pi * float(i) / float(64);
				dr.drawSphere(make_vector(sinf(ang), cosf(ang), 0.0f) * g_arenaRadius, 0.015f, DebugRenderer::randomColor(3, 1.0f), "perimiter");
			}
			Aabb aabb = make_aabb(make_vector(-g_arenaRadius, -g_arenaRadius, -0.001f), make_vector(g_arenaRadius.value(), g_arenaRadius.value(), 0.001f));
			dr.drawAabb(aabb, DebugRenderer::randomColor(6, 0.75f), "aabb");
		});

		glDisable(GL_DEPTH_TEST);

		int rw = std::max(0, g_width - leftOffset);

		glViewport(leftOffset, 0, rw, g_height);
		glScissor(leftOffset, 0, rw, g_height);
		glEnable(GL_SCISSOR_TEST);
		//glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		float4x4 projectionMatrix = perspectiveMatrix(g_fov, float(rw) / float(g_height), 0.01f, 10000.0f);
		float4x4 viewMatrix = lookAt(g_camera.getPosition(), g_camera.getPosition() + g_camera.getDirection(), g_camera.getUp());
		glDisable(GL_TEXTURE_2D);
		glEnable(GL_DEPTH_TEST);
		CHECK_GL_ERROR();

		//glDepthFunc(GL_LEQUAL);
		if (!frame.points.empty())
		{
			glPointSize(g_pointSize);

			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			g_pointsShader->begin();
			g_pointsShader->setUniform("worldToClipTransform", projectionMatrix * viewMatrix);
			g_pointsShader->setUniform("worldToArenaUnitSpaceTransform", make_scale<float4x4>(make_vector3(1.0f) / make_vector<float>(g_arenaRadius, g_arenaRadius, g_arenaRadius)) * g_worldToArenaTfm);
			g_pointsShader->setUniform("fadeInner", DYNAMIC_TWEAKABLEVAR_UI(float, "points_render/fadeInner", 1.1f, Slider(0.0f, 3.0f)));
			g_pointsShader->setUniform("fadeOuter", DYNAMIC_TWEAKABLEVAR_UI(float, "points_render/fadeOuter", 1.5f, Slider(0.0f, 3.0f)));
			g_pointsShader->setUniform("fadeLowerStart", DYNAMIC_TWEAKABLEVAR_UI(float, "points_render/fadeLowerStart", -0.1f, Slider(-1.0f, 1.0f)));
			g_pointsShader->setUniform("fadeLowerStop" , DYNAMIC_TWEAKABLEVAR_UI(float, "points_render/fadeLowerStop", -0.5f, Slider(-1.0f, 1.0f)));

			g_primitiveRenderer->drawPoints(int(frame.points.size()), frame.points.data(), frame.pointColours.data());

			g_pointsShader->setUniform("worldToArenaUnitSpaceTransform", make_identity<float4x4>());
			g_pointsShader->setUniform("fadeInner", 1000.0f);
			g_pointsShader->setUniform("fadeOuter", 2000.0f);
			g_pointsShader->setUniform("fadeLowerStart", 1000.0f);
			g_pointsShader->setUniform("fadeLowerStop", 2000.0f);

			g_pointsShader->end();
			glDisable(GL_BLEND);

			if (!inLeftWindow(g_mousePos))
			{
				float4x4 clipToWorldTfm = inverse(projectionMatrix * viewMatrix);
				// to create camera picking ray dir
				float2 mouseClipPos = make_vector2(-1.0f) + 2.0f * make_vector<float>(g_mousePos.x - leftOffset, g_height - g_mousePos.y) / make_vector(float(rw), float(g_height));
				float3 pickRayDir = normalize(transformPoint(clipToWorldTfm, make_vector3(mouseClipPos, -1.0f)) - transformPoint(clipToWorldTfm, make_vector3(mouseClipPos, 1.0f)));
				float3 pickRayOrigin = g_camera.getPosition();

				float3 best = make_vector3(0.0f);
				float bestDist = std::numeric_limits<float>::infinity();
				for (const auto &p : frame.points)
				{
					float3 dProj = pickRayOrigin + pickRayDir * dot(pickRayDir, p - pickRayOrigin);
					float dist = length(dProj - p);
					if (dist < bestDist)
					{
						best = p;
						bestDist = dist;
					}
				}
				if (bestDist < std::numeric_limits<float>::infinity())
				{
					g_debugShader->begin();
					g_debugShader->setUniform("viewProjectionTransform", projectionMatrix * viewMatrix);
					g_debugShader->setUniform("normalMat", viewMatrix);
					//g_debugShader->setUniform("modelTransform", chag::make_identity<chag::float4x4>());
					g_debugShader->setUniform("worldUpDirection", make_vector(0.0f, 0.0f, 1.0f));
					float4 colour = DebugRenderer::yellow;
					g_debugShader->setUniform("color", colour);
					g_debugShader->setUniform("emissiveColor", make_vector3(colour));

					g_primitiveRenderer->setTransform(chag::make_identity<chag::float4x4>());
					g_primitiveRenderer->drawSphere(best, 0.01f, g_debugShader, false);

					g_debugShader->setUniform("emissiveColor", make_vector3(0.0f));
					g_debugShader->end();

					// detect click
					for (auto ck : rightWindowClickCoords)
					{
						if (ck == g_mousePos)
						{
							float4x4 tmp = make_identity<float4x4>();
							float3 pos = best;
							// snap to arena plane
							pos.z = evalPlaneEq(g_arenaPlaneEq, pos.x, pos.y);
							tmp = make_matrix_from_zAxis(pos, make_vector3(g_arenaPlaneEq), make_vector(0.0f, 1.0f, 0.0f));
							postTrackingThreadCmd([=]()
							{
								g_tracker->setTransform(g_selectedObjectName, tmp);
							});
							break;
						}
					}
	
				}
			}

		}
		CHECK_GL_ERROR();

		// Draw debug renderer UI & objects
		drawDebugRendererUiAndObjects(viewMatrix, projectionMatrix);

		// Draw cursor for the selected object, and template discs, as well as annotations
		if (g_showTemplates)
		{
			for (const auto &os : g_mainThreadTrackerState.objectStates)
			{
				float4x4 tfm = os.objectToWorldTfm;
				g_primitiveRenderer->setTransform(tfm);

				if (os.id == g_selectedObjectName.value())
				{
					float4x4 arenaspaceTfm = g_worldToArenaTfm * tfm;

					float4x4 clampedTfm = g_arenaToWorldTfm * make_matrix_from_yAxis(arenaspaceTfm.getTranslation(), arenaspaceTfm.getYAxis(), make_vector(0.0f, 0.0f, 1.0f));

					g_objModelShader->begin();

					Aabb aabb = make_aabb(int(os.templ.discs.size()), [&](int i) { return os.templ.discs[i].position; });

					g_objModelShader->setUniform("viewProjectionTransform", projectionMatrix * viewMatrix);
					g_objModelShader->setUniform("normalMat", viewMatrix);
					g_objModelShader->setUniform("worldUpDirection", make_vector(0.0f, 0.0f, 1.0f));
					g_objModelShader->setUniform("color", make_vector(1.0f, 1.0f, 1.0f, 1.0f));
					g_objModelShader->setUniform("modelTransform", clampedTfm * chag::make_scale<chag::float4x4>(make_vector3(0.15f)));// length(aabb.getHalfSize()) * 0.75f)));

					g_cursorModel->render(nullptr);
					g_objModelShader->end();
				}


				glDisable(GL_BLEND);
				glLineWidth(3.0f);
				g_debugShader->begin();
				g_debugShader->setUniform("viewProjectionTransform", projectionMatrix * viewMatrix);
				g_debugShader->setUniform("normalMat", viewMatrix);
				g_debugShader->setUniform("worldUpDirection", make_vector(0.0f, 0.0f, 1.0f));
				g_debugShader->setUniform("color", make_vector(0.0f, 0.0f, 0.0f, 1.0f));
				float4 colour = g_modelColours.count(os.id) ? g_modelColours[os.id] : DebugRenderer::pink;
				g_debugShader->setUniform("emissiveColor", make_vector3(colour));


				for (const auto &d : os.templ.discs)
				{
					g_primitiveRenderer->drawDisc(d.position, d.normal, d.radius, g_debugShader, false);
				}
				//g_debugShader->setUniform("color", DebugRenderer::randomColor(nameInd, 1.0f));
				g_debugShader->setUniform("emissiveColor", make_vector3(0.1f));
				for (const auto &d : os.templ.discs)
				{
					g_primitiveRenderer->drawDisc(d.position, d.normal, d.radius, g_debugShader, true);
				}
				g_debugShader->setUniform("emissiveColor", make_vector3(0.0f));
				g_debugShader->end();
				for (const auto &a : os.templ.annotations)
				{
					float4x4 aTfm = tfm * make_matrix_from_zAxis(a.position, a.direction, make_vector(0.0f, 0.0f, 1.0f)) * chag::make_scale<chag::float4x4>(make_vector3(a.radius * 5.0f));

					g_debugShader->begin();
					//g_debugShader->setUniform("emissiveColor", make_vector3(0.0f));
					float4 colour = make_vector(0.99f, 0.2f, 0.2f, 1.0f);
					g_debugShader->setUniform("color", colour);
					g_primitiveRenderer->drawSphere(a.position, a.radius, g_debugShader, false);
					g_debugShader->end();

					g_templateGui->m_objModelShader->begin();
					g_templateGui->m_objModelShader->setUniform("viewProjectionTransform", projectionMatrix * viewMatrix);
					g_templateGui->m_objModelShader->setUniform("normalMat", viewMatrix);
					g_templateGui->m_objModelShader->setUniform("worldUpDirection", make_vector(0.0f, 0.0f, 1.0f));
					g_templateGui->m_objModelShader->setUniform("color", colour);
					g_templateGui->m_objModelShader->setUniform("modelTransform", aTfm);
					g_templateGui->m_zArrowModel->render(nullptr);
					g_templateGui->m_objModelShader->end();
				}
				glLineWidth(1.0f);
			}
		}
	}


	if (g_imageOuput && !g_outputFullFrame && g_imageOuput->isOpen())
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		// ensure no buffer object bound
		glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);

		std::vector<uint8_t> pixelData(3 * g_width * g_height);
		glPixelStorei(GL_PACK_ALIGNMENT, 1);
		glReadPixels(0, 0, g_width, g_height, GL_RGB, GL_UNSIGNED_BYTE, pixelData.data());
		g_imageOuput->writeFrame(g_width, g_height, pixelData.data());
		g_frameNr += 1;
	}
}



static void renderFrame(int width, int height)
{
#if !USE_TRACKING_THREAD
	{
		if (DYNAMIC_TWEAKABLEVAR_UI(bool, "enable_tracking", true, Toggle('t')))
		{
			trackingThreadFunc();
		}
	}
#endif // USE_TRACKING_THREAD

	g_logger->begin("render_frame");

	g_logger->write("paused", g_paused.value());
	g_logger->write("width", width);
	g_logger->write("height", height);

	g_width = width;
	g_height = height;
	{
	PROFILE_SCOPE("Frame", TT_Cpu);
	if (g_showTemplatesGui)
	{
		g_templateGui->render(width, height);
	}
	else
	{
		renderTrackingFrame(width, height);
	}
	} // End profiler scope


	{
		std::map<std::string, tv::TweakVarUiBase *> uisById;
		tv::VariableManager::UiList uis = tv::VariableManager::get().getUis();
		for (auto twr : uis)
		{
			uisById[twr->getId()] = twr;
		}
		drawMainUi(uisById);
		drawTweakVarGui();
		//ImGui::ShowDemoWindow();
		printInfo();
	}
	double trackingTime = gatherSum("tracking", Profiler::instance());

	g_logger->begin("performance");

		g_logger->write("frame_time", float(gatherSum("Frame", Profiler::instance())));
		g_logger->write("frame_input", float(gatherSum("FrameInput", Profiler::instance())));
		g_logger->write("tracking", float(gatherSum("tracking", Profiler::instance())));
		g_logger->write("normals", float(gatherSum("normals", Profiler::instance())));
		g_logger->write("cluster", float(gatherSum("cluster", Profiler::instance())));
		g_logger->write("rigid_irat_template", float(gatherSum("TrackedPointsObject::update", Profiler::instance())));
		g_logger->write("multi_template_update", float(gatherSum("TrackedMultiTemplateObject::update", Profiler::instance())));
		g_logger->write("find_correspondences_gpu", float(gatherSum("findBestPointsBatchedKernel", Profiler::instance())));
		g_logger->write("update_transforms_gpu", float(gatherSum("updateTransformBatchedKernel", Profiler::instance())));
		g_logger->write("rendering", float(gatherSum("Rendering", Profiler::instance())));
		g_logger->write("input_point_count", float(gatherSum("un_floor", Profiler::instance())));
		//g_logger->write("rendering", float(gatherSum("Rendering", Profiler::instance())));
		//g_logger->write("rendering", float(gatherSum("Rendering", Profiler::instance())));
		g_logger->end();
	Profiler::instance().clearCommandBuffer();
	g_logger->end();
}




static void update(GLFWwindow* window, float dt)
{
	ImGuiIO& io = ImGui::GetIO();

	if (!io.WantCaptureMouse)
	{
		if (g_showTemplatesGui)
		{
			g_templateGui->onMouseMotion(g_mousePos, glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS, glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS, glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_MIDDLE) == GLFW_PRESS);
			g_templateGui->update(dt);
		}
		else
		{

			if (g_mouseLookActive)
			{
				if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS)
				{
					g_camera.handleMouseInput(g_mousePos);
				}
				else
				{
					g_mouseLookActive = false;
				}
			}
			g_camera.update(dt);
		}
	}

	// Control logic for image capure open/close
	if (g_imageOuput)
	{
		//waitTime -= dt;
		if ((!g_paused || DYNAMIC_TWEAKABLEVAR_UI(bool, "shoot_while_paused", false, Toggle())) && g_framesToShoot > 0)
		{
			if (!g_imageOuput->isOpen())
			{

				static char takeName[512];
				{
					do
					{
						sprintf(takeName, "%s/%s_%03d", g_imageOutputPath.string().c_str(), g_outputTagBase.c_str(), int(g_takeNumber));
						g_takeNumber += 1;
					} while (g_imageOuput->outputExists(takeName));
				}
				g_imageOuput->open(takeName, g_width, g_height);
			}
		}
		// if next frame is out of range, stop
		if (g_imageOuput->isOpen() && g_frameNr >= g_framesToShoot)
		{
			g_imageOuput->close();
			g_framesToShoot = -1;
			g_frameNr = 0;
		}
	}

	g_realTime += dt;
}



static void onKeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (action == GLFW_PRESS)
	{
		if (key == GLFW_KEY_F1)
		{
			g_showMainUi = !g_showMainUi;
		}
		if (key == GLFW_KEY_F2)
		{
			g_showInfo = !g_showInfo;
		}
		if (key == GLFW_KEY_F3)
		{
			g_showProfilerInfo = !g_showProfilerInfo;

		}
		if (key == GLFW_KEY_F4)
		{
			g_showDebugRenderer = !g_showDebugRenderer;

		}
		if (key == GLFW_KEY_F9)
		{
			// set the pose to current
			if (!g_showTemplatesGui)
			{
				TemplateLibraryPtr lib = nullptr;


				for (const auto &v : g_objectInstances)
				{
					if (v.name == g_selectedObjectName.value())
					{
						const TrackedObjectType &t = g_objectTypes[v.typeId];
						lib = g_templateLibs[t.templateLibraryName];
						break;
					}
				}

				std::string bestPoseName;
				for (const auto &os : g_mainThreadTrackerState.objectStates)
				{
					if (os.id == g_selectedObjectName.value())
					{
						bestPoseName = os.templ.poseName;
					}
				}
				if (lib)
				{
					g_templateGui->m_lib = lib;

					// TODO: 
					g_templateGui->setPoseByName(bestPoseName);
					g_showTemplatesGui = true;
				}
			}
			else
			{
				g_templateGui->m_lib = 0;
				g_showTemplatesGui = false;
			}
		}
	}

	ImGui_ImplGlfw_KeyCallback(window, key, scancode, action, mods);

	ImGuiIO& io = ImGui::GetIO();

	if (io.WantCaptureKeyboard)
	{
		return;
	}
	if (action == GLFW_REPEAT)
	{
		return;
	}


	if (g_showTemplatesGui)
	{
		g_templateGui->onKey(key, action);
	}
	else
	{

		if (action == GLFW_PRESS && (key == GLFW_KEY_LEFT || key == GLFW_KEY_RIGHT))
		{
			int currentIndex = 0;
			int num = int(g_objectInstances.size());
			for (int i = 0; i < num; ++i)
			{
				const auto &os = g_objectInstances[i];
				if (g_selectedObjectName.value() == os.name)
				{
					currentIndex = i;
					break;
				}
			}
			if (currentIndex >= 0 && num)
			{
				if (key == GLFW_KEY_LEFT)
				{
					currentIndex = (currentIndex + num - 1)  % num;
				}
				if (key == GLFW_KEY_RIGHT)
				{
					currentIndex = (currentIndex + 1)  % num;
				}
				currentIndex = clamp(currentIndex, 0, num - 1);

				g_selectedObjectName = g_objectInstances[currentIndex].name;
				return;
			}
		}

		if (action == GLFW_PRESS)
		{
			int lcKey = tolower(key);
			tweakVarUiOnKey(lcKey);
			ImGuiX::onKeyPress(lcKey);
		}
		//g_camera.setMoveVelocity(length(g_model->getAabb().getHalfSize()) / 10.0f);
		if (g_camera.handleKeyInput(key, action == GLFW_PRESS))
		{
			return;
		}
	}
}


void onScrollCallback(GLFWwindow *window, double xOffset, double yOffset)
{
	ImGui_ImplGlfw_ScrollCallback(window, xOffset, yOffset);
	ImGuiIO& io = ImGui::GetIO();
	if (io.WantCaptureMouse)
	{
		return;
	}
	if (g_showTemplatesGui)
	{
		g_templateGui->onMouseScroll(float(yOffset));
	}
	else
	{
		g_pointSize += float(yOffset);
		g_pointSize = std::max(g_pointSize, 1.0f);
	}
}


void onMouseButtonCallback(GLFWwindow*window, int button, int action, int mods)
{
	ImGui_ImplGlfw_MouseButtonCallback(window, button, action, mods);

	ImGuiIO& io = ImGui::GetIO();
	//g_mousePos = { int(io.MousePos.x), int(io.MousePos.y) };

	// stop events from getting to other parts if they hit the ImGUI
	if (io.WantCaptureMouse)
	{
		return;
	}

	if (g_showTemplatesGui)
	{
		g_templateGui->onMouseButton(button, action, g_mousePos);
	}
	else
	{

		if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
		{
			g_mouseLeftClickDownPos = g_mousePos;
		}
		if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE)
		{
			int2 delta = g_mouseLeftClickDownPos - g_mousePos;
			if (lengthSquared(delta) <= 1)
			{
				g_clickCoords.push_back(g_mousePos);
			}
		}

		if (inLeftWindow(g_mousePos))
		{
			// This ought to not happen..
			g_mouseLookActive = false;
		}
		else
		{
			g_camera.resetMouseInputPosition();
			if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
			{
				g_mouseLookActive = true;
			}
		}
	}
}


static void bindObjModelAttributes(SimpleShader *shader)
{
	shader->bindAttribLocation(OBJModel::AA_Position, "position");
	shader->bindAttribLocation(OBJModel::AA_Normal, "normalIn");
	shader->bindAttribLocation(OBJModel::AA_TexCoord, "texCoordIn");
	shader->bindAttribLocation(OBJModel::AA_Tangent, "tangentIn");
	shader->bindAttribLocation(OBJModel::AA_Bitangent, "bitangentIn");
}

static void setObjModelUniformBindings(SimpleShader *shader)
{
	shader->begin();
	shader->setUniform("diffuse_texture", OBJModel::TU_Diffuse);
	shader->setUniform("opacity_texture", OBJModel::TU_Opacity);
	shader->setUniform("specular_texture", OBJModel::TU_Specular);
	shader->setUniform("normal_texture", OBJModel::TU_Normal);
	shader->end();
	shader->setUniformBufferSlot("MaterialProperties", OBJModel::UBS_MaterialProperties);
	//shader->setUniformBufferSlot("InstanceProperties", OBJModel::UBS_InstanceProperties);
	//shader->setUniformBufferSlot("WorldMatrixPalette", OBJModel::UBS_WorldMatrixPalette);

	//shader->setUniformBufferSlot("Globals", UBS_Globals);
}

static void createShaders()
{
	boost::filesystem::path shadersPath(g_envRootPath / "data" / "shaders");

	SimpleShader::Context shaderCtx;
	shaderCtx.m_includePath = shadersPath.string();

	deleteIfThere(g_debugShader);
	g_debugShader = new SimpleShader((shadersPath / "debug_vertex.glsl").string(), (shadersPath / "debug_fragment.glsl").string(), shaderCtx);
	g_debugShader->bindFragDataLocation(0, "resultColor");
	g_debugShader->link();
	g_debugShader->begin();
	g_debugShader->setUniform("modelTransform", chag::make_identity<chag::float4x4>());
	g_debugShader->setUniform("emissiveColor", make_vector(0.0f, 0.0f, 0.0f));

	g_debugShader->end();

	deleteIfThere(g_pointsShader);
	
	g_pointsShader = new SimpleShader(true, R"SOMETAG(
#version 330
in vec3 positionAttrib;
in vec3 colourAttrib;

out VertOut
{
	vec4 v2f_colour;
};

uniform mat4 worldToClipTransform;
uniform mat4 worldToArenaUnitSpaceTransform;
uniform float fadeInner;
uniform float fadeOuter;
uniform float fadeLowerStart;
uniform float fadeLowerStop;

void main() 
{
  //gl_PointSize = viewportSize.y * viewToClipTransform[1][1] * positionXYZ_SizeW.w / clipSpacePos.w;
	vec4 arenaUnitSpacePos = worldToArenaUnitSpaceTransform * vec4(positionAttrib, 1.0);
	float fade = 1.0 - smoothstep(fadeInner, fadeOuter, length(arenaUnitSpacePos.xyz));
	float fade2 = fade * (1.0 - smoothstep(fadeLowerStart, fadeLowerStop, arenaUnitSpacePos.z));
	//if (fade2 <= 0.0001)
	//{
//		discard;
	//}
	v2f_colour = vec4(colourAttrib, fade2);
	gl_Position = worldToClipTransform * vec4(positionAttrib, 1.0);
}
)SOMETAG",
R"SOMETAG(
#version 330

in VertOut
{
	vec4 v2f_colour;
};

out vec4 fragmentColor;

void main() 
{
	fragmentColor = v2f_colour;
}
)SOMETAG", shaderCtx, g_primitiveRenderer->getAttributeLocations());

	deleteIfThere(g_texturedQuadShader);
	
	g_texturedQuadShader = new SimpleShader(true, R"SOMETAG(
#version 330
in vec3 positionAttrib;
in vec2 texCoordAttrib;

out VertOut
{
	vec2 v2f_texCoord;
};
uniform mat4 transform;
uniform bool flipX;

void main() 
{
	if (flipX)
	{
		v2f_texCoord = vec2(1.0 - texCoordAttrib.x, texCoordAttrib.y);
	}
	else
	{
		v2f_texCoord = texCoordAttrib;
	}
	gl_Position = transform * vec4(positionAttrib, 1.0);
}
)SOMETAG",
R"SOMETAG(
#version 330

in VertOut
{
	vec2 v2f_texCoord;
};

uniform sampler2D tex;

out vec4 fragmentColor;

void main() 
{
	fragmentColor = texture(tex, v2f_texCoord);
}
)SOMETAG", shaderCtx, g_primitiveRenderer->getAttributeLocations());


	deleteIfThere(g_objModelShader);
	g_objModelShader = new SimpleShader((shadersPath / "SimpleObjModel_vertex.glsl").string(), (shadersPath / "SimpleObjModel_fragment .glsl").string(), shaderCtx);
	g_objModelShader->bindFragDataLocation(0, "resultColor");
	bindObjModelAttributes(g_objModelShader);

	g_objModelShader->link();
	setObjModelUniformBindings(g_objModelShader);
	g_objModelShader->begin();
	g_objModelShader->setUniform("modelTransform", chag::make_identity<chag::float4x4>());
	g_objModelShader->end();

}


int main(int argc, char* argv[])
{
	CrashHandler ch;
	ch.SetProcessExceptionHandlers();
	ch.SetThreadExceptionHandlers();

	try
	{
		// 1. get root path, assumes that executable is in root/bin/platform
		// TODO: move this to after the program options, to allow override.
		// TODO: check if the program is installed in program dir, if so, use user/program data path instead (and copy initial files).

		uint32_t depthStreams = ColorDepthSource::s_allStreamBit;

		std::string inputSource = "kinect2";
		std::string optionsTagBase = "";
		std::string defaultOptionsName = "";
		std::string ffmpegPath = "C:\\tools\\ffmpeg\\bin\\ffmpeg.exe";
		g_envRootPath = bfs::canonical(bfs::path(argv[0])).parent_path().parent_path().parent_path();
		std::string customEnvRoot;
		int pauseFrameCount = -1;
		bool forceCpu = false;
		{
			bool showHelp = false;
			bool depthOnly = false;
			namespace po = boost::program_options;
			po::options_description desc("Allowed options");
			desc.add_options()
				("help,h", "produce this help message.")
				("input,i", po::value<std::string>(&inputSource), "Name of the input file to load or 'kinect2' to specify the depth camera.")
				("optionsTag,t", po::value<std::string>(&optionsTagBase), "Used to name specific options file to load, useful when many recordings from one setup need to be processed, default is to use the input name.")
				("outputTag,o", po::value<std::string>(&g_outputTagBase), "Used to tag the output, e.g., the data log, the default is to use the input name.")
				("defaultOptionsName", po::value<std::string>(&defaultOptionsName), "name of options file that will be loaded after the default.txt ones but before specific settings. Makes it easier to propagate a set of parameters.")
				("depthOnly,d", po::bool_switch(&depthOnly)->default_value(false), "Turns off the Color stream from the input (the Kinect), this is needed in low light as the exposure time is increased, throttling the frame-rate.")
				("forceCpuBackend,c", po::bool_switch(&forceCpu)->default_value(false), "Makes the tracker use the CPU backend (even if a CUDA capable GPU is present).")
				("pauseFrameCount,p", po::value<int>(&pauseFrameCount)->default_value(pauseFrameCount), "Number of frames to run before pausing.")
				("ffmpegPath,f", po::value<std::string>(&ffmpegPath)->default_value(ffmpegPath), "Set the path to the ffmpeg executable, only needed to record video, not for tracking.")
				("envRootPath,r", po::value<std::string>(&customEnvRoot)->default_value(g_envRootPath.string()), "Set the root path of the project/environment, defaults to grandparent of exe path.")
				;
			try
			{
				po::variables_map vm;
				po::store(po::command_line_parser(argc, argv).
					options(desc).run(), vm);
				po::notify(vm);

				showHelp = vm.count("help") != 0;
				//useOurTree = !vm["use_their"].as<bool>();
			}
			catch (const po::unknown_option& eUO)
			{
				fprintf(stderr, "<error> %s\n", eUO.what());
				return 1;
			}
			if (depthOnly)
			{
				//
				printf("'depthOnly' set - disabling colour stream!\n");
				depthStreams = ColorDepthSource::s_depthStreamBit;
			}
			if (showHelp)
			{
				std::cerr << desc << "\n";
				return 1;
			}
		}
		g_envRootPath = customEnvRoot;
		printf("Using root directory '%s' for options etc\n", g_envRootPath.string().c_str());


		bfs::path dataDir(g_envRootPath / "data");
		bfs::path optionsDir(g_envRootPath / "options");
		bfs::path logDir(g_envRootPath / "logs");
		bfs::path templateLibsPath(g_envRootPath / "template_libs");

		// TODO: separate out the working dir of the program from that of the install path (from which non-mutable data should be loaded)
		bfs::path shadersDir(dataDir / "shaders");
		bfs::path modelsDir(g_envRootPath / "data" / "models");

		g_imageOutputPath = g_envRootPath / g_imageOutputSubPath;
		g_arenaSetupsPath = g_envRootPath / g_arenaSetupsSubPath;
		g_trackedObjectsSetupsPath = g_envRootPath / g_trackedObjectsSetupsSubPath;
		bfs::path recordingsPath = g_envRootPath / g_recordingsOutputSubPath;

		// ensure logs subdirectory is present
		boost::filesystem::create_directories(logDir);
		boost::filesystem::create_directories(g_imageOutputPath);
		boost::filesystem::create_directories(g_arenaSetupsPath);
		boost::filesystem::create_directories(g_trackedObjectsSetupsPath);
		boost::filesystem::create_directories(recordingsPath);
		boost::filesystem::create_directories(templateLibsPath);

		boost::filesystem::create_directories(optionsDir);
		if (!defaultOptionsName.empty())
		{
			readVars(optionsDir / (defaultOptionsName + ".txt"));
		}
		else
		{
			readVars(optionsDir / "default.txt");
		}
		loadDebugRendererState(optionsDir);

		printf("INFO: Opening input source '%s'\n", inputSource.c_str());
		std::function<double(void)> timeSourceFn = [&]()->double {return g_timer.getElapsedTime(); };

		ColorDepthSource *trueSource = nullptr;
		if (inputSource == "kinect2")
		{
			trueSource = ColorDepthSource::createKinect2(timeSourceFn, depthStreams);
			if (!trueSource)
			{
				printf("ERROR: Failed to open Kinect, make sure it is plugged in and all those things.\n");
				exit(EXIT_FAILURE);
			}
		}
		else // treat as file name
		{
			auto bs = ChunkInStreamerPtr(new ChunkInStreamer(timeSourceFn));

			for (auto sourceName :{ inputSource, (recordingsPath / inputSource).string(), (g_envRootPath / inputSource).string() })
			{
				if (bs->open(sourceName))
				{
					g_blockStreamPlayer = ChunkInStreamPlayerPtr(new ChunkInStreamPlayer(bs, 1.0 / 30.0));
					trueSource = ColorDepthSource::createFromBlockFile(g_blockStreamPlayer, timeSourceFn);
					printf("  INFO: Opened file source '%s'.\n", sourceName.c_str());
					break;
				}
				else
				{
					printf("  INFO: Failed to open file source '%s' (re-trying other paths).\n", sourceName.c_str());
				}
			}
			if (!trueSource)
			{
				printf("ERROR: Failed to open file source '%s', make sure the name and path is correct.\n", inputSource.c_str());
				exit(EXIT_FAILURE);
			}
		}
		g_source = new FileOutputStreamerDepthSource(timeSourceFn, trueSource);
		g_tracker = new TemplateTracker(forceCpu, g_source->getId());

		//glfwSetErrorCallback(glfwErrorCallback);
		if (!glfwInit())
		{
			exit(EXIT_FAILURE);
		}

		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
		glfwWindowHint(GLFW_SAMPLES, 8);


		GLFWwindow* window = glfwCreateWindow(g_startWidth, g_startHeight, "Tracking System Build: '" TRACKER_VERSION_STRING "'", NULL, NULL);
		if (!window)
		{
			glfwTerminate();
			exit(EXIT_FAILURE);
		}

		//glfwSetKeyCallback(window, glfwKeyCallback);

		glfwMakeContextCurrent(window);

		gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);

		glfwSwapInterval(1);


		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
		glFrontFace(GL_CCW);
		glDisable(GL_DITHER);
		glDisable(GL_ALPHA_TEST);
		//glEnable(GL_MULTISAMPLE);

		createShaders();

		ImGui::CreateContext();

		static std::string imguiIniPath = (optionsDir / "tracker_imgui.ini").string();
		ImGui::GetIO().IniFilename = imguiIniPath.c_str();

		ImGui_ImplGlfwGL3_Init(window, true);


		glfwSetMouseButtonCallback(window, onMouseButtonCallback);
		glfwSetKeyCallback(window, onKeyCallback);
		glfwSetScrollCallback(window, onScrollCallback);

		//glfwSetScrollCallback(window, ImGui_ImplGlfw_ScrollCallback);
		//glfwSetKeyCallback(window, ImGui_ImplGlfw_KeyCallback);
		//glfwSetCharCallback(window, ImGui_ImplGlfw_CharCallback);


		ImGui::StyleColorsDark();


		ImGuiX::getKeyToTextMap() = [&](int keyId)->std::string
		{
			std::string tmp = TweakableVar::keyToString(keyId);
			if (!tmp.empty())
			{
				return " hotkey: '" + tmp +"'";
			}
			return std::string();
		};

		g_primitiveRenderer = new PrimitiveRenderer;
		g_primitiveRenderer->init(g_debugShader);

		DebugRenderer::instance().setThreadId("main");

		g_output = ClientOutput::create();

		//g_imageOuput = ImageStreamOutput::createImage();// "frame_%05d.png");
		g_imageOuput = ImageStreamOutput::createFfmpeg(ffmpegPath);// "frame_%05d.png");
		if (!g_imageOuput)
		{
			printf("WARNING: Failed to open image output module, ensure FFmpeg binary is available at '%s'\n  Set the path using command line argument --ffmpegPath[-f] <path>\n", ffmpegPath.c_str());
		}
		bool exitProgramFlag = false;

		tweakVarUiAddButton("Exit", SK_Escape, [&]()
		{
			exitProgramFlag = true;
		}, "Quits the program, saving options.");

		tweakVarUiAddButton("Next Camera Loc", SK_Tab, [&]()
		{
			if (!g_cameraPoints.empty())
			{
				g_cameraPoints.nextSnap();
				useCurrentCameraLocation();
				// to remember current point.
				g_cameraPoints.save();
			}
		}, "Set view to the next saved camera location");
		tweakVarUiAddButton("Smooth Camera Loc", SK_Add, [&]()
		{
			if (!g_cameraPoints.empty())
			{
				g_cameraPoints.nextSnap();
				useCurrentCameraLocation(1.0f);
				// to remember current point.
				g_cameraPoints.save();
			}
		}, "Set view to the next saved camera location");

		tweakVarUiAddButton("Del Camera Loc", SK_Del, [&]()
		{
			if (!g_cameraPoints.empty())
			{
				g_cameraPoints.removeSnap();
				g_cameraPoints.save();
			}
		}, "Delete the currenct saved camera location.");
		tweakVarUiAddButton("Save Camera Loc", SK_Insert, [&]()
		{
			saveCurrentCameraLocation();
		}, "Add the current camera locaiton last in the list of saved locations, stores to disk.");

		tweakVarUiAddButton("Focus Camera On Tracked Item", SK_PageUp, [&]()
		{
			postTrackingThreadCmd([]() {
				if (TemplateTracker::TrackedObjectPtr o = g_tracker->get(g_selectedObjectName))
				{
					if (o->isActive())
					{
						float4x4 tfm = o->getObjectToWorldTfm();
						Aabb aabb = o->getAabb();
						float3 target = tfm.getTranslation();
						float3 pos = target - aabb.getDiagonal() * 2.0f;

						// Note: this is not thread-safe, but data races here should not cause crashes, only possibly weird visual glitches.
						g_camera.setTransform(make_matrix_from_zAxis(pos, target - pos, make_vector(0.0f, 0.0f, -1.0f)), 1.0f);
					}
				}});
		}, "Focus Camera On Tracked Item.");
		tweakVarUiAddButton("Vertical Focus Camera On Tracked", SK_PageDown, [&]()
		{
			postTrackingThreadCmd([]() {
				// TODO: this is not 100% thread safe, as the transform may change
			if (TemplateTracker::TrackedObjectPtr o = g_tracker->get(g_selectedObjectName))
			{
				if (o->isActive())
				{
					float4x4 tfm = o->getObjectToWorldTfm();
					Aabb aabb = o->getAabb();
					float3 target = tfm.getTranslation();
					float3 pos = target;// +make_vector(0.0f, 0.0f, -length(aabb.getDiagonal()) * 2.0f);
					pos.z = g_camera.getPosition().z;

					// Note: this is not thread-safe, but data races here should not cause crashes, only possibly weird visual glitches.
					g_camera.setTransform(make_matrix_from_zAxis(pos, target - pos, make_vector(0.0f, 1.0f, 0.0f)), 1.0f);
				}
			}});
		}, "Vertical Focus Camera On Tracked Item.");

		if (optionsTagBase.empty())
		{
			optionsTagBase = g_source->getId();
		}
		if (g_outputTagBase.empty())
		{
			g_outputTagBase = g_source->getId();
		}
		const bfs::path sourceOptionsFileName = optionsDir / (optionsTagBase +".txt");
		readVars(sourceOptionsFileName);

		if (inputSource != "kinect2")
		{
			// if we are not the kinect2, start paused
			g_paused = true;
		}

		// override options file with command line things here...
		if (pauseFrameCount >= 0)
		{
			g_pauseFrameCount = pauseFrameCount;
		}

		// Initialize such that the up vector is out of the plane of the arena (which is typically roughly {0.0f, 0.0f, -1.0f})
		g_camera.init(chag::make_vector(0.0f, 0.0f, -1.0f), chag::make_vector(0.0f, 1.0f, 0.0f), chag::make_vector(0.0f, 0.0f, 0.0f));
		// Then set the camera to look straight down from the origin
		g_camera.setTransform(make_matrix_from_zAxis(chag::make_vector(0.0f, 0.0f, 0.0f), chag::make_vector(0.0f, 0.0f, 1.0f), chag::make_vector(0.0f, 1.0f, 0.0f)));
		g_camera.setMoveVelocity(0.1f);

		g_cameraPoints.load((optionsDir / (optionsTagBase + "_cameras.txt")).string());
		
		JsonDataLoggerBackend jsonBackend(logDir / (g_outputTagBase + "-" + getTimeStampStr() + ".json"));
		DataLogger logger(&jsonBackend);
		g_logger = &logger;


		loadAndSetupTrackedObjectTypes();

		g_templateGui = new TemplateEditor(nullptr, g_primitiveRenderer, g_objModelShader, dataDir.string());

		g_templateGui->loadOptions((optionsDir / "templategui.json").string());

		g_cursorModel = new OBJModel((modelsDir / "tracking_cursor.obj").string());

		g_dumpToFile = false;
		useCurrentCameraLocation();

		if (length(g_arenaPlaneEq) > 0.0f && length(g_arenaCentre) > 0.0f)
		{
			updateArenaTransform();
		}

		// load arena setup, which overwrites some other things too...
		if (!g_arenaSetupName.value().empty())
		{
			loadArenaSetup(g_arenaSetupsPath / g_arenaSetupName.value(), false);
		}
		g_timer.start();


		logger.begin("intro");
		logger.write("time_stamp", getTimeStampStr(false));
		logger.write("tracking_backend_device_type", g_tracker->m_trackingBackend->isCuda() ? "GPU" : "CPU");
		logger.write("tracking_backend_device_desc", g_tracker->m_trackingBackend->getDeviceDesc());
		logger.begin("parameters");
		logger.write("input_source_id", g_source->getId());
		{
			std::vector<std::string> cmdLine;
			for (int i = 1; i < argc; ++i)
			{
				cmdLine.push_back(argv[i]);
			}
			logger.write("command_line", cmdLine);
		}

		logger.end();

		{
			// Pipe variabsettings to the logger
			DataLoggerLogStream jls(&logger);
			logger.begin("settings");
			TweakableVar::VariableManager::get().serialize(jls);
			logger.end();
		}
		logger.end();
		g_timer.restart();

#if USE_TRACKING_THREAD
		g_trackingThread = std::thread(trackingThreadFunc);
#endif // USE_TRACKING_THREAD
		double currentTime = g_timer.getElapsedTime();
		while (!glfwWindowShouldClose(window) && !exitProgramFlag)
		{
			int width, height;
			glfwGetFramebufferSize(window, &width, &height);
			ImGui_ImplGlfwGL3_NewFrame();


			double newTime = g_timer.getElapsedTime();
			float dt = float(newTime - currentTime);
			currentTime = newTime;

			double mouse_x, mouse_y;
			glfwGetCursorPos(window, &mouse_x, &mouse_y);
			g_mousePos = make_vector<int>(mouse_x, mouse_y);

			update(window, dt);
			renderFrame(width, height);
			
			ImGuiX::clearHotKeyState();
			// Creates command lists from all the UI rendering calls in the frame and passes this to the implementations that uses opengl to render them.
			ImGui::Render();
			ImGui_ImplGlfwGL3_RenderDrawData(ImGui::GetDrawData());

			if (g_imageOuput && g_outputFullFrame && g_imageOuput->isOpen())
			{
				glBindFramebuffer(GL_FRAMEBUFFER, 0);
				// ensure no buffer object bound
				glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);

				std::vector<uint8_t> pixelData(3 * g_width * g_height);
				glPixelStorei(GL_PACK_ALIGNMENT, 1);
				glReadPixels(0, 0, g_width, g_height, GL_RGB, GL_UNSIGNED_BYTE, pixelData.data());
				g_imageOuput->writeFrame(g_width, g_height, pixelData.data());
				g_frameNr += 1;
			}

			glfwSwapBuffers(window);
			glfwPollEvents();
		}

		// close tracking thread & others peacefully and wait for them to finish.
#if USE_TRACKING_THREAD
		if (g_imageOuput && g_imageOuput->isOpen())
		{
			g_imageOuput->close();
		}
		g_exitTracking = true;
		g_source->stop();
		g_trackingThread.join();
#endif // USE_TRACKING_THREAD

		g_templateGui->saveOptions((optionsDir / "templategui.json").string());
		delete g_templateGui;
		saveDebugRendererState(optionsDir);

		writeVars(sourceOptionsFileName);
		for (auto a : g_templateLibs)
		{
			//TODO: switch to explicit saving, e.g., add a prompt
			a.second->save(templateLibsPath);
		}

		// LOG settings at the end to the data logger, as this can be used to see if anything changed during the session.
		logger.begin("outro");
		logger.write("time_stamp", getTimeStampStr(false));
		{
			DataLoggerLogStream jls(&logger);
			logger.begin("settings");
			TweakableVar::VariableManager::get().serialize(jls);
			logger.end();
		}
		logger.end();

		ImGui_ImplGlfwGL3_Shutdown();
		return 0;
	}
	catch (...)
	{
		std::clog << boost::current_exception_diagnostic_information() << std::endl;
	}
}


static void drawPerfNodeImGUI(const PerfTreeNode* n)
{
	if (n->message.empty())
	{
		ImGui::Text("%8.2f %8.2f %8I64d", 1000.0 * n->time, 1000.0 * n->average, n->count);
	}
	else
	{
		ImGui::Text("%17s %8I64d", n->message.c_str(), n->count);
	}
}


static void drawPerfNodeListImGUI(const std::vector<PerfTreeNode*> &nl)
{
	for (std::vector<PerfTreeNode*>::const_iterator it = nl.begin(); it != nl.end(); ++it)
	{
		if (!(*it)->children.empty())
		{
			ImGui::PushID((*it)->label.c_str());                      // Use object uid as identifier. Most commonly you could also use the object pointer as a base ID.
			ImGui::AlignTextToFramePadding();  // Text and Tree nodes are less high than regular widgets, here we add vertical spacing to make the tree lines equal high.
			bool node_open = ImGui::TreeNodeEx((*it)->label.c_str(), ImGuiTreeNodeFlags_SpanAllColumns | ImGuiTreeNodeFlags_DefaultOpen);
			ImGui::NextColumn();
			ImGui::AlignTextToFramePadding();
			drawPerfNodeImGUI(*it);
			ImGui::NextColumn();
			if (node_open)
			{
				drawPerfNodeListImGUI((*it)->children);
				ImGui::TreePop();
			}
			ImGui::PopID();
		}
		else
		{
			ImGui::Selectable((*it)->label.c_str(), false, ImGuiSelectableFlags_SpanAllColumns);
			ImGui::SetItemAllowOverlap();

			ImGui::NextColumn();
			ImGui::PushItemWidth(-1);
			drawPerfNodeImGUI(*it);
			ImGui::PopItemWidth();
			ImGui::NextColumn();
		}
		//printString(indent * 20 + g_width / 2, yOffset, "%-25s: %8.2f %8.2f %8I64d", (*it)->label.c_str(), 1000.0 * (*it)->time, 1000.0 * (*it)->average, (*it)->count);
		//yOffset += yStep;
		//screenPrintNodeList((*it)->children, yOffset, yStep, indent + 1);
	}
}

static PerfTreeBuilder g_perfTreeBuilder;

#if USE_TRACKING_THREAD
static PerfTreeBuilder g_trackingPerfTreeBuilder;
// This is used to keep a mirror of the tracker state on the main thread... needs a neater solution someday.
static Profiler g_trackerProfilerMirror;
static std::vector<PerfTreeNode*> g_trackingPerfTreeRoots;
#endif // USE_TRACKING_THREAD
static void drawPerfResImGUI()
{
#if USE_TRACKING_THREAD
	for (const auto &b : g_trackerProfilerCommands)
	{
		g_trackerProfilerMirror.setCommandBuffer(b);
		g_trackingPerfTreeRoots = g_trackingPerfTreeBuilder.build(g_trackerProfilerMirror);
	}
	g_trackerProfilerCommands.clear();
#endif // USE_TRACKING_THREAD
	if (g_showProfilerInfo)
	{
		std::vector<PerfTreeNode*> roots = g_perfTreeBuilder.build(Profiler::instance());

		ImGui::SetNextWindowSize(ImVec2(430, 450), ImGuiCond_FirstUseEver);
		if (!ImGui::Begin("Profiler Info", &g_showProfilerInfo))
		{
			ImGui::End();
			return;
		}
		ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(2, 2));
		ImGui::Columns(2);


		drawPerfNodeListImGUI(roots);
#if USE_TRACKING_THREAD
		drawPerfNodeListImGUI(g_trackingPerfTreeRoots);
#endif // USE_TRACKING_THREAD
		ImGui::Columns(1);
		ImGui::PopStyleVar();

		ImGui::End();
	}
}

static void printInfo()
{
	if (!g_showTemplatesGui)
	{
		{
			const float DISTANCE = 10.0f;
			static int corner = 1;
			ImVec2 window_pos = ImVec2((corner & 1) ? ImGui::GetIO().DisplaySize.x - DISTANCE : DISTANCE, (corner & 2) ? ImGui::GetIO().DisplaySize.y - DISTANCE : DISTANCE);
			ImVec2 window_pos_pivot = ImVec2((corner & 1) ? 1.0f : 0.0f, (corner & 2) ? 1.0f : 0.0f);
			ImGui::SetNextWindowPos(window_pos, ImGuiCond_Always, window_pos_pivot);
			ImGui::SetNextWindowBgAlpha(0.5f); // Transparent background
			if (ImGui::Begin("Example: Fixed Overlay", 0, ImGuiWindowFlags_NoTitleBar|ImGuiWindowFlags_NoResize|ImGuiWindowFlags_AlwaysAutoResize|ImGuiWindowFlags_NoMove|ImGuiWindowFlags_NoSavedSettings|ImGuiWindowFlags_NoFocusOnAppearing|ImGuiWindowFlags_NoNav))
			{
				if (ImGui::IsMouseHoveringWindow())
				{
					ImGui::SetTooltip("(right-click to change position)");
				}
				ImGui::Text("Toggle Main UI: <F1>");
				ImGui::Text("Toggle Tweak UI: <F2>");
				ImGui::Text("Toggle Profiler UI: <F3>");
				ImGui::Text("Toggle Debug Render UI: <F4>");
				ImGui::Text("Toggle Template UI: <F9>");

				ImGui::Separator();

				for (const auto &os : g_mainThreadTrackerState.objectStates)
				{
					float4x4 tmp = g_worldToArenaTfm * os.objectToWorldTfm;
					ImGui::Text("%s: {%0.3f, %0.3f, %0.3f}", os.id.c_str(), tmp.getTranslation().x, tmp.getTranslation().y, tmp.getTranslation().z);
				}
				ImGui::Separator();

				if (g_source->isOutStreamOpen())
				{
					ImGui::PushStyleColor(ImGuiCol_Text, (ImVec4)ImColor(sinf(g_realTime * 8.0f) * 0.45f + 0.55f, 0.1f, 0.1f));
					ImGui::Text("DUMPING To FILE!");
					ImGui::PopStyleColor();
					ImGui::Text("(press 'f' to toggle)");
				}
				if (g_poseCaptureMode)
				{
					ImGui::PushStyleColor(ImGuiCol_Text, (ImVec4)ImColor(sinf(g_realTime * 12.0f) * 0.45f + 0.55f, 0.1f, 0.1f));
					ImGui::Text("Capture MODE enabled!");
					ImGui::PopStyleColor();
				}
				if (g_source->isOutStreamOpen() || g_source->writeQueueBytes() > 0)
				{
					ImGui::Text("Bytes buffered: %0.2f Mb", float(g_source->writeQueueBytes()) / (1024.0f * 1024.0f));
				}

				if (ImGui::BeginPopupContextWindow())
				{
					if (ImGui::MenuItem("Top-left", NULL, corner == 0)) corner = 0;
					if (ImGui::MenuItem("Top-right", NULL, corner == 1)) corner = 1;
					if (ImGui::MenuItem("Bottom-left", NULL, corner == 2)) corner = 2;
					if (ImGui::MenuItem("Bottom-right", NULL, corner == 3)) corner = 3;
					ImGui::EndPopup();
				}
				ImGui::End();
			}
		}

		if (g_source->getFrameCount() >= 0)
		{
			float height = 50.0f;
			ImGui::SetNextWindowPos(ImVec2(ImGui::GetIO().DisplaySize.x * 0.5f, ImGui::GetIO().DisplaySize.y - 5.0f), ImGuiCond_Always, ImVec2(0.5f, 1.0f));
			ImGui::SetNextWindowSize(ImVec2(ImGui::GetIO().DisplaySize.x * 0.75f, height), ImGuiCond_Always);
			ImGui::SetNextWindowBgAlpha(0.5f); // Transparent background
			if (ImGui::Begin("Frame Controller", 0, ImGuiWindowFlags_NoTitleBar|ImGuiWindowFlags_NoResize|ImGuiWindowFlags_NoFocusOnAppearing))
			{
				int frameInd = g_source->getCurrentFrame();
				ImGui::PushItemWidth(-1);
				if (ImGui::SliderInt("##Frame", &frameInd, 0, g_source->getFrameCount()-1))
				{
					// Don't do anything if set back to the same as before...
					if (frameInd != g_source->getCurrentFrame())
					{
						g_frameIndexSet = true;
						g_source->seek(frameInd);
					}
				}
				ImGui::PopItemWidth();
				if (ImGui::Button("Play/Pause"))
				{
					g_paused = !g_paused;
				}
				ImGui::SameLine();
				if (ImGui::Button("Step Forwards"))
				{
					g_pauseFrameCount = 1;
				}

				//ImGui::Separator();
				ImGui::End();
			}
		}
	}
	drawPerfResImGUI();
}



namespace TweakableVar
{
	// This function must be supplied to map keys to strings for printing GUI info
	std::string keyToString(int key)
	{
		static std::map<int, const char *> keyMap(
		{
			{ SK_F1, "F1" },
			{ SK_F2, "F2" },
			{ SK_F3, "F3" },
			{ SK_F4, "F4" },
			{ SK_F5, "F5" },
			{ SK_F6, "F6" },
			{ SK_F7, "F7" },
			{ SK_F8, "F8" },
			{ SK_F9, "F9" },
			{ SK_F10, "F10" },
			{ SK_F11, "F11" },
			{ SK_F12, "F12" },
			{ SK_Left, "Left" },
			{ SK_Right, "Right" },
			{ SK_Up, "Up" },
			{ SK_Down, "Down" },
			{ SK_PageUp, "PageUp" },
			{ SK_PageDown, "PageDown" },
			{ SK_Home, "Home" },
			{ SK_End, "End" },
			{ SK_Insert, "Insert" },
			{ SK_Del, "Del" },
			{ SK_Enter, "Enter" },
			{ SK_Escape, "Escape" },
			{ SK_Tab, "Tab" },
			{ SK_Divide , "Divide" },
			{ SK_Multiply , "Multiply" },
			{ SK_Subtract , "Subtract" },
			{ SK_Add, "Add" }
		}
		);

		auto it = keyMap.find(key);
		if (it != keyMap.end())
		{
			return (*it).second;
		}

		std::stringstream ss;
		ss << char(key);
		return ss.str();
}

}; // namespace TweakableVar


namespace TweakableVar
{
	template <typename T>
	inline void to_json(nlohmann::json& j, const Variable<T> &p)
	{
		j = p.value();
	}
	template <typename T>
	inline void from_json(nlohmann::json& j, Variable<T> &p)
	{
		p.value() = j;
	}
};

template <typename T>
inline void to_json(nlohmann::json& j, const CudaMirrorBuffer<T> &b)
{
#if 0
	std::vector<float> tmp;
	for (chag::float4 v : b)
	{
		tmp.push_back(v.x);
		tmp.push_back(v.y);
		tmp.push_back(v.z);
		tmp.push_back(v.w);
	}
#else
	std::vector<T> tmp(b.begin(), b.end());
#endif // 
	j = tmp;
}

std::future<bool> loadSetupFuture;

void loadArenaSetup(const bfs::path &name, bool useAsyncSetting)
{
	// we pack this up as an asycn job, because it takes bloody ages to load...
	if (!loadSetupFuture.valid() || loadSetupFuture.wait_for(std::chrono::seconds(0)) == std::future_status::ready)
	{
		auto job = [=]()
		{

			std::ifstream i(name.string());
			if (i.is_open())
			{
				json j;
				i >> j;

				g_arenaPlaneEq = j["arena_plane_eq"].get<float4>();
				g_arenaRadius = j["arena_radius"].get<float>();
				g_arenaCentre = j["arena_radius_centre"].get<float3>();

				updateArenaTransform();
			}
			return true;
		};
		if (useAsyncSetting)
		{
			loadSetupFuture = std::async(std::launch::async, job);
		}
		else
		{
			job();
		}
	}
}

std::future<bool> saveSetupFuture;

void saveArenaSetup(const bfs::path &name)
{
	// we pack this up as an asycn job, such that we can allow it to wait on both threads,
	// don't do if one is alerady in progress
	if (!saveSetupFuture.valid() || saveSetupFuture.wait_for(std::chrono::seconds(0)) == std::future_status::ready)
	{
		saveSetupFuture = std::async(std::launch::async, [=]()
		{
			json j;
			// 1. store arena radius and centre and plane eq
			// 2. store the depth distribution, if any.
			j["arena_plane_eq"] = g_arenaPlaneEq;
			j["arena_radius"] = g_arenaRadius;
			j["arena_radius_centre"] = g_arenaCentre;

			std::ofstream o(name.string());
			if (o.is_open())
			{
				//o << std::setw(4) << j;
				o << j;
			}
			return true;
		});
	}
}

inline void from_json(const nlohmann::json& j, TrackedObjectType &tt)
{
	//tt.id = j.at("id").get<std::string>();
	tt.templateLibraryName = j.at("template_library_name").get<std::string>();
	tt.objectClass = j.at("object_class").get<std::string>();  
}

std::map<std::string, TrackedObjectType> loadTrackedObjectTypes(const bfs::path &fileName)
{
	std::map<std::string, TrackedObjectType> types;
	std::ifstream i(fileName.string());
	if (i.is_open())
	{
		json j;
		i >> j;
		types = j.get<std::map<std::string, TrackedObjectType>>();
	}
	else
	{
		fprintf(stderr, "ERROR: Failed to load trackec object types from '%s', without-which there will be no objects to track!\n", fileName.string().c_str());
	}
	boost::filesystem::path templateLibsPath(g_envRootPath / "template_libs");
	for (const auto &t : types)
	{
		if (!bfs::is_directory(templateLibsPath / t.second.templateLibraryName))
		{
			fprintf(stderr, "WARNING: Missing template library '%s' in Tracked object type '%s' (file '%s)!\n", t.second.templateLibraryName.c_str(), t.first.c_str(), fileName.string().c_str());
		}
	}
	return types;
}

inline void from_json(const nlohmann::json& j, TrackedObjectTypeInstance &tti)
{
	//tt.id = j.at("id").get<std::string>();
	tti.name = j.at("name").get<std::string>();
	tti.typeId = j.at("type_id").get<std::string>();
	tti.displayColour = j.at("display_colour").get<float3>();
}

std::vector<TrackedObjectTypeInstance> loadTrackedObjectInstances(const bfs::path &fileName)
{
	std::vector<TrackedObjectTypeInstance> res;
	std::ifstream i(fileName.string());
	if (i.is_open())
	{
		json j;
		i >> j;
		res = j.get<std::vector<TrackedObjectTypeInstance>>();
	}
	return res;

}


struct DebugLayerUiInfo
{
	DebugLayerUiInfo(bool v = false, bool wf = false, bool svDef = true, const std::map<std::string, bool> &subVis = std::map<std::string, bool>()) :
		visible(v),
		wireFrame(wf),
		subItemVisibleDefault(svDef),
		subItemVisible(subVis)
	{}

	bool visible;
	bool wireFrame;
	bool subItemVisibleDefault;
	std::map<std::string, bool> subItemVisible;
};

static std::map<std::string, DebugLayerUiInfo> g_debugLayerUi;

void setParentsVisible(const std::string &_path)
{
	std::string path = _path;
	size_t offs = std::string::npos;
	while ((offs = path.rfind('/')) != std::string::npos)
	{
		path.resize(offs);

		if (path.size() > 1 && g_debugLayerUi.count(path))
		{
			g_debugLayerUi[path].visible = true;
		}
	}
}

static void setDebugUiChildrenVisible(const std::string &path, bool showSubItems)
{
	// First ensure the node is visible itself,
	setParentsVisible(path);
	// then propagate to children
	for (auto &item : g_debugLayerUi)
	{
		if (item.first.find(path) == 0)
		{
			item.second.visible = true;
			item.second.subItemVisibleDefault = true;
			if (showSubItems)
			{
				for (auto &si : item.second.subItemVisible)
				{
					si.second = true;
				}
			}
		}
	}
}




using NsDataPtr = DebugRenderer::NameSpaceDataPtr;

void showDebugDrawNamespaceUi(bool drawUi, std::string &path, const NsDataPtr &p, int subIndex, int numSiblings)
{
	std::stringstream ss;
	ss << p->id;
	ss << "[" << subIndex << "]";
	std::string id = ss.str();

	path += "/" + id;

	DebugLayerUiInfo &item = g_debugLayerUi[path];

	if (drawUi)
	{
		ImGui::PushID(id.c_str());
		const char *dispId = numSiblings > 1 ? id.c_str() : p->id.c_str();
		if (p->subSpaces.empty() && item.subItemVisible.empty())
		{
			ImGui::Selectable(dispId, false, ImGuiSelectableFlags_SpanAllColumns);
			ImGui::SetItemAllowOverlap();
			ImGui::NextColumn();
			ImGui::PushItemWidth(-1);
			if (ImGui::Checkbox("##lchech", &item.visible) && item.visible)
			{
				setParentsVisible(path);
			}
			ImGui::PopItemWidth();
			ImGui::NextColumn();
		}
		else
		{
			ImGui::AlignTextToFramePadding();  // Text and Tree nodes are less high than regular widgets, here we add vertical spacing to make the tree lines equal high.
			bool node_open = ImGui::TreeNodeEx(dispId, ImGuiTreeNodeFlags_SpanAllColumns | ImGuiTreeNodeFlags_DefaultOpen);
			ImGui::NextColumn();
			ImGui::AlignTextToFramePadding();
			if (ImGui::Checkbox("##lchech", &item.visible))
			{
				if (item.visible)
				{
					setParentsVisible(path);
				}
				// TODO: propagate to all children on set?
				//for (const auto &subN : subs)
				//{
				//	DebugLayerUiInfo &subInfo = g_debugLayerUi[subN];
				//	subInfo.visible = layerInfo.visible;
				//}
			}
			ImGui::NextColumn();
			if (node_open)
			{
				for (auto &si : item.subItemVisible)
				{
					ImGui::PushID(si.first.c_str());
					ImGui::Selectable(si.first.c_str(), false, ImGuiSelectableFlags_SpanAllColumns);
					ImGui::SetItemAllowOverlap();
					ImGui::NextColumn();
					ImGui::PushItemWidth(-1);
					if (ImGui::Checkbox("##lcheck", &si.second) && si.second)
					{
						item.visible = true;
						setParentsVisible(path);
					}
					ImGui::PopItemWidth();
					ImGui::NextColumn();
					ImGui::PopID();
				}
			}
			for (auto &ssGroup : p->subSpaces)
			{
				for (int i = 0; i < int(ssGroup.second.size()); ++i)
				{
					showDebugDrawNamespaceUi(node_open, path, ssGroup.second[i], i, int(ssGroup.second.size()));
				}
			}
			if (node_open)
			{
				ImGui::TreePop();
			}
		}
		ImGui::PopID();
	}
	else
	{
		for (auto &ssGroup : p->subSpaces)
		{
			for (int i = 0; i < int(ssGroup.second.size()); ++i)
			{
				showDebugDrawNamespaceUi(false, path, ssGroup.second[i], i, int(ssGroup.second.size()));
			}
		}
	}
	path.resize(path.size() - id.size() - 1);
};




void drawDebugRenderItems(std::string &path, DebugRenderer::Backend &be, const NsDataPtr &p, int subIndex, int numSiblings)
{
	std::stringstream ss;
	ss << p->id;
	ss << "[" << subIndex << "]";
	std::string id = ss.str();

	path += "/" + id;

	DebugLayerUiInfo &item = g_debugLayerUi[path];


	if (item.visible)
	{
		be.setSubIdFilter(&item.subItemVisible, item.subItemVisibleDefault);
		p->beginDraw(be);
		be.setSubIdFilter();
		for (auto &ssGroup : p->subSpaces)
		{
			for (int i = 0; i < int(ssGroup.second.size()); ++i)
			{
				drawDebugRenderItems(path, be, ssGroup.second[i], i, int(ssGroup.second.size()));
			}
		}
		be.setSubIdFilter(&item.subItemVisible, item.subItemVisibleDefault);
		p->endDraw(be);
		be.setSubIdFilter();
	}
	path.resize(path.size() - id.size() - 1);
};

void drawDebugRendererUiAndObjects(const float4x4 & viewMatrix, const float4x4 &projectionMatrix)
{
	DebugRenderer::MasterNamespace mns = DebugRenderer::instance().getMasterNamespace();

	if (g_showDebugRenderer)
	{
		ImGui::SetNextWindowSize(ImVec2(430, 450), ImGuiCond_FirstUseEver);
		if (ImGui::Begin("Debug Rendering", &g_showDebugRenderer))
		{
			ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(2, 2));
			ImGui::Columns(2);

			std::string path;

			for (auto &threadNameSpace : mns)
			{
				const std::string &id = threadNameSpace.first;
				path += "/" + id;
				DebugLayerUiInfo &item = g_debugLayerUi[path];

				ImGui::AlignTextToFramePadding();  // Text and Tree nodes are less high than regular widgets, here we add vertical spacing to make the tree lines equal high.
				bool node_open = ImGui::TreeNodeEx(id.c_str(), ImGuiTreeNodeFlags_SpanAllColumns | ImGuiTreeNodeFlags_DefaultOpen);
				if (ImGui::IsItemClicked())
				{
					item.visible = !item.visible;
				}
				ImGui::NextColumn();
				ImGui::AlignTextToFramePadding();
				ImGui::Checkbox("##lchech", &item.visible);
				ImGui::NextColumn();

				for (auto &ns : threadNameSpace.second)
				{
					showDebugDrawNamespaceUi(node_open, path, ns.second, 0, 1);
				}
				if (node_open)
				{
					ImGui::TreePop();
				}
				path.resize(path.size() - id.size() - 1);
			}
			ImGui::Columns(1);
			ImGui::PopStyleVar();
		}
		ImGui::End();
	}

	g_debugShader->begin();
	g_debugShader->setUniform("color", make_vector(0.2f, 0.3f, 0.4f, 0.8f));
	g_debugShader->setUniform("viewProjectionTransform", projectionMatrix * viewMatrix);
	g_debugShader->setUniform("normalMat", viewMatrix);
	g_debugShader->setUniform("worldUpDirection", make_vector(0.0f, 0.0f, -1.0f));
	{
		std::string path;
		DebugRenderer::Backend be(g_primitiveRenderer, g_debugShader);

		for (auto &threadNameSpace : mns)
		{
			const std::string &id = threadNameSpace.first;
			path += "/" + id;
			DebugLayerUiInfo &item = g_debugLayerUi[path];
			if (item.visible)
			{
				for (auto &ns : threadNameSpace.second)
				{
					drawDebugRenderItems(path, be, ns.second, 0, 1);
				}
			}
			path.resize(path.size() - id.size() - 1);
		}
	}
	g_debugShader->end();
}


void loadDebugRendererState(const bfs::path &optionsDir)
{
	std::ifstream i((optionsDir / "debug_ui.json").string());
	if (i.is_open())
	{
		json j;
		i >> j;

		for (auto it = j.begin(); it != j.end(); ++it)
		{
			g_debugLayerUi[it.key()] = { it.value()["visible"], it.value()["wireFrame"], it.value()["default_sub_vis"], it.value()["sub_items"] };
			//j[v.first] = { { "visible", v.second.visible },{ "wireFrame", v.second.wireFrame } };
		}
	}
}


void saveDebugRendererState(const bfs::path &optionsDir)
{
	std::ofstream o((optionsDir / "debug_ui.json").string());
	if (o.is_open())
	{
		json j;
		for (const auto &v : g_debugLayerUi)
		{
			j[v.first] = { { "visible", v.second.visible },{ "wireFrame", v.second.wireFrame },{ "default_sub_vis", v.second.subItemVisibleDefault },{ "sub_items", v.second.subItemVisible } };
		}
		o << std::setw(4) << j;
	}
}



