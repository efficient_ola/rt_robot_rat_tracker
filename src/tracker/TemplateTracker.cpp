/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#define _USE_MATH_DEFINES 1
#include "TemplateTracker.h"

#include <algorithm>
#include <numeric>
#include <list>

#include "utils/ChagIntersection.h"
#include "utils/DebugRenderer.h"
#include <Eigen/Dense>
#include "profiler/Profiler.h"
#include "Config.h"
#include "utils/TweakableVarImGui.h"
#include <boost/lexical_cast.hpp>
#include "TemplateLibrary.h"
#include "NearTree.h"
#include "utils/ChagJsonUtils.h"
#include <linmath/type_conversions.h>
#include "TemplateTrackingBackend.h"


#if ENABLE_TRACKER_ADAPTION_DEBUG_RENDERING
#define DBG_RENDERER_CMD_TA(_cmd_) DBG_RENDERER_CMD(_cmd_)
#else // !ENABLE_TRACKER_ADAPTION_DEBUG_RENDERING
#define DBG_RENDERER_CMD_TA(_cmd_)
#endif // ENABLE_TRACKER_ADAPTION_DEBUG_RENDERING



// TODO: move to debug renderer?
using namespace chag;


static void subDivide(std::vector<chag::float3> &dest, const chag::float3 &v0, const chag::float3 &v1, const chag::float3 &v2, int level)
{
	if (level)
	{
		chag::float3 v3 = normalize(v0 + v1);
		chag::float3 v4 = normalize(v1 + v2);
		chag::float3 v5 = normalize(v2 + v0);

		subDivide(dest, v0, v3, v5, level - 1);
		subDivide(dest, v3, v4, v5, level - 1);
		subDivide(dest, v3, v1, v4, level - 1);
		subDivide(dest, v5, v4, v2, level - 1);
	}
	else
	{
		dest.push_back(v0);
		dest.push_back(v1);
		dest.push_back(v2);
	}
}

std::vector<chag::float3> generateUnitHemiSphereTriangles(int subdivs)
{
	std::vector<chag::float3> dest;

	subDivide(dest, chag::make_vector<float>(-1, 1, 0), chag::make_vector<float>(1, 1, 0), chag::make_vector<float>(0, 0, 1), subdivs);
	subDivide(dest, chag::make_vector<float>(-1, 1, 0), chag::make_vector<float>(0, 0, 1), chag::make_vector<float>(-1, -1, 0), subdivs);

	subDivide(dest, chag::make_vector<float>(-1, -1, 0), chag::make_vector<float>(0, 0, 1), chag::make_vector<float>(1, -1, 0), subdivs);
	subDivide(dest, chag::make_vector<float>(1, -1, 0), chag::make_vector<float>(0, 0, 1), chag::make_vector<float>(1, 1, 0), subdivs);

	return dest;
}


void debugDrawCluster(DebugRenderer::Backend &be, const TemplateTracker::Cluster &c, chag::float4 colour, const std::string &layer, float scale = 1.0f)
{
	be.drawSphere(c.center, c.radius * scale, colour, layer);
	be.drawLine(c.center, c.center + c.normal * c.radius * 3.0f * scale, colour, layer);
}

void debugDrawCluster(DebugRenderer::Backend &be, const TemplateTrackingBackend::Cluster &c, chag::float4 colour, const std::string &layer, float scale = 1.0f)
{
	be.drawSphere(c.center, c.radius * scale, colour, layer);
	be.drawLine(c.center, c.center + c.normal * c.radius * 3.0f * scale, colour, layer);
}


TemplateTracker::TemplateTracker(bool forceCpu, const std::string &inputSourceId)
	: m_trackingBackend(0),
		m_inputSourceId(inputSourceId),
		m_inputSourceFrameNumber(-1)
{
	m_trackingBackend = TemplateTrackingBackend::create(forceCpu);
}


void TemplateTracker::TrackedObject::outputLogInfo(DataLogger& logger)
{
	logger.write("transform", m_objectToWorldTfm);
	logger.write("aabb", m_aabb);
	logger.write("active", m_active);
	logger.write("found", m_found);
}


void TemplateTracker::setDepthToCameraCoeffs(const std::vector<chag::float2>& coeffs, bool forceOverwrite)
{
	if (forceOverwrite || m_trackingBackend->m_depthToCameraSpaceCoeffs.empty())
	{
		m_trackingBackend->setDepthToCameraCoeffs(coeffs);
	}
}

void TemplateTracker::update(const RawFrameData &frame, const Params &params, int inputFrameNumber)
{
	using namespace chag;
	// if the previous frame was the same, then we're paused (most likely)
	bool paused = inputFrameNumber != -1 && m_inputSourceFrameNumber == inputFrameNumber;

	m_inputSourceFrameNumber = inputFrameNumber;
	m_params = params;
	m_trackingBackend->setParams(params);
	m_trackingBackend->preProcessFrame(frame);

#if ENABLE_TRACKER_DEBUG_RENDERING


	if (m_trackingBackend->isCuda())
	{
		m_trackingBackend->m_frameClusters.reflectToHost();
	}
	
	{
		DBG_RENDERER_OBJECT_TR("AllClusters", [clusters= m_trackingBackend->m_frameClusters](DebugRenderer::Backend &be) 
		{
			for (const auto &c : clusters)
			{
				be.drawSphere(c.center, c.radius, DebugRenderer::random);
			}
		});
	}
#endif // ENABLE_TRACKER_DEBUG_RENDERING
	DBG_RENDERER_OBJECT_SCOPE("tracked_objects");

	for (auto it = m_objects.begin(); it != m_objects.end(); ++it)
	{
		TrackedObjectPtr o = (*it).second;
		o->update(this);
	}
}

// used by NearTree...
inline float distToAabbSq(const float3& pt, const Aabb& aabb)
{
	float3 aabbPt = closestPointOnAabb(aabb, pt);
	return lengthSquared(aabbPt - pt);
}

struct ClusterAdaptor
{
	static const float3 &position(const std::vector<TemplateTracker::Cluster> &p, int i) { return p[i].center; }
};


inline float weight(float r, float h)
{
	float r2 = (r*r) / (h * h);
	if (r2 < 1.0f)
	{
		return powf(1.0f - r2, 4.0f);

	}
	return 0.0f;
}



template <int K, typename KNN_FN_T>
bool projectApssKnn(const float3 &x, KNN_FN_T knnFn, float3 &projectedPt, float3 &projectedNormal, const int maxIters = 5, float beta = 0.5f)
{
	//const float beta = 0.1f;// 9f;
	float3 q = x;
	float3 n = projectedNormal;
	float dist = std::numeric_limits<float>::max();
	const float eps = 0.00001f;
	std::pair<float3, float3> knn[K];
	for (int k = 0; k < maxIters && (dist > eps); ++k)
	{
		std::pair<float3, float3> knn[K];
		//int knn[K];
		int nnCount = knnFn(x, knn);

		float totalWeight = 0.0f;
		float a = 0.0f;
		float3 c = { 0.0f, 0.0f, 0.0f };
		float d = 0.0f;
		float3 f = { 0.0f, 0.0f, 0.0f };


		float radius = 0.0f;
		for (size_t i = 0; i < nnCount; ++i)
		{
			const std::pair<float3, float3> &pointDir = knn[i];
			radius = std::max(radius, length(x - pointDir.first));
		}
		radius *= 1.01f;


		for (size_t i = 0; i < nnCount; ++i)
		{
			const std::pair<float3, float3> &pointDir = knn[i];
			//const TemplateTracker::TrackedMultiTemplateObject::TemplateDisc &td = templ.m_sampleDiscs[i];
			//float w = weight(length(q - pointDir.first));
			float w = weight(length(q - pointDir.first), radius);

			a += w * dot(pointDir.first, pointDir.second);
			c += pointDir.second * w;
			d += w * dot(pointDir.first, pointDir.first);
			f += pointDir.first * w;

			totalWeight += w;
		}
		float3 b = f / totalWeight;
		float3 e = b;


		float u4 = beta * 0.5f * (a - dot(b, c)) / (d - dot(e, f));
		float3 u123 = c / totalWeight - b * 2.0f * u4;

		float u0 = -dot(u123, b) - u4 * d / totalWeight;
		// 3. get spherical projection of x
		float3 sphereCentre = u123 * -0.5 / u4;
		float3 sphereN = normalize(x - sphereCentre);

		float r = sqrtf(dot(sphereCentre, sphereCentre) - u0 / u4);
		n = normalize(sphereN);
		if (dot(c, n) < 0.0f)
		{
			n = -n;
		}
		float3 qN = sphereCentre + sphereN * r;

		dist = length(qN - q);
		q = qN;
	}

	projectedNormal = n;
	projectedPt = q;
	return dist < eps;
}



float unsignedDistToDiscPlane(const float3 &pt, const TemplateTracker::TrackedMultiTemplateObject::TemplateDisc &td)
{
	return fabsf(dot(pt - td.position, td.direction));
}


template <typename PROJ_TO_SURFACE_FN_T>
float3 projPt(const float3 &pt, PROJ_TO_SURFACE_FN_T surfProj)
{
	float3 pp = pt;
	float3 pn = pt;
	surfProj(pt, pp, pn);
	return pp;
}

template <typename PROJ_TO_SURFACE_FN_T>
const float discMaxDist(const TemplateTracker::TrackedMultiTemplateObject::TemplateDisc &td, PROJ_TO_SURFACE_FN_T surfProj)
{
	const float3 tangent = normalize(perpendicular(td.direction));
	const float3 biTangent = cross(tangent, td.direction);

	// TODO: could sample more points aroun perimiter?
	return std::max(
		std::max(unsignedDistToDiscPlane(projPt(td.position + tangent * td.radius, surfProj), td),
			unsignedDistToDiscPlane(projPt(td.position - tangent * td.radius, surfProj), td)),
		std::max(unsignedDistToDiscPlane(projPt(td.position + biTangent * td.radius, surfProj), td),
			unsignedDistToDiscPlane(projPt(td.position - biTangent * td.radius, surfProj), td)));
}

template <typename PROJ_TO_SURFACE_FN_T>
TemplateTracker::TrackedMultiTemplateObject::TemplateDisc fitDisc(float discRadius, float discMinRadius, const float3 &samplePt, PROJ_TO_SURFACE_FN_T surfProj, float &maxDist)
{
	float3 projPt = samplePt;
	float3 projNormal = normalize(samplePt);
	surfProj(samplePt, projPt, projNormal);

	TemplateTracker::TrackedMultiTemplateObject::TemplateDisc res;
	res.direction = projNormal;
	res.position = projPt;
	res.radius = discRadius;
	
	// Also completely arbitrary, controls slope... somehow
	const float epsilon = discMinRadius / 2.0f;

	// Next we'll sample the extremities of the disc to see if the fit is ok wrt curvature, if distance is too big shrink disc until ok, or min reached
	while ((maxDist = discMaxDist(res, surfProj)) > epsilon && fabsf(res.radius - discMinRadius) > 0.00001f)
	{
		res.radius = lerp(res.radius, discMinRadius, 0.1f);
	}
	
	return res;
}

template <typename PROJ_TO_SURFACE_FN_T>
TemplateTracker::TrackedMultiTemplateObject::TemplateDisc fitDisc(float discRadius, float discMinRadius, const float3 &samplePt, PROJ_TO_SURFACE_FN_T surfProj)
{
	float maxDist = 0.0f;
	return fitDisc(discRadius, discMinRadius, samplePt, surfProj, maxDist);
}


template <typename T>
inline void debugDrawDisc(DebugRenderer::Backend &be, const T &d, const float4 &colour, const std::string &layer)
{
	be.drawLine(d.position, d.position + d.direction * d.radius, colour, layer);
	be.drawDisc(d.position, d.direction, d.radius, colour, layer);
}

// returns the angle in [0,2PI] (otherwise just atan2f(dir.y, dir.x)
template <typename T>
const float dirToAngle2Pi(const T &dir)
{
	float ang = atan2f(dir.y, dir.x);
	// map to [0,2PI]
	if (ang < 0.0f)
	{
		ang += g_pi * 2.0f;
	}
	return ang;
}
struct DiscGraphNode
{
	TemplateTracker::TrackedMultiTemplateObject::TemplateDisc disc;
	std::vector<int> edges;
	int depth;

	struct Space
	{
		float3 normal; // z
		float3 tangent; // y
		float3 biTangent; // x
		float3 position;

		float3 project(const float3 &pt) const
		{
			return projectLocal(pt - position);
		};
		float3 projectLocal(const float3 &lp) const
		{
			return make_vector(dot(lp, biTangent), dot(lp, tangent), dot(lp, normal));
		};
		float3 unProject(const float2 &localSpacePt) const
		{
			return position + localSpacePt.x * biTangent + localSpacePt.y * tangent;
		};
		float3 unProject(const float3 &localSpacePt) const
		{
			return position + localSpacePt.x * biTangent + localSpacePt.y * tangent + localSpacePt.z * normal;
		};
	};
	struct EdgeMap
	{
		std::vector<float2> angleIntervals;

		void build(const DiscGraphNode &n, const Space &nSpace, const std::vector<DiscGraphNode> &nodes, float angleStep)
		{
			for (int edgeInd : n.edges)
			{
				float3 lDir = nSpace.project(nodes[edgeInd].disc.position);
				float ang = dirToAngle2Pi(lDir);
				float2 tmp = make_vector(cosf(ang), sinf(ang));
				angleIntervals.push_back(make_vector(ang, angleStep / 2.0f));
			}
			std::sort(angleIntervals.begin(), angleIntervals.end(), [&](const float2 & a, const float2 &b) { return a.x < b.x; });
		}
		void add(float2 interval)
		{
			angleIntervals.push_back(interval);
			std::sort(angleIntervals.begin(), angleIntervals.end(), [&](const float2 & a, const float2 &b) { return a.x < b.x; });
		}

		int size() const { return int(angleIntervals.size()); }

		const float2 &get(int ai)
		{
			return angleIntervals[ai % int(angleIntervals.size())];
		}
	};

	Space makeSpace()
	{
		Space s;
		const float3 up = { 0.0f, 0.0f, 1.0f };
		s.normal = normalize(disc.direction);
		s.biTangent = normalize(cross(up, s.normal));
		if (lengthSquared(s.biTangent) < 0.000001f)
		{
			s.biTangent = perpendicular(s.normal);
		}
		// tangent AKA x-axis
		s.tangent = cross(s.biTangent, s.normal);
		s.position = disc.position;
		return s;
	}

};

// helper 
struct LocalNodeInfo
{
	int index;
	float radius;
	float3 localPos;
	float angleDir;
	float2 angleInterval;
	float distance2D;
	// 
	void init(int ind, const DiscGraphNode::Space &space, const std::vector<DiscGraphNode> &nodes)
	{
		index = ind;
		update(space, nodes);
	}

	void update(const DiscGraphNode::Space &space, const std::vector<DiscGraphNode> &nodes)
	{
		const DiscGraphNode &n = nodes[index];
		radius = n.disc.radius;
		localPos = space.project(n.disc.position);
		distance2D = length(make_vector(localPos.x, localPos.y));
		angleInterval.x = dirToAngle2Pi(localPos);
		// angle subtended by disc
		angleInterval.y = asinf(radius / distance2D);
	}
};




/**
 * So this is a bit of a beast of a function... Key goals: fit discs that belong to a tracked object that is now in a different pose. Be able to do this even if the object is not convex. Not get too much background included.
 * Assumptions: a previously converged tracked position. 
 * Input: world-space points and normals.
 * The basic idea is to start somewhere (the centre of the AABB of previous match) and walk the implicit surface defined by the point cloud until some terminating criteria is met.
 * During the walk discs are added incrementally. There are a whole host of ad-hoc termination criteria, each of which is tweakable.
 * If I were to re-do this I'd probably simplify a lot and make it more interactive instead.
 * Note that this all runs on the CPU as this is a user-triggered once-in-a-while opertion during setup.
 */
template <typename CONTAINER_T>
void adaptTemplate(const float4x4 &templateToArenaTfm, const float4x4 &arenaToWorldTfm, TemplateTracker::TrackedMultiTemplateObject::DiscTemplate &templ, const CONTAINER_T &pointsWorldSpace, const CONTAINER_T &normalsWorldSpace, bool updateTemplate, float arenaRadius)
{
	using namespace chag::type_conversions;

	int debugDiscInd = DYNAMIC_TWEAKABLEVAR_UI(int, "debug_sample_disc_index", -1, Input(-1));
	int debugEdgesDiscInd = DYNAMIC_TWEAKABLEVAR_UI(int, "TemplateAdaption/debug_edges_disc_index", -1, Slider(-1, 256));
	int debugEdgesDiscChildInd = DYNAMIC_TWEAKABLEVAR_UI(int, "TemplateAdaption/debug_edges_child_index", 0, Slider(0, 6));

	float4x4 templateToWorldTfm = arenaToWorldTfm * templateToArenaTfm;
	float4x4 worldToTemplateSpaceTfm = inverse(templateToWorldTfm);
	using Cluster = TemplateTracker::Cluster;
	using Disc = TemplateTracker::TrackedMultiTemplateObject::TemplateDisc;


	Aabb prevAabb = make_aabb(int(templ.m_sampleDiscs.size()), [&](int i) { return templ.m_sampleDiscs[i].position; });
	// create a 25% larger aabb, remove anything outside this...
	float3 aabbExp = prevAabb.getDiagonal() * 0.25f;
	Aabb maxAabb = prevAabb;
	maxAabb.min -= aabbExp;
	maxAabb.max += aabbExp;
	// quadruple height, we keep having issues here...
	maxAabb.max.z *= 4.0f;

	// Begin a compound object which contains other objects and 
	DBG_RENDERER_OBJECT_SCOPE("AdaptTemplate", [=] (DebugRenderer::Backend &be){
		be.pushTransform(templateToWorldTfm);
		be.drawAabb(maxAabb, DebugRenderer::red * 0.5f, "adapt_aabb");
	});

	// manufacture clusters from the input point set, note that the radius is totally made up...
	std::vector<Cluster> inliers;
	ASSERT(pointsWorldSpace.size() == pointsWorldSpace.size());

	for (int ci = 0; ci < int(pointsWorldSpace.size()); ++ci)
	{
		//const TemplateTracker::Cluster &c = clusters[ci];
		const float4 &p = pointsWorldSpace[ci];
		const float4 &n = normalsWorldSpace[ci];

		// filter out invalid points & failed normals, we can't deal with these... need to run a normal reconstruction phase first in that case...
		if (p.w <= 0.0f || n.w <= 0.0f || length(n) < 0.00001f)
		{
			continue;
		}

		// transform to template space
		TemplateTracker::Cluster tc;
		tc.radius = 0.001f;
		tc.center = transformPoint(worldToTemplateSpaceTfm, f3(p));
		tc.normal = transformDirection(worldToTemplateSpaceTfm, f3(n));
		tc.aabb = make_aabb(tc.center, tc.radius);
		tc.debugColorId = ci;

		// skip points outside of the extended aabb
		if (!inside(maxAabb, tc.center))
		{
			continue;
		}
		inliers.push_back(tc);
	}
	
	// too few points, never going to work
	if (inliers.size() < 16)
	{
		return;
	}

	// To be able to perform reasonaly efficient knn-queries on the points we build a BVH.
	using Tree = NearTree<ClusterAdaptor>;
	Tree tree;
	tree.build(inliers);

	// now project the template points onto the surface of the point cloud.
	float beta = DYNAMIC_TWEAKABLEVAR_UI(float, "TemplateAdaption/beta", 0.5f, Slider(0.0f, 1.0f));

	// function that is used to sample the implicit surface of the point-cloud, uses Apss to reconstruct the surface between the sample points.
	// This helps fill gaps in a meaningful way, but also introduces some error and might not be the totally best solution.
	auto sampleFn = [&](const float3 &samplePt, float3 &projPt, float3 &projNormal) -> bool
	{
		// Need to restate K several times because lambda capture and constants are not friends it seems.
		constexpr int K = 16;
		return projectApssKnn<K>(samplePt, [&](const float3 &pt, std::pair<float3, float3> knn[16]) 
		{ 
			int knnInds[16];
			int nns = tree.findKnn<16>(pt, inliers, knnInds);

			for (int i = 0; i < nns; ++i)
			{
				knn[i] = std::make_pair(inliers[knnInds[i]].center, inliers[knnInds[i]].normal);
			}
			return nns;
		}, projPt, projNormal, 64, beta);
	};

	std::vector<Disc> newDiscs;
	{
		// TODO: move these to own name-group and hide that by default in the UI...
		float discRadius = DYNAMIC_TWEAKABLEVAR_UI(float, "TemplateAdaption/discRadius", 0.010f, Slider(0.0f, 0.1f));
		float discMinRadiusScale = DYNAMIC_TWEAKABLEVAR_UI(float, "TemplateAdaption/discMinRadiusScale", 0.75f, Slider(0.0f, 1.0f));

		const float angleCutoff = DYNAMIC_TWEAKABLEVAR_UI(float, "TemplateAdaption/angleCutoff", -0.33f, Slider(-1.0f, 1.0f));
		const float slopeCutoff = DYNAMIC_TWEAKABLEVAR_UI(float, "TemplateAdaption/slopeCutoff", -0.5f, Slider(-2.0f, 2.0f));

		const float floorCutoffDist = DYNAMIC_TWEAKABLEVAR_UI(float, "TemplateAdaption/floorCutoffDist", 0.05f, Slider(0.0f, 0.1f));
		const float floorSimThreshold = DYNAMIC_TWEAKABLEVAR_UI(float, "TemplateAdaption/floorSimThreshold", 0.5f, Slider(0.0f, 1.0f));

		const float wallCutoffDist = DYNAMIC_TWEAKABLEVAR_UI(float, "TemplateAdaption/wallCutoffDist", 0.05f, Slider(0.0f, 0.2f));
		const float wallSimThreshold = DYNAMIC_TWEAKABLEVAR_UI(float, "TemplateAdaption/wallSimThreshold", 0.5f, Slider(0.0f, 1.0f));

		const int maxDepth = DYNAMIC_TWEAKABLEVAR_UI(int, "TemplateAdaption/maxDepth", 15, Slider(0, 30));
		const int pushPullIters = DYNAMIC_TWEAKABLEVAR_UI(int, "TemplateAdaption/pushPullIters", 5, Slider(0, 30));

		const float edgeLinkAngleCutoff = DYNAMIC_TWEAKABLEVAR_UI(float, "TemplateAdaption/edgeLinkAngleCutoff", 0.0f, Slider(-1.0f, 1.0f));
		const float edgeLinkDepthCutoffScale = DYNAMIC_TWEAKABLEVAR_UI(float, "TemplateAdaption/edgeLinkDepthCutoffScale", 0.75f, Slider(0.01f, 2.0f));
		const float edgeLinkDistCutoffScale = DYNAMIC_TWEAKABLEVAR_UI(float, "TemplateAdaption/edgeLinkDistCutoffScale", 1.75f, Slider(1.0f, 3.0f));

		const float discMinRadius = discRadius * discMinRadiusScale; // arbitrary!

		std::vector<Cluster> filteredInliers;

		for (Cluster c : inliers)
		{
			float3 pp = c.center;
			float3 pn = c.normal;
			if (DYNAMIC_TWEAKABLEVAR_UI(bool, "TemplateAdaption/smooth_input", false, Toggle()))
			{
				sampleFn(c.center, pp, pn);
			}
			// Cut off for flooriness
			float3 arenaPt = transformPoint(templateToArenaTfm, pp);
			float3 arenaDir = transformDirection(templateToArenaTfm, pn);
			float normDist = std::max(0.0f, 1.0f - arenaPt.z / floorCutoffDist);
			float angDist = arenaDir.z;
			Cluster sc = c;
			sc.center = pp;
			sc.normal = pn;

			float3 arenaPt2d = make_vector(arenaPt.x, arenaPt.y, 0.0f);

			// signed distance to wall, negative on the outside
			float wallDist = arenaRadius - length(arenaPt2d);
			float normDistWall = std::max(0.0f, 1.0f - wallDist / wallCutoffDist);
			float angDistWall = std::max(0.0f, -dot(normalize(arenaPt2d), arenaDir));

			if (arenaPt.z < 0.0f || normDist * angDist > floorSimThreshold)
			//if (arenaPt.z < 0.0f || normDist > floorSimThreshold)
			{
				PROFILE_COUNTER("floor", 1);
				DBG_RENDERER_CMD([=](DebugRenderer::Backend &be) {
					debugDrawCluster(be, sc, DebugRenderer::randomColor(0, 0.8f), "floor");
				});
			}
			else if (wallDist < 0.0f || normDistWall * angDistWall > wallSimThreshold)
			{
				PROFILE_COUNTER("wall", 1);
				DBG_RENDERER_CMD([=](DebugRenderer::Backend &be) {
					debugDrawCluster(be, sc, DebugRenderer::randomColor(2, 0.8f), "wall");
				});
			}
			else
			{
				PROFILE_COUNTER("un_floor", 1);
				DBG_RENDERER_CMD([=](DebugRenderer::Backend &be) {
					debugDrawCluster(be, sc, DebugRenderer::randomColor(1, 0.8f), "un_floor");
				});
				filteredInliers.push_back(sc);
			}
		}

		if (DYNAMIC_TWEAKABLEVAR_UI(bool, "TemplateAdaption/filter_input", false, Toggle()))
		{
			// Re-build tree with just the non-floor and non-wall points
			inliers = filteredInliers;
			tree.build(inliers);
		}

		std::vector<DiscGraphNode> nodes;

		// create and fit the root node disc.
		DiscGraphNode rn;
		rn.disc = fitDisc(discRadius, discMinRadius, templ.m_aabb.getCentre() + make_vector(0.0f, 0.0f, templ.m_aabb.getHalfSize().z), sampleFn);
		rn.depth = 0;

		nodes.push_back(rn);

		// start breadth first traversal
		std::list<int> jobQueue;
		jobQueue.push_back(0);
		while (!jobQueue.empty())
		{
			const int currentItem = *jobQueue.begin();
			DiscGraphNode &n = nodes[currentItem];

			// Stop adding discs at some depth cutoff from the root disc.
			if (n.depth >= maxDepth)
			{
				jobQueue.pop_front();
				continue;
			}
			jobQueue.pop_front();

			DiscGraphNode::Space nSpace = n.makeSpace();
			DBG_RENDERER_CMD_TA([=](DebugRenderer::Backend &be) {
				be.drawLine(nSpace.position, nSpace.position + nSpace.tangent * n.disc.radius * 2.0f, DebugRenderer::red, "tangents");
				be.drawLine(nSpace.position, nSpace.position + nSpace.biTangent * n.disc.radius * 2.0f, DebugRenderer::green, "tangents");
				be.drawLine(nSpace.position, nSpace.position + nSpace.normal * n.disc.radius * 2.0f, DebugRenderer::blue, "tangents");
			});

			//
			// TODO: [ensure the edges are always sorted in local] increasing angle order, then just find the largest gap and if it is big enough, plonk the disc at one end.
			//       having sorted edges is probably going to be useful for other queries.
			//

			// Possibly over-complicated code to find next free neighbouring slot around the edge of the disc
			const float angleStep = 2.0f * asinf(0.5f);
			std::vector<float> candidateAngles;
			if (n.edges.empty())
			{
				candidateAngles = { 0.0f, angleStep, angleStep * 2.0f, angleStep * 3.0f, angleStep * 4.0f, angleStep *5.0f };
			}
			else
			{
				DiscGraphNode::EdgeMap edgeMap;
				edgeMap.build(n, nSpace, nodes, angleStep);

				// next spin around and add neighbours 60 degrees, starting from an arbitrary one
				float startAngle = 0.0f;

				for (int a = 0; a < 6 && candidateAngles.size() < 6; ++a)
				{
					// find next large-enough gap
					for (int ai = 0; ai < edgeMap.size(); ++ai)
					{
						const float2 &a = edgeMap.get(ai);
						const float2 &b = edgeMap.get(ai + 1);
						// wrapt into positive space...
						float aUpper = a.x + a.y;
						float bLower = b.x - b.y;
						if (ai + 1 == edgeMap.size())
						{
							bLower += g_pi * 2.0f;
						}
						// found a gap.
						if (bLower - aUpper > angleStep * 0.8f)
						{
							float ang = fmodf(aUpper + angleStep * 0.5f, g_pi * 2.0f);
							edgeMap.add(make_vector(ang, angleStep * 0.5f));
							candidateAngles.push_back(ang);
							break;
						}
					}
				}
			}
#if ENABLE_TRACKER_ADAPTION_DEBUG_RENDERING
			if (currentItem == debugEdgesDiscInd)
			{
				DBG_RENDERER_CMD_TA([=](DebugRenderer::Backend &be) {
					debugDrawDisc(be, n.disc, DebugRenderer::randomColor(n.depth, 1.0f), "parent_discs");
				});
			}
#endif// ENABLE_TRACKER_ADAPTION_DEBUG_RENDERING
			
			// attempt to add up to 6 discs
			for (int ai = 0; ai < candidateAngles.size(); ++ai)
			{
				float ang = candidateAngles[ai];
				// re-get reference to n since the nodes vector changes... icky stuff
				DiscGraphNode &n = nodes[currentItem];

				// step along the local space angle direction
				float2 localOffset = make_vector(cosf(ang), sinf(ang)) * discRadius * 2.0f;

#if ENABLE_TRACKER_ADAPTION_DEBUG_RENDERING
				if (debugDiscInd == currentItem)
				{
					DBG_RENDERER_CMD_TA([=](DebugRenderer::Backend &be) {
						be.drawLine(n.disc.position, n.disc.position + localOffset.x * nSpace.biTangent + localOffset.y * nSpace.tangent, DebugRenderer::randomColor(n.depth, 0.8f), "localOffsets");
					});
				}
#endif// ENABLE_TRACKER_ADAPTION_DEBUG_RENDERING

				// we have a candidate position in 'localOffset' now go through with the usual fitting....
				float3 newPos = nSpace.unProject(localOffset);
				float maxDistToDisc = 0.0f;

				// project this new position to the implicit surface.
				Disc newD = fitDisc(discRadius, discMinRadius, newPos, sampleFn, maxDistToDisc);

				// This is a measure of how curved the surface is under the disc.
				if (maxDistToDisc > discMinRadius * 0.5f)
				{
					DBG_RENDERER_CMD_TA([=](DebugRenderer::Backend &be) {
						debugDrawDisc(be, newD, DebugRenderer::randomColor(nodes.size(), 0.8f), "unfit_discarded_discs");
					});
					continue;
				}
				// terminate if outside the max aabb.
				if (!inside(maxAabb, newD.position))
				{
					DBG_RENDERER_CMD_TA([=](DebugRenderer::Backend &be) {
						debugDrawDisc(be, newD, DebugRenderer::randomColor(nodes.size(), 0.8f), "outside_aabb_discarded_discs");
					});
					continue;
				}

				// 
				DiscGraphNode newNode;
				newNode.disc = newD;
				newNode.depth = n.depth + 1;
				newNode.edges.push_back(currentItem);

				const float neighbourhoodMaxDistance = discRadius * 3.0f;

				bool showEdgeDebug = currentItem == debugEdgesDiscInd && ai == debugEdgesDiscChildInd;

				// 1. construct 2D neighbourhood and move new point into place, using weights
				DiscGraphNode::Space newSpace = newNode.makeSpace();
				std::vector<LocalNodeInfo> neighbourhood;
				bool discardOverlap = false;
				bool discardCurv = false;
#if ENABLE_TRACKER_ADAPTION_DEBUG_RENDERING
				if (showEdgeDebug)
				{
					DBG_RENDERER_CMD_TA([=](DebugRenderer::Backend &be) {
						be.drawSphere(newD.position, neighbourhoodMaxDistance, DebugRenderer::randomColor(int(nodes.size()), 0.75f), "neighbour_cutoff");
						debugDrawDisc(be, newD, DebugRenderer::randomColor(newNode.depth, 1.0f), "candidate_discs");
					});
				}
#endif// ENABLE_TRACKER_ADAPTION_DEBUG_RENDERING

				bool discardAng = false;

				float totalAngle = 0.0f;
				float totalAngleWeight = 0.0f;

				for (int i = 0; i < int(nodes.size()); ++i)
				{
					// const because we'll add link to other node later if the new one is accepted
					const DiscGraphNode &n2 = nodes[i];

					// skip those too far away
					if (lengthSquared(n2.disc.position - newD.position) < square(neighbourhoodMaxDistance))
					{
						// perform local space checks to ensure it is a nearby node:

						// skip those too far away
						if (lengthSquared(n2.disc.position - newD.position) > square(discRadius * 3.0f))
						{
#if ENABLE_TRACKER_ADAPTION_DEBUG_RENDERING
							if (showEdgeDebug)
							{
								DBG_RENDERER_CMD_TA([=](DebugRenderer::Backend &be) {
									be.drawSphere(n2.disc.position, discRadius * 0.3f, DebugRenderer::randomColor(i), "edge_test_too_far");
								});
							}
#endif// ENABLE_TRACKER_ADAPTION_DEBUG_RENDERING
							continue;
						}

						// 4. check angle tolerange
						if (dot(newD.direction, n2.disc.direction) < edgeLinkAngleCutoff)
						{
#if ENABLE_TRACKER_ADAPTION_DEBUG_RENDERING
							if (showEdgeDebug)
							{
								DBG_RENDERER_CMD_TA([=](DebugRenderer::Backend &be) {
									be.drawSphere(n2.disc.position, discRadius * 0.3f, DebugRenderer::randomColor(i), "edge_test_angle_cutoff");
								});
							}
#endif// ENABLE_TRACKER_ADAPTION_DEBUG_RENDERING
							continue;
						}

						// Create point on other disc edge closest to the new disc
						float3 v = newD.position - n2.disc.position;
						float3 ep = n2.disc.position + normalize(cross(cross(-v, n2.disc.direction), n2.disc.direction)) * n2.disc.radius;
						// 2. project into local space
						float3 localEp = newSpace.project(ep);
						float3 lPos = newSpace.project(n2.disc.position);

#if ENABLE_TRACKER_ADAPTION_DEBUG_RENDERING
						if (showEdgeDebug)
						{
							DBG_RENDERER_CMD_TA([=](DebugRenderer::Backend &be) {
								float3 pp = newSpace.unProject(make_vector(localEp.x, localEp.y, 0.0f));
								be.drawSphere(pp, discRadius * 0.05f, DebugRenderer::randomColor(i), "edge_test_proj");
								be.drawLine(pp, n2.disc.position, DebugRenderer::randomColor(i), "edge_test_proj");

								float3 pp2 = newSpace.unProject(make_vector(lPos.x, lPos.y, 0.0f));
								be.drawSphere(pp2, discRadius * 0.05f, DebugRenderer::randomColor(i), "centre_proj");
								be.drawLine(pp2, n2.disc.position, DebugRenderer::randomColor(i), "centre_proj");

								be.drawSphere(ep, discRadius * 0.05f, DebugRenderer::randomColor(i), "edge_test_point");
								be.drawLine(ep, n2.disc.position, DebugRenderer::randomColor(i), "edge_test_point");
							});
						}
#endif// ENABLE_TRACKER_ADAPTION_DEBUG_RENDERING

						// 3. check height tolerance
						if (std::abs(localEp.z) > discRadius * edgeLinkDepthCutoffScale)
						{
#if ENABLE_TRACKER_ADAPTION_DEBUG_RENDERING
							if (showEdgeDebug)
							{
								DBG_RENDERER_CMD_TA([=](DebugRenderer::Backend &be) {
									float3 pp = newSpace.unProject(make_vector(localEp.x, localEp.y, 0.0f));
									be.drawSphere(pp, discRadius * 0.05f, DebugRenderer::randomColor(i), "edge_test_proj_depth");
									be.drawLine(pp, n2.disc.position, DebugRenderer::randomColor(i), "edge_test_proj_depth");
								});
							}
#endif// ENABLE_TRACKER_ADAPTION_DEBUG_RENDERING
							continue;
						}
						// 5. check the distance in 2D
						const float dist2DSq  = lengthSquared(make_vector(localEp.x, localEp.y));
						if (dist2DSq > square(discRadius * edgeLinkDistCutoffScale))
						{
#if ENABLE_TRACKER_ADAPTION_DEBUG_RENDERING
							if (showEdgeDebug)
							{
								DBG_RENDERER_CMD_TA([=](DebugRenderer::Backend &be) {
									float3 pp = newSpace.unProject(make_vector(localEp.x, localEp.y, 0.0f));
									be.drawSphere(pp, discRadius * 0.05f, DebugRenderer::randomColor(i), "edge_test_proj_radius");
									be.drawLine(pp, n2.disc.position, DebugRenderer::randomColor(i), "edge_test_proj_radius");
								});
							}
#endif// ENABLE_TRACKER_ADAPTION_DEBUG_RENDERING
							continue;
						}

#if ENABLE_TRACKER_ADAPTION_DEBUG_RENDERING
						if (showEdgeDebug)
						{
							DBG_RENDERER_CMD_TA([=](DebugRenderer::Backend &be) {
								debugDrawDisc(be, n2.disc, DebugRenderer::randomColor(n2.depth, 1.0f), "neighbour_discs");
							});
						}
#endif// ENABLE_TRACKER_ADAPTION_DEBUG_RENDERING

						// Neighbour is ok, now check if it overlaps the new node too much
						if (lengthSquared(lPos) < square(discRadius) || dist2DSq < square(discRadius * 0.5f))
						{
							discardOverlap = true;
							break;
						}


						const float3 pn = normalize(newD.position - n2.disc.position);
						if (-dot(n2.disc.direction, pn) + dot(newD.direction, pn) < slopeCutoff)
						{
							discardCurv = true;
							break;
						}

						float w = weight(length(newD.position - n2.disc.position), newD.radius * 3.0f);
						totalAngle += dot(n2.disc.direction, newD.direction) * w;
						totalAngleWeight += w;

						LocalNodeInfo ni;
						ni.init(i, newSpace, nodes);
						neighbourhood.push_back(ni);
					}
				}

				// 3. discard if not a good node...
				if (discardOverlap)
				{
					if (debugDiscInd == currentItem)
					{
						DBG_RENDERER_CMD_TA([=](DebugRenderer::Backend &be) {
							debugDrawDisc(be, newD, DebugRenderer::randomColor(newNode.depth, 1.0f), "rejected_neighbour_discs");
						});
					}

					continue;
				}


				if (discardCurv)
				{
					DBG_RENDERER_CMD_TA([=](DebugRenderer::Backend &be) {
						debugDrawDisc(be, newD, DebugRenderer::randomColor(newNode.depth, 0.8f), "slope_discarded_discs");
					});
					continue;
				}
				if (discardAng)
				{
					DBG_RENDERER_CMD_TA([=](DebugRenderer::Backend &be) {
						debugDrawDisc(be, newD, DebugRenderer::randomColor(newDiscs.size(), 0.8f), "angle_discarded_discs");
					});
					continue;
				}
				if (totalAngle / totalAngleWeight <= angleCutoff)
				{
					DBG_RENDERER_CMD_TA([=](DebugRenderer::Backend &be) {
						debugDrawDisc(be, newD, DebugRenderer::randomColor(newDiscs.size(), 0.8f), "angle_discarded_discs");
					});
					continue;
				}

				// Cut off for inconsistency - gather points in sphere around disc and calculate mean deviation for plane and mean deviation from normal
				std::vector<int> points = tree.findInRange(newD.position, newD.radius * 1.5f, inliers);
				if (!points.empty())
				{
					float meanDist = 0.0f;
					float distVar = 0.0f;
					float meanDir = 0.0f;
					float dirVar = 0.0f;
					for (int pInd : points)
					{
						const Cluster &c = inliers[pInd];
						distVar += square(newSpace.project(c.center).z);

						dirVar += square(1.0f - dot(c.normal, newD.direction));
					}
					distVar /= float(points.size());
					dirVar /= float(points.size());

					float distSd = sqrtf(distVar);
					float dirSd = sqrtf(dirVar);

					if (distSd > newD.radius * 0.5f)
					{
						DBG_RENDERER_CMD_TA([=](DebugRenderer::Backend &be) {
							debugDrawDisc(be, newD, DebugRenderer::randomColor(newDiscs.size(), 0.8f), "distSd_discarded_discs");
						});
						continue;
					}
					if (dirSd > 0.5f)
					{
						DBG_RENDERER_CMD_TA([=](DebugRenderer::Backend &be) {
							debugDrawDisc(be, newD, DebugRenderer::randomColor(newDiscs.size(), 0.8f), "dirSd_discarded_discs");
						});
						continue;
					}
				}

				{
					// Cut off for flooriness
					float3 arenaPt = transformPoint(templateToArenaTfm, newD.position);
					float3 arenaDir = transformDirection(templateToArenaTfm, newD.direction);
					float normDist = std::max(0.0f, 1.0f - arenaPt.z / floorCutoffDist);
					float angDist = arenaDir.z;

					if (arenaPt.z < 0.0f || normDist * angDist > floorSimThreshold)
					{
						DBG_RENDERER_CMD_TA([=](DebugRenderer::Backend &be) {
							debugDrawDisc(be, newD, DebugRenderer::randomColor(newDiscs.size(), 0.8f), "floor_discarded_discs");
						});
						continue;
					}

					float3 arenaPt2d = make_vector(arenaPt.x, arenaPt.y, 0.0f);

					// signed distance to wall, negative on the outside
					float wallDist = arenaRadius - length(arenaPt2d);
					float normDistWall = std::max(0.0f, 1.0f - wallDist / wallCutoffDist);
					float angDistWall = std::max(0.0f, -dot(normalize(arenaPt2d), arenaDir));

					if (wallDist < 0.0f || normDistWall * angDistWall > wallSimThreshold)
					{
						DBG_RENDERER_CMD_TA([=](DebugRenderer::Backend &be) {
							debugDrawDisc(be, newD, DebugRenderer::randomColor(newDiscs.size(), 0.8f), "wall_discarded_discs");
						});
						continue;
					}
				}

				// Next relax position by iterating a few times (Maybe this should be done before discarding nodes???)
				for (int r = 0; r < pushPullIters; ++r)
				{
					float3 offset = make_vector3(0.0f);
					float weight = 0.0f;
					for (LocalNodeInfo &ln : neighbourhood)
					{
						ln.update(newSpace, nodes);

						const Disc &nDisc = nodes[ln.index].disc;
						float3 dir = newD.position - nDisc.position;
						float delta = (nDisc.radius + newD.radius) - length(dir);

						offset += normalize(dir) * delta;
						// TODO change the weight to use weight function? Downweight opposing normals...
						weight += 1.0f;
					}

					offset /= weight;

					//be.drawLine(newD.position, newD.position + offset, DebugRenderer::randomColor(newDiscs.size(), 0.8f), "push_pull_offset");
					newD = fitDisc(discRadius, discMinRadius, newD.position + offset, sampleFn);
					newNode.disc = newD;
					newSpace = newNode.makeSpace();
				}

				int newInd = int(nodes.size());
				jobQueue.push_back(newInd);
				// New node must have been ok, add
				// only do this if the node is accepted
				for (int edgeDestInd : newNode.edges)
				{
					DiscGraphNode &n2 = nodes[edgeDestInd];
					// this should not be needed...
					if (std::find(n2.edges.begin(), n2.edges.end(), newInd) == n2.edges.end())
					{
						n2.edges.push_back(newInd);
					}
				}
				nodes.push_back(newNode);
			}
		}
		for (int i = 0; i < int(nodes.size()); ++i)
		{
			const auto &n = nodes[i];
			newDiscs.push_back(n.disc);
		}
		DBG_RENDERER_CMD_TA([nodes=nodes](DebugRenderer::Backend &be) {
			// Re-fetch these here, which makes them able to be changed even if the adaption is not called
			int debugEdgesDiscInd = DYNAMIC_TWEAKABLEVAR_UI(int, "TemplateAdaption/debug_edges_disc_index", -1, Slider(-1, 256));
			int debugDiscInd = DYNAMIC_TWEAKABLEVAR_UI(int, "debug_sample_disc_index", -1, Input(-1));

			for (int i = 0; i < int(nodes.size()); ++i)
			{
				const auto &n = nodes[i];
				if (debugEdgesDiscInd == -1 || debugEdgesDiscInd == i)
				{
					for (int edgeInd : n.edges)
					{
						const DiscGraphNode &n2 = nodes[edgeInd];

						be.drawLine(n.disc.position, n2.disc.position, DebugRenderer::randomColor(n.depth), "edges");
					}
				}
				if (debugDiscInd == -1 || debugDiscInd == i)
				{
					debugDrawDisc(be, n.disc, DebugRenderer::randomColor(n.depth, 0.8f), "new_disc_depth_discs");
				}
			}
		});
	}

	float averageConeAngle = acosf(1.0f - 1.0f / float(newDiscs.size()));

	for (int i = 0; i < int(newDiscs.size()); ++i)
	{
		Disc &td = newDiscs[i];
		if (debugDiscInd == -1 || debugDiscInd == i)
		{
			DBG_RENDERER_CMD_TA([=](DebugRenderer::Backend &be) {
				be.drawLine(td.position, td.position + td.direction * td.radius, DebugRenderer::randomColor(i, 0.8f), "target_discs");
				be.drawDisc(td.position, td.direction, td.radius, DebugRenderer::randomColor(i, 0.8f), "target_discs");
			});
		}

		td.coneAngle = averageConeAngle;
		td.offset = td.radius  / tanf(averageConeAngle);
	}

	if (updateTemplate)
	{
		templ.m_sampleDiscs = newDiscs;
	}
}




TemplateTracker::TrackedMultiTemplateObject::DiscTemplate TemplateTracker::TrackedMultiTemplateObject::generateEllipsoidTemplate(chag::float3 ellipsiodRanges, int subDivisions)
{
	DiscTemplate dt;
	dt.isDefaultPose = true;
	// To local space ellipsoid
	float4x4 unitSphereToEllipsiod = make_scale<float4x4>(ellipsiodRanges);
	float4x4 unitSphereToEllipsiodNormal = inverse(transpose(unitSphereToEllipsiod));

	float4x4 ellipsiodToUnitSphere = inverse(unitSphereToEllipsiod);

	std::vector<float3> triangles = generateUnitHemiSphereTriangles(subDivisions); // 4^2 = 4*4*4 =  64
	std::stringstream ss;
	ss << "generateEllipsoidTemplate_" << triangles.size() / 3;
	dt.name = ss.str();

	dt.m_sampleDiscs.resize(triangles.size() / 3);

	// from solid angle we can get the average cone angle (not accounting for distortion due to ellipsoid objectToWorldTfm, maybe we should?)
	float averageConeAngle = acosf(1.0f - 1.0f / float(dt.m_sampleDiscs.size()));

	for (int i = 0; i < int(dt.m_sampleDiscs.size()); ++i)
	{
		float3 v[] =
		{
			triangles[i * 3 + 0],
			triangles[i * 3 + 1],
			triangles[i * 3 + 2],
		};
		float3 triCentre = normalize(v[0] + v[1] + v[2]);
		float3 normal = triCentre;

		dt.m_sampleDiscs[i].direction = normalize(transformDirection(unitSphereToEllipsiodNormal, normal));
		float3 ep = transformPoint(unitSphereToEllipsiod, triCentre);
		dt.m_sampleDiscs[i].position = ep;
		float3 ev[] =
		{
			transformPoint(unitSphereToEllipsiod, v[0]),
			transformPoint(unitSphereToEllipsiod, v[1]),
			transformPoint(unitSphereToEllipsiod, v[2]),
		};
		// find longest distance from centroid to vertex
		float radius = std::min(std::min(length(ep - ev[0]), length(ep - ev[1])), length(ep - ev[2]));
		dt.m_sampleDiscs[i].radius = radius;
		dt.m_sampleDiscs[i].coneAngle = averageConeAngle;
		dt.m_sampleDiscs[i].offset = radius  / tanf(averageConeAngle);
	}

	dt.m_aabb = make_aabb(-ellipsiodRanges, ellipsiodRanges);

	return dt;
}




void TemplateTracker::TrackedMultiTemplateObject::update(TemplateTracker *tracker)
{
	using namespace chag::type_conversions;
	PROFILE_SCOPE_VAR(m_id.c_str(), TT_Cpu);


	const Params &params = tracker->m_params;
	// TODO: Move out
	const int numIters = params.trackingIterations;

	const float arenaRadius = params.arenaRadius;

	TemplateTrackingBackend *trackingBackend = tracker->m_trackingBackend;

	float4x4 objectToArenaTfm = params.worldToArenaTfm * m_objectToWorldTfm;
	
	Aabb aabb2 = make_inverse_extreme_aabb();
	for (const auto &t : m_templates)
	{
		aabb2 = combine(aabb2, make_aabb(int(t.m_sampleDiscs.size()), [&](int i) { return t.m_sampleDiscs[i].position; }));
	}

	float3 aabbExp = aabb2.getHalfSize() * DYNAMIC_TWEAKABLEVAR_UI(float, "TrackedMultiTemplateObject/aabb_expansion", 0.5f, Slider(0.1f, 3.0f));
	float3 maxExp = make_vector3(std::max(aabbExp.x, std::max(aabbExp.y, aabbExp.z)));
	aabb2.min -= maxExp;
	aabb2.max += maxExp;
	DBG_RENDERER_OBJECT("exp_aabb", ([aabb2,tfm=m_objectToWorldTfm](DebugRenderer::Backend &be) {
		be.pushTransform(tfm);
		be.drawAabb(aabb2, DebugRenderer::red * 0.5f);
	}));
	// TODO: DYNAMIC_TWEAKABLEVAR_UI(bool, "TrackedMultiTemplateObject/usePoints", true, Toggle())

	// NOTE: instead of setting the points/clusters in the backed, we SELECT (and transform) the current set since the points are already there...
	//       This also builds the h-clusters.
	trackingBackend->selectAndTransformClusters(inverse(m_objectToWorldTfm), aabb2, params.worldToArenaTfm);

#if ENABLE_TRACKER_DEBUG_RENDERING


	if (trackingBackend->isCuda())
	{
		trackingBackend->m_clusters.reflectToHost();
		trackingBackend->m_hClusters.reflectToHost();
		//trackingBackend->m_depthDistMap.reflectToHost();
	}
	//for (const auto &sc : trackingBackend->m_clusters)
	//{		
		//debugDrawCluster(sc, make_vector4(heatMap(sc.priorWeight, 0.0f, 1.0f), 1.0f), "aabb_points_as/all_clusters");
		//debugDrawCluster(sc, length(sc.normal) > 0.5f ? DebugRenderer::randomColor(1, 0.8f) : DebugRenderer::randomColor(0, 0.8f), "aabb_points_as/all_clusters");
	//}

	DBG_RENDERER_OBJECT("h_clusters", ([arenaTfm=params.arenaToWorldTfm, hClusters=trackingBackend->m_hClusters,clusters=trackingBackend->m_clusters](DebugRenderer::Backend &be) {
		be.pushTransform(arenaTfm);
		int debugHcInd = DYNAMIC_TWEAKABLEVAR_UI(int, "debug_hcluster_index", -1, Input(-1));
		for (size_t i = 0; i < hClusters.size(); ++i)
		{
			if (debugHcInd == -1 || int(i) == debugHcInd)
			{
				const auto &hc = hClusters[i];
				be.drawSphere(hc.centre, hc.radius, DebugRenderer::randomColor(i, 0.7f), "h_clusters");
				for (int ci = hc.start; ci < hc.end; ++ci)
				{
					const auto &c = clusters[ci];
					be.drawSphere(c.center, c.radius, DebugRenderer::randomColor(i, 1.0f), "h_cluster_clusters");
					if (ci + 1 < hc.end)
					{
						const auto &c2 = clusters[ci+1];
						be.drawLine(c.center, c2.center, DebugRenderer::randomColor(i, 1.0f), "h_cluster_clusters_order");
						be.drawSphere(c.center, c.radius * 0.3f, DebugRenderer::heatMap(float(ci), float(hc.start), float(hc.end)), "aabb_points_as/h_cluster_clusters_order");
					}
				}
			}
		}
	}));
#endif //ENABLE_TRACKER_DEBUG_RENDERING

	{
		PROFILE_SCOPE("BackendUpload", TT_Cpu);

		// upload set of templates, only needed if they have changed (but likely not enough overhead to bother moving)
		trackingBackend->setTemplates(m_templates);
	}

	float bestDistance = std::numeric_limits<float>::max();
	// NOTE: transform search is done in arena space...
	float4x4 bestTfm = objectToArenaTfm;
	int bestPoseIndex = -1;
	float bestConfidence = 0.0f;

	{
		// the transform was reset, so we shouhd do a lot of rotations and search
		if (m_reset)
		{
			const int aSteps = 8;
			for (int a = 0; a < aSteps; ++a)
			{
				float ang = 2.0f * g_pi * float(a) / float(aSteps);
				float4x4 arenaSpaceTfmRotated = objectToArenaTfm * make_rotation_z<float4x4>(ang);

				// search starting from the rotated starting position
				TemplateTrackingBackend::BatchedBestTransformRes r = trackingBackend->findBestTransformBatched(arenaSpaceTfmRotated, std::min(10, numIters));

				if (bestConfidence < r.confidence)
				{
					bestTfm = r.transform;
					bestPoseIndex = r.poseIndex;
					bestDistance = r.rmseW;
					bestConfidence = r.confidence;
				}
			}
			m_reset = false;

			// makes the normal search (which might have a lot more iterations) go from here
			objectToArenaTfm = bestTfm;
		}

		{
			DBG_RENDERER_OBJECT_SCOPE_BE("Correspondence", ([arenaTfm = params.arenaToWorldTfm](DebugRenderer::Backend &be)	{	be.pushTransform(arenaTfm); }));

			TemplateTrackingBackend::BatchedBestTransformRes r = trackingBackend->findBestTransformBatched(objectToArenaTfm, numIters);

			bestTfm = r.transform;
			bestPoseIndex = r.poseIndex;
			bestDistance = r.rmseW;
			bestConfidence = r.confidence;
		}
		if (bestPoseIndex >= 0)
		{
			PROFILE_MESSAGE("Best Pose", "%d", bestPoseIndex);
			PROFILE_MESSAGE("Best Distance", "%0.3e", bestDistance);
			PROFILE_MESSAGE("Best Confidence", "%0.3f", bestConfidence);

			static int delay = 0;// DYNAMIC_TWEAKABLEVAR_UI(int, "update_delay", 0, Slider(0, 10000));
			if (delay++ >= DYNAMIC_TWEAKABLEVAR_UI(int, "TrackedMultiTemplateObject/update_delay", 0, Slider(0, 10000)))
			{
				delay = 0;
				//if (bestConfidence >= 0.75f)
				{
					m_objectToWorldTfm = params.arenaToWorldTfm * bestTfm;
				}
			}

			DiscTemplate &bt = m_templates[bestPoseIndex];
			DBG_RENDERER_OBJECT("BestPoseTemplate", ([tfm = m_objectToWorldTfm, bt = bt](DebugRenderer::Backend &be){
				//DiscTemplate &bt = m_templates[bestPoseIndex];
				for (int i = 0; i < int(bt.m_sampleDiscs.size()); ++i)
				{
					int debugInd = DYNAMIC_TWEAKABLEVAR_UI(int, "debug_sample_disc_index", -1, Input(-1));
					bool drawDebug = debugInd < 0 || debugInd == i;
					if (drawDebug)
					{
						const TemplateDisc &d = bt.m_sampleDiscs[i];

						float3 position = transformPoint(tfm, d.position);
						float3 dir = transformDirection(tfm, d.direction);
						float4 plane = make_vector4(dir, -dot(dir, position));
						float3 apex0 = position - d.offset * dir;
						float3 apex1 = position + d.offset * dir;

						be.drawDisc(position, dir, d.radius, DebugRenderer::randomColor(i, 0.8f), "discs");
						be.drawLine(position, position + dir * d.radius, DebugRenderer::randomColor(i, 0.8f), "disc_normals");


						be.drawCone(apex0, dir, 0.3f, d.coneAngle, DebugRenderer::randomColor(i, 0.5f), "cones");
						be.drawCone(apex1, -dir, 0.3f, d.coneAngle, DebugRenderer::randomColor(i, 0.5f), "cones");
					}
				}
			}));
#if ENABLE_TRACKER_ADAPTION_DEBUG_RENDERING
			// This is only for visualizing the effects of adapting, does not actually capture
			if (DYNAMIC_TWEAKABLEVAR_GET(bool, "poseCaptureMode", false))
			{
				// This tweak var is a global in main program, somewhat inelegant perhaps...
				std::string currentSelectedObject = DYNAMIC_TWEAKABLEVAR_GET(std::string, "selectedObjectName", std::string());
				if (m_id == currentSelectedObject)
				{

					if (trackingBackend->isCuda())
					{
						trackingBackend->m_framePoints.reflectToHost();
						trackingBackend->m_frameNormals.reflectToHost();
					}
					adaptTemplate(bestTfm, params.arenaToWorldTfm, bt, trackingBackend->m_framePoints, trackingBackend->m_frameNormals, false, params.arenaRadius);
				}
			}
#endif // ENABLE_TRACKER_ADAPTION_DEBUG_RENDERING
			if (m_capturePoseNextFrame)
			{
				m_capturePoseNextFrame = false;
				if (m_templateLibrary)
				{
					DiscTemplate nt = bt;
					nt.isDefaultPose = false;
					if (trackingBackend->isCuda())
					{
						trackingBackend->m_framePoints.reflectToHost();
						trackingBackend->m_frameNormals.reflectToHost();
					}
					adaptTemplate(bestTfm, params.arenaToWorldTfm, nt, trackingBackend->m_framePoints, trackingBackend->m_frameNormals, true, params.arenaRadius);
					Aabb prevAabb = make_aabb(int(bt.m_sampleDiscs.size()), [&](int i) { return bt.m_sampleDiscs[i].position; });
					nt.m_aabb = make_aabb(int(nt.m_sampleDiscs.size()), [&](int i) { return nt.m_sampleDiscs[i].position; });

					// constrain #points & surface area...
					if (nt.m_sampleDiscs.size() > 16)// && nt.m_aabb.getSurfaceArea() < prevAabb.getSurfaceArea() * 1.5f && nt.m_aabb.getSurfaceArea() > prevAabb.getSurfaceArea() * 0.75f)
					{
						// TODO: somehow reject crazy ones?
						m_templates.push_back(nt);

						json meta;
						meta["input_source_id"] = tracker->m_inputSourceId;
						meta["source_frame_number"] = tracker->m_inputSourceFrameNumber;
						meta["transform"] = m_objectToWorldTfm;
						meta["aabb"] = m_aabb;

						// TODO: make thread safe(!!!?)
						// Not needed at the moment as only the tracking thread 
						m_templateLibrary->add(int(nt.m_sampleDiscs.size()), [&](int i) { return TemplateLibrary::Disc{ nt.m_sampleDiscs[i].position, nt.m_sampleDiscs[i].direction, nt.m_sampleDiscs[i].radius }; }, meta);
					}
				}
			}
		}
#if LOG_ALL_CONFIDENCES
		m_poseConfidences.clear();
		if (trackingBackend)
		{
			m_poseConfidences.resize(trackingBackend->m_batchedTransformRes.size());
			trackingBackend->m_batchedTransformRes.reflectToHost();
			for (size_t i = 0; i < trackingBackend->m_batchedTransformRes.size(); ++i)
			{
				m_poseConfidences[i] = trackingBackend->m_batchedTransformRes[i].confidence;
			}
		}
#endif // LOG_ALL_CONFIDENCES
		
		m_prevBestPose = bestPoseIndex;
		m_prevBestConfidence = bestConfidence;
		m_active = true;
	}
	
	// Communicate 
	if (bestConfidence <= 0.0f)
	{
#if LOG_ALL_CONFIDENCES
		m_poseConfidences.clear();
#endif // LOG_ALL_CONFIDENCES
		//m_objectToWorldTfm = make_identity<float4x4>();
		m_active = false;
		m_found = false;
	}
}



void TemplateTracker::TrackedMultiTemplateObject::outputLogInfo(DataLogger & logger)
{
	TrackedObject::outputLogInfo(logger);

	logger.write("reset", m_reset);
	logger.write("pose_index", m_prevBestPose);
	logger.write("pose_name", m_prevBestPose >= 0 ? m_templates[m_prevBestPose].name : "INVALID");
	logger.write("confidence", m_prevBestConfidence);
	logger.write("found", m_found);

#if LOG_ALL_CONFIDENCES
	logger.write("all_pose_confidences", m_poseConfidences);
#endif // LOG_ALL_CONFIDENCES
}
