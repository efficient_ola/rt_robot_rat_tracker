/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#include "ClientOutput.h"

#define USE_TCP_SOCKETS 1

#if USE_TCP_SOCKETS
#	if defined(_WIN32)
#		include <windows.h>
//#		include <winsock2.h>
typedef int ssize_t;
#		define TCPSEND_DEFAULT_FLAGS 0

#pragma comment(lib, "Ws2_32.lib")

#	else // other platforms
#		include <fcntl.h>
#		include <errno.h>
#		include <netdb.h>

#		include <sys/types.h>
#		include <sys/socket.h>
#		include <netinet/in.h>
#		include <netinet/tcp.h>

#		define TCPSEND_DEFAULT_FLAGS MSG_NOSIGNAL

#define SOCKET int
#define INVALID_SOCKET -1
#define SOCKET_ERROR -1
#define WSAGetLastError() ((long int) errno)
#define BOOL int
#define TRUE 1
#define FALSE 0
#define SOCKADDR struct sockaddr

#	endif // USE_TCP_SOCKETS

#include <boost/thread/locks.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>
#include <boost/thread/condition_variable.hpp>


static void socket_close(SOCKET fd)
{
#	if defined(_WIN32)
	closesocket(fd);
#	else
	close(fd);
#	endif // platform dep
}
static bool socket_set_nonblock(int fd)
{
#	if defined(_WIN32)
	unsigned long arg = 1;

	if (SOCKET_ERROR == ioctlsocket(fd, FIONBIO, &arg))
	{
		// TODO - someting with FormatMessage().
		int err = WSAGetLastError();

		fprintf(stderr, "ioctlsocket(FIONBIO) failed: %d\n", err);
		return false;
	}
#	else
	int oldFlags = fcntl(fd, F_GETFL, 0);
	if (-1 == oldFlags)
	{
		perror("fcntl(F_GETFL) failed");
		return false;
	}

	if (-1 == fcntl(fd, F_SETFL, oldFlags | O_NONBLOCK))
	{
		perror("fcntl(F_SETFL) failed");
		return false;
	}
#	endif // platform dep

	return true;
}

template <typename T>
bool sendFixedSizeMessage(SOCKET s, const T* bufT, int bytesToSend)
{
	const char *buf = reinterpret_cast<const char *>(bufT);
	for (int sentData = 0; sentData < bytesToSend; )
	{
		ssize_t ret = ::send(s, &buf[sentData], bytesToSend - sentData, TCPSEND_DEFAULT_FLAGS);
		if (ret < 0)
		{
			return false;
		}
		sentData += ret;
	}
	return true;
}
#endif // USE_TCP_SOCKETS


class ClientOutput_Tcp : public ClientOutput
{
public:
	enum State
	{
		S_Idle,
		S_Accepting,
		S_Connected,
		S_Max,
	};

	ClientOutput_Tcp() :
		m_state(S_Idle),
		m_clientSocket(INVALID_SOCKET),
		m_listenSocket(INVALID_SOCKET)
	{
		int iResult;
		#ifdef _WIN32
		WSADATA wsaData;

		iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
		if (iResult != NO_ERROR) {
			fprintf(stderr, "WSAStartup failed: %d\n", iResult);
			return;
		}
		#endif


		m_listenSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (m_listenSocket == INVALID_SOCKET) {
			fprintf(stderr, "socket failed with error: %ld\n", WSAGetLastError());
			m_listenSocket = INVALID_SOCKET;
			return;
		}

		BOOL tt = TRUE;
		iResult = setsockopt(m_listenSocket, SOL_SOCKET, TCP_NODELAY, (const char *)&tt, sizeof(tt));

		sockaddr_in service;
		service.sin_family = AF_INET;
		service.sin_addr.s_addr = htonl(INADDR_ANY); //inet_addr("127.0.0.1");
		service.sin_port = htons(28542);

		if (bind(m_listenSocket,
			(SOCKADDR *)& service, sizeof(service)) == SOCKET_ERROR) {
			fprintf(stderr, "bind failed with error: %ld\n", WSAGetLastError());
			socket_close(m_listenSocket);
			m_listenSocket = INVALID_SOCKET;
			return;
		}
		//----------------------
		// Listen for incoming connection requests.
		// on the created socket
		if (listen(m_listenSocket, 1) == SOCKET_ERROR) {
			fprintf(stderr, "listen failed with error: %ld\n", WSAGetLastError());
			socket_close(m_listenSocket);
			m_listenSocket = INVALID_SOCKET;
			return;
		}
		listen(m_listenSocket, 1);

		m_sendingThread = boost::thread(&ClientOutput_Tcp::sendingThreadFunc, this);
	}

	virtual void send(const std::vector<Location> &ls)
	{
		// send the block of data somehow,
		// TODO: we could use a queue, right now it may loose data if the network can't receive it fast enough (seems unlikely) but it would be better to have a queue so that we can drop data knowingly, 
		// instead of just losing it.
		{
			boost::unique_lock<boost::mutex> lock(m_mutex);
			m_data = ls;
		}
		m_dataAvailable.notify_one();
	}

	void sendingThreadFunc()
	{
		std::vector<Location> sendData;
		for (;; )
		{
			// Connection dropped or not connected?
			if (m_clientSocket == INVALID_SOCKET)
			{
				if (m_listenSocket != INVALID_SOCKET)
				{
					printf("ClientOutput_Tcp: Waiting for connection...\n");
					m_clientSocket = accept(m_listenSocket, NULL, NULL);
					printf("ClientOutput_Tcp: Connected (TODO, TODO).\n");
				}

			}
			else
			{
				{
					boost::unique_lock<boost::mutex> lock(m_mutex);
					m_dataAvailable.wait(lock);
					std::swap(sendData, m_data);
				}
				// send the block of data somehow
				if (INVALID_SOCKET != m_clientSocket  && !sendData.empty())
				{
					size_t sentData = 0;
					uint64_t sendBufferSize = sendData.size() * sizeof(sendData[0]);
					if (!sendFixedSizeMessage(m_clientSocket, &sendBufferSize, sizeof(sendBufferSize)))
					{
						fprintf(stderr, "send(sendSize) - error: `%s'\n", strerror(errno));
						fprintf(stderr, "  closing and re-accept!\n");

						socket_close(m_clientSocket);
						m_clientSocket = INVALID_SOCKET;
						continue;
					}

					if (!sendFixedSizeMessage(m_clientSocket, &sendData[0], int(sendBufferSize)))
					{
						fprintf(stderr, "send(sendBuffer) - error: `%s'\n", strerror(errno));
						fprintf(stderr, "  closing and re-accept!\n");

						socket_close(m_clientSocket);
						m_clientSocket = INVALID_SOCKET;
						continue;
					}
				}
			}
		}
	}

	State m_state;
	std::vector<Location> m_data;
	// TODO: change this to use std:: and use the queue to simplify logic.
	boost::mutex m_mutex; //< used to protect the data waiting to be sent
	boost::thread m_sendingThread; //< thread doint the actual sending
	boost::condition_variable m_dataAvailable; //< used to signal availability of data to sending thread
	SOCKET m_clientSocket;
	SOCKET m_listenSocket;
};

ClientOutput *ClientOutput::create()
{
	return new ClientOutput_Tcp();
}
