/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#ifndef _DataLogger_h_
#define _DataLogger_h_

#include "Config.h"

#include <vector>
#include <string>
#include <unordered_map>
#include <functional>
#include <memory>
#include <functional>
#include <utils/BlockingQueue.h>

#include <linmath/Aabb.h>
#include <linmath/float4x4.h>
#include <linmath/float3.h>

/**
 * Default interface to data logger BackEnd, used to log typed data to some backend file format. 
 * A user may implement this to store the data in any format desired. The backend does not
 * need to be thread-safe, and may write data incrementally or at shutdown.
 */
class DataLoggerBackend
{
public:
	virtual void begin(const std::string &section) = 0;
	virtual void end() = 0;

	virtual void write(const std::string &key, float value) = 0;
	virtual void write(const std::string &key, double value) = 0;
	virtual void write(const std::string &key, const chag::float3 &value) = 0;
	virtual void write(const std::string &key, const chag::float4x4 &value) = 0;
	virtual void write(const std::string &key, const chag::Aabb &value) = 0;
	virtual void write(const std::string &key, bool value) = 0;
	virtual void write(const std::string &key, const std::string &value) = 0;
	virtual void write(const std::string &key, int value) = 0;
	virtual void write(const std::string &key, const std::vector<float> &value) = 0;
	virtual void write(const std::string &key, const std::vector<std::string> &value) = 0;
};

// This base class just exists to remove infrastructure from the templated DataLogger, below.
class DataLoggerBase
{
public:
	virtual void begin(const std::string &section) = 0;
	virtual void end() = 0;

protected:
	using Action = std::function<void(DataLoggerBackend &)>;
	using ActionGroup = std::vector<Action>;
	using ActionGroupPtr = std::shared_ptr<ActionGroup>;
	struct PerThreadData
	{
		PerThreadData() : m_depth(0)
		{
			m_actions = ActionGroupPtr(new ActionGroup);
		}

		template <typename FN_T>
		inline void postAction(FN_T fn)
		{
			m_actions->push_back(Action(fn));
		}

		ActionGroupPtr m_actions;
		int m_depth;
	};
	PerThreadData *getThreadData();



	BlockingQueue<ActionGroupPtr> m_threadActionQueue;
	DataLoggerBackend *m_backend;
};

/**
 * Concrete implementation of daga logger, provides a thread safe interface with low overhead
 * for use to log experimental data. Not used for debug / diagnostic logging.
 * Data may be structured using begin/end statements. Note that the backend may be substituted for
 * a non-virtual implementation if desired. This means that practically any type may be supported.
 * The implementation currently waits until shutdown before the entire queue of work is processed.
 * This obviously puts some strain on memory, and creates a stall at shutdown, but is easier to 
 * guarantee a consistent file format, for example with the json format currently used.
 */
template <typename BACKEND_T = DataLoggerBackend>
class DataLoggerT : public DataLoggerBase
{
public:
	using Backend = BACKEND_T;


	DataLoggerT(Backend *backend) :
		m_backend(backend)
	{
	}

	~DataLoggerT()
	{
		// TODO: wait for other threads to exit at this point?
		//       For now, that is somebody elses problem...
		// TODO: Check that no threads still have their stack depth of more than 0

		// At this point everything is queued up in the m_threadActionQueue
		// so we just execute them all. Each thread will have independent sections that do not overlap.
		// nor is the main thread special in this regard. 
		// TODO: drain this queue using a thread.
		ActionGroupPtr ag;
		while (m_threadActionQueue.pop_front_no_wait(ag))
		{
			for (auto &a : *ag)
			{
				a(*m_backend);
			}
		}

	}

	virtual void begin(const std::string & section)
	{
		PerThreadData *td = getThreadData();
		td->m_depth += 1;
		td->postAction([section = section](Backend &be)
		{
			be.begin(section);
		}
		);
	}

	virtual void end()
	{
		PerThreadData *td = getThreadData();
		assert(td->m_depth >= 1);

		td->postAction([](Backend &be)
		{
			be.end();
		}
		);

		td->m_depth -= 1;

		if (td->m_depth == 0)
		{
			// if we're back at zero-level, we send the block of actions off towards the main thread.
			m_threadActionQueue.push_back(td->m_actions);
			td->m_actions = ActionGroupPtr(new ActionGroup);
		}
	}

	/**
	 * Post anything over to the main thread, but avoid pointer types. Also, will generate compile-time
	 * error if it is a type not supported by the backend.
	 */
	template<typename T>
	inline void write(const std::string & key, const T & value, typename std::enable_if<!std::is_pointer<T>::value >::type* = 0)
	{
		getThreadData()->postAction([key, value](Backend &be)
		{
			be.write(key, value);
		}
		);
	}

	// specialized overload to take care of the common case of writing a string literal
	inline void write(const std::string & key, const char *value)
	{
		write(key, std::string(value));
	}

private:

	Backend *m_backend;
};

// Default type
using DataLogger = DataLoggerT<DataLoggerBackend>;

// Helper class to ensure a begin is closed with an end at the end of the currect scope.
struct DataLoggerScopeHelper
{
	DataLoggerScopeHelper(DataLoggerBase *l, const std::string &section) : m_logger(l)
	{
		m_logger->begin(section);
	}

	~DataLoggerScopeHelper()
	{
		m_logger->end();
	}

	DataLoggerBase *m_logger;
};

#endif // _DataLogger_h_
