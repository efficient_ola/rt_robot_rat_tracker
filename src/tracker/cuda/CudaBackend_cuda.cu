/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#include "CudaBackend.h"
#include "ColorDepthSource.h"
#include "profiler/Profiler.h"

#include <linmath/float4.h>
#include <linmath/int3.h>

#if ENABLE_CUDA
//using namespace chag;
#include "utils/CudaCheckError.h"
#include <utils/DebugRenderer.h>
#include "BackendSharedImplementation.h"

#include <hemi/hemi.h>
#include <hemi/parallel_for.h>

#include <cub/device/device_scan.cuh>
#include <cub/device/device_radix_sort.cuh>

#include "utils/CudaHelpers.h"
#include "utils/CudaRuntimeApiWrapper.h"

#if GENERATE_COMPACTED_POINTS
#include <stream_transform/stream_transform_sp.h>
namespace st = stream_transform_sp;
#endif // GENERATE_COMPACTED_POINTS

using DiscTemplate = CudaBackend::DiscTemplate;
using TemplateDisc = CudaBackend::TemplateDisc;
using Params = CudaBackend::Params;
using Cluster = CudaBackend::Cluster;
using chag::lerp;


// batched kernel with one thread / disc
__global__ static void findBestPointsBatchedKernel(CudaDeviceBufferRef<const chag::float4x4> tfms,
	CudaDeviceBufferRef<const DiscTemplate> templates, CudaDeviceBufferRef<const TemplateDisc> discs,
	CudaDeviceBufferRef<const HCluster> hClusters, CudaDeviceBufferRef<const Cluster> clusters,
	const Params params,
	CudaDeviceBufferRef<chag::float4> bestPoints_Weight,
	CudaDeviceBufferRef<chag::float4> bestTemplatePoints_Dist)
{
	int discIndex = blockIdx.x * blockDim.x + threadIdx.x;
	if (discIndex >= int(discs.size()))
	{
		return;
	}
	const TemplateDisc td = discs[discIndex];

	DiscTemplate templ = templates.loadAligned(td.templateIndex);
	// this is kind of stupid...
	const float invDistanceWeightRange = 1.0f / (length(templ.aabb.getHalfSize()) * params.distanceWeightRangeScale);
	chag::float4x4 tfm = tfms.loadAligned(td.templateIndex);

	chag::float3 bestPoint = chag::make_vector3(-1.0f);
	chag::float3 bestTemplatePoint = chag::make_vector3(-1.0f);
	float weight = -1.0f;
	float dist = -1.0f;
	int best = processDisc(tfm, td, hClusters, clusters, params, invDistanceWeightRange,
		bestPoint, bestTemplatePoint, weight, dist);


	bestPoints_Weight[discIndex] = make_vector4(bestPoint, weight);
	bestTemplatePoints_Dist[discIndex] = make_vector4(bestTemplatePoint, dist);
}


// Launch one thread per each template, computes the transform, possibly better to run a warp / block per each
// depends on numbers in the end. However, per thread coherency will be rubbish as they access different point lists.
// This one also updates the current transform of each template
__global__ static void updateTransformBatchedKernel(CudaDeviceBufferRef<chag::float4x4> tfms, CudaDeviceBufferRef<const DiscTemplate> templates,
	const Params params,
	CudaDeviceBufferRef<const chag::float4> bestPoints_Weight,
	CudaDeviceBufferRef<const chag::float4> bestTemplatePoints_Dist,
	CudaDeviceBufferRef<CudaBackend::BatchedTransformRes> res)
{

	// TODO: Optimize by letting threads in a warp help out with each template, seems the svd is not the bottleneck so spreading the points out over the threads should help
	// does of course require reduction, but that should be prefereable. Shared atomics on newer hardware - starting to need my warp processing code...

	//int templateIndex = blockIdx.x * blockDim.x + threadIdx.x;
	// Note one warp per template to allow for better load balancing... warp is coded in Y dimension of block 
	int templateIndex = blockIdx.x * blockDim.y + threadIdx.y;
	// skip out of range indices and threads not first in each warp
	if (templateIndex >= int(templates.size()) || threadIdx.x != 0)
	{
		return;
	}

	const DiscTemplate templ = templates[templateIndex];

	float totalDistSqW = 0.0f;
	float totalWeight = 0.0f;

	chag::float4x4 tfm = tfms.loadAligned(templateIndex);
	chag::float4x4 estTfm = calcTransformExtractRot(bestTemplatePoints_Dist.sub(templ.start, templ.count), bestPoints_Weight.sub(templ.start, templ.count), totalWeight, totalDistSqW);
	tfm = estTfm * tfm;

	if (params.constrainTransform)
	{
		tfm = constrainTfm(tfm, params.minMaxRotXZ, params.minMaxTransZ);
	}

	tfms[templateIndex] = tfm;

	CudaBackend::BatchedTransformRes btr;

	btr.transform = estTfm;
	btr.totalWeight = totalWeight;
	btr.rmseW = sqrtf(totalDistSqW / totalWeight);
	btr.confidence = totalWeight / float(templ.count);
	res[templateIndex] = btr;

	//printf("%d, %03f,\n", templateIndex, btr.confidence);
}


// Works for positive floatingpoint numbers of any size..
inline __device__ float atomicMinPosFloat(float *addr, float val)
{
	assert(val >= 0.0f);
	int old = atomicMin(reinterpret_cast<int *>(addr), __float_as_int(val));
	return __int_as_float(old);
}


template <typename TFM_BUFFER_T, typename BTR_BUFFER_T>
FUNC_CUDA_HD CudaBackend::BatchedBestTransformRes selectBestRes(const TFM_BUFFER_T &tfms,
	const BTR_BUFFER_T &tfmRes)
{
	float bestRmseW = std::numeric_limits<float>::max();
	float bestConfidence = 0.0f;
	CudaBackend::BatchedBestTransformRes rr;
	rr.poseIndex = -1;
	for (int templateIndex = 0; templateIndex < int(tfms.size()); ++templateIndex)
	{
		CudaBackend::BatchedTransformRes btr = tfmRes[templateIndex];

		//printf("%d, %03f,\n", templateIndex, btr.confidence);
		// If the previous value was larger, we store the new one.
		// This could concievably lead to race conditions where half the transform from one 
		if (btr.confidence >= bestConfidence)
		{
			bestConfidence = btr.confidence;
			bestRmseW = btr.rmseW;
			rr.transform = tfms[templateIndex];
			rr.totalWeight = btr.totalWeight;
			rr.poseIndex = templateIndex;
			rr.rmseW = bestRmseW;
			rr.confidence = bestConfidence;
		}
	}
	return rr;
}

// Launch as single! thread, just loop through...
__global__ static void selectBestKernel(CudaDeviceBufferRef<const chag::float4x4> tfms,
	CudaDeviceBufferRef<const CudaBackend::BatchedTransformRes> tfmRes,
	CudaDeviceBufferRef<CudaBackend::BatchedBestTransformRes> res)
{
	res[0] = selectBestRes(tfms, tfmRes);
}



CudaBackend::CudaBackend(const std::string &deviceName) 
	: m_deviceName(deviceName)
{
}


template <typename T>
FUNC_CUDA_HD void swap(T &a, T &b)
{
	T c(a);
	a = b;
	b = c;
}

void CudaBackend::preProcessFrame(const RawFrameData & frame)
{
	using namespace chag::type_conversions;

	Params params = m_params;

	m_frameDepthData = frame.depthData;
	m_frameDepthData.reflectToDevice(true);

	int depthWidth = frame.depthWidth;
	int depthHeight = frame.depthHeight;

	// 1. Generate 3D data.
	constexpr float fMax = std::numeric_limits<float>::max();
	// 1.1 build full (2D mapped including invalid ones marked with w == 0) & compact (no invalid) set of points .
	m_framePoints.resize(depthHeight * depthWidth, true);

	auto fpsDev = m_framePoints.deviceRef();
	auto coeffsDev = m_depthToCameraSpaceCoeffs.deviceRefRo();
	auto depthDataDev = m_frameDepthData.deviceRefRo();

	{
		PROFILE_SCOPE("Points", TT_Cuda);
		hemi::parallel_for(0, frame.depthData.size(), [=] HEMI_LAMBDA(int i)
		{
			float depth = float(depthDataDev[i]);
			chag::float2 trans = coeffsDev[i];
			if (depth > 0.0f && isfinite(trans.x) && isfinite(trans.y))
			{
				// scale to metres and camera space...
				chag::float3 camPt = chag::make_vector(trans.x * depth, trans.y * depth, depth) * (1.0f / 1000.0f);
				fpsDev[i] = chag::make_vector4(camPt, 1.0f);
			}
			else
			{
				// Invalid point.
				fpsDev[i] = chag::make_vector(fMax, fMax, fMax, 0.0f);
			}
		});
	}
	m_frameNormals.resize(depthHeight * depthWidth, true);
	auto fNormDev = m_frameNormals.deviceRef();

	float depthThresh = params.depthClusterThreshold;
	{
		PROFILE_SCOPE("normals", TT_Cuda);

		hemi::parallel_for(0, depthHeight * depthWidth, [=] HEMI_LAMBDA(int ind)
		{
			int j = ind / depthWidth;
			int i = ind - j * depthWidth;

			chag::float4 n = buildNormal(i, j, depthThresh, [&](int i, int j)
			{
				if (i < 0 || i >= depthWidth || j < 0 || j >= depthHeight)
				{
					return chag::make_vector(0.0f, 0.0f, 0.0f, 0.0f);
				}
				int ind = depthWidth * j + i;
				return fpsDev[ind];
			});
			fNormDev[ind] = n;
		});

	}

#if GENERATE_COMPACTED_POINTS
	// NOTE: size set to max since result of compaction will be on GPU side.
	m_framePointsCompact.resize(frame.depthHeight * frame.depthWidth, true);
	m_frameNormalsCompact.resize(frame.depthHeight * frame.depthWidth, true);
	m_compactPointsCount.resize(1, true);

	auto fpsCompDev = m_framePointsCompact.deviceRef();
	auto fNormCompDev = m_frameNormalsCompact.deviceRef();
	//
	{
		PROFILE_SCOPE("compact_points", TT_Cuda);
		st::gen_transform_gen(0, frame.depthHeight * frame.depthWidth, [=]HEMI_LAMBDA(uint32_t i)
		{
			chag::float4 p = fpsDev.loadAligned(i);
			return p.w != 0.0f ? 1 : 0;
		}, [=]HEMI_LAMBDA(uint32_t inInd, uint32_t outOffset, uint32_t subI)
		{
			fpsCompDev[outOffset] = fpsDev.loadAligned(inInd);
			fNormCompDev[outOffset] = fNormDev.loadAligned(inInd);
		}, m_compactPointsCount.cudaPtr());
		CUDA_CHECK_ERROR("st::gen_transform_gen - m_compactPointsCount");
	}
#endif // GENERATE_COMPACTED_POINTS


	// TODO: set size from the count on GPU-side (if we actually need these points...?)
#if 1
	{
		PROFILE_SCOPE("cluster", TT_Cuda);

		m_frameClusters.reserve(frame.depthHeight * frame.depthWidth, true);
		m_frameClusters.clear();

		auto frameClustersDev = m_frameClusters.deviceRef();

		constexpr float maxArenaHeight = 0.35f;
		constexpr int g_tileSizeXy = 3; // current clustering scheme is no good for large tiles!
																		//int2 gridSize = make_vector(g_frame.depthWidth, g_frame.depthHeight) + make_vector2(g_tileSizeXy - 1) / g_tileSizeXy;

		const float arenaRadExp = m_params.arenaRadius * 1.3f;
		chag::Aabb arenaAabbExp = chag::make_aabb(chag::make_vector(-arenaRadExp, -arenaRadExp, -maxArenaHeight), chag::make_vector(arenaRadExp, arenaRadExp, maxArenaHeight));
		const uint32_t threshold = (uint32_t(65535.0f * (m_params.depthClusterThreshold / (2.0f * maxArenaHeight))) << 16U) | 0x0000FFFF;

		chag::int2 gridSize = chag::make_vector(frame.depthWidth / g_tileSizeXy, frame.depthHeight / g_tileSizeXy);
		chag::float4x4 worldToArenaTfm = m_params.worldToArenaTfm;

		// 1. create keys for segmented radix sort of each tile.
		//m_params.depthClusterThreshold
		hemi::parallel_for(0, gridSize.x * gridSize.y, [=] HEMI_LAMBDA(int ind)
		{
			int gj = ind / gridSize.x;
			int gi = ind - gj * gridSize.x;

			chag::int2 tp = chag::make_vector(gi, gj) * g_tileSizeXy;

			constexpr int N = g_tileSizeXy * g_tileSizeXy;
			uint32_t sortedInds[N];
			for (int j = 0; j < g_tileSizeXy; ++j)
			{
				for (int i = 0; i < g_tileSizeXy; ++i)
				{
					sortedInds[j * g_tileSizeXy + i] = 0xFFFFFFFF;
				}
			}

			for (int j = 0; j < g_tileSizeXy; ++j)
			{
				for (int i = 0; i < g_tileSizeXy; ++i)
				{
					int sampleOffs = (tp.y + j) * depthWidth + tp.x + i;
					chag::float4 pt = fpsDev.loadAligned(sampleOffs);

					uint32_t k = 0xFFFFFFFF;
					if (pt.w != 0.0f)
					{
						chag::float3 aPt = transformPoint(worldToArenaTfm, f3(pt));
						// ignore points that are out of range (height-wise), and filter out any that are outside the arena
						if (inside(arenaAabbExp, aPt))
						{
							// Quantize depth to 16 bits around arena floor
							uint32_t quantDepth = int(65535.0f * chag::clamp((aPt.z + maxArenaHeight) / (2.0f * maxArenaHeight), 0.0f, 1.0f));
							// pack the depth in uppermost 16 bits to ensure sorting order
							k = (quantDepth << 16) | (j << 8) | i;
						}
					}

					// perform insertion sort (if this is not unrolled then this will suck bigtime...(same for all the loops)
					for (int s = 0; s < N; ++s)
					{
						if (k < sortedInds[s])
						{
							swap(sortedInds[s], k);
						}
					}
				}
			}
			uint32_t prevD = sortedInds[0];

			chag::Aabb aabb = chag::make_inverse_extreme_aabb();
			//float3 pts[g_tileSizeXy * g_tileSizeXy];
			chag::float4 avgCentreW = { 0.0f, 0.0f, 0.0f, 0.0f };
			chag::float4 avgNormalW = { 0.0f, 0.0f, 0.0f, 0.0f };

			for (int i = 0; i < N; ++i)
			{
				uint32_t d = sortedInds[i];

				// if break found or this is the last value, or it is invalid AND the cluster is valid, then emit a box and reset
				if ((d - prevD >= threshold || d == 0xFFFFFFFF || i == N-1) && avgCentreW.w != 0.0f)
				{
					Cluster c;
					c.center = f3(avgCentreW) / avgCentreW.w;
					if (avgNormalW.w > 0.0f)
					{
						c.normal = normalize(f3(avgNormalW) / avgNormalW.w);
					}
					else
					{
						c.normal = chag::make_vector3(0.0f);
					}

					c.radius = std::max(0.001f, length(aabb.getHalfSize()));// should probably figure out projection on plane here...
					c.priorWeight = 1.0f;
#if 0
					c.aabb = aabb;
					if (numPoints == 1)
					{
						c.aabb = make_aabb(c.center, c.radius);
					}
					c.debugColorId = (tp.y) * frame.depthWidth + tp.x + i;
#endif
					frameClustersDev.push_back(c);

					// reset
					aabb = chag::make_inverse_extreme_aabb();
					avgNormalW = avgCentreW = chag::make_vector4(0.0f);
				}

				// if value is valid, load and update current aabb etc
				if (d != 0xFFFFFFFF)
				{
					int i = d & 255U;
					int j = (d >> 8U) & 255U;
					int sampleOffs = (tp.y + j) * depthWidth + tp.x + i;
					// we could buffer these in a local array in case we don't trust the cache to do its job...
					const chag::float4 pt = fpsDev.loadAligned(sampleOffs);
					const chag::float4 n = fNormDev.loadAligned(sampleOffs);
					avgNormalW += n;
					avgCentreW += pt;
					aabb = combine(aabb, f3(pt));
				}
			}
		});
		CUDA_CHECK_ERROR("hemi::parallel_for - clustering");
	}
//#if ENABLE_BACKEND_DEBUG_RENDERING
//	m_frameClusters.reflectToHost();
//	for (const auto &c : m_frameClusters)
//	{
//		chag::DebugRenderer::instance().addSphere(c.center, c.radius, chag::DebugRenderer::random, "aabb_points_ws/all_clusters");
//	}
//#endif // ENABLE_BACKEND_DEBUG_RENDERING
#endif //0
}

namespace hemi_x
{
	// Indirect-capable version, that fetches the index range from the container _in_ the lambda (which means it may or may not be fetched on the device-side)
	template <typename CONTAINER_T, typename F>
	void parallel_for(const hemi::ExecutionPolicy &p, const CONTAINER_T &container, F function)
	{
		hemi::launch(p, [=] HEMI_LAMBDA() 
		{
			uint32_t first = 0U;
			uint32_t last = container.size();
			for (auto idx : hemi::grid_stride_range(first, last))
			{
				function(idx);
			}
		});
	}

	template <typename CONTAINER_T, typename F>
	void parallel_for(const CONTAINER_T &container, F function) {
		hemi::ExecutionPolicy p;
		hemi_x::parallel_for(p, container, function);
	}
}

#define BUILD_H_CLUSTERS_ON_HOST 0

void CudaBackend::selectAndTransformClusters(const chag::float4x4 &worldToAabbTfm, const chag::Aabb & aabb, const chag::float4x4 &worldtoArenaTfm)
{
#if BUILD_H_CLUSTERS_ON_HOST
	chag::float4x4 arenaToAabbTfm = worldToAabbTfm * inverse(worldtoArenaTfm);

	m_frameClusters.reflectToHost();

	m_clusters.clear();
	m_clusters.reserve(m_frameClusters.size(), true);

	for (int i = 0; i < int(m_frameClusters.size()); ++i)
	{
		const Cluster &c = m_frameClusters[i];
		chag::float3 atp = transformPoint(worldToAabbTfm, c.center);
		if (inside(aabb, atp))
		{
			// Note: currenly selected clusters are in arena-space
			chag::float3 tp = transformPoint(worldtoArenaTfm, c.center);

			Cluster tc = c;
			tc.center = tp;
			tc.normal = transformDirection(worldtoArenaTfm, c.normal);

			m_clusters.push_back(tc);
		}
	}

	rebuildHierearchicalClustersHost(arenaToAabbTfm);

	m_hClusters.reflectToDevice();
	m_clusters.reflectToDevice();
#else // !BUILD_H_CLUSTERS_ON_HOST
	m_clusters.clear();
	// NOTE: we match the capacity here as that is an upper bound of the memory needs, the size is not actually known at this point on the host-side
	m_clusters.reserve(m_frameClusters.capacity(), true);


	m_clustersTmp.clear();
	m_clustersTmp.reserve(m_frameClusters.capacity(), true);

	auto frameClustersDev = m_frameClusters.deviceRef();
	auto clustersDev = m_clustersTmp.deviceRef();

	{
		PROFILE_SCOPE("select_clusters", TT_Cuda);

		// Note, this version of the parallel for gets the range from the container, and does so on the device side, which means it will use the correct size
		// even if not reflected to the host.
		hemi_x::parallel_for(frameClustersDev, [=] HEMI_LAMBDA(int i)
		{
			const Cluster c = frameClustersDev[i];
			chag::float3 atp = chag::transformPoint(worldToAabbTfm, c.center);
			if (inside(aabb, atp))
			{
				chag::float3 tp = chag::transformPoint(worldtoArenaTfm, c.center);

				Cluster tc = c;
				tc.center = tp;
				tc.normal = chag::transformDirection(worldtoArenaTfm, c.normal);

				clustersDev.push_back(tc);
			}
		});
		CUDA_CHECK_ERROR("select_clusters");

	}
#if 0 //ENABLE_BACKEND_DEBUG_RENDERING
	m_clustersTmp.reflectToHost();
	for (const auto &c : m_clustersTmp)
	{
		chag::DebugRenderer::instance().addSphere(c.center, c.radius, chag::DebugRenderer::random, "aabb_points_as/selected_clusters");
	}
#endif // ENABLE_BACKEND_DEBUG_RENDERING
	// Finally build the hierarhcical grid level, we'll build this one as a simple 3D grid and select the resolution based on the idea that 
	// we want about K clusters per H cluster - perhaps 16. Noting that the data is more or less 2D we perform a 2D estimate of cell-size
	// and then extend this to 3D, which would lead to slightly too large cells. Then we just add each cluster to the H-cluster where the 
	// centre belongs, and extend the H-cluster aabb, easy.

	{
		PROFILE_SCOPE("build_h_clusters", TT_Cuda);

		m_hClusters.reserve(m_clusters.capacity(), true);
		m_hClusters.clear();

		auto hClustersDev = m_hClusters.deviceRef();
		
		// we've reorganized to populate a temporary one first.
		m_clusters.copySize(m_clustersTmp);
		auto clusters2Dev = m_clusters.deviceRef();

		// to transform to grid we need the transform from arena space (where selected points are, to aabb space, where the grid is)
		chag::float4x4 arenaToAabbTfm = worldToAabbTfm * inverse(worldtoArenaTfm);
		chag::float3 zeroF = chag::make_vector3(0.0f);
		
		float subDivisions = float(m_params.hClusterSubDivs);

		float cellSize = std::max(std::max(aabb.getDiagonal().x, aabb.getDiagonal().y), aabb.getDiagonal().z) / subDivisions;

		chag::uint3 gridDim = chag::make_vector<uint32_t>(ceil(aabb.getDiagonal() / cellSize) + 0.5f);
		chag::float3 gridBoundF = chag::make_vector<float>(gridDim) - 1.0f;

		m_gridCounts.resize(gridDim.x * gridDim.y * gridDim.z, true);
		cudaMemset(m_gridCounts.cudaPtr(), 0, m_gridCounts.byteSize());

		auto gridCountsDev = m_gridCounts.deviceRef();
		// 1. count for each cell using atomics
		hemi_x::parallel_for(clustersDev, [=] HEMI_LAMBDA(int ci)
		{
			const Cluster c = clustersDev.loadAligned(ci);
			// to transform to grid we need te 
			chag::float3 aPt = transformPoint(arenaToAabbTfm, c.center);

			chag::uint3 gridPos = chag::make_vector<uint32_t>(clamp((aPt - aabb.min) / cellSize, zeroF, gridBoundF));
			uint32_t ind = (gridPos.z * gridDim.y + gridPos.y) * gridDim.x + gridPos.x;
			// todo, perhaps store ind, to save recomputing and potential error...
			atomicAdd(&gridCountsDev[ind], 1);
		});
		CUDA_CHECK_ERROR("build_h_clusters-1");

		// 2. calc prefix sum
		m_gridOffsets.resize(gridDim.x * gridDim.y * gridDim.z, true);

		size_t temp_storage_bytes = 0;
		cub::DeviceScan::ExclusiveSum(nullptr, temp_storage_bytes, m_gridCounts.cudaPtr(), m_gridOffsets.cudaPtr(), int(m_gridOffsets.size()));
		m_cubTmp.resize(temp_storage_bytes, true);
		cub::DeviceScan::ExclusiveSum(m_cubTmp.cudaPtr(), temp_storage_bytes, m_gridCounts.cudaPtr(), m_gridOffsets.cudaPtr(), int(m_gridOffsets.size()));

		CUDA_CHECK_ERROR("build_h_clusters-2");

		// 3. re-order clusters into their respecive grid cell
		cudaMemset(m_gridCounts.cudaPtr(), 0, m_gridCounts.byteSize());
		auto gridOffsetsDev = m_gridOffsets.deviceRef();
		hemi_x::parallel_for(clustersDev, [=] HEMI_LAMBDA(int ci)
		{
			const Cluster c = clustersDev.loadAligned(ci);
			// to transform to grid we need te 
			chag::float3 aPt = transformPoint(arenaToAabbTfm, c.center);

			chag::uint3 gridPos = chag::make_vector<uint32_t>(clamp((aPt - aabb.min) / cellSize, zeroF, gridBoundF));
			uint32_t ind = (gridPos.z * gridDim.y + gridPos.y) * gridDim.x + gridPos.x;

			uint32_t offset = gridOffsetsDev[ind];
			uint32_t count = atomicAdd(&gridCountsDev[ind], 1);
			clusters2Dev[offset + count] = c;
		});
		CUDA_CHECK_ERROR("build_h_clusters-3");

		// 4. finally emit the h clusters and build aabbs etc
		hemi::parallel_for(0, gridDim.x * gridDim.y * gridDim.z, [=] HEMI_LAMBDA(int cellInd)
		{
			uint32_t offset = gridOffsetsDev[cellInd];
			uint32_t count = gridCountsDev[cellInd];

			chag::Aabb cellAabb = chag::make_inverse_extreme_aabb();

			for (uint32_t ci = offset; ci < offset + count; ++ci)
			{
				// Note: in arena-space
				const Cluster c = clusters2Dev.loadAligned(ci);
				// to transform to grid we need te 
				cellAabb = combine(cellAabb, make_aabb(c.center, c.radius));
			}
			if (count)
			{
				HCluster hc;
				hc.start = offset;
				hc.centre = cellAabb.getCentre();
				hc.radius = length(cellAabb.getHalfSize());
				hc.end = offset + count;
				hClustersDev.push_back(hc);
			}
		});
		CUDA_CHECK_ERROR("build_h_clusters-4");
		// 5. TODO: possibly add a fusing-pass to merge ones that are imbalanced (only a few items) or subsumed (lots of overlap with a neighbour)

		CUDA_CHECK_ERROR("build_h_clusters");

	}
#endif // BUILD_H_CLUSTERS_ON_HOST
}

CudaBackend::BatchedBestTransformRes CudaBackend::findBestTransformBatched(const chag::float4x4 &startTfm, int numIters)
{
	m_bestPoints_WeightTmp.resize(m_discs.size(), true);
	m_bestTemplatePoints_DistTmp.resize(m_discs.size(), true);
	m_batchedTransformRes.resize(m_templates.size(), true);

	m_transforms.resize(m_templates.size(), true);
	auto tfmsDev = m_transforms.deviceRef();
	hemi::parallel_for(0, m_transforms.size(), [=] HEMI_LAMBDA(int i)
	{
		tfmsDev[i] = startTfm;
	});

	m_bestResultRes.resize(1, true);


	for (int k = 0; k < numIters; ++k)
	{
		{
			PROFILE_SCOPE("findBestPointsBatchedKernel", TT_Cuda);
			findBestPointsBatchedKernel<<<(uint32_t(m_discs.size()) + 512 - 1) / 512, 512>>>(m_transforms.deviceRefRo(), m_templates.deviceRefRo(), m_discs.deviceRefRo(), m_hClusters.deviceRefRo(), m_clusters.deviceRefRo(),
				m_params, m_bestPoints_WeightTmp.deviceRef(), m_bestTemplatePoints_DistTmp.deviceRef());
			CUDA_CHECK_ERROR("findBestPointsBatchedKernel");
		}
		{
			PROFILE_SCOPE("updateTransformBatchedKernel", TT_Cuda);
			// Launch one warp per template of size 1 and allow the GPU to schedule them a bit more freely
			auto trDev = m_templates.deviceRefRo();
			updateTransformBatchedKernel<<<(uint32_t(m_templates.size()) + 4 - 1) / 4, dim3(32,4) >>>(m_transforms.deviceRef(), trDev, m_params, m_bestPoints_WeightTmp.deviceRefRo(), m_bestTemplatePoints_DistTmp.deviceRefRo(), m_batchedTransformRes.deviceRef());
			CUDA_CHECK_ERROR("updateTransformBatchedKernel");
		}
	}
	{
		PROFILE_SCOPE("selectBestKernel", TT_Cuda);
		// NOTE: single GPU thread(!) - not enough parallelism to worry about, just on GPU to recude amount of data to transfer.
		selectBestKernel<<<1,1>>>(m_transforms.deviceRefRo(), m_batchedTransformRes.deviceRefRo(), m_bestResultRes.deviceRef());
		CUDA_CHECK_ERROR("selectBestKernel");
	}
	// 3. postprocess the data
	{
		PROFILE_SCOPE("result_transfer", TT_Cpu);
		m_bestResultRes.reflectToHost();
	}
	return m_bestResultRes[0];
}


CudaBackend *CudaBackend::create()
{
	std::string deviceName;
	if (cuda_wrapper::detectCudaDevice(&deviceName))
	{
		printf("INFO: CUDA detected, initializing using device '%s'\n", deviceName.c_str());
		//int cudaDevice = 0; // TODO -- which device should we use?
		//cutilSafeCall(cudaGLSetGLDevice(cudaDevice));
		//CUT_CHECK_ERROR("cudaGLSetGLDevice");

		return new CudaBackend(deviceName);
	}
	else
	{
		printf("INFO: NO CUDA device/driver detected, CPU fallback will be used.\n");
		// TODO move this to a better place.
		Profiler::setEnabledTimers(Profiler::TT_Cpu);
	}

	// cudaDeviceSetLimit(cudaLimitPrintfFifoSize, 1024 * 1024 * 32);
	return 0;
}

#endif // ENABLE_CUDA
