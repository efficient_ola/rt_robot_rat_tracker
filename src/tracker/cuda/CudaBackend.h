/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#ifndef _CudaBackend_h_
#define _CudaBackend_h_

#include "Config.h"
#include <vector>
#include <map>
#include <algorithm>
#include <numeric>
#include "linmath/float3.h"
#include "linmath/Aabb.h"
#include "linmath/float4x4.h"
#include <float.h>
#include "Common.h"
#include "TemplateTracker.h"
#include "TemplateTrackingBackend.h"

// Note that ENABLE_CUDA is automatically set if the build integraions for CUDA are found
// you have to change this manually in the .vcxproj file...
#if ENABLE_CUDA

#include "utils/CudaMirrorBuffer.h"
#include "utils/CudaBuffer.h"

/**
 * CUDA implementation of the processing steps.
 */
class CudaBackend : public TemplateTrackingBackend
{
public:
	CudaBackend(const std::string &deviceName);

	virtual bool isCuda() const { return true; }
	virtual std::string getDeviceDesc() const { return m_deviceName; }


	virtual void preProcessFrame(const RawFrameData &frame);
	virtual void selectAndTransformClusters(const chag::float4x4 &worldToAabbTfm, const chag::Aabb & aabb, const chag::float4x4 &worldtoArenaTfm);

	/**
		* setTemplates, setPoints, and setParams must be called before (not setTfms, as they are calculated during iteration).
		*/
	BatchedBestTransformRes findBestTransformBatched(const chag::float4x4 &startTfm, int numIters);

	static CudaBackend *create();

protected:
	CudaMirrorBuffer<uint8_t> m_cubTmp;
	CudaMirrorBuffer<int> m_gridOffsets;
	CudaMirrorBuffer<int> m_gridCounts;
	CudaMirrorBuffer<chag::Aabb> m_gridAabbs;
	std::string m_deviceName;
};

#endif // ENABLE_CUDA

#endif // _CudaBackend_h_
