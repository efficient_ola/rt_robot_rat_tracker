/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#ifndef _ImageStreamOutput_h_
#define _ImageStreamOutput_h_

#include "Config.h"

#include <vector>
#include <map>
#include "linmath/float3.h"
#include "linmath/Aabb.h"
#include "linmath/float4x4.h"



/**
 * Abstract interface hiding details of output to image files, or also to network clients.
 */
class ImageStreamOutput
{
public:
	ImageStreamOutput() : 
		m_isOpen(false),
		m_width(-1),
		m_height(-1)
	{ 
	}
	virtual ~ImageStreamOutput() { }
	/**
	 * output a frame to the stream - note the format!.
	 */
	virtual void writeFrame(int width, int height, const uint8_t *pixelDataRgb) = 0;

	virtual bool outputExists(const std::string &outputPath) = 0;
	/**
	 * outputPath is either a folder to output the files to, or a file name for a video stream, video streams require the 
	 * width and height to be specified up front, whereas image streams don't care much.
	 */
	bool open(const std::string &outputPath, int width, int height)
	{
		assert(!m_isOpen);
		m_outputPath = outputPath;
		m_width = width;
		m_height = height;
		m_isOpen = internalOpen();
		return m_isOpen;
	}

	bool close(bool flush = false)
	{
		assert(m_isOpen);
		m_isOpen = false;
		m_outputPath = std::string();
		return internalClose(flush);
	}

	virtual bool isOpen() { return m_isOpen; }
	//virtual bool flush() {};


	static ImageStreamOutput *createFfmpeg(const std::string &ffmpegExePath);
	static ImageStreamOutput *createImage(const char *fileNameFormat = "frame_%05d.tga");

protected:
	virtual bool internalClose(bool flush) = 0;
	virtual bool internalOpen() = 0;
	std::string m_outputPath;
	bool m_isOpen;
	int m_width;
	int m_height;
};

#endif // _ImageStreamOutput_h_
