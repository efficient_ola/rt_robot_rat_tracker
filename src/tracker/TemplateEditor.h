/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#ifndef _TemplateEditor_h_
#define _TemplateEditor_h_

#include "Config.h"
#include <vector>
#include <set>
#include <algorithm>
#include "linmath/float3.h"
#include "linmath/int2.h"
#include "linmath/Aabb.h"
#include "linmath/float4x4.h"
#include "TemplateLibrary.h"
#include <float.h>

/**
 *
 */

class OBJModel;
class TemplateLibrary;
class HostTrackingBackend;

namespace chag
{
	class PrimitiveRenderer;
	class SimpleShader;
};

class TemplateEditor
{
public:
	TemplateEditor(TemplateLibraryPtr lib, chag::PrimitiveRenderer *primRenderer, chag::SimpleShader *objModelShader, const std::string &dataDir);

	void loadOptions(const std::string &optionsFileName = "options/templategui.json");
	void saveOptions(const std::string &optionsFileName = "options/templategui.json");

	void onMouse();
	void onMouseMotion(const chag::int2 &mousePos, bool leftDown, bool rightDown, bool middleDown);
	void onMouseButton(int button, int action, const chag::int2 &mousePos);
	void onMouseScroll(float step);
	bool onKey(int key, int action);
	void update(float dt);
	void render(int width, int height);
	void deleteSelected();
	void recenterPose(TemplateLibrary::Template &t);
	void transformPose(TemplateLibrary::Template &t, const chag::float4x4 &m);

	// Copy the annotation from the given template to those that are similar and checked, if overWrite is true then it replaces 
	// annotations of the same name in the destination templates.
	void copyAnnotationToSimilar(const TemplateLibrary::Annotation &a, TemplateLibrary::Template &t, bool overWrite);

	void setPose(int newPoseInd);
	void setPoseByName(const std::string &pose);

	enum SelectionType
	{
		ST_Disc,
		ST_Annotation,
		ST_Max
	};

	float m_pitch;
	float m_yaw;
	int m_currentPose;
	int m_prevPose;
	bool m_mouseLookActive;
	chag::int2 m_lastMousePos;
	chag::int2 m_leftDownMousePos;
	float m_distanceScale;
	bool m_selectTool;
	float m_selectToolRange;
	std::set<int> m_selected;
	SelectionType m_selectionType;
	std::vector<int> m_highlighted;
	SelectionType m_highLightedType;
	std::set<std::string> m_visibleAnnotations;
	OBJModel *m_coordinatesModel;
	OBJModel *m_zArrowModel;
	chag::float4x4 m_viewMatrix;
	chag::Aabb m_aabb;
	TemplateLibraryPtr m_lib;
	int m_width;
	int m_height;
	float m_fov;
	bool m_showSimilar;
	chag::SimpleShader *m_objModelShader;
	chag::PrimitiveRenderer *m_primRenderer;
	std::vector<bool> m_similarShown;

	struct SimilarityData
	{
		int index;
		float similarity;
		chag::float4x4 tfm;
	};

	std::vector<SimilarityData> m_similarities;
	bool m_computeSimilarity;
	bool m_computeAnnotationSimilarity;
	bool m_applyTransform;
	HostTrackingBackend *m_trackingBackend;
	//std::vector<
};

#endif // _TemplateEditor_h_
