/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#ifndef _BackendSharedImplementation_h_
#define _BackendSharedImplementation_h_

#include "Config.h"
#include <vector>
#include <map>
#include <algorithm>
#include <numeric>
#include "linmath/float3.h"
#include "linmath/Aabb.h"
#include "linmath/float4x4.h"
#include "linmath/type_conversions.h"
#include <float.h>
#include "Common.h"

#include "TemplateTrackingBackend.h"
#include "utils/CudaCompat.h"
#include <Eigen/Dense>

// If not enabled, then the cone test is only performed for the top level (h) clusters. This is because the extra calculation
// required for the test was not recouped. This is dependent on the size of the h-clusters, so may change in the future.
#define ENABLE_SECONDARY_CONE_TEST 0

namespace backend_shared_utils
{
	// The caller may supply a callback that is used to debug render the data from the inner loop.
	// Obvioulsy has rather high overhead, so this dummy is used when the debugging is disabled.
	struct DummyDbgFn
	{
		FUNC_CUDA_HD void operator()(const HCluster*, const TemplateTrackingBackend::Cluster *) {}
	};

	// Helper to test intersection with the double cone anchored at the disc. Precomputes factors to make the test
	// quick to compute.
	struct FatDoubleCone
	{
		FUNC_CUDA_HD FatDoubleCone(const chag::float3 &coneApex, const chag::float3 &coneAxis, float coneAngle, float discRad)
		{
			apex = coneApex;
			axis = coneAxis;
			cosConeAngle = cosf(coneAngle);
			tanConeAngle = tanf(coneAngle);
			discRadPerp = cosConeAngle * discRad;
		}
		FUNC_CUDA_HD bool test(const chag::float3 &pt, float radius)
		{
			chag::float3 v = pt - apex;
			// absolute value for double cone, the rest works the same
			float a = fabsf(dot(v, axis));
			float b = a * tanConeAngle;
			float c = sqrtf(dot(v, v) - a*a);
			float d = c - b;
			float e = d * cosConeAngle;

			// add on the cone disc radius for fat cone test
			return e < discRadPerp + radius;
		}

		chag::float3 apex;
		chag::float3 axis;
		float tanConeAngle;
		float cosConeAngle;
		float discRadPerp;
	};
};




/**
 * Given a disc and a transform to bring this disc into the same space as the clusters and hClusters (this is usually arena-space, for no paritcular reason).
 * Iterates the clusters and computes the cone projection onto the disc for those that pass double cone test. Then tracks the highest weight, and returns the 
 * projection, point and weight. Uses the hierarchical clusters to speed things up.
 */
template <typename HCLUSTERS_T, typename CLUSTERS_T, typename TYPENAME_DBG_CALLBACK_T = backend_shared_utils::DummyDbgFn>
FUNC_CUDA_HD int processDisc(const chag::float4x4 tfm, TemplateTrackingBackend::TemplateDisc td,
	const HCLUSTERS_T &hClusters, const CLUSTERS_T &clusters,
	const TemplateTrackingBackend::Params params, const float invDistanceWeightRange,
	chag::float3 &bestPoint,
	chag::float3 &bestTemplatePoint,
	float &weight,
	float &dist,
	TYPENAME_DBG_CALLBACK_T dbgFn = backend_shared_utils::DummyDbgFn())
{
	chag::float3 position = transformPoint(tfm, td.position);
	chag::float3 dir = transformDirection(tfm, td.direction);

	backend_shared_utils::FatDoubleCone fc(position, dir, td.coneAngle, td.radius);

	int best = -1;
	float bestWeight = 0.0f;

	// The 'while-while' concept is from 'Understanding the efficiency of ray traversal on GPUs': https://dl.acm.org/citation.cfm?id=1572792
	// The idea is to ensure all threads in the warp (SIMD-thread-unit) continue processing the top level of the hierarhcy until 
	// all have found a batch of sub-clusters to process. A nested for loop yields much poorer utilization.
#define USE_WHILE_WHILE 1

#if USE_WHILE_WHILE
	int hci = 0;

	// while there is still work to do...
	const int numHClusters = int(hClusters.size());
	while (hci < numHClusters)
	{
		int clusterStart = 0;
		int clusterEnd = 0;
		// scan until we find a valid h-cluster
		for (; hci < numHClusters; ++hci)
		{
			HCluster hc = hClusters.loadAligned(hci);
#else // !USE_WHILE_WHILE
	for (int hci = 0; hci < int(hClusters.size()); ++hci)
	{
		HCluster hc = hClusters[hci];
#endif // USE_WHILE_WHILE

		if (!fc.test(hc.centre, hc.radius))
		{
			continue;
		}
		chag::float3 v = hc.centre - fc.apex;
		float hcDist = max(0.0f, length(v) - hc.radius);

		float hcDistWeight = max(0.0f, 1.0f - hcDist * invDistanceWeightRange);

		// can'td get better than this, bail out
		if (hcDistWeight < bestWeight)
		{
			continue;
		}
		clusterStart = hc.start;
		clusterEnd = hc.end;

		dbgFn(&hc, nullptr);
#if USE_WHILE_WHILE
		// found a cluster so bail out
		++hci;
		break;
		}
#endif // USE_WHILE_WHILE
	for (int ci = clusterStart; ci < clusterEnd; ++ci)
	{
		TemplateTrackingBackend::Cluster c = clusters.loadAligned(ci); // clusters[ci];

		chag::float3 lp = c.center - position;

#if ENABLE_SECONDARY_CONE_TEST
		{
			if (!fc.test(c.center, c.radius))
			{
				continue;
			}
		}
#endif // ENABLE_SECONDARY_CONE_TEST
		float dist = length(lp);

		float cosAngle = dot(c.normal, dir);
		float distWeight = max(0.0f, 1.0f - dist * invDistanceWeightRange);
		float dirWeight = max(0.0f, cosAngle);
		float weight = distWeight * dirWeight;;

		// call the debug function once per tested cluster
		dbgFn(nullptr, &c);

		{
			if (weight > bestWeight)
			{
				bestWeight = weight;
				best = ci;
			}
		}
	}
	}

	// If a 'best' index was found, we recompute the weight and also conpute the projection onto the disc (using the cone).
	if (best >= 0)
	{
		const TemplateTrackingBackend::Cluster c = clusters[best];

		chag::float3 tmp = c.center;

		// pick cone to project using which side the point is on and project to cone
		float distToDiscPlane = dot(c.center - position, dir);
		if (distToDiscPlane >= 0.0f)
		{
			chag::float3 apex0 = position - td.offset * dir;
			chag::float3 lpA = c.center - apex0;
			float distA = distToDiscPlane + td.offset;
			tmp = apex0 + lpA * td.offset / distA;
		}
		else
		{
			chag::float3 apex1 = position + td.offset * dir;
			chag::float3 lpB = c.center - apex1;
			float distB = -distToDiscPlane + td.offset;
			tmp = apex1 + lpB * td.offset / distB;
		}

		chag::float3 p = tmp;

		float cosAngle = dot(c.normal, dir);
		dist = length(c.center - p);
		float distWeight = max(0.0f, 1.0f - dist * invDistanceWeightRange);
		float dirWeight = max(0.0f, cosAngle);

		weight = distWeight * dirWeight;

		bestPoint = c.center;
		bestTemplatePoint = p;
	}
	return best;
}


#if !defined(__CUDACC__)

/**
 * Computes the transformation as a 4x4 matrix given a set of weighted point-point correspondences using SVD. Only 
 * works on CPU, as Eigen does not support calling SVD from CUDA kernels.
 */
template <typename POINTS_BUFFER_T>
inline chag::float4x4 calcTransformSvd(
	const POINTS_BUFFER_T &bestTemplatePoints_Dist,
	const POINTS_BUFFER_T &bestPoints_Weight,
	float &totalWeight,
	float &totalDistSqW)
{
	float weightSum = 0.0f;
	float distSqWSum = 0.0f;

	chag::float3 X_mean = chag::make_vector3(0.0f);
	chag::float3 Y_mean = chag::make_vector3(0.0f);
	for (size_t i = 0; i < bestPoints_Weight.size(); ++i)
	{
		chag::float4 bp_w = bestPoints_Weight.loadAligned(i);
		float weight = bp_w.w;
		if (weight > 0.0f)
		{
			chag::float4 btp_d = bestTemplatePoints_Dist.loadAligned(i);
			X_mean += make_vector3(btp_d) * weight;
			Y_mean += make_vector3(bp_w) * weight;
			weightSum += weight;
		}
	}
	float invWeightSum = 1.0f / weightSum;
	X_mean *= invWeightSum;
	Y_mean *= invWeightSum;

	/// Compute rotation 
	chag::float3x3 _sigma = {
		{ 0.0f, 0.0f, 0.0f },
		{ 0.0f, 0.0f, 0.0f },
		{ 0.0f, 0.0f, 0.0f },
	};
	for (size_t i = 0; i < bestPoints_Weight.size(); ++i)
	{
		chag::float4 bp_w = bestPoints_Weight.loadAligned(i);
		chag::float4 btp_d = bestTemplatePoints_Dist.loadAligned(i);
		if (bp_w.w > 0.0f)
		{
			distSqWSum += btp_d.w * btp_d.w * bp_w.w;

			float weight = bp_w.w * invWeightSum;
			chag::float3 xl = make_vector3(btp_d) - X_mean;
			chag::float3 yl = make_vector3(bp_w) - Y_mean;
			_sigma[0] += xl * yl.x * weight;
			_sigma[1] += xl * yl.y * weight;
			_sigma[2] += xl * yl.z * weight;
		}
	}

	Eigen::Matrix3f sigma;
	sigma <<	_sigma.c1[0], _sigma.c2[0], _sigma.c3[0],
		_sigma.c1[1], _sigma.c2[1], _sigma.c3[1],
		_sigma.c1[2], _sigma.c2[2], _sigma.c3[2];

	PROFILE_BEGIN_BLOCK("Eigen::JacobiSVD", TT_Cpu);
	Eigen::JacobiSVD<Eigen::Matrix3f> svd(sigma, Eigen::ComputeFullU | Eigen::ComputeFullV);
	PROFILE_END_BLOCK();
	Eigen::Matrix3f resRot;
	if (svd.matrixU().determinant()*svd.matrixV().determinant() < 0.0)
	{
		Eigen::Vector3f S = Eigen::Vector3f::Ones(); S(2) = -1.0;
		resRot = svd.matrixV()*S.asDiagonal()*svd.matrixU().transpose();
	}
	else
	{
		resRot = svd.matrixV()*svd.matrixU().transpose();
	}

	chag::float3x3 resRot2 = chag::make_matrix<chag::float3x3>(resRot.data());

	totalWeight = weightSum;
	totalDistSqW = distSqWSum;

	return chag::make_matrix(resRot2, Y_mean - resRot2 * X_mean);
}

#endif // !defined(__CUDACC__)



/**
 * Somewhat arbirary constraint function. Constrains the transform by:
 *  1. clamping the z component (pitch) of the forwards direction (Y-axis) to [-minMaxDirXZ,minMaxDirXZ]
 *  2. clamping the z-component of the X-axis (think  roll) to [-minMaxDirXZ,minMaxDirXZ]
 *  3. clamping the z component of the translation to range [-minMaxTransZ,minMaxTransZ].
 */
FUNC_CUDA_HD chag::float4x4 constrainTfm(const chag::float4x4 &tfm, float minMaxDirXZ, float minMaxTransZ)
{
	// 1. clamping the z component (pitch) of the forwards direction (Y-axis) to [-minMaxDirXZ,minMaxDirXZ]
	chag::float3 fwdDir = tfm.getYAxis();
	fwdDir.z = chag::clamp(fwdDir.z, -minMaxDirXZ, minMaxDirXZ);
	chag::float3 y = normalize(fwdDir);

	// 2. clamping the z-component of the X-axis (think  roll) to [-minMaxDirXZ,minMaxDirXZ]
	chag::float3 x = cross(y, tfm.getZAxis());
	x.z = chag::clamp(x.z, -minMaxDirXZ, minMaxDirXZ);
	x = normalize(x);
	chag::float3 z = normalize(cross(x, y));

	chag::float3 trans = tfm.getTranslation();
	trans.z = chag::clamp(trans.z, -minMaxTransZ, minMaxTransZ);

	chag::float4x4 m =
	{
		{ x.x, x.y, x.z, 0.0f },
		{ y.x, y.y, y.z, 0.0f },
		{ z.x, z.y, z.z, 0.0f },
		{ trans.x,  trans.y,  trans.z,  1.0f }
	};
	return m;
}




// From: "A Robust Method to Extract the Rotational Part of Deformations"
// https://dl.acm.org/citation.cfm?doid=2994258.2994269
FUNC_CUDA_HD void extractRotation(const Eigen::Matrix3f &A, Eigen::Quaternionf &q, const unsigned int maxIter)
{
	using namespace Eigen;
	for (unsigned int iter = 0; iter < maxIter; iter++)
	{
		Matrix3f R = q.matrix();
		Vector3f omega = (R.col(0).cross(A.col(0)) + R.col(1).cross(A.col(1)) +
			R.col(2).cross(A.col(2))) * (1.0f / fabs(R.col(0).dot(A.col(0)) + R.col
			(1).dot(A.col(1)) + R.col(2).dot(A.col(2))) + 1.0e-9f);
		float w = omega.norm();
		if (w < 1.0e-9f)
			break;
		q = Quaternionf(AngleAxisf(w, (1.0f / w) * omega)) * q;
		q.normalize();
	}
}

// Uses not SVD but the far more GPU friendly methond above. 
// Note: I used 'svd3', a purported GPU-friendly SVD implementation which compiles and runs but produces highly unstable results.
template <typename POINTS_BUFFER_T>
FUNC_CUDA_HD chag::float4x4 calcTransformExtractRot(
	const POINTS_BUFFER_T &bestTemplatePoints_Dist,
	const POINTS_BUFFER_T &bestPoints_Weight,
	float &totalWeight,
	float &totalDistSqW)
{
	using namespace Eigen;

	float weightSum = 0.0f;
	float distSqWSum = 0.0f;
	// TODO: do in local space of template to remove need for mean (!)
	chag::float3 X_mean = chag::make_vector3(0.0f);
	chag::float3 Y_mean = chag::make_vector3(0.0f);
	for (size_t i = 0; i < bestPoints_Weight.size(); ++i)
	{
		chag::float4 bp_w = bestPoints_Weight.loadAligned(i);
		float weight = bp_w.w;
		if (weight > 0.0f)
		{
			chag::float4 btp_d = bestTemplatePoints_Dist.loadAligned(i);
			X_mean += make_vector3(btp_d) * weight;
			Y_mean += make_vector3(bp_w) * weight;
			weightSum += weight;
		}
	}
	float invWeightSum = 1.0f / weightSum;
	X_mean *= invWeightSum;
	Y_mean *= invWeightSum;

	/// Compute transformation
	chag::float3x3 _sigma = {
		{ 0.0f, 0.0f, 0.0f },
		{ 0.0f, 0.0f, 0.0f },
		{ 0.0f, 0.0f, 0.0f },
	};
	for (size_t i = 0; i < bestPoints_Weight.size(); ++i)
	{
		chag::float4 bp_w = bestPoints_Weight.loadAligned(i);
		chag::float4 btp_d = bestTemplatePoints_Dist.loadAligned(i);
		if (bp_w.w > 0.0f)
		{
			distSqWSum += btp_d.w * btp_d.w * bp_w.w;

			float weight = bp_w.w * invWeightSum;
			chag::float3 xl = make_vector3(btp_d) - X_mean;
			chag::float3 yl = make_vector3(bp_w) - Y_mean;
			_sigma[0] += xl * yl.x * weight;
			_sigma[1] += xl * yl.y * weight;
			_sigma[2] += xl * yl.z * weight;
		}
	}

	Eigen::Matrix3f sigma;
	sigma <<	_sigma.c1[0], _sigma.c2[0], _sigma.c3[0],
		_sigma.c1[1], _sigma.c2[1], _sigma.c3[1],
		_sigma.c1[2], _sigma.c2[2], _sigma.c3[2];


	Quaternionf q = Quaternionf::Identity();
	extractRotation(sigma, q, 10);
	Eigen::Matrix3f resRot = q.matrix().transpose();

	chag::float3x3 resRot2 = chag::make_matrix<chag::float3x3>(resRot.data());

	totalWeight = weightSum;
	totalDistSqW = distSqWSum;

	return chag::make_matrix(resRot2, Y_mean - resRot2 * X_mean);
}


// Simple height-field normal estimator. Works by converting the depth to world space and finding the slope among
// the cardinal axis neighbours. Neighbours where there is a large jump in depth are ignored.
template <typename POINTS_FN_T>
FUNC_CUDA_HD chag::float4 buildNormal(int i, int j, float maxDepthRange, const POINTS_FN_T& points)
{
	using namespace chag;
	using namespace chag::type_conversions;

	chag::float4 cP = points(i, j);

	// ensure centre is valid
	if (cP.w)
	{
		// default to large value instead of infinite (so we can compare)
		chag::float4 rP = points(i + 1, j);
		chag::float4 lP = points(i - 1, j);
		chag::float4 uP = points(i, j + 1);
		chag::float4 dP = points(i, j - 1);

		chag::float3 xDir = f3(lP - rP);
		chag::float3 yDir = f3(dP - uP);

		// if slope too large or right one is invalid
		if (fabsf(xDir.z) > maxDepthRange || rP.w == 0.0f)
		{
			xDir = f3(lP - cP);
		}
		// if slope still too large or left one is invalid
		if (fabsf(xDir.z) > maxDepthRange || lP.w == 0.0f)
		{
			xDir = f3(cP - rP);
		}

		if (fabsf(yDir.z) > maxDepthRange || uP.w == 0.0f)
		{
			yDir = f3(dP - cP);
		}
		if (fabsf(yDir.z) > maxDepthRange || dP.w == 0.0f)
		{
			yDir = f3(cP - uP);
		}

		// we ignore depth discontinuities
		if (fabsf(xDir.z) < maxDepthRange && fabsf(yDir.z) < maxDepthRange)
		{
			return f4(normalize(cross(xDir, yDir)), 1.0f);
		}
	}
	return chag::make_vector(0.0f, 0.0f, 0.0f, 0.0f);
}



#endif // _BackendSharedImplementation_h_
