/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#ifndef _ClientOutput_h_
#define _ClientOutput_h_

#include "Config.h"

#include <vector>
#include <map>
#include <ClientLocationMessage.h>

#include "linmath/float3.h"
#include "linmath/Aabb.h"
#include "linmath/float4x4.h"



/**
 * Abstract interface hiding details of output to clients, implement ROS version in the future, probably, at least for Linux.
 * TODO: add time-stamps and possibly frame info?
 */
class ClientOutput
{
public:
	static constexpr int s_maxIdChars = ClientLocationMessage::s_maxIdChars;
	static const int s_maxAnnotaions = ClientLocationMessage::s_maxAnnotaions;


	using Annoation = ClientLocationMessage::Annoation;
	using Location = ClientLocationMessage::Location;

	/**
	 * send location updates to all concerned parties. 
	 */
	virtual void send(const std::vector<Location> &ls) = 0;


	static ClientOutput *create();
	// TODO: static ClientOutput *create_ros(const std::string &rosPubNameThingo, ...);
};

#endif // _ClientOutput_h_
