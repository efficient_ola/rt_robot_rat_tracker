/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#ifndef _ColorDepthSource_h_
#define _ColorDepthSource_h_

#include "Config.h"

#include <linmath/int2.h>
#include <linmath/float3.h>
#include <linmath/float2.h>
#include <vector>
#include <stdint.h>
#include <string>
#include <functional>
#include <atomic>

#include <utils/BlockingQueue.h>
#include <utils/FileDataStreamer.h>


/**
 This file contains the interface to the depth camera, the 'ColorDepthSource'.
    Create an instance using: createFromFile or createKinect2.

	Also notable is the 'FileOutputStreamerDepthSource' which is used to redirect the input to a file output stream. It is 
	implemented as a 'ColorDepthSource' and wraps the actual source such that it can record from any input source.
**/

/**
 * Returned by the ColorDepthSource read function. Color data may not be present.
 */
struct RawFrameData
{
	std::vector<uint16_t> depthData;
	int depthWidth;
	int depthHeight;
	uint32_t bytesDepthSample;
	int minDepth;
	int maxDepth;

	std::vector<uint8_t> colorData;
	int colorWidth;
	int colorHeight;
	int cBytesPerPixel;

	double timeStamp; // timestamp created by the system (using the provided time source) recorded when the frame is read from the device 
	                  // NOTE: for recordings this means that the time-stamp is from when it was _recorded_ NOT when read from the disc and so may be totally different from the current time
	double deviceTimeStamp; // Intended to be time-stamp that the device supplies, if it does, not sure.
	int sequenceNumber; // Ordered index, increases monotonically (when reading from a non-seekable stream source that is)
};

/**
 * Note: legacy declaration, inlcudes reconstructed data that may or may not be done at this point...
 */
struct FrameData : public RawFrameData
{
	std::vector<chag::float2> depthToCameraSpaceCoeffs;
	// camera space point cloud, in metric units or what?
	// Stores only valid points.
	std::vector<chag::float3> points;
	// Index of each point in points, ie index into depth data array where this point came from
	std::vector<int> pointInds;
	std::vector<chag::float3> pointColours;

	void generatePoints();
};



class ColorDepthSource
{
public:

	static const uint32_t s_colorStreamBit = 1U;
	static const uint32_t s_depthStreamBit = 1U << 1;
	static const uint32_t s_allStreamBit = s_colorStreamBit | s_depthStreamBit;

	virtual ~ColorDepthSource() {}

	/**
	 * Reading a frame retrieves the most recent frame if the underlying source is a real sensor. This means that frames may be dropped, this can be detected
	 * by looking at the time-stamps. Typically blocks until a frame is available.
	 * If the underlying source is a file stream, the source may return the next frame immediately, or possibly delay until some time-out has occured.
	 */
	virtual bool readFrameRaw(RawFrameData &frame) = 0;
	virtual void reset() = 0;
	virtual std::string getId() = 0;

	virtual bool isStopped() = 0;
	// signal shutdown and wait
	virtual void stop(bool wait = true) = 0;

	static ColorDepthSource *createKinect2(std::function<double(void)> timeSource, uint32_t flags = s_allStreamBit);
	static ColorDepthSource *createFromBlockFile(ChunkInStreamPlayerPtr inStreamer, std::function<double(void)> timeSource);

	// Image Streams from kinnect has x axis flipped...
	virtual bool isXImageAxisFlipped() const = 0;

	/**
	 * Returns -1 for live feeds.
	 */
	virtual int getFrameCount() { return -1; }
	virtual int getCurrentFrame() { return -1; }
	/**
	 * Skip to the requested frame (does nothing for live feeds).
	 */
	virtual void seek(int frame) { }

	virtual const std::vector<chag::float2> &getDepthToCameraSpaceCoeffs() const
	{
		return m_depthToCameraSpaceCoeffs;
	}
	
	struct CameraIntrinsics
	{
		chag::float2 f;
		chag::float2 p;
		float k1;
		float k2;
		float k3;
	};

	virtual const CameraIntrinsics getIntrinsics() const = 0;

protected:
	ColorDepthSource(std::function<double(void)> timeSource);
	uint32_t m_flags;
	std::vector<chag::float2> m_depthToCameraSpaceCoeffs;
	std::function<double(void)> m_timeSource;
};

/**
 * Wraps a depth source and is also capable of outputing the input to a file, uses it's own thread(s) internally to ensure data is
 * not lost. 
 * TODO: Track statistics, such as when frames are dropped?
 */
class FileOutputStreamerDepthSource : public ColorDepthSource
{
public:
	FileOutputStreamerDepthSource(std::function<double(void)> timeSource, ColorDepthSource *realSource);
	virtual ~FileOutputStreamerDepthSource() {}

	bool openOutStream(const std::string &fileName);
	void closeOutStream();
	bool isOutStreamOpen() const;
	//size_t writeQueueDepth() const { return m_outputFile.queuesize(); }
	size_t writeQueueBytes() const { return m_outputFile.queueDepthBytes(); }
	
	// if the recording is not active these just forward to the underlying source. 
	// Otherwise it is more complex
	// 1. In synchronous mode, the source does not drop frames, eg, a file. Then readFrame can just forward the call and put the frame on an internal 
	//    write-queue as well.
	// 2. In asynchronouse mode, the streamer creates its own thread to do the reading in a tight loop to ensure no frames are lost and the recording is 
	//    as close as possible to what comes off the sensor. This is needed if the source may drop frames (e.g the Kinect). 
	//    The reader thread then feeds two queues, both the disc-writer queue (of large depth, possibly finite though) and the reader queue, which is used by
	//    this function and is of max-depth 1.
	// NOTE: Mode is decided based on the underlying source, if it is seekable synchronous mode is used.
	virtual bool readFrameRaw(RawFrameData &frame);
	virtual void reset();
	virtual void seek(int frame);
	virtual const std::vector<chag::float2> &getDepthToCameraSpaceCoeffs() const;
	virtual const CameraIntrinsics getIntrinsics() const;


	// these just forward to the underlying source.
	virtual std::string getId() { return m_realSource->getId(); }
	virtual bool isXImageAxisFlipped() const { return m_realSource->isXImageAxisFlipped(); }
	virtual int getFrameCount() { return m_realSource->getFrameCount(); }
	virtual int getCurrentFrame() { return m_realSource->getCurrentFrame(); }

	virtual bool isStopped();
	// signal shutdown and wait
	virtual void stop(bool wait = true);

protected:

	// helper to write the data of a frame to the file stream thingo
	void writeFrame(const RawFrameData & frame);
	// created at startu
	void readerThreadFunc();

	using FrameRef = std::shared_ptr<RawFrameData>;

	BlockingQueue<FrameRef> m_readerQueue;

	ColorDepthSource *m_realSource;
	std::atomic<bool> m_open;
	bool m_asyncMode;
	std::thread m_writerThread;
	std::thread m_readerThread;
	size_t m_frameByteSize;
	std::atomic<bool> m_stopped;

	ChunkOutStreamer m_outputFile;
};


#endif // _ColorDepthSource_h_
