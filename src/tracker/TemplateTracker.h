/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#ifndef _TemplateTracker_h_
#define _TemplateTracker_h_

#include "Config.h"
#include <vector>
#include <map>
#include <algorithm>
#include "linmath/float3.h"
#include "linmath/Aabb.h"
#include "linmath/float4x4.h"
#include "utils/Assert.h"

#include "Common.h"

#include "DataLogger.h"

#include <float.h>


#if ENABLE_TRACKER_DEBUG_RENDERING
#define DBG_RENDERER_OBJECT_SCOPE_TR(...) DBG_RENDERER_OBJECT_SCOPE(__VA_ARGS__)
#define DBG_RENDERER_OBJECT_TR(_name_, _draw_func_) DBG_RENDERER_OBJECT(_name_, _draw_func_)
#else // 
#define DBG_RENDERER_OBJECT_SCOPE_TR(...)
#define DBG_RENDERER_OBJECT_TR(_name_, _draw_func_)
#endif // ENABLE_TRACKER_DEBUG_RENDERING


class TemplateTrackingBackend;
struct RawFrameData;
class TemplateLibrary;
using TemplateLibraryPtr = std::shared_ptr<TemplateLibrary>;


/**
 * NOTE: NOT Thread safe, this is intentional for best performance the application must deal with this where appropriate such that the code is called from a single thread.
 *       Otherwise the class must perform a lot of redundant synchronization to ensure safety.
 */
class TemplateTracker
{
public:
	TemplateTracker(bool forceCpu, const std::string &inputSourceId);

	struct Params
	{
		chag::float4x4 worldToArenaTfm;
		chag::float4x4 arenaToWorldTfm;
		float arenaRadius;

		float distanceWeightRangeScale;
		bool constrainTransform;
		float minMaxRotXZ;
		float minMaxTransZ;

		float depthClusterThreshold;
		float normalsDepthThreshold;
		int hClusterSubDivs;
		int trackingIterations;
	};

	struct Cluster
	{
		chag::Aabb aabb;
		chag::float3 normal;
		chag::float3 center;
		float radius;
		int debugColorId;
	};

	// TODO: rename objectToWorldTfm and other transforms such that space is more obvious
	class TrackedObject
	{
	public:
		TrackedObject(const std::string &id, const std::string &objectClass) : m_active(false), m_id(id), m_objectClass(objectClass), m_capturePoseNextFrame(false) {}

		virtual void update(TemplateTracker *tracker) = 0;

		const chag::float4x4 &getObjectToWorldTfm() const { return m_objectToWorldTfm; }
		const chag::Aabb &getAabb() const { return m_aabb; }

		// TODO: Sort ouf these flags... are they maintained?
		bool isActive() const { return m_active; }
		bool isFound() const { return m_found; }

		virtual void outputLogInfo(DataLogger &logger);
		const std::string &getObjectClass() const { return m_objectClass; }

		void setCaptureNextFrame() { m_capturePoseNextFrame = true; }
	protected:
		friend class TemplateTracker;

		virtual void resetTransform(const chag::float4x4 &t) { m_objectToWorldTfm = t; }

		chag::float4x4 m_objectToWorldTfm;
		chag::Aabb m_aabb;
		bool m_active;
		bool m_found;
		bool m_capturePoseNextFrame;
		std::string m_id;
		std::string m_objectClass;
	};
	using TrackedObjectPtr = std::shared_ptr<TrackedObject>;
	using ObjMap = std::map<std::string, TrackedObjectPtr>;

	class TrackedMultiTemplateObject : public TrackedObject
	{
	public:

		struct TemplateDisc
		{
			chag::float3 position;
			chag::float3 direction;
			float radius;
			float offset;
			float coneAngle;
		};
		struct DiscTemplate
		{
			std::string name;
			bool isDefaultPose;
			chag::Aabb m_aabb;
			std::vector<TemplateDisc> m_sampleDiscs;


			void generateDependentData()
			{
				m_aabb = chag::make_inverse_extreme_aabb();
				// from solid angle we can get the average cone angle (not accounting for distortion due to ellipsoid objectToWorldTfm, maybe we should?)
				float averageConeAngle = acosf(1.0f - 1.0f / float(m_sampleDiscs.size()));
				for (auto &sd : m_sampleDiscs)
				{
					sd.coneAngle = averageConeAngle;
					sd.offset = sd.radius  / tanf(averageConeAngle);
					m_aabb = chag::combine(m_aabb, chag::make_aabb(sd.position, sd.radius));
				}
			}
		};

		static DiscTemplate generateEllipsoidTemplate(chag::float3 ellipsiodRanges, int subDivisions);

		TrackedMultiTemplateObject(const std::string &id, const std::string &objectClass, const std::vector<DiscTemplate> &templates, const chag::float4x4 &initialTransform, TemplateLibraryPtr templateLibrary) :
			TrackedObject(id, objectClass),
			m_templateLibrary(templateLibrary)
		{
			m_reset = false;
			m_templates = templates;
			m_objectToWorldTfm = initialTransform;
			m_initialTfm = initialTransform;
			m_aabb = templates[0].m_aabb;
			m_prevBestPose = -1;
			m_prevBestConfidence = 0.0f;
			for (const auto &t : templates)
			{
				m_aabb = combine(m_aabb, t.m_aabb);
			}
		}

		virtual void resetTransform(const chag::float4x4 &t)
		{
			m_reset = true;
			m_initialTfm = t;
			m_objectToWorldTfm = t;
		}

		virtual void update(TemplateTracker *tracker);


		void outputLogInfo(DataLogger& logger);

		bool m_reset;
		chag::float4x4 m_initialTfm;
		std::vector<DiscTemplate> m_templates;
#if LOG_ALL_CONFIDENCES
		std::vector<float> m_poseConfidences;
#endif // LOG_ALL_CONFIDENCES
		int m_prevBestPose;
		float m_prevBestConfidence;
		TemplateLibraryPtr m_templateLibrary;
	};

	void addObject(TrackedObjectPtr o)
	{
		m_objects[o->m_id] = o;
	}

	TrackedObjectPtr get(const std::string &id)
	{
		auto it = m_objects.find(id);
		if (it != m_objects.end())
		{
			ASSERT((*it).second->m_id == id);
			return (*it).second;
		}
		return 0;
	}

	void setDepthToCameraCoeffs(const std::vector<chag::float2> &coeffs, bool forceOverwrite);

	void update(const RawFrameData &frame, const Params &params, int inputFrameNumber);

	void setTransform(const std::string &id, const chag::float4x4 &tfm)
	{
		auto it = m_objects.find(id);
		if (it != m_objects.end())
		{
			ASSERT((*it).second->m_id == id);
			(*it).second->resetTransform(tfm);
		}
	}

	ObjMap::iterator begin() { return m_objects.begin(); }
	ObjMap::iterator end() { return m_objects.end(); }

	void clearObjects()
	{
		m_objects.clear();
	}

	TemplateTrackingBackend *m_trackingBackend;
protected:
	Params m_params;
	ObjMap m_objects;
	std::string m_inputSourceId;
	int m_inputSourceFrameNumber;
};


#endif // _TemplateTracker_h_
