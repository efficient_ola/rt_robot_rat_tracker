/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#include "ImageStreamOutput.h"
#include "stb_image_write.h"
#include <boost/filesystem.hpp>
#include <sstream>

class ImageStreamOutputImages : public ImageStreamOutput
{
public:
	ImageStreamOutputImages(const std::string &fileNameFormat) 
		: m_fileNameFormat(fileNameFormat)
	{
		m_outputTga = m_fileNameFormat.rfind(".tga") == m_fileNameFormat.length() - strlen(".tga")
			|| m_fileNameFormat.rfind(".TGA") == m_fileNameFormat.length() - strlen(".TGA");
	}

	virtual void writeFrame(int width, int height, const uint8_t *pixelDataRgb)
	{
		assert(m_isOpen);
		char fileName[512];
		sprintf(fileName, m_fileNameFormat.c_str(), m_frameNumber++);
		std::string imagePath = m_outputPath + "/" + std::string(fileName);

		stbi_flip_vertically_on_write(1);
		if (m_outputTga)
		{
			stbi_write_tga(imagePath.c_str(), width, height, 3, pixelDataRgb);
		}
		else
		{
			stbi_write_png(imagePath.c_str(), width, height, 3, pixelDataRgb, 3 * width);
		}
	}

	virtual bool internalClose(bool flush)
	{
		m_frameNumber = 0;
		return true;
	}

	virtual bool internalOpen() 
	{
		boost::filesystem::create_directories(boost::filesystem::path(m_outputPath));
		m_frameNumber = 0;
		return true;
	}

	virtual bool outputExists(const std::string &outputPath)
	{
		return boost::filesystem::is_directory(boost::filesystem::path(outputPath));
	}


	std::string m_fileNameFormat;
	int m_frameNumber;
	bool m_outputTga;
};



class ImageStreamOutputFFmpeg : public ImageStreamOutput
{
public:
	ImageStreamOutputFFmpeg(const std::string &ffmpegExePath)//, const std::vector<std::string> &extraOptions)
		: m_ffmpegExePath(ffmpegExePath),
			m_ffmpegFile(nullptr)
	{
	}

	virtual void writeFrame(int width, int height, const uint8_t *pixelDataRgb)
	{
		assert(m_width == width);
		assert(m_height == height);
		assert(m_ffmpegFile);
		fwrite(pixelDataRgb, sizeof(uint8_t)*3*width*height, 1, m_ffmpegFile);
	}

	virtual bool internalOpen()
	{
		std::replace(m_outputPath.begin(), m_outputPath.end(), '/', '\\');
		boost::filesystem::create_directories(boost::filesystem::path(m_outputPath).remove_filename());

		std::stringstream cmd;
		cmd << m_ffmpegExePath << " -f rawvideo -pix_fmt rgb24 "
			<< "-s " << m_width << "x" << m_height << " "
			<< " -r 30 -i - -y -threads 4 "
			<< "-preset veryfast " // Encoding speed: ultrafast, superfast, veryfast, faster, fast, medium, slow, slower, veryslow
			<< "-c:v libx264 -pix_fmt yuv420p -crf 18 " // codec & quality. Range is logarithmic 0 (lossless) to 51 (worst quality). Default is 23.
			<< "-vf vflip "
			//<< "asd.mp4";
		<< m_outputPath << ".mp4";
		
		m_ffmpegFile = _popen(cmd.str().c_str(), "wb");

		return m_ffmpegFile != nullptr;
	}

	virtual bool outputExists(const std::string &outputPath)
	{
		return boost::filesystem::exists(outputPath + ".mp4");
	}

	virtual bool internalClose(bool flush)
	{
		_pclose(m_ffmpegFile);
		return true;
	}


	std::string m_ffmpegExePath;
	FILE *m_ffmpegFile;
};




ImageStreamOutput *ImageStreamOutput::createFfmpeg(const std::string &ffmpegExePath)
{
	if (boost::filesystem::exists(ffmpegExePath))
	{
		return new ImageStreamOutputFFmpeg(ffmpegExePath);
	}
	else
	{
		return nullptr;
	}
}

ImageStreamOutput *ImageStreamOutput::createImage(const char *fileNameFormat)
{
	return new ImageStreamOutputImages(fileNameFormat);
}
