//
// This code is not originally part of this project and was published at CodeProject:
// https://www.codeproject.com/Articles/207464/Exception-Handling-in-Visual-Cplusplus
// License: https://www.codeproject.com/info/cpol10.aspx
//

#pragma once

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
#endif // WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <tchar.h>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <new.h>
#include <signal.h>
#include <exception>
#include <sys/stat.h>
#include <psapi.h>
#include <rtcapi.h>
#include <Shellapi.h>
#include <dbghelp.h>


// TODO: reference additional headers your program requires here
