/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#include "TemplateTrackingBackend.h"
#include "cuda/CudaBackend.h"
#include "HostTrackingBackend.h"

#include "NearTree.h"
#include <profiler/Profiler.h>

TemplateTrackingBackend * TemplateTrackingBackend::create(bool forceCpu)
{
#if ENABLE_CUDA
	// Unless CPU is forced, we'll try creating a  GPU backend first
	if (!forceCpu)
	{
		TemplateTrackingBackend * be = CudaBackend::create();
		if (be)
		{
			return be;
		}
	}
#endif // ENABLE_CUDA

	// fall through to CPU backend
	return new HostTrackingBackend();
}



namespace
{
	struct ClusterNearTreeAdaptor
	{
		static const chag::float3 &position(const CudaMirrorBuffer<TemplateTrackingBackend::Cluster> &p, int i) { return p[i].center; }
	};
};

void TemplateTrackingBackend::rebuildHierearchicalClustersHost(const chag::float4x4 &arenaToAabbTfm)
{
	using Tree = NearTree<ClusterNearTreeAdaptor, 32>;
	Tree nt;
	{
		PROFILE_SCOPE("NearTree::build", TT_Cpu);
		nt.build(m_clusters);
	}
	// reshuffle clusters to avoid indirection and improve coherency
	std::vector<Cluster> clusters2(m_clusters.size());
	m_hClusters.reserve(m_clusters.size());
	m_hClusters.clear();
	{
		PROFILE_SCOPE("hClusters", TT_Cpu);

		for (int li : nt.m_leafNodeInds)
		{
			const Tree::Node &n = nt.m_tree[li];
			HCluster hc;
			hc.centre = n.aabb.getCentre();
			hc.radius = length(n.aabb.getHalfSize());
#if 1
			// reshuffle clusters to avoid indirection and improve coherency
			for (int ii = n.start; ii < n.end; ++ii)
			{
				const Cluster &c = m_clusters[nt.m_indices[ii]];
				clusters2[ii] = c;

				// TODO: build normal cone?
			}
#endif
			hc.start = n.start;
			hc.end = n.end;
			m_hClusters.push_back(hc);
		}
		m_clusters = clusters2;
	}

	// sort in order from the innermost to outermost - might not do much
	std::sort(m_hClusters.begin(), m_hClusters.end(), [&](const HCluster &a, const HCluster &b) { return length(transformPoint(arenaToAabbTfm, a.centre)) <  length(transformPoint(arenaToAabbTfm, b.centre)); });
}