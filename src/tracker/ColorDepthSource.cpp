/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#include "ColorDepthSource.h"
#define NOMINMAX
#if ENABLE_KINECT
#include <Kinect.h>
#endif // ENABLE_KINECT
#include <assert.h>
#include <boost/filesystem.hpp>
#include "utils/FileUtils.h"
#include <boost/algorithm/string.hpp>    

#define LOGF(...)

// Safe release for interfaces
template<class Interface>
inline void SafeRelease(Interface *& pInterfaceToRelease)
{
	if (pInterfaceToRelease != NULL)
	{
		pInterfaceToRelease->Release();
		pInterfaceToRelease = NULL;
	}
}

template <typename T>
T read(FILE *f)
{
	T tmp = T(-1);
	fread(&tmp, sizeof(tmp), 1, f);
	return tmp;
}


template <typename T>
bool read(T &v, FILE *f)
{
	size_t numRead = fread(&v, sizeof(v), 1, f);
	return numRead == 1;
}

template <typename T>
bool read(std::vector<T> &v, int size, FILE *f)
{
	v.resize(size);
	size_t numRead = size != 0 ? fread(&v[0], sizeof(v[0]), v.size(), f) : 0U;
	return numRead == v.size();
}

inline chag::float3 bgrToFloat3(const uint8_t *c, int n)
{
	assert(n >= 3);
	uint8_t r = c[2];
	uint8_t g = c[1];
	uint8_t b = c[0];
	return chag::make_vector(float(r), float(g), float(b)) * (1.0f / 255.0f);
}


void FrameData::generatePoints()
{
	points.clear();
	pointInds.clear();
	pointColours.clear();
	points.reserve(depthData.size());
	pointInds.reserve(depthData.size());
	pointColours.reserve(depthData.size());

	const uint8_t defaultColour[] = { 192U, 192U, 192U, 255U };

	for (size_t i = 0; i < depthData.size(); ++i)
	{
		float depth = float(depthData[i]);
		chag::float2 trans = depthToCameraSpaceCoeffs[i];
		if (depth > 0.0f && isfinite(trans.x) && isfinite(trans.y))
		{
			const uint8_t *c = colorData.empty() ? defaultColour : &colorData[i * cBytesPerPixel];
			// scale to metres and camera space...
			chag::float3 camPt = chag::make_vector(trans.x * depth, trans.y * depth, depth) * (1.0f / 1000.0f);
			points.push_back(camPt);
			pointInds.push_back(int(i));
			pointColours.push_back(bgrToFloat3(c, cBytesPerPixel));
		}
	}
}



ColorDepthSource::ColorDepthSource(std::function<double(void)> timeSource)
	: m_timeSource(timeSource), 
		m_flags(0) 
{
}


class ColorDepthSource_Blocks : public ColorDepthSource
{
public:

	ColorDepthSource_Blocks(ChunkInStreamPlayerPtr inStreamer, std::function<double(void)> timeSource)
		: ColorDepthSource(timeSource),
			m_in(inStreamer->getStream()),
			m_fileName(inStreamer->getFileName()),
			m_frameBlockTag("depth_frame"),
			m_headerBlockTag("depth_header"),
			m_player(inStreamer)
	{
		if (m_in->isOpen() && m_in->m_tagIndex.count(m_headerBlockTag) > 0)
		{
			ChunkInStreamer::BlockReader br = m_in->readBlock(m_in->m_tagIndex[m_headerBlockTag][0]);
			br.read(m_depthToCameraSpaceCoeffs);
			br.read(m_depthCameraIntrisics);
		}

		double currT = m_player->getCurrT();

		const auto &frameHeaders = m_in->m_tagIndex[m_frameBlockTag];
		// automatically set to first frame time if not initialized...
		if (currT < 0.0)
		{
			currT = frameHeaders.front().timeStamp;
			m_player->seekTime(currT);
		}
	}

	virtual ~ColorDepthSource_Blocks() override
	{
	}
	bool readFrameRaw(RawFrameData &frame) override
	{
		std::lock_guard<std::mutex> lock(m_mutex);

		const auto &frameHeaders = m_in->m_tagIndex[m_frameBlockTag];

		double currT = m_player->getCurrT();

		// automatically set to first frame time if not initialized...
		if (currT < 0.0)
		{
			currT = frameHeaders.front().timeStamp;
			m_player->seekTime(currT);
		}

		auto bhIt = m_in->begin(m_frameBlockTag, currT);

		// if not paused, we allow the frame source to drive the playback. This might seem weird
		// but we want to ensure that frames are read sequentially which is easiest if we drive the playback from the 
		// depth source.
		if (!m_player->paused())
		{
			// 1. step to next frame
			++bhIt;
			if (bhIt != m_in->end(m_frameBlockTag))
			{
				m_player->advance((*bhIt).timeStamp);
			}
		}

		if (bhIt == m_in->end(m_frameBlockTag))
		{
			return false;
		}

		ChunkInStreamer::BlockReader br = m_in->readBlock(*bhIt);
		// TODO: we can release the lock here, really

		if (br.read(frame.sequenceNumber) &&
			br.read<double>(frame.timeStamp) &&
			br.read<double>(frame.deviceTimeStamp) &&
			br.read<int>(frame.depthWidth) &&
			br.read<int>(frame.depthHeight) &&
			br.read<uint32_t>(frame.bytesDepthSample) &&
			br.read<int>(frame.minDepth) &&
			br.read<int>(frame.maxDepth) &&
			br.read(frame.depthData) &&
			br.read<int>(frame.colorWidth) &&
			br.read<int>(frame.colorHeight) &&
			br.read<int>(frame.cBytesPerPixel) &&
			br.read(frame.colorData))
		{
			frame.timeStamp = br.m_timeStamp;
			return true;
		}
		return false;
	}

	virtual void reset() override
	{
		std::lock_guard<std::mutex> lock(m_mutex);
		const auto &frameHeaders = m_in->m_tagIndex[m_frameBlockTag];
		double currT = frameHeaders.front().timeStamp;
		m_player->seekTime(currT);
	}

	virtual std::string getId()
	{
		// strip away any absolute path info
		std::string tmp = boost::filesystem::basename(m_fileName) + boost::filesystem::extension(m_fileName);
		// remove dots
		std::replace(tmp.begin(), tmp.end(), '.', '_');
		return tmp;
	}

	virtual bool isXImageAxisFlipped() const { return true; }

	virtual int getFrameCount() 
	{ 
		return int(m_in->m_tagIndex[m_frameBlockTag].size());
	}
	/**
	* Skip to the requested frame (does nothing for live feeds).
	*/
	virtual void seek(int frame) 
	{ 
		std::lock_guard<std::mutex> lock(m_mutex);

		const auto &frameHeaders = m_in->m_tagIndex[m_frameBlockTag];
		frame = std::max(0, std::min(frame, int(frameHeaders.size() - 1)));
		m_player->seekTime(frameHeaders[frame].timeStamp);

	}

	virtual int getCurrentFrame()
	{
		std::lock_guard<std::mutex> lock(m_mutex);

		double currT = m_player->getCurrT();
		const auto &frameHeaders = m_in->m_tagIndex[m_frameBlockTag];
		auto bhIt = m_in->begin(m_frameBlockTag, currT);
		if (bhIt != frameHeaders.end())
		{
			return int(bhIt - frameHeaders.begin());
		}
		return -1;
	}

	virtual bool isStopped()
	{
		std::lock_guard<std::mutex> lock(m_mutex);
		return !m_in->isOpen();
	}

	virtual void stop(bool wait)
	{
		std::lock_guard<std::mutex> lock(m_mutex);

		m_in->close();
	}

	virtual const CameraIntrinsics getIntrinsics() const override 
	{ 
		return m_depthCameraIntrisics; 
	}

	std::mutex m_mutex;
	ChunkInStreamerPtr m_in;
	std::string m_fileName;

	std::string m_frameBlockTag;
	std::string m_headerBlockTag;
	ChunkInStreamPlayerPtr m_player;
	CameraIntrinsics m_depthCameraIntrisics;
};


ColorDepthSource *ColorDepthSource::createFromBlockFile(ChunkInStreamPlayerPtr inStreamer, std::function<double(void)> timeSource)
{
	return new ColorDepthSource_Blocks(inStreamer, timeSource);
}


#if ENABLE_KINECT

class ColorDepthSource_Kinect2 : public ColorDepthSource
{
public:
	IMultiSourceFrameReader *m_multiFrameReader;
	IKinectSensor* m_kinectSensor;
	ICoordinateMapper *m_coordinateMapper;
	std::vector<ColorSpacePoint> m_coordinateMappings;
	std::vector<RGBQUAD> m_colourTmp;
	int m_actualColorWidth;
	int m_actualColorHeight;
	PointF *m_depthToCameraSpaceMap;
	uint32_t m_numDepthToCameraMapPts;
	int m_sequenceNumber;
	WAITABLE_HANDLE m_frameEventHandle;
	CameraIntrinsics m_depthCameraIntrisics;

	ColorDepthSource_Kinect2(IKinectSensor* kinectSensor, IMultiSourceFrameReader *multiFrameReader, uint32_t flags, std::function<double(void)> timeSource) :
		ColorDepthSource(timeSource),
		m_kinectSensor(kinectSensor),
		m_multiFrameReader(multiFrameReader), 
		m_actualColorWidth(0), 
		m_actualColorHeight(0),
		m_sequenceNumber(0)
	{
		m_flags = flags;

		m_kinectSensor->get_CoordinateMapper(&m_coordinateMapper);

		m_frameEventHandle = 0;

		HRESULT hr = m_multiFrameReader->SubscribeMultiSourceFrameArrived(&m_frameEventHandle);
		if (FAILED(hr)) {
			printf("Couldn't subscribe frame\n");
		}

		m_depthToCameraSpaceMap = 0;
		m_numDepthToCameraMapPts = 0;
	}

	virtual ~ColorDepthSource_Kinect2() override
	{
		SafeRelease(m_coordinateMapper);
		SafeRelease(m_multiFrameReader);
		SafeRelease(m_kinectSensor);
	}

	virtual bool isStopped()
	{
		if (m_kinectSensor == nullptr)
		{
			return false;
		}
		BOOLEAN isOpen = FALSE;
		m_kinectSensor->get_IsOpen(&isOpen);

		return isOpen != TRUE;
	}

	virtual void stop(bool wait)
	{
		if (m_kinectSensor)
		{
			m_kinectSensor->Close();
		}
	}


	bool readFrameRaw(RawFrameData &frame) override
	{
		if (isStopped())
		{
			return false;
		}


		HRESULT hr = S_OK;


		IMultiSourceFrameArrivedEventArgs *pFrameArgs = nullptr;
		do
		{
			hr = E_FAIL;
			
			HANDLE handles[] = { reinterpret_cast<HANDLE>(m_frameEventHandle) };
			
			uint32_t idx = WaitForMultipleObjects(1, handles, FALSE, 100);
			switch (idx) 
			{
			case WAIT_TIMEOUT:
				//printf("timeout!\n");
				break;
			case WAIT_OBJECT_0:
				hr = m_multiFrameReader->GetMultiSourceFrameArrivedEventData(m_frameEventHandle, &pFrameArgs);
				//frame arrived
				break;
				// NOTE: we could do this, but sinec the things is waking up 10x/s anyway at the moment then we will not bother.
				/*case WAIT_OBJECT_0 + 1:
				quit = true;
				continue;*/
			default:
				printf("Impossible!\n");
			}

			if (isStopped())
			{
				return false;
			}
		}
		while (!SUCCEEDED(hr)); //TODO: Add timeout(!)


		//printf("Got Frame: %d!\n", m_sequenceNumber);
		if (SUCCEEDED(hr))
		{
			HRESULT hr;
			IMultiSourceFrameReference *pFrameReference = nullptr;

			//cout << "got a valid frame" << endl;
			hr = pFrameArgs->get_FrameReference(&pFrameReference);
			if (SUCCEEDED(hr))
			{

				IMultiSourceFrame* mFrame = 0;

				//cout << "got a valid frame" << endl;
				hr = pFrameReference->AcquireFrame(&mFrame);
				if (SUCCEEDED(hr))
				{
					IDepthFrameReference *dfr = 0;
					if (SUCCEEDED(mFrame->get_DepthFrameReference(&dfr)))
					{
						IDepthFrame* pDepthFrame = NULL;
						if (SUCCEEDED(dfr->AcquireFrame(&pDepthFrame)))
						{
							INT64 nTime = 0;
							IFrameDescription* pFrameDescription = NULL;
							int nWidth = 0;
							int nHeight = 0;
							USHORT nDepthMinReliableDistance = 0;
							USHORT nDepthMaxDistance = 0;

							hr = pDepthFrame->get_RelativeTime(&nTime);
							//printf("  nTime %d!\n", int(nTime));

							if (SUCCEEDED(hr))
							{
								hr = pDepthFrame->get_FrameDescription(&pFrameDescription);
								//printf("  pFrameDescription %d!\n", int(pFrameDescription));
							}

							if (SUCCEEDED(hr))
							{
								hr = pFrameDescription->get_Width(&nWidth);
							}

							if (SUCCEEDED(hr))
							{
								hr = pFrameDescription->get_Height(&nHeight);
							}

							if (SUCCEEDED(hr))
							{
								hr = pDepthFrame->get_DepthMinReliableDistance(&nDepthMinReliableDistance);
							}

							if (SUCCEEDED(hr))
							{
								// In order to see the full range of depth (including the less reliable far field depth)
								// we are setting nDepthMaxDistance to the extreme potential depth threshold
								nDepthMaxDistance = USHRT_MAX;
								//printf("  nDepthMaxDistance %d!\n", int(nDepthMaxDistance));

								// Note:  If you wish to filter by reliable depth distance, uncomment the following line.
								//// hr = pDepthFrame->get_DepthMaxReliableDistance(&nDepthMaxDistance);
							}

							UINT16 *pBuffer = NULL;
							UINT nBufferSize = 0;
							if (SUCCEEDED(hr))
							{
								hr = pDepthFrame->AccessUnderlyingBuffer(&nBufferSize, &pBuffer);
								//printf("  nBufferSize %d!\n", nBufferSize);
							}

							if (SUCCEEDED(hr))
							{
								// Copy the depth data into sensible format:
								if (frame.depthData.size() != nWidth * nHeight)
								{
									frame.depthData.resize(nWidth * nHeight);
								}
								assert(nBufferSize == int(frame.depthData.size()));
								memcpy(&frame.depthData[0], pBuffer, sizeof(pBuffer[0]) * frame.depthData.size());
								frame.bytesDepthSample = 2;
								frame.depthWidth = nWidth;
								frame.depthHeight = nHeight;
								frame.minDepth = nDepthMinReliableDistance;
								frame.maxDepth = nDepthMaxDistance;
								// Apparently in increments of 100ns so divide by 10 000 000 to get to seconds.
								frame.deviceTimeStamp = double(nTime) / 10000000.0;
								//printf("  FSZ 0 %d!\n", int(frame.depthData.size()));
							}
							else
							{
								printf("Yikes!\n");
							}
							//printf("  FSZ 1 %d!\n", int(frame.depthData.size()));

							SafeRelease(pFrameDescription);
							SafeRelease(pDepthFrame);
						}
						else
						{
							printf("Failed to Acquire the depth frame! What's with that\n");
						}
						SafeRelease(dfr);
					}
					else
					{
						printf("Got NO depth frame! What's with that\n");
					}

					//printf("  FSZ 2 %d!\n", int(frame.depthData.size()));

					if (m_flags & s_colorStreamBit)
					{
						IColorFrameReference *cfr = 0;
						if (SUCCEEDED(mFrame->get_ColorFrameReference(&cfr)))
						{
							IColorFrame* pColorFrame = NULL;
							if (SUCCEEDED(cfr->AcquireFrame(&pColorFrame)))
							{
								INT64 nTime = 0;
								IFrameDescription* pFrameDescription = NULL;
								int nWidth = 0;
								int nHeight = 0;
								ColorImageFormat imageFormat = ColorImageFormat_None;

								hr = pColorFrame->get_RelativeTime(&nTime);

								if (SUCCEEDED(hr))
								{
									hr = pColorFrame->get_FrameDescription(&pFrameDescription);
								}

								if (SUCCEEDED(hr))
								{
									hr = pFrameDescription->get_Width(&nWidth);
								}

								if (SUCCEEDED(hr))
								{
									hr = pFrameDescription->get_Height(&nHeight);
								}

								if (SUCCEEDED(hr))
								{
									hr = pColorFrame->get_RawColorImageFormat(&imageFormat);
								}

								if (SUCCEEDED(hr))
								{
									/*if (imageFormat == ColorImageFormat_Bgra)
									{
									hr = pColorFrame->AccessRawUnderlyingBuffer(&nBufferSize, reinterpret_cast<BYTE**>(&pBuffer));
									}
									else if (m_pColorRGBX)*/
									{
										static_assert(sizeof(RGBQUAD) == 4, "Agm!");

										const int nBufferSize = nWidth * nHeight * sizeof(RGBQUAD);
										if (m_colourTmp.size() != nWidth * nHeight * 4)
										{
											m_colourTmp.resize(nWidth * nHeight * 4);
										}

										hr = pColorFrame->CopyConvertedFrameDataToArray(nBufferSize, reinterpret_cast<uint8_t*>(&m_colourTmp[0]), ColorImageFormat_Bgra);
									}
									/*else
									{
										hr = E_FAIL;
									}*/
								}

								if (SUCCEEDED(hr))
								{
									frame.cBytesPerPixel = 4;
									m_actualColorWidth = nWidth;
									m_actualColorHeight = nHeight;
								}

								SafeRelease(pFrameDescription);

								SafeRelease(pColorFrame);
							}
							SafeRelease(cfr);
						}
					}
					SafeRelease(mFrame);
				}
				SafeRelease(pFrameReference);
			}
			SafeRelease(pFrameArgs);
		}
		if (SUCCEEDED(hr))
		{
			frame.sequenceNumber = m_sequenceNumber++;
			m_coordinateMappings.resize(frame.depthData.size());
			if (!frame.depthData.empty())
			{
				m_coordinateMapper->MapDepthFrameToColorSpace(uint32_t(frame.depthData.size()), &frame.depthData[0], uint32_t(m_coordinateMappings.size()), &m_coordinateMappings[0]);
			}
			frame.colorWidth = m_colourTmp.empty() ? 0 : frame.depthWidth;
			frame.colorHeight = m_colourTmp.empty() ? 0 : frame.depthHeight;
			frame.cBytesPerPixel = 4;

			if (frame.colorData.size() != frame.colorWidth * frame.colorHeight * 4)
			{
				frame.colorData.resize(frame.colorWidth * frame.colorHeight * 4);
			}
			
			// Remap colour frame to depth frame.
			if (RGBQUAD *outPtr = frame.colorData.empty() ? 0 : reinterpret_cast<RGBQUAD *>(&frame.colorData[0]))
			{
				RGBQUAD defaultColour = { 192U, 192U, 192U, 0 };
				for (size_t i = 0; i < frame.depthData.size(); ++i)
				{
					ColorSpacePoint point = m_coordinateMappings[i];

					// round down to the nearest pixel
					int colorX = int(point.X + 0.5);
					int colorY = int(point.Y + 0.5);

					// make sure the pixel is part of the image
					if (colorX >= 0 && colorX < m_actualColorWidth && colorY >= 0 && colorY < m_actualColorHeight)
					{
						RGBQUAD tmp = m_colourTmp[m_actualColorWidth * colorY + colorX];
						outPtr[i] = tmp;
					}
					else
					{
						outPtr[i] = defaultColour;
					}
				}
			}


			// This only happens the first time around
			if (!m_depthToCameraSpaceMap)
			{
				m_coordinateMapper->GetDepthFrameToCameraSpaceTable(&m_numDepthToCameraMapPts, &m_depthToCameraSpaceMap);
				static_assert(sizeof(m_depthToCameraSpaceMap[0]) == sizeof(m_depthToCameraSpaceCoeffs[0]), "Oh my!");
				m_depthToCameraSpaceCoeffs.clear();
				const chag::float2 *ftp = reinterpret_cast<const chag::float2 *>(m_depthToCameraSpaceMap);
				m_depthToCameraSpaceCoeffs.insert(m_depthToCameraSpaceCoeffs.begin(), ftp, ftp + m_numDepthToCameraMapPts);

				::CameraIntrinsics intr;
				m_coordinateMapper->GetDepthCameraIntrinsics(&intr);
				m_depthCameraIntrisics.f = chag::make_vector(intr.FocalLengthX, intr.FocalLengthY);
				m_depthCameraIntrisics.p = chag::make_vector(intr.PrincipalPointX, intr.PrincipalPointY);
				m_depthCameraIntrisics.k1 = intr.RadialDistortionSecondOrder;
				m_depthCameraIntrisics.k2 = intr.RadialDistortionFourthOrder;
				m_depthCameraIntrisics.k3 = intr.RadialDistortionSixthOrder;
			}
		}

		if (frame.depthData.size() == 0)
		{
			printf("Noooo!\n");
		}

		//printf("Input complete: %d!\n", m_sequenceNumber);

		return SUCCEEDED(hr);
	}

	virtual std::string getId()
	{
		/*WCHAR buffo[1024];
		m_kinectSensor->get_UniqueKinectId(1024, buffo);
		char ch[1024];
		char defChar = ' ';
		WideCharToMultiByte(CP_ACP, 0, buffo, -1, ch, 1024, &defChar, NULL);
		return ch;*/
		return "kinect_2";
	}

	virtual bool isXImageAxisFlipped() const { return true; }

	virtual void reset() override	{ }


	virtual const CameraIntrinsics getIntrinsics() const override { return m_depthCameraIntrisics; }
};





ColorDepthSource *ColorDepthSource::createKinect2(std::function<double(void)> timeSource, uint32_t flags)
{
	HRESULT hr;

	IKinectSensor* kinectSensor;
	hr = GetDefaultKinectSensor(&kinectSensor);
	if (FAILED(hr) || !kinectSensor)
	{
		return 0;
	}

	// Initialize the Kinect and get the depth reader
	hr = kinectSensor->Open();
	if (FAILED(hr))
	{
		SafeRelease(kinectSensor);
		return 0;
	}

	DWORD sourceTypes = 0U;
	if (flags & s_colorStreamBit)
	{
		sourceTypes |= FrameSourceTypes::FrameSourceTypes_Color;
	}
	if (flags & s_depthStreamBit)
	{
		sourceTypes |= FrameSourceTypes::FrameSourceTypes_Depth;
	}

	// We depend on deoth being read for a few things
	assert(sourceTypes & FrameSourceTypes::FrameSourceTypes_Depth);

	IMultiSourceFrameReader *multiFrameReader = 0;
	hr = kinectSensor->OpenMultiSourceFrameReader(sourceTypes, &multiFrameReader);

	if (FAILED(hr))
	{
		SafeRelease(kinectSensor);
		return 0;
	}

	return new ColorDepthSource_Kinect2(kinectSensor, multiFrameReader, flags, timeSource);
}
#else //!ENABLE_KINECT
ColorDepthSource *ColorDepthSource::createKinect2(std::function<double(void)> timeSource, uint32_t flags)
{
	return 0;
}
#endif // ENABLE_KINECT




FileOutputStreamerDepthSource::FileOutputStreamerDepthSource(std::function<double(void)> timeSource, ColorDepthSource * realSource)
	: ColorDepthSource(timeSource),
	m_realSource(realSource),
	m_open(false),
	m_asyncMode(false),
	m_readerQueue(30),
	m_stopped(false),
	m_outputFile(timeSource)
{ 
	// if no known frame count, then assume frames may be lost if not read fast enough, so create reader thread.
	m_asyncMode = m_realSource->getFrameCount() == -1;

	if (m_asyncMode)
	{
		m_readerThread = std::thread(&FileOutputStreamerDepthSource::readerThreadFunc, this);
	}
}




bool FileOutputStreamerDepthSource::openOutStream(const std::string &fileName)
{
	m_outputFile.open(fileName);

	auto b = m_outputFile.beginBlock("depth_header", "2.0");
	b.write(getDepthToCameraSpaceCoeffs());
	b.write(getIntrinsics());
	m_outputFile.endBlock(b);
	return true;
}

void FileOutputStreamerDepthSource::closeOutStream()
{
	m_outputFile.close();
}

bool FileOutputStreamerDepthSource::isOutStreamOpen() const
{
	return m_outputFile.isOpen();
}

bool FileOutputStreamerDepthSource::readFrameRaw(RawFrameData& frame)
{
	if (m_asyncMode)
	{
		// 
		std::list<FrameRef> frs;
		m_readerQueue.pop_front_batch(frs);
		if (!frs.empty())
		{
			if (frs.size() > 1)
			{
				LOGF(WARNING, "WARNING: %d frames skipped!\n", int(frs.size()) - 1);
			}
			// Note: picks the most recently pushed item in case of more than one
			FrameRef fr = *frs.rbegin();
			if (fr)
			{
				frame = *fr;
				return true;
			}
			return false;
		}
		else
		{
			// Note: this ought to not happen except possibly at shutdown, since the queue blocks until data is available.
			LOGF(WARNING, "frame queue was empty!\n");
		}
	}
	else
	{
		// read directly from soruce, which may of may not be blocking.
		if (m_realSource->readFrameRaw(frame))
		{
			m_frameByteSize = sizeof(frame) + frame.bytesDepthSample * frame.depthData.size() + frame.colorData.size();
			writeFrame(frame);
			return true;
		}
	}
	return false;
}


void FileOutputStreamerDepthSource::writeFrame(const RawFrameData &frame)
{
	// Only do this if is open? 
	if (m_outputFile.isOpen())
	{
		auto b = m_outputFile.beginBlock("depth_frame", "1.0");

		b.write(frame.sequenceNumber);
		b.write(frame.timeStamp);
		b.write(frame.deviceTimeStamp);

		b.write(frame.depthWidth);
		b.write(frame.depthHeight);
		const uint32_t bytesDepthSample = 2;
		b.write(frame.bytesDepthSample);

		b.write(frame.minDepth);
		b.write(frame.maxDepth);
		b.write(frame.depthData);

		// and aso store the color info
		b.write(frame.colorWidth);
		b.write(frame.colorHeight);
		b.write(frame.cBytesPerPixel);
		b.write(frame.colorData);

		m_outputFile.endBlock(b);
	}
}

void FileOutputStreamerDepthSource::reset()
{
	// This is not really safe in async. mode.
	if (!m_asyncMode)
	{
		m_realSource->reset();
	}
}

void FileOutputStreamerDepthSource::seek(int frame)
{
	// This is not really safe in async. mode.
	if (!m_asyncMode)
	{
		m_realSource->seek(frame);
	}
}

const std::vector<chag::float2> &FileOutputStreamerDepthSource::getDepthToCameraSpaceCoeffs() const
{
	// TODO: how do we get this in a thread safe way?
	return m_realSource->getDepthToCameraSpaceCoeffs();
}

const ColorDepthSource::CameraIntrinsics FileOutputStreamerDepthSource::getIntrinsics() const
{
	return m_realSource->getIntrinsics();
}

bool FileOutputStreamerDepthSource::isStopped()
{
	return m_stopped;
}

void FileOutputStreamerDepthSource::stop(bool wait)
{
	m_stopped = true;

	m_realSource->stop(wait);
	if (m_asyncMode && wait)
	{
		m_readerThread.join();
	}
}


void FileOutputStreamerDepthSource::readerThreadFunc()
{
	for (;!m_stopped;)
	{
		RawFrameData frame;
		if (m_realSource->readFrameRaw(frame))
		{
			FrameRef fr(new RawFrameData(frame));
			m_readerQueue.push_back(fr);
			if (frame.depthData.size() == 0)
			{
				printf("Noooo!\n");
			}
			writeFrame(frame);
		}
		else
		{
			// An empty pointer signals read failure.
			m_readerQueue.push_back(FrameRef());
		}
	}
}
