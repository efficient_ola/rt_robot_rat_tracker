/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#ifndef _TemplateLibrary_h_
#define _TemplateLibrary_h_

#include "Config.h"
#include <vector>
#include <map>
#include <algorithm>
#include "linmath/float3.h"
#include "linmath/Aabb.h"
#include "linmath/float4x4.h"
#include <float.h>
#include <memory>

#include <boost/filesystem.hpp>
#include <nlohmann/json.hpp>
using json = nlohmann::json;

/**
 *
 */
class TemplateLibrary
{
public:
	TemplateLibrary() : 
		m_nextPoseNum(0) ,
		m_modified(false)
	{
	}

	struct Disc
	{
		chag::float3 position;
		chag::float3 normal;
		float radius;
	};

	struct Annotation
	{
		std::string id;
		chag::float3 position;
		chag::float3 direction;
		float radius; //?
	};


	struct Template
	{
		std::string poseName;
		std::vector<Disc> discs;
		std::vector<Annotation> annotations;
		json meta; // TODO: possibly abstract the 'json' part?
	};

	void add(Template t, const json &meta)
	{
		t.poseName = allocatePoseName();
		t.meta = meta;
		templates.push_back(t);
		m_modified = true;
	}

	//
	template <typename DISC_FN_T>
	void add(int num, DISC_FN_T discFn, const json &meta)
	{
		Template t;
		for (int i = 0; i < num; ++i)
		{
			t.discs.push_back(discFn(i));
		}
		t.poseName = allocatePoseName();
		t.meta = meta;
		templates.push_back(t);
		m_modified = true;
	}


	Template *find(const std::string &name)
	{
		auto it = std::find_if(templates.begin(), templates.end(), [=](const auto &t) { return t.poseName == name; });
		return it != templates.end() ? &(*it) : nullptr;
	}
	// 

	int findInd(const std::string &name)
	{
		auto it = std::find_if(templates.begin(), templates.end(), [=](const auto &t) { return t.poseName == name; });
		return it != templates.end() ? int(it - templates.begin()) : -1;
	}
	// 
	void load(const boost::filesystem::path &dataFolder, const std::string &libraryName);
	void save(const boost::filesystem::path &dataFolder);

	// updates the next pose number counter and returns a fromatted name string
	std::string allocatePoseName();

	std::vector<Template> templates;
	int m_nextPoseNum;
	bool m_modified;
	std::string m_name;
};

using TemplateLibraryPtr = std::shared_ptr<TemplateLibrary>;

#endif // _TemplateLibrary_h_
