#ifndef _ApssMeshLab_h_
#define _ApssMeshLab_h_

#include "Config.h"
#include <linmath/float3.h>


/**
This file contains some code extracted from the MeshLab program (http://www.meshlab.net) and modified to work with my types.
In some cases the code is modified and extended significantly. The original code is licenced under GPL and so this file is
probably also then, if anyone has a problem with it's inclusion in the code base it is probably easiest to just remove the code
since it is only used in the editor.
**/

namespace ApssMeshLab
{

using namespace chag;

struct FittingResult
{
	enum Status
	{
		ASS_PLANE,
		ASS_SPHERE,
		ASS_UNDETERMINED,
	};
	Status mStatus;
	float3 uLinear;
	float uQuad;
	float uConstant;

	float3 mCenter;
	float  mRadius;
};

template <typename POINT_FN_T, typename WEIGHT_FN_T>
bool fit(const float3& x, size_t nofSamples, POINT_FN_T pointDirFn, WEIGHT_FN_T weightFn, const int maxIters, float beta, FittingResult &res)
{
	if (nofSamples==0)
	{
		return false;
	}
	else if (nofSamples==1)
	{
		std::pair<float3, float3> pointDir = pointDirFn(0);

		float3 p = pointDir.first; //vcg::Point3<float>::Construct(mPoints[id].cP());
		float3 n = pointDir.second;// vcg::Point3<float>::Construct(mPoints[id].cN());

		res.uLinear = n;
		res.uConstant = -dot(p, res.uLinear);
		res.uQuad = 0;
		res.mStatus = FittingResult::ASS_PLANE;
		return true;
	}

	float3 sumP = make_vector3(0.0f);// sumP.SetZero();
	float3 sumN = make_vector3(0.0f);// sumN.SetZero();
	float sumDotPN = 0.0f;
	float sumDotPP = 0.0f;
	float sumW = 0.0f;

	for (int i = 0; i < nofSamples; i++)
	{
		std::pair<float3, float3> pointDir = pointDirFn(i);

		float3 p = pointDir.first; //vcg::Point3<float>::Construct(mPoints[id].cP());
		float3 n = pointDir.second;// vcg::Point3<float>::Construct(mPoints[id].cN());

		float w = weightFn(i, x);
		//float w = mCachedWeights.at(i);

		sumP += p * w;
		sumN += n * w;
		sumDotPN += w * dot(n, p);
		sumDotPP += w * lengthSquared(p);
		sumW += w;
	}

	float invSumW = float(1)/sumW;
	float aux4 = beta * float(0.5) *
		(sumDotPN - invSumW*dot(sumP, sumN))
		/(sumDotPP - invSumW*lengthSquared(sumP));
	res.uLinear = (sumN-sumP*(float(2)*aux4))*invSumW;
	res.uConstant = -invSumW*(dot(res.uLinear, sumP) + sumDotPP*aux4);
	res.uQuad = aux4;

	// finalize
	if (fabs(res.uQuad)>1e-7f)
	{
		res.mStatus = FittingResult::ASS_SPHERE;
		float b = 1.0f/res.uQuad;
		res.mCenter = res.uLinear*(-0.5f*b);
		res.mRadius = sqrt(lengthSquared(res.mCenter) - b*res.uConstant);
	}
	else if (res.uQuad==0.0f)
	{
		res.mStatus = FittingResult::ASS_PLANE;
		float s = float(1)/length(res.uLinear);
		//assert(!vcg::math::IsNAN(s) && "normal should not have zero len!");
		res.uLinear *= s;
		res.uConstant *= s;
	}
	else
	{
		res.mStatus = FittingResult::ASS_UNDETERMINED;
		// normalize the gradient
		float f = 1.0f/sqrt(lengthSquared(res.uLinear) - float(4)*res.uConstant*res.uQuad);
		res.uConstant *= f;
		res.uLinear *= f;
		res.uQuad *= f;
	}

	// cache some values to be used by the mls gradient
	//mCachedSumP = sumP;
	//mCachedSumN = sumN;
	//mCachedSumW = sumW;
	//mCachedSumDotPP = sumDotPP;
	//mCachedSumDotPN = sumDotPN;

	//mCachedQueryPoint = x;
	//mCachedQueryPointIsOK = true;
	return true;
}



template <typename POINT_FN_T, typename WEIGHT_FN_T>
bool project(const float3& x, size_t inputSize, POINT_FN_T pointDirFn, WEIGHT_FN_T weightFn, float3 &projectedPt, float3 &projectedNormal, const int maxIters = 5, float beta = 0.5f, const float epsilon = 0.0001f)
{
	int iterationCount = 0;
	float3 lx = x;
	float3 position = lx;
	float3 normal;
	float3 previousPosition;
	float delta2;
	const float epsilon2 = epsilon * epsilon;
	do {
		FittingResult fr;
		if (!fit(position, inputSize, pointDirFn, weightFn, maxIters, beta, fr))
		{
			//if (errorMask)
			//*errorMask = MLS_TOO_FAR;
			//std::cout << " proj failed\n";
			return false;
		}

		previousPosition = position;
		// local projection
		if (fr.mStatus==FittingResult::ASS_SPHERE)
		{
			normal = normalize(lx - fr.mCenter);
			position = fr.mCenter + normal *fr.mRadius;

			normal = normalize(fr.uLinear + position * (float(2) * fr.uQuad));
		}
		else if (fr.mStatus==FittingResult::ASS_PLANE)
		{
			normal = fr.uLinear;
			position = lx - fr.uLinear * (dot(lx, fr.uLinear) + fr.uConstant);
		}
		else
		{
			// Newton iterations
			float3 grad;
			float3 dir = fr.uLinear+lx*(2.0f*fr.uQuad);
			float ilg = 1.0f/length(dir);
			dir *= ilg;
			float ad = fr.uConstant + dot(fr.uLinear, lx) + fr.uQuad * lengthSquared(lx);
			float delta = -ad*std::min<float>(ilg, 1.);
			float3 p = lx + dir*delta;
			for (int i = 0; i<2; ++i)
			{
				grad = fr.uLinear+p*(2.*fr.uQuad);
				ilg = 1.0f/length(grad);
				delta = -(fr.uConstant + dot(fr.uLinear, p) + fr.uQuad * lengthSquared(p))*std::min<float>(ilg, 1.);
				p += dir*delta;
			}
			position = p;

			normal = normalize(fr.uLinear + position * (float(2) * fr.uQuad));
		}

		delta2 = lengthSquared(previousPosition - position);
	} while (delta2>epsilon2 && ++iterationCount < maxIters);

#if 0
	if (pNormal)
	{
		if (mGradientHint==MLS_DERIVATIVE_ACCURATE)
		{
			VectorType grad;
			mlsGradient(vcg::Point3<float>::Construct(position), grad);
			grad.Normalize();
			*pNormal = grad;
		}
		else
		{
			for (int k = 0; k<3; ++k)
				(*pNormal)[k] = normal[k];
		}
	}
	if (iterationCount>=mMaxNofProjectionIterations && errorMask)
		*errorMask = MLS_TOO_MANY_ITERS;
#else
	projectedNormal = normal;
#endif 
	projectedPt = position;
	return iterationCount < maxIters;
}



// Ola's extended KNN based formulation that allows neighbourhood shrinkage as closeness increases...
template <int K, typename KNN_FN_T, typename WEIGHT_FN_T>
bool fit_knn(const float3& x, KNN_FN_T knnFn, WEIGHT_FN_T weightFn, const int maxIters, float beta, FittingResult &res)
{
	std::pair<float3, float3> knn[K];
	//int knn[K];
	int nofSamples = knnFn(x, knn);

	float totalWeight = 0.0f;
	float a = 0.0f;
	float3 c = { 0.0f, 0.0f, 0.0f };
	float d = 0.0f;
	float3 f = { 0.0f, 0.0f, 0.0f };

	float radius = 0.0f;

	for (size_t i = 0; i < nofSamples; ++i)
	{
		const std::pair<float3, float3> &pointDir = knn[i];
		radius = std::max(radius, length(x - pointDir.first));
	}
	radius *= 1.01f;

	float weights[K];
	// TODO: Handle infinity!
	// Normalize weights
	float weightSum = 0.0f;
	for (size_t i = 0; i < nofSamples; ++i)
	{
		const std::pair<float3, float3> &pointDir = knn[i];
		float w = weightFn(pointDir, radius, x);
		weightSum += w;
		weights[i] = w;
	}
#if 1
	// Normalize weights
	for (size_t i = 0; i < nofSamples; ++i)
	{
		weights[i] /= weightSum;
	}

	// Discard any below a very small number:
	const float weightEpsilon = 1.0e-4f;
	for (size_t i = 0; i < nofSamples;)
	{
		if (weights[i] < weightEpsilon)
		{
			--nofSamples;
			weights[i] = weights[nofSamples];
			knn[i] = knn[nofSamples];
		}
		else
		{
			++i;
		}
	}
#endif



	if (nofSamples==0)
	{
		return false;
	}
	else if (nofSamples==1)
	{
		const std::pair<float3, float3> &pointDir = knn[0];

		float3 p = pointDir.first; //vcg::Point3<float>::Construct(mPoints[id].cP());
		float3 n = pointDir.second;// vcg::Point3<float>::Construct(mPoints[id].cN());

		res.uLinear = n;
		res.uConstant = -dot(p, res.uLinear);
		res.uQuad = 0;
		res.mStatus = FittingResult::ASS_PLANE;
		return true;
	}

	float3 sumP = make_vector3(0.0f);// sumP.SetZero();
	float3 sumN = make_vector3(0.0f);// sumN.SetZero();
	float sumDotPN = 0.0f;
	float sumDotPP = 0.0f;
	float sumW = 0.0f;

	for (int i = 0; i < nofSamples; i++)
	{
		const std::pair<float3, float3> &pointDir = knn[i];

		float3 p = pointDir.first; //vcg::Point3<float>::Construct(mPoints[id].cP());
		float3 n = pointDir.second;// vcg::Point3<float>::Construct(mPoints[id].cN());

		float w = weights[i];
		//float w = mCachedWeights.at(i);

		sumP += p * w;
		sumN += n * w;
		sumDotPN += w * dot(n, p);
		sumDotPP += w * lengthSquared(p);
		sumW += w;
	}

	float invSumW = float(1)/sumW;
	float aux4 = beta * float(0.5) *
		(sumDotPN - invSumW*dot(sumP, sumN))
		/(sumDotPP - invSumW*lengthSquared(sumP));
	assert(isfinite(aux4));
	res.uLinear = (sumN-sumP*(float(2)*aux4))*invSumW;
	res.uConstant = -invSumW*(dot(res.uLinear, sumP) + sumDotPP*aux4);
	res.uQuad = aux4;

	// finalize
	if (fabs(res.uQuad)>1e-7f)
	{
		res.mStatus = FittingResult::ASS_SPHERE;
		float b = 1.0f/res.uQuad;
		res.mCenter = res.uLinear*(-0.5f*b);
		res.mRadius = sqrt(lengthSquared(res.mCenter) - b*res.uConstant);
	}
	else if (res.uQuad==0.0f)
	{
		res.mStatus = FittingResult::ASS_PLANE;
		float s = float(1)/length(res.uLinear);
		//assert(!vcg::math::IsNAN(s) && "normal should not have zero len!");
		res.uLinear *= s;
		res.uConstant *= s;
	}
	else
	{
		res.mStatus = FittingResult::ASS_UNDETERMINED;
		// normalize the gradient
		float f = 1.0f/sqrt(lengthSquared(res.uLinear) - float(4)*res.uConstant*res.uQuad);
		res.uConstant *= f;
		res.uLinear *= f;
		res.uQuad *= f;
	}

	// cache some values to be used by the mls gradient
	//mCachedSumP = sumP;
	//mCachedSumN = sumN;
	//mCachedSumW = sumW;
	//mCachedSumDotPP = sumDotPP;
	//mCachedSumDotPN = sumDotPN;

	//mCachedQueryPoint = x;
	//mCachedQueryPointIsOK = true;
	return true;
}

namespace detail
{
	struct DbgSphereFnNada
	{
		inline void operator()(const float3& c, float r, int iter)const { }
	};
}

template <int K, typename KNN_FN_T, typename WEIGHT_FN_T, typename DBG_SPHERE_FN_T = detail::DbgSphereFnNada>
bool project_knn(const float3& x, KNN_FN_T knnFn, WEIGHT_FN_T weightFn, float3 &projectedPt, float3 &projectedNormal, const int maxIters = 5, float beta = 0.5f, const float epsilon = 0.000001f, DBG_SPHERE_FN_T dbgSphere = detail::DbgSphereFnNada())
{
	int iterationCount = 0;
	float3 lx = x;
	float3 position = lx;
	float3 normal;
	float3 previousPosition;
	float delta2;
	const float epsilon2 = epsilon * epsilon;
	do {
		FittingResult fr;
		if (!fit_knn<K>(position, knnFn, weightFn, maxIters, beta, fr))
		{
			//if (errorMask)
			//*errorMask = MLS_TOO_FAR;
			//std::cout << " proj failed\n";
			return false;
		}

		previousPosition = position;
		// local projection
		if (fr.mStatus==FittingResult::ASS_SPHERE)
		{
			dbgSphere(fr.mCenter, fr.mRadius, iterationCount);

			normal = normalize(lx - fr.mCenter);
			position = fr.mCenter + normal *fr.mRadius;

			normal = normalize(fr.uLinear + position * (float(2) * fr.uQuad));
		}
		else if (fr.mStatus==FittingResult::ASS_PLANE)
		{
			normal = fr.uLinear;
			position = lx - fr.uLinear * (dot(lx, fr.uLinear) + fr.uConstant);
		}
		else
		{
			// Newton iterations
			float3 grad;
			float3 dir = fr.uLinear+lx*(2.0f*fr.uQuad);
			float ilg = 1.0f/length(dir);
			dir *= ilg;
			float ad = fr.uConstant + dot(fr.uLinear, lx) + fr.uQuad * lengthSquared(lx);
			float delta = -ad*std::min<float>(ilg, 1.);
			float3 p = lx + dir*delta;
			for (int i = 0; i<2; ++i)
			{
				grad = fr.uLinear+p*(2.*fr.uQuad);
				ilg = 1.0f/length(grad);
				delta = -(fr.uConstant + dot(fr.uLinear, p) + fr.uQuad * lengthSquared(p))*std::min<float>(ilg, 1.);
				p += dir*delta;
			}
			position = p;

			normal = normalize(fr.uLinear + position * (float(2) * fr.uQuad));
		}

		delta2 = lengthSquared(previousPosition - position);
	} while (delta2>epsilon2 && ++iterationCount < maxIters);

#if 0
	if (pNormal)
	{
		if (mGradientHint==MLS_DERIVATIVE_ACCURATE)
		{
			VectorType grad;
			mlsGradient(vcg::Point3<float>::Construct(position), grad);
			grad.Normalize();
			*pNormal = grad;
		}
		else
		{
			for (int k = 0; k<3; ++k)
				(*pNormal)[k] = normal[k];
		}
	}
	if (iterationCount>=mMaxNofProjectionIterations && errorMask)
		*errorMask = MLS_TOO_MANY_ITERS;
#else
	projectedNormal = normal;
#endif 
	projectedPt = position;
	return iterationCount < maxIters;
}



namespace detail
{

	struct GradientPotentialResult
	{
		float3 mCachedGradient;
		float mCachedPotential;
	};

};

template <int K, typename KNN_FN_T, typename WEIGHT_FN_T>
bool rimls_computePotentialAndGradient(const float3& x, KNN_FN_T knnFn, WEIGHT_FN_T weightFn, detail::GradientPotentialResult &stuff)
{
	// NOTE (from paper): 
	/*
	If we assume the norm of the gradient is close to one, sigmaN
	is used to scale the difference	between unit vectors,
	and then typical choices range from 0:5 to 1:5.
	Smaller values lead to sharper results
	*/
	const float sigmaN = 1.0f;

	/*
	This way, the choice of sigmaR does not depend on the object
	scale anymore, and it can be set once for all. In this paper
	we used sr = 0:5.
	*/
	const float sigmaR = 0.5f;

	// Not sure about these...
	int mMinRefittingIters = 1;
	int mMaxRefittingIters = 3;
	const float mRefittingThreshold = 1e-3f;

	std::pair<float3, float3> knn[K];
	//int knn[K];
	int nofSamples = knnFn(x, knn);

	float radius = 0.0f;

	for (size_t i = 0; i < nofSamples; ++i)
	{
		const std::pair<float3, float3> &pointDir = knn[i];
		radius = std::max(radius, length(x - pointDir.first));
	}
	radius *= 1.01f;

	float weights[K];
	float3 lGrads[K];
	// TODO: Handle infinity!
	// Normalize weights
	float weightSum = 0.0f;
	for (size_t i = 0; i < nofSamples; ++i)
	{
		const std::pair<float3, float3> &pointDir = knn[i];
		//float w = weightFn(pointDir, radius, x);
		float dW = 0.0f;
		float w = weightFn(knn[i], radius, x, lGrads[i], dW); // mCachedWeights.at(i);
		weightSum += w;
		weights[i] = w;
	}
#if 1
	// Normalize weights
	for (size_t i = 0; i < nofSamples; ++i)
	{
		weights[i] /= weightSum;
		lGrads[i] /= weightSum;
	}

	// Discard any below a very small number:
	const float weightEpsilon = 1.0e-4f;
	for (size_t i = 0; i < nofSamples;)
	{
		if (weights[i] < weightEpsilon)
		{
			--nofSamples;
			weights[i] = weights[nofSamples];
			lGrads[i] = lGrads[nofSamples];
			knn[i] = knn[nofSamples];
		}
		else
		{
			++i;
		}
	}
#endif

	if (nofSamples<1)
	{
		stuff.mCachedGradient = make_vector3(0.0f);
		stuff.mCachedPotential = 1e9;
		return false;
	}

	//if (mCachedRefittingWeights.size()<nofSamples)
	//	mCachedRefittingWeights.resize(nofSamples+5);

	float3 source = x;
	float3 grad = make_vector3(0.0f);
	float3 previousGrad = grad;
	float invSigma2 = float(1) / (sigmaN*sigmaN);
	float invSigmaR2 = 0.0f;
	if (sigmaR > 0.0f)
	{
		invSigmaR2 = float(1) / (sigmaR*sigmaR);
	}
	float potential = 0.0f;
	int iterationCount = 0;
	do
	{
		float3 sumGradWeight = make_vector3(0.0f);;
		float3 sumGradWeightPotential = make_vector3(0.0f);
		float3 sumN = make_vector3(0.0f);
		float sumW = 0.0f;
		potential = 0.0f;

		previousGrad = grad;
		//sumGradWeight.SetZero();
		//sumGradWeightPotential.SetZero();
		//sumN.SetZero();
		//potential = 0.;
		//sumW = 0.;

		for (int i = 0; i<nofSamples; i++)
		{
			//int id = mNeighborhood.index(i);
			float3 diff = source - knn[i].first;
			float3 normal = knn[i].second;// mPoints[id].cN();
			float f = dot(diff, normal);

			float refittingWeight = 1.0f;
			if (iterationCount > 0)
			{
				refittingWeight = expf(-lengthSquared(normal - previousGrad) * invSigma2);
				//  not my edit:  if (mSigmaR>0)
				//                 {
				//                     float residual = (ddotn - potentialPrev) / mRadii.at(id);
				//                     refittingWeight *= exp(-residual*residual * invSigmaR2);
				//                 }
			}
			//mCachedRefittingWeights.at(i) = refittingWeight;
			float dW = 0.0f;
			float3 lGrad = make_vector3(0.0f);
			float ww = weights[i];// weightFn(knn[i], radius, x, lGrad, dW); // mCachedWeights.at(i);
			float w = ww * refittingWeight;
			float3 gw = lGrads[i] * refittingWeight;

			sumGradWeight += gw;
			sumGradWeightPotential += gw * f;
			sumN += normal * w;
			potential += w * f;
			sumW += w;
		}

		if (sumW == 0.0f)
		{
			return false;
		}

		potential /= sumW;
		grad = (-sumGradWeight*potential + sumGradWeightPotential + sumN) * (1./sumW);

		iterationCount++;

	} while ((iterationCount < mMinRefittingIters)
		|| (lengthSquared(grad - previousGrad) > mRefittingThreshold && iterationCount < mMaxRefittingIters));

	stuff.mCachedGradient = grad;
	stuff.mCachedPotential = potential;

	return true;
}


template <int K, typename KNN_FN_T, typename WEIGHT_FN_T>
bool project_rimls_knn(const float3& x, KNN_FN_T knnFn, WEIGHT_FN_T weightFn, float3 &projectedPt, float3 &projectedNormal, const int maxIters = 5, const float epsilon = 1.0e-4f)
{
	detail::GradientPotentialResult stuff;
	int iterationCount = 0;
	float3 position = x;
	float3 normal;
	float delta;
	//float epsilon = 1.0e-4f;
	do
	{
		if (!rimls_computePotentialAndGradient<K>(position, knnFn, weightFn, stuff))
		{
			//if (errorMask)
			//	*errorMask = MLS_TOO_FAR;
			//std::cerr << " proj failed\n";
			projectedPt = x;
			return false;
		}

		normal = normalize(stuff.mCachedGradient);
		delta = stuff.mCachedPotential;
		position = position - normal*delta;
	} while (fabs(delta)>epsilon && ++iterationCount < maxIters);

	//if (iterationCount>=mMaxNofProjectionIterations && errorMask)
	//	*errorMask = MLS_TOO_MANY_ITERS;

	projectedNormal = normal;
	projectedPt = position;

	return iterationCount <= maxIters;
}



}; // namespace ApssMeshLab

#endif // _ApssMeshLab_h_
