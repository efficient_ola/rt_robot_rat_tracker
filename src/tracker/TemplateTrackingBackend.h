/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#ifndef _TemplateTrackingBackend_h_
#define _TemplateTrackingBackend_h_

#include "Config.h"
#include <vector>
#include <map>
#include <algorithm>
#include <numeric>
#include "linmath/float3.h"
#include "linmath/Aabb.h"
#include "linmath/float4x4.h"
#include <float.h>
#include "Common.h"
#include "TemplateTracker.h"

#include "utils/CudaMirrorBuffer.h"

struct RawFrameData;

#define GENERATE_COMPACTED_POINTS 0

#if ENABLE_BACKEND_DEBUG_RENDERING
#define DBG_RENDERER_OBJECT_SCOPE_BE(...) DBG_RENDERER_OBJECT_SCOPE(__VA_ARGS__)
#define DBG_RENDERER_OBJECT_BE(_name_, _draw_func_) DBG_RENDERER_OBJECT(_name_, _draw_func_)
#define DBG_RENDERER_CMD_BE(_draw_func_) DBG_RENDERER_CMD(_draw_func_)
#else // !ENABLE_BACKEND_DEBUG_RENDERING
#define DBG_RENDERER_OBJECT_SCOPE_BE(...)
#define DBG_RENDERER_OBJECT_BE(_name_, _draw_func_)
#define DBG_RENDERER_CMD_BE(_draw_func_) 
#endif // ENABLE_BACKEND_DEBUG_RENDERING


/**
 * The backend is responsible for most of the processing, it is abstracted like this to enable an insertion point for the
 * CUDA vs CPU implementations that exist. Relies heavily on the 'CudaMirrorBuffer' to get data moved.
 */
class TemplateTrackingBackend
{
public:
	static TemplateTrackingBackend *create(bool forceCpu = false);

	void rebuildHierearchicalClustersHost(const chag::float4x4 & arenaToAabbTfm);

	virtual bool isCuda() const { return false; }
	virtual std::string getDeviceDesc() const { return "unknown"; }

	using Params = TemplateTracker::Params;

	void setParams(const Params& params)
	{
		m_params = params;
	}

	struct BatchedRes
	{
		size_t pointsStart;
		size_t pointsEnd;
		float rmseW;
		float rmse;
		float confidence;
	};


	struct BatchedTransformRes
	{
		chag::float4x4 transform;
		float rmseW;
		float totalWeight;
		float confidence;
	};

	struct BatchedBestTransformRes
	{
		chag::float4x4 transform;
		float rmseW;
		float totalWeight;
		float confidence;
		int poseIndex;
	};
	/**
		* setTemplates, setPoints, and setParams must be called before (not setTfms, as they are calculated during iteration).
		* Given the start transformation, finds the best matching pose and transform.
		*/
	virtual BatchedBestTransformRes findBestTransformBatched(const chag::float4x4 &startTfm, int numIters) = 0;


	struct TemplateAdaptor
	{
		template <typename T>
		inline void beginTemplate(const T &t)	{ }

		template <typename T>
		inline chag::Aabb getAabb(const T &t) const { return t.m_aabb; }
		template <typename T>
		inline const auto &getDiscs(const T &t) const { return t.m_sampleDiscs; }

		template <typename D>
		inline const auto &getDirection(const D &d) const { return d.direction; }
		template <typename D>
		inline float getOffset(const D &d) const { return d.offset; }
		template <typename D>
		inline float getConeAngle(const D &d) const { return d.coneAngle; }
	};

	/**
	 * Copy some container of templates into the backend representation. Uses the adaptor to convert the input data to the
	 * internal representation. 
	 * TODO: move the conversion and data for a template set into its own class, and just swap a pointer as needed. (not a huge overhead at the moment)
	 */
	template <typename IN_TEMPLATE_CONTAINER_T, typename TEMPLATE_ADAPTOR_T>
	void setTemplates(const IN_TEMPLATE_CONTAINER_T &input, TEMPLATE_ADAPTOR_T ta)
	{
		uint32_t totalDiscs = std::accumulate(input.begin(), input.end(), 0U, [=](uint32_t l, const auto &r) { return l + uint32_t(ta.getDiscs(r).size()); });
		m_discs.reserve(totalDiscs, true);
		m_discs.clear();
		m_templates.reserve(input.size(), true);
		m_templates.clear();
		for (size_t i = 0; i < input.size(); ++i)
		{
			const auto &t = input[i];
			ta.beginTemplate(t);
			DiscTemplate dt;
			dt.aabb = ta.getAabb(t);
			dt.start = int(m_discs.size());
			for (const auto &d : ta.getDiscs(t))
			{
				TemplateDisc td;
				td.position = d.position;
				td.radius = d.radius;
				td.direction = ta.getDirection(d);
				td.offset = ta.getOffset(d);
				td.coneAngle = ta.getConeAngle(d);
				td.templateIndex = int(i);
				m_discs.push_back(td);
			}
			dt.count = int(m_discs.size()) - dt.start;
			m_templates.push_back(dt);
		}
		// Upload to CUDA
		m_discs.reflectToDevice(true);
		m_templates.reflectToDevice(true);
	}
	
	template <typename IN_TEMPLATE_CONTAINER_T>
	void setTemplates(const IN_TEMPLATE_CONTAINER_T &input)
	{
		setTemplates(input, TemplateTrackingBackend::TemplateAdaptor());
	}


	/**
	 * Utility accessor class to convert data from some any representation to extract the position and weight of a cluster.
	 */
	struct ClusterAdaptor
	{
		template <typename C>
		inline const auto &getCentre(const C &c) const { return c.center; }
		template <typename C>
		inline float getPriorWeight(const C &c) const { return 1.0f; }
	};

	/**
	 * Set the points (aka clusters) and hierarchical clusters, instead of building them from an input frame.
	 * Useful if the data-source is completely different, but not used in the main tracking implementation.
	 */
	template <typename CLUSTER_CONTAINER_T, typename CLUSTER_ADAPTOR_T>
	void setPoints(const std::vector<HCluster> &hClusters, const CLUSTER_CONTAINER_T &clusters, CLUSTER_ADAPTOR_T ca)
	{
		m_hClusters = hClusters;
		m_clusters.reserve(clusters.size(), true);
		m_clusters.clear();
		for (const auto &ic : clusters)
		{
			Cluster c;
			c.center = ca.getCentre(ic);
			c.radius = ic.radius;
			c.normal = ic.normal;
			c.priorWeight = ca.getPriorWeight(ic);
			m_clusters.push_back(c);
		}
		// Upload to CUDA
		m_hClusters.reflectToDevice(true);
		m_clusters.reflectToDevice(true);
	}

	template <typename CLUSTER_CONTAINER_T>
	void setPoints(const std::vector<HCluster> &hClusters, const CLUSTER_CONTAINER_T &clusters)
	{
		setPoints(hClusters, clusters, ClusterAdaptor());
	}

	/**
	 * Select and transform the clusters for the current tracking operation, populates m_clusters & m_hClusters, 
	 * Note that the aabb is in some other space (typically that of the tracked object).
	 */
	virtual void selectAndTransformClusters(const chag::float4x4 &worldToAabbTfm, const chag::Aabb &aabb, const chag::float4x4 &worldtoArenaTfm) = 0;


	void setDepthToCameraCoeffs(const std::vector<chag::float2> &coeffs)
	{
		m_depthToCameraSpaceCoeffs = coeffs;
		m_depthToCameraSpaceCoeffs.reflectToDevice(true);
	}

	/**
	 * This is called first when a new frame arrives, and transfers the minimal data needed to the GPU.
	 * Reconstruct points, build normals, build clusters, and other per-frame processing, setDepthToCameraCoeffs must have been called beforehand.
	 */
	virtual void preProcessFrame(const RawFrameData &frame) = 0;

public:
	// 
	struct TemplateDisc
	{
		chag::float3 position;
		float radius;
		chag::float3 direction;
		float offset;
		chag::float2 padding; // pad to 16 byte alignment
		float coneAngle;
		int templateIndex;
	};
	struct DiscTemplate
	{
		chag::Aabb aabb;
		int start; // offsets into disc array
		int count; // number of discs 
	};

	struct Cluster
	{
		chag::float3 center;
		float radius;
		chag::float3 normal;
		float priorWeight;
	};
#if ENABLE_TRACKER_DEBUG_RENDERING
	std::vector<int> m_discIndex;
#endif // ENABLE_TRACKER_DEBUG_RENDERING

	Params m_params;
	CudaMirrorBuffer<chag::float2> m_depthToCameraSpaceCoeffs;

	// template data
	CudaMirrorBuffer<TemplateDisc> m_discs;
	CudaMirrorBuffer<DiscTemplate> m_templates;
	CudaMirrorBuffer<HCluster> m_hClusters;
	CudaMirrorBuffer<Cluster> m_clusters;
	CudaMirrorBuffer<Cluster> m_clustersTmp; // needed for reordering...
	CudaMirrorBuffer<chag::float4x4> m_transforms;

	// frame data - note use of float4 for alignment as CUDA cares...
	CudaMirrorBuffer<uint16_t> m_frameDepthData;
	// frame (i.e., full 2D grid) 3D-data - note use of float4 for alignment as CUDA cares...
	// We set the w component of points is 0.0f for invalid data.
	CudaMirrorBuffer<chag::float4> m_framePoints;
	CudaMirrorBuffer<chag::float4> m_frameNormals;
	
	// Not needed at present for any of the processing stages.
#if GENERATE_COMPACTED_POINTS
	CudaMirrorBuffer<chag::float4> m_framePointsCompact;
	CudaMirrorBuffer<chag::float4> m_frameNormalsCompact;
	CudaMirrorBuffer<uint32_t> m_compactPointsCount;
#endif // GENERATE_COMPACTED_POINTS
	CudaMirrorBuffer<Cluster> m_frameClusters;

	// result buffers
	CudaMirrorBuffer<chag::float4> m_bestPoints_WeightTmp;
	CudaMirrorBuffer<chag::float4> m_bestTemplatePoints_DistTmp;
	CudaMirrorBuffer<BatchedTransformRes> m_batchedTransformRes;
	CudaMirrorBuffer<BatchedBestTransformRes> m_bestResultRes;

protected:
	TemplateTrackingBackend()
	{
	}
	virtual ~TemplateTrackingBackend()
	{
	}
};

#endif // _TemplateTrackingBackend_h_
