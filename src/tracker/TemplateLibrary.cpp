/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#include "TemplateLibrary.h"
#include <boost/filesystem.hpp>
#include "utils/ChagJsonUtils.h"

using json = nlohmann::json;


void to_json(json& j, const TemplateLibrary::Disc &d)
{
	j["position"] = d.position;
	j["normal"] = d.normal;
	j["radius"] = d.radius;
}

void to_json(json& j, const TemplateLibrary::Annotation &a)
{
	j["position"] = a.position;
	j["direction"] = a.direction;
	j["radius"] = a.radius;
	j["id"] = a.id;
}

void to_json(json& j, const TemplateLibrary::Template &t)
{
	j["discs"] = t.discs;
	j["annotations"] = t.annotations;
	j["poseName"] = t.poseName;
	j["meta"] = t.meta;
}

void from_json(const json& j, TemplateLibrary::Disc &d)
{
	d.position = j.at("position").get<chag::float3>();
	d.normal = j.at("normal").get<chag::float3>();
	d.radius = j.at("radius").get<float>();
}

void from_json(const json& j, TemplateLibrary::Annotation &a)
{
	a.position = j.at("position").get<chag::float3>();
	a.direction = j.at("direction").get<chag::float3>();
	a.radius = j.at("radius").get<float>();
	a.id = j.at("id").get<std::string>();
}

void from_json(const json& j, TemplateLibrary::Template &t)
{
	t.discs = j.at("discs").get<std::vector<TemplateLibrary::Disc>>();
	t.annotations = j.at("annotations").get<std::vector<TemplateLibrary::Annotation>>();
	t.poseName = j.at("poseName").get<std::string>();
	t.meta = j.count("meta") ? j["meta"] : json();
}


std::string formatNumber(int n)
{
	char buffer[32];
	sprintf(buffer, "%05d", n);
	return buffer;
}

static std::string getFileName(const std::string & dataFolder, int n)
{

}

void TemplateLibrary::load(const boost::filesystem::path& dataFolder, const std::string &libraryName)
{
	m_name = libraryName;
	boost::filesystem::path libDir = dataFolder/ libraryName;
	std::ifstream i((libDir / "library.json").c_str());
	if (i.is_open())
	{
		json j;
		i >> j;

		std::vector<std::string> poseList = j["pose_list"];
		m_nextPoseNum = j["next_pose_number"];
		for (auto poseName : poseList)
		{
			std::ifstream pif((libDir / (poseName + ".json")).c_str());
			if (pif.is_open())
			{
				json j;
				pif >> j;
				Template t = j;
				templates.push_back(t);
			}
		}
	}
}


void TemplateLibrary::save(const boost::filesystem::path &dataFolder)
{
	boost::filesystem::path libDir = dataFolder/ m_name;
	boost::filesystem::create_directories(libDir);

	{
		std::ofstream o((libDir / "library.json").c_str());
		if (o.is_open())
		{
			std::vector<std::string> poseList;
			for (const auto &t : templates)
			{
				poseList.push_back(t.poseName);
			}
			json j;
			j["pose_list"] = poseList;
			j["next_pose_number"] = m_nextPoseNum;
			o << std::setw(4) << j;
		}
	}

	for (const auto &t : templates)
	{
		std::ofstream o((libDir/ (t.poseName + ".json")).c_str());
		if (o.is_open())
		{
			o << std::setw(4) << json(t);
		}
	}
}


std::string TemplateLibrary::allocatePoseName()
{
	return "template_" + formatNumber(m_nextPoseNum++);
}
