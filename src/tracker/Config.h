/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#ifndef _Config_h_
#define _Config_h_

#ifndef ENABLE_TRACKER_DEBUG_RENDERING
#define ENABLE_TRACKER_DEBUG_RENDERING 0
#endif // ENABLE_TRACKER_DEBUG_RENDERING

#ifndef ENABLE_TRACKER_ADAPTION_DEBUG_RENDERING
#define ENABLE_TRACKER_ADAPTION_DEBUG_RENDERING 1
#endif // ENABLE_TRACKER_ADAPTION_DEBUG_RENDERING

#define ENABLE_PRE_PROCESSING_IN_TRACKER 1

#ifndef ENABLE_BACKEND_DEBUG_RENDERING
#define ENABLE_BACKEND_DEBUG_RENDERING 0
#endif // ENABLE_BACKEND_DEBUG_RENDERING

// Force CUDA off (for main program at least)
#if 0//ENABLE_CUDA
	#undef ENABLE_CUDA
	#define ENABLE_CUDA 0
#endif // ENABLE_CUDA



#ifndef USE_TRACKING_THREAD
/**
 * IF enabled the tracking runs in its own thread to avoid having to wait for rendering etc.
 */
#define USE_TRACKING_THREAD 1
#endif // USE_TRACKING_THREAD



/**
 * Enables outputting all the confidences (and not just the best one) into the log stream, adds some overhead for reading back the data from the GPU,
 * and inflates the log size.
 */
#define LOG_ALL_CONFIDENCES 0

#ifndef TRACKER_VERSION_STRING
#define TRACKER_VERSION_STRING "unknown/000000"
#endif //TRACKER_VERSION_STRING 

#endif _Config_h_
