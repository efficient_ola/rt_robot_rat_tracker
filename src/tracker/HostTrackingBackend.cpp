/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#include "HostTrackingBackend.h"
#include "profiler/Profiler.h"
#include "BackendSharedImplementation.h"
#include "ColorDepthSource.h"
#include <linmath/type_conversions.h>

#include <utils/Math.h>

#if ENABLE_BACKEND_DEBUG_RENDERING
#include <utils/DebugRenderer.h>
#include <utils/TweakableVarImGui.h>
#endif // ENABLE_BACKEND_DEBUG_RENDERING

#include "NearTree.h"
#include <utils/PlatformCompat.h>

using std::max;
using chag::lerp;

inline std::string getExCpuId(uint32_t exId, uint32_t maxExId)
{
	if (exId <= maxExId)
	{
		// we make it 5 ints long and make sure the last one is zero, so we can guarantee null terminated string...
		int data[5] = { 0, 0, 0, 0, 0 };
		CpuId(exId, data);
		data[4] = 0;
		return std::string(reinterpret_cast<const char *>(data));
	}
	return std::string();
}

std::string HostTrackingBackend::getDeviceDesc() const
{
	// Get the max id of extended info string parts...
	int CPUInfo[4] = { -1 };
	CpuId(0x80000000, CPUInfo);
	uint32_t maxExId = CPUInfo[0];
	// get string data for each of these...
	std::string result = getExCpuId(0x80000002, maxExId) 
		+ getExCpuId(0x80000003, maxExId) 
		+ getExCpuId(0x80000004, maxExId);

	return result;
}

void HostTrackingBackend::preProcessFrame(const RawFrameData &frame)
{
	using namespace chag;
	using namespace chag::type_conversions;

	// 1. Generate 3D data.
	constexpr float fMax = std::numeric_limits<float>::max();
	// 1.1 build full (2D mapped including invalid ones marked with w == 0) & compact (no invalid) set of points .
	m_framePoints.resize(frame.depthHeight * frame.depthWidth);
	for (size_t i = 0; i < frame.depthData.size(); ++i)
	{
		float depth = float(frame.depthData[i]);
		chag::float2 trans = m_depthToCameraSpaceCoeffs[i];
		if (depth > 0.0f && isfinite(trans.x) && isfinite(trans.y))
		{
			// scale to metres and camera space...
			chag::float3 camPt = chag::make_vector(trans.x * depth, trans.y * depth, depth) * (1.0f / 1000.0f);
			m_framePoints[i] = make_vector4(camPt, 1.0f);
		}
		else
		{
			// Invalid point.
			m_framePoints[i] = make_vector(fMax, fMax, fMax, 0.0f);
		}
	}

	m_frameNormals.resize(frame.depthHeight * frame.depthWidth);

	float depthThresh = m_params.normalsDepthThreshold;
	{
		PROFILE_SCOPE("normals", TT_Cpu);

		for (int j = 0; j < frame.depthHeight; ++j)
		{
			for (int i = 0; i < frame.depthWidth; ++i)
			{
				float4 n = buildNormal(i, j, depthThresh, [&](int i, int j) 
				{
					if (i < 0 || i >= frame.depthWidth || j < 0 || j >= frame.depthHeight)
					{
						return make_vector(0.0f, 0.0f, 0.0f, 0.0f);
					}
					int ind = frame.depthWidth * j + i;
					return m_framePoints[ind];
				});
				m_frameNormals[j * frame.depthWidth + i] = n;
			}
		}
	}

#if GENERATE_COMPACTED_POINTS
	m_framePointsCompact.reserve(frame.depthHeight * frame.depthWidth, true);
	m_framePointsCompact.clear();
	m_frameNormalsCompact.reserve(frame.depthHeight * frame.depthWidth, true);
	m_frameNormalsCompact.clear();

	for (size_t i = 0; i < frame.depthData.size(); ++i)
	{
		float4 p = m_framePoints[i];
		if (p.w)
		{
			m_framePointsCompact.push_back(p);
			m_frameNormalsCompact.push_back(m_frameNormals[i]);
#if 0
			if (m_frameNormals[i].w > 0.0f)
			{
				DebugRenderer::instance().addSphere(f3(p), 0.002f, DebugRenderer::randomColor(1, 0.8f), "aabb_points_ws/valid_normal");
			}
			else
			{
				DebugRenderer::instance().addSphere(f3(p), 0.002f, DebugRenderer::randomColor(0, 0.8f), "aabb_points_ws/invalid_normals");
			}
#endif
		}
	}
#endif // GENERATE_COMPACTED_POINTS

	{
		{
			PROFILE_SCOPE("cluster", TT_Cpu);

			m_frameClusters.reserve(frame.depthHeight * frame.depthWidth, true);
			m_frameClusters.clear();

#if 0
			// new version based on something more metric and hopefully more easy to make parallel
			Aabb arenaAabb = make_aabb(make_vector(-m_params.arenaRadius, -m_params.arenaRadius, -0.35f), make_vector(m_params.arenaRadius, m_params.arenaRadius, 0.35f));

			m_params.depthClusterThreshold
#else
			// TODO: this also is too slow, optimize either by moving to GPU, or by processing in tiles and using vector instructions on CPU
			//       probably most worthwhile to move the lot to GPU

			constexpr float maxArenaHeight = 0.35f;
			const float arenaRadExp = m_params.arenaRadius * 1.3f;
			Aabb arenaAabbExp = make_aabb(make_vector(-arenaRadExp, -arenaRadExp, -maxArenaHeight), make_vector(arenaRadExp, arenaRadExp, maxArenaHeight));

			constexpr int g_tileSizeXy = 3; // current clustering scheme is no good for large tiles!
																	//int2 gridSize = make_vector(g_frame.depthWidth, g_frame.depthHeight) + make_vector2(g_tileSizeXy - 1) / g_tileSizeXy;
			constexpr int N = g_tileSizeXy * g_tileSizeXy;

																	// NOTE: Truncated  - avoids needing to check for edges BUT leaves out rightmost edge of input
			int2 gridSize = make_vector(frame.depthWidth / g_tileSizeXy, frame.depthHeight / g_tileSizeXy);

#define USE_INSERTION_SORT 1

			for (int gj = 0; gj < gridSize.y; ++gj)
			{
				for (int gi = 0; gi < gridSize.x; ++gi)
				{
					Aabb tileAabb = make_inverse_extreme_aabb();

					int2 tp = make_vector(gi, gj) * g_tileSizeXy;
#if USE_INSERTION_SORT
					uint32_t sortedInds[N];
					for (int i = 0; i < N; ++i)
					{
						sortedInds[i] = 0xFFFFFFFF;
					}
#else // !USE_INSERTION_SORT
					std::pair<int, float> tileDepths[g_tileSizeXy * g_tileSizeXy];
#endif // USE_INSERTION_SORT
					int num = 0;

					for (int j = 0; j < g_tileSizeXy; ++j)
					{
						for (int i = 0; i < g_tileSizeXy; ++i)
						{
							int sampleOffs = (tp.y + j) * frame.depthWidth + tp.x + i;
							const float4 pt = m_framePoints[sampleOffs];

#if USE_INSERTION_SORT
							uint32_t k = 0xFFFFFFFF;
							if (pt.w != 0.0f)
							{
								chag::float3 aPt = transformPoint(m_params.worldToArenaTfm, f3(pt));
								// ignore points that are out of range (height-wise), and filter out any that are outside the arena
								if (inside(arenaAabbExp, aPt))
								{
									// Quantize depth to 16 bits around arena floor
									uint32_t quantDepth = int(65535.0f * chag::clamp((aPt.z + maxArenaHeight) / (2.0f * maxArenaHeight), 0.0f, 1.0f));
									// pack the depth in uppermost 16 bits to ensure sorting order
									k = (quantDepth << 16) | (j << 8) | i;
								}
							}

							// perform insertion sort (if this is not unrolled then this will suck bigtime...(same for all the loops)
							for (int s = 0; s < N; ++s)
							{
								if (k < sortedInds[s])
								{
									std::swap(sortedInds[s], k);
								}
							}
#else // !USE_INSERTION_SORT
							if (pt.w != 0.0f)
							{
								tileAabb = combine(tileAabb, f3(pt));

								tileDepths[num] = std::make_pair(sampleOffs, pt.z);
								++num;
							}
#endif // USE_INSERTION_SORT
						}
					}

					// check stats
					const float tileForegroundProb = 1.0f;

#if USE_INSERTION_SORT
					// transform threshold to sorting key units (set last bits to 1 to round up)
					const uint32_t threshold = (uint32_t(65535.0f * (m_params.depthClusterThreshold / (2.0f * maxArenaHeight))) << 16U) | 0x0000FFFF;
					int prevEnd = 0;
					uint32_t prevD = sortedInds[0];

					Aabb aabb = make_inverse_extreme_aabb();
					//float3 pts[g_tileSizeXy * g_tileSizeXy];
					float4 avgCentreW = { 0.0f, 0.0f, 0.0f, 0.0f };
					float4 avgNormalW = { 0.0f, 0.0f, 0.0f, 0.0f };

					for (int i = 0; i < N; ++i)
					{
						uint32_t d = sortedInds[i];

						// if break found or this is the last value, or it is invalid AND the cluster is valid, then emit a box and reset
						if ((d - prevD >= threshold || d == 0xFFFFFFFF || i == N-1) && avgCentreW.w != 0.0f)
						{
							Cluster c;
							c.center = f3(avgCentreW) / avgCentreW.w;
							if (avgNormalW.w > 0.0f)
							{
								c.normal = normalize(f3(avgNormalW) / avgNormalW.w);
							}
							else
							{
								c.normal = make_vector3(0.0f);
							}

							c.radius = std::max(0.001f, length(aabb.getHalfSize()));// should probably figure out projection on plane here...
							c.priorWeight = tileForegroundProb;
#if 0
							c.aabb = aabb;
							if (numPoints == 1)
							{
								c.aabb = make_aabb(c.center, c.radius);
							}
							c.debugColorId = (tp.y) * frame.depthWidth + tp.x + i;
#endif
							m_frameClusters.push_back(c);

							// reset
							aabb = make_inverse_extreme_aabb();
							avgNormalW = avgCentreW = make_vector4(0.0f);
						}

						// if value is valid, load and update current aabb etc
						if (d != 0xFFFFFFFF)
						{
							int i = d & 255U;
							int j = (d >> 8U) & 255U;
							int sampleOffs = (tp.y + j) * frame.depthWidth + tp.x + i;
							// we could buffer these in a local array in case we don't trust the cache to do its job...
							const float4 pt = m_framePoints[sampleOffs];
							const float4 n = m_frameNormals[sampleOffs];
							avgNormalW += n;
							avgCentreW += pt;
							aabb = combine(aabb, f3(pt));
						}
					}
#else // USE_INSERTION_SORT
					// early out if away from arena
					if (tileAabb.min.x >= tileAabb.max.x || !overlaps(arenaAabb, m_params.worldToArenaTfm * tileAabb))
					{
						continue;
					}
					// TODO: instead to a local plane search thing that respects curvature better and breaks up curved things more...
					// sort in depth order
					std::sort(tileDepths, &tileDepths[num], [&](const std::pair<int, float> &a, const std::pair<int, float> &b) { return a.second < b.second; });
					// iterate and find discontinuities, jumps larger than Xmm
					const float threshold = m_params.depthClusterThreshold;
					int prevEnd = 0;
					float prevD = tileDepths[0].second;
					for (int i = 1; i < num; ++i)
					{
						float d = tileDepths[i].second;
						if (fabsf(d - prevD) >= threshold || i == (num - 1))
						{
							Aabb aabb = make_inverse_extreme_aabb();
							//float3 pts[g_tileSizeXy * g_tileSizeXy];
							float3 centre = { 0.0f, 0.0f, 0.0f };
							float4 avgNormalW = { 0.0f, 0.0f, 0.0f, 0.0f };
							// process range found so far

							for (int k = prevEnd; k < i; ++k)
							{
								std::pair<int, float> td = tileDepths[k];

								const float4 pt = m_framePoints[td.first];
								const float4 n = m_frameNormals[td.first];
								avgNormalW += n;
								centre += f3(pt);
								aabb = combine(aabb, f3(pt));
							}
							int numPoints = i - prevEnd;
							if (numPoints > 0)
							{
								centre *= 1.0f / float(i - prevEnd);

								Cluster c;
								c.center = centre;
								if (avgNormalW.w > 0.0f)
								{
									c.normal = normalize(f3(avgNormalW) / avgNormalW.w);
								}
								else
								{
									c.normal = make_vector3(0.0f);
								}


								c.radius = std::max(0.001f, length(aabb.getHalfSize()));// should probably figure out projection on plane here...
								c.priorWeight = 1.0f;
#if 0
								c.aabb = aabb;
								if (numPoints == 1)
								{
									c.aabb = make_aabb(c.center, c.radius);
								}
								c.debugColorId = (tp.y) * frame.depthWidth + tp.x + i;
#endif
								m_frameClusters.push_back(c);
							}

							prevEnd = i;
							//printf("\n");
						}
						prevD = d;
					}
				}
#endif // USE_INSERTION_SORT
				}
#endif
			}
		}
	}
}

void HostTrackingBackend::selectAndTransformClusters(const chag::float4x4 &worldToAabbTfm, const chag::Aabb & aabb, const chag::float4x4 &worldtoArenaTfm)
{
	//PROFILE_SCOPE("getClustersInAabb", TT_Cpu);
	using namespace chag;

	m_clusters.clear();
	m_clusters.reserve(m_frameClusters.size(), true);

	for (int i = 0; i < int(m_frameClusters.size()); ++i)
	{
		const Cluster &c = m_frameClusters[i];
		float3 atp = transformPoint(worldToAabbTfm, c.center);
		if (inside(aabb, atp))
		{
			// Note: currenly selected clusters are in arena-space
			float3 tp = transformPoint(worldtoArenaTfm, c.center);

			Cluster tc = c;
			tc.center = tp;
			tc.normal = transformDirection(worldtoArenaTfm, c.normal);

			m_clusters.push_back(tc);
		}
	}
#if 1 // new method, to match GPU better
	// Finally build the hierarhcical grid level, we'll build this one as a simple 3D grid and select the resolution based on the idea that 
	// we want about K clusters per H cluster - perhaps 16. Noting that the data is more or less 2D we perform a 2D estimate of cell-size
	// and then extend this to 3D, which would lead to slightly too large cells. Then we just add each cluster to the H-cluster where the 
	// centre belongs, and extend the H-cluster aabb, easy.

	m_hClusters.reserve(m_clusters.size());
	m_hClusters.clear();

	if (!m_clusters.empty())
	{
		// TODO: make this a parameter
		// select K clusters / h-cluster
		// Make subDivisions clusters along longest axis
		float subDivisions = float(m_params.hClusterSubDivs);

		// TODO: this is king of weird and might be make much better with just using a tight aabb (as this one is likely way overdimensioned in the z direction)
		// 1. compute cell size, normally do 3D volume / numbe, but since moslty 2D this will lead to overcrowding where there are actually 
		//    points. Lets go with it anyway, then the side should be the cube-root. No - way too many in most clusters, maybe 2D then
		//float aabb25dArea = aabb.getDiagonal().x * aabb.getDiagonal().y * sqrtf(aabb.getDiagonal().z);
		//float cellSize = powf(K * aabb25dArea / float(m_clusters.size()), 1.0f / 2.5f);
		float cellSize = std::max(std::max(aabb.getDiagonal().x, aabb.getDiagonal().y), aabb.getDiagonal().z) / subDivisions;
		uint3 gridDim = make_vector<uint32_t>(ceil(aabb.getDiagonal() / cellSize) + 0.5f);
		float3 gridBoundF = make_vector<float>(gridDim) - 1.0f;
		float3 zeroF = make_vector3(0.0f);
		std::vector<std::vector<int>> grid(gridDim.x * gridDim.y * gridDim.z);

		// to transform to grid we need the transform from arena space (where selected points are, to aabb space, where the grid is)
		chag::float4x4 arenaToAabbTfm = worldToAabbTfm * inverse(worldtoArenaTfm);

		for (size_t i = 0; i < m_clusters.size(); ++i)
		{
			// Note: in arena-space
			const Cluster &c = m_clusters[i];
			// to transform to grid we need te 
			chag::float3 aPt = transformPoint(arenaToAabbTfm, c.center);

			uint3 gridPos = make_vector<uint32_t>(clamp((aPt - aabb.min) / cellSize, zeroF, gridBoundF));

			grid[(gridPos.z * gridDim.y + gridPos.y) * gridDim.x + gridPos.x].push_back(int(i));
		}

		std::vector<Cluster> clusters2;

		for (const std::vector<int> &inds : grid)
		{
			if (!inds.empty())
			{
				HCluster hc;
				hc.start = int(clusters2.size());
				chag::Aabb aabb = make_inverse_extreme_aabb();
				for (int i : inds)
				{
					const Cluster &c = m_clusters[i];
					aabb = combine(aabb, make_aabb(c.center, c.radius));
					clusters2.push_back(c);
				}
				hc.centre = aabb.getCentre();
				hc.radius = length(aabb.getHalfSize());
				hc.end = int(clusters2.size());
				m_hClusters.push_back(hc);
			}
		}
		m_clusters = clusters2;
	}
#else //
	rebuildHierearchicalClusters();
#endif 
}

// TODO: While this implementation is simple and matches the GPU one it would have made more sense to start from for example the Hemi library and abstracting the
//       underlying parallelism (which is actually relatively straightforward at this point). Then the CPU implementation could have used modern C++ vector parallelism
//       which could have been rather awesome if it were to work (vector units and all that). It is possible, even likely, however, that the algorithm will instead become 
//       more clever, relying less of brute-force and more on pruning the search space. So maybe the CPU side will be both more complex and faster in the next version.
HostTrackingBackend::BatchedBestTransformRes HostTrackingBackend::findBestTransformBatched(const chag::float4x4 &startTfm, int numIters)
{
	PROFILE_SCOPE("findBestTransformBatched", TT_Cpu);
	using namespace chag;
	using namespace chag::type_conversions;


#if ENABLE_BACKEND_DEBUG_RENDERING
	int debugDiscInd = DYNAMIC_TWEAKABLEVAR_UI(int, "debug_sample_disc_index", -1, Input(-1));
#endif // ENABLE_BACKEND_DEBUG_RENDERING


	HostTrackingBackend::BatchedBestTransformRes res;
	res.confidence = 0.0f;
	res.poseIndex = -1;
	res.rmseW = std::numeric_limits<float>::max();
	res.totalWeight = 0.0f;
	res.transform = startTfm;

	for (size_t tInd = 0; tInd < m_templates.size(); ++tInd)
	{
		DBG_RENDERER_OBJECT_SCOPE_BE("Pose");

		const DiscTemplate &templ = m_templates[tInd];

		// Note these will grow to accomodate the largest size and stay there, can be pre-sized to avoid run-time allocs but still be safe in case assumptions change
		m_bestPoints_WeightTmp.reserve(templ.count, true);
		m_bestTemplatePoints_DistTmp.reserve(templ.count, true);

		chag::float4x4 tfm = startTfm;

		float templateConfidence = 0.0f;
		float templateRmseW = std::numeric_limits<float>::max();
		float templateTotalWeight = 0.0f;

		for (int iter = 0; iter < numIters; ++iter)
		{
			// Note: these get clobbered every iteration, so that only the last round actually is used for selection
			//       perhaps this could be changed?
			templateConfidence = -std::numeric_limits<float>::max();
			templateRmseW = std::numeric_limits<float>::max();
			templateTotalWeight = 0.0f;

			// just resets the lengths!
			m_bestPoints_WeightTmp.clear();
			m_bestTemplatePoints_DistTmp.clear();

			{
				DBG_RENDERER_OBJECT_SCOPE_BE("Iteration");

				PROFILE_SCOPE("findBestPoints", TT_Cpu);
				// find correspondence for each template disc.
				for (int discIndex = templ.start; discIndex < templ.start + templ.count; ++discIndex)
				{
					const TemplateDisc td = m_discs[discIndex];

					// this is kind of stupid...
					const float invDistanceWeightRange = 1.0f / (length(templ.aabb.getHalfSize()) * m_params.distanceWeightRangeScale);

					chag::float3 bestPoint = chag::make_vector3(-1.0f);
					chag::float3 bestTemplatePoint = chag::make_vector3(-1.0f);
					float weight = -1.0f;
					float dist = -1.0f;
					int best = processDisc(tfm, td, m_hClusters, m_clusters, m_params, invDistanceWeightRange, bestPoint, bestTemplatePoint, weight, dist,
#if ENABLE_BACKEND_DEBUG_RENDERING
						[&](const HCluster* hc, const TemplateTrackingBackend::Cluster *c)
					{
						if (debugDiscInd == discIndex || debugDiscInd == -1)
						{
							if (hc)
							{
								DBG_RENDERER_CMD_BE(([hc=*hc, discIndex](DebugRenderer::Backend &be) {
									be.drawSphere(hc.centre, hc.radius, DebugRenderer::randomColor(discIndex, 0.75f), "h_clusters");
								}));
							}

							if (c)
							{
								DBG_RENDERER_CMD_BE(([c=*c, discIndex](DebugRenderer::Backend &be) {
									be.drawSphere(c.center, c.radius, DebugRenderer::randomColor(discIndex), "cone_clusters");
								}));
							}
						}
					}
#else // !ENABLE_BACKEND_DEBUG_RENDERING
						backend_shared_utils::DummyDbgFn()
#endif // ENABLE_BACKEND_DEBUG_RENDERING
					);

					if (best >= 0)
					{
						m_bestPoints_WeightTmp.push_back(make_vector4(bestPoint, weight));
						m_bestTemplatePoints_DistTmp.push_back(make_vector4(bestTemplatePoint, dist));
#if ENABLE_BACKEND_DEBUG_RENDERING
						DBG_RENDERER_CMD_BE([=](DebugRenderer::Backend &be) {
							int debugDiscInd = DYNAMIC_TWEAKABLEVAR_UI(int, "debug_sample_disc_index", -1, Input(-1));
							if (debugDiscInd == discIndex || debugDiscInd == -1)
							{
								be.drawSphere(bestPoint, 0.002f, DebugRenderer::randomColor(discIndex), "best");
								be.drawSphere(bestTemplatePoint, 0.002f, DebugRenderer::randomColor(discIndex), "template_points");
							}
						});
#endif //ENABLE_BACKEND_DEBUG_RENDERING
					}
				}
			}

			if (m_bestPoints_WeightTmp.size() > 5)
			{
				PROFILE_SCOPE("calcTransform", TT_Cpu);
				float totalDistSqW = 0.0f;
				chag::float4x4 estTfm =	calcTransformExtractRot(m_bestTemplatePoints_DistTmp, m_bestPoints_WeightTmp, templateTotalWeight, totalDistSqW);
				tfm = estTfm * tfm;

				if (m_params.constrainTransform)
				{
					tfm = constrainTfm(tfm, m_params.minMaxRotXZ, m_params.minMaxTransZ);
				}

				templateRmseW = sqrtf(totalDistSqW / templateTotalWeight);
				templateConfidence = templateTotalWeight / float(templ.count);
			}
		}


		if (templateConfidence >= res.confidence)
		{
			res.rmseW = templateRmseW;
			res.confidence = templateConfidence;
			res.transform = tfm;
			res.totalWeight = templateTotalWeight;
			res.poseIndex = int(tInd);
		}
	}


	return res;
}


std::vector<HostTrackingBackend::BatchedTransformRes> HostTrackingBackend::findTransformBatched(const chag::float4x4 &startTfm, int numIters)
{
	std::vector<HostTrackingBackend::BatchedTransformRes> res;

	for (size_t tInd = 0; tInd < m_templates.size(); ++tInd)
	{
		const DiscTemplate &templ = m_templates[tInd];

		// Note these will grow to accomodate the largest size and stay there, can be pre-sized to avoid run-time allocs but still be safe in case assumptions change
		m_bestPoints_WeightTmp.reserve(templ.count, true);
		m_bestTemplatePoints_DistTmp.reserve(templ.count, true);

		chag::float4x4 tfm = startTfm;


		float templateConfidence = 0.0f;
		float templateRmseW = std::numeric_limits<float>::max();
		float templateTotalWeight = 0.0f;

		for (int iter = 0; iter < numIters; ++iter)
		{
			// Note: these get clobbered every iteration, so that only the last round actually is used for selection
			//       perhaps this could be changed?
			templateConfidence = 0.0f;
			templateRmseW = std::numeric_limits<float>::max();
			templateTotalWeight = 0.0f;

			// just resets the lengths!
			m_bestPoints_WeightTmp.clear();
			m_bestTemplatePoints_DistTmp.clear();

			// find correspondence for each template disc.
			for (int discIndex = templ.start; discIndex < templ.start + templ.count; ++discIndex)
			{
				const TemplateDisc td = m_discs[discIndex];

				// this is kind of stupid...
				const float invDistanceWeightRange = 1.0f / (length(templ.aabb.getHalfSize()) * m_params.distanceWeightRangeScale);

				chag::float3 bestPoint = chag::make_vector3(-1.0f);
				chag::float3 bestTemplatePoint = chag::make_vector3(-1.0f);
				float weight = -1.0f;
				float dist = -1.0f;
				int best = processDisc(tfm, td, m_hClusters, m_clusters, m_params, invDistanceWeightRange, bestPoint, bestTemplatePoint, weight, dist);

				if (best >= 0)
				{
					m_bestPoints_WeightTmp.push_back(make_vector4(bestPoint, weight));
					m_bestTemplatePoints_DistTmp.push_back(make_vector4(bestTemplatePoint, dist));
				}
			}

			if (m_bestPoints_WeightTmp.size() > 5)
			{
				PROFILE_SCOPE("calcTransform", TT_Cpu);
				float totalDistSqW = 0.0f;

#if 1
				chag::float4x4 estTfm = /*m_params.constrainRotationTo2d ?
					calcTransform2D(m_bestPoints_WeightTmp, m_bestTemplatePoints_DistTmp, templateTotalWeight, totalDistSqW) :*/
					calcTransformSvd(m_bestTemplatePoints_DistTmp, m_bestPoints_WeightTmp, templateTotalWeight, totalDistSqW);
				//estTfmX = orthoNormalize(estTfmX);
				tfm = estTfm * tfm;
#else
				std::vector<chag::float3> templatePts;
				std::vector<chag::float3> nearestPts;
				std::vector<float> weights;
				for (size_t i = 0; i < m_bestPoints_WeightTmp.size(); ++i)
				{
					templatePts.push_back(make_vector3(m_bestTemplatePoints_DistTmp[i]));
					nearestPts.push_back(make_vector3(m_bestPoints_WeightTmp[i]));
					weights.push_back(m_bestPoints_WeightTmp[i].w);
					templateTotalWeight += m_bestPoints_WeightTmp[i].w;
					totalDistSqW += m_bestPoints_WeightTmp[i].w * m_bestTemplatePoints_DistTmp[i].w * m_bestTemplatePoints_DistTmp[i].w;
			}

				using namespace jox;
				auto tmpTemplatePts = toEigen<double>(templatePts);
				auto tmpNearestPts = toEigen<double>(nearestPts);
				auto tmpWeights = toEigen<double>(weights);
				Eigen::Affine3d estTfm = calcTransform(tmpTemplatePts, tmpNearestPts, tmpWeights);
				float4x4 estTfm2 = make_matrix<float4x4>(estTfm.cast<float>().data());
				chag::float4x4 estTfmY = calcTransformEigen(m_bestTemplatePoints_DistTmp, m_bestPoints_WeightTmp, templateTotalWeight, totalDistSqW);

				tfm = estTfmY * tfm;
#endif 




#if 1
				if (m_params.constrainTransform)
				{
					tfm = constrainTfm(tfm, m_params.minMaxRotXZ, m_params.minMaxTransZ);
				}
				//else
				//{
				//	objectToWorldTfm = orthoNormalize(objectToWorldTfm);
				//}
#endif // 0


				templateRmseW = sqrtf(totalDistSqW / templateTotalWeight);
				templateConfidence = templateTotalWeight / float(templ.count);
			}
		}
		res.push_back(BatchedTransformRes{ tfm, templateRmseW, templateTotalWeight, templateConfidence });
	}


	return res;
}

namespace
{

	struct ClusterNearTreeAdaptor
	{
		static const chag::float3 &position(const CudaMirrorBuffer<TemplateTrackingBackend::Cluster> &p, int i) { return p[i].center; }
	};

};

void HostTrackingBackend::rebuildHierearchicalClusters()
{
	using Tree = NearTree<ClusterNearTreeAdaptor, 32>;
	Tree nt;
	{
		PROFILE_SCOPE("NearTree::build", TT_Cpu);
		nt.build(m_clusters);
	}
	// reshuffle clusters to avoid indirection and improve coherency
	std::vector<Cluster> clusters2(m_clusters.size());
	m_hClusters.reserve(m_clusters.size());
	m_hClusters.clear();
	{
		PROFILE_SCOPE("hClusters", TT_Cpu);

		for (int li : nt.m_leafNodeInds)
		{
			const Tree::Node &n = nt.m_tree[li];
			HCluster hc;
			hc.centre = n.aabb.getCentre();
			hc.radius = length(n.aabb.getHalfSize());
#if 1
			// reshuffle clusters to avoid indirection and improve coherency
			for (int ii = n.start; ii < n.end; ++ii)
			{
				const Cluster &c = m_clusters[nt.m_indices[ii]];
				clusters2[ii] = c;

				// TODO: build normal cone?
			}
#endif
			hc.start = n.start;
			hc.end = n.end;
			m_hClusters.push_back(hc);
		}
		m_clusters = clusters2;
		// sort in order from the innermost to outermost - might not do much
		//std::sort(hClusters.begin(), hClusters.end(), [&](const HCluster &a, const HCluster &b) { return length(transformPoint(arenaSpaceToLocalTfm, a.centre)) <  length(transformPoint(arenaSpaceToLocalTfm, b.centre)); });
	}
}

