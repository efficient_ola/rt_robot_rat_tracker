/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#ifndef _NearTree_h_
#define _NearTree_h_

#include "Config.h"

#include <linmath/Aabb.h>
#include <linmath/float3.h>
#include <linmath/int3.h>

struct IdentityAdaptor
{
	//template <typename T>
	//T & get(T *values, int i) { return values[i]; }
	template <typename POINTS_CONTAINER_T>
	static const auto & position(const POINTS_CONTAINER_T &values, int i) { return values[i]; }
};

/**
 * NearTree is a BVH used for searching for nearest neighbours, the construction is done through simple spatial 
 * median splitting of the longest axis. The DataAdaptor is struct with a static function 
 * that returs the position of whatever data type is to be used.
 * The tree stores indices into the data, such that a node is represented by a range, the point-data is not stored,
 * but is passed in whenever the tree is traversed.
 */
template<typename DataAdaptor = IdentityAdaptor, int LEAF_SIZE = 128>
struct NearTree
{
	static constexpr int m_leafSize = LEAF_SIZE;
	enum SplitAxis
	{
		SA_X,
		SA_Y,
		SA_Z,
		SA_Max,
	};
	struct Node
	{
		chag::Aabb aabb;
		int start;
		int end;
		int left;
		int right;
		SplitAxis sa;
		float splitPos;
		int level;
	};

	template <typename POINTS_CONTAINER_T>
	int buildRec(const chag::Aabb &nodeAabb, int start, int end, std::vector<int> &inds, const POINTS_CONTAINER_T &samplePoints, std::vector<Node> & result, int level)
	{
		using namespace chag;
		// negative child indexes => this is a leaf
		Node n = { nodeAabb, start, end, -1, -1, SA_Max, 0.0f, level };
		int nodeIndex = int(result.size());
		result.push_back(n);

		if (end - start <= m_leafSize)
		{
			m_leafNodeInds.push_back(nodeIndex);
			return nodeIndex;
		}

		chag::float3 d = nodeAabb.getDiagonal();
		// 1. get longest side
		SplitAxis bestAx = (d.x > d.y) ? (d.x > d.z ? SA_X : SA_Z) : (d.y > d.z ? SA_Y : SA_Z);
		n.sa = bestAx;
		Aabb rightAabb = make_inverse_extreme_aabb();
		Aabb leftAabb = make_inverse_extreme_aabb();

		chag::float3 c = nodeAabb.getCentre();
		float splitPos = c[bestAx];
		n.splitPos = splitPos;
		int endInd = end - 1;
		int splitInd;
		for (splitInd = start; splitInd <= endInd;)
		{
			const chag::float3 &pt = DataAdaptor::position(samplePoints, inds[splitInd]);
			float sd = pt[bestAx];
			if (sd <= splitPos)
			{
				leftAabb = combine(leftAabb, pt);
				++splitInd;
			}
			else
			{
				rightAabb = combine(rightAabb, pt);
				std::swap(inds[splitInd], inds[endInd]);
				--endInd;
		}
	}
		n.left = buildRec(leftAabb, start, splitInd, inds, samplePoints, result, level + 1);
		n.right = buildRec(rightAabb, splitInd, end, inds, samplePoints, result, level + 1);
		result[nodeIndex] = n;
		return nodeIndex;
}
	template <typename POINTS_CONTAINER_T>
	void build(const POINTS_CONTAINER_T &samplePoints)
	{
		using namespace chag;

		m_aabb = make_aabb(int(samplePoints.size()), [&](int i) { return DataAdaptor::position(samplePoints, i); });
		m_indices.resize(samplePoints.size());
		std::iota(m_indices.begin(), m_indices.end(), 0);
		m_tree.resize(0);
		m_leafNodeInds.resize(0);
		if (!samplePoints.empty())
		{
			buildRec(m_aabb, 0, int(m_indices.size()), m_indices, samplePoints, m_tree, 0);
		}
	}

	/**
	 * Return all points withing some radius of the given point,
	 */
	template <typename T>
	std::vector<int> findInRange(const chag::float3 &point, float radius, const std::vector<T> &samplePoints) const
	{
		using namespace chag;

		std::vector<int> result;
		if (samplePoints.empty())
		{
			return result;
		}

		const float rSquared = square(radius);
		int stack[32];
		stack[0] = 0;
		int stackSize = 1;
		while (stackSize > 0)
		{
			int nodeInd = stack[--stackSize];

			const Node &n = m_tree[nodeInd];

			// Check if in range
			if (distToAabbSq(point, n.aabb) < rSquared)
			{
				// Leaf. we should check the points
				if (n.left < 0)
				{
					for (int i = n.start; i < n.end; ++i)
					{
						int ind = m_indices[i];
						float distSq = lengthSquared(DataAdaptor::position(samplePoints, ind) - point);
						if (distSq < rSquared)
						{
							result.push_back(ind);
						}
					}
				}
				else
				{
					int a = n.left;
					int b = n.right;

					stack[stackSize++] = b;
					stack[stackSize++] = a;
				}
			}
	}
		return result;
	}


	/**
	 * Return the K nearest points to 'point'.
	 */
	template <int K, typename T>
	int findKnn(const chag::float3 &point, const std::vector<T> &samplePoints, int knn[K]) const
	{
		using namespace chag;
		// distance to farthest point in set of K - used to prune nodes
		float worstDistSq = -std::numeric_limits<float>::max();
		int k = 0;
		float dists[K];
		int stack[32];
		stack[0] = 0;
		int stackSize = 1;
		while (stackSize > 0)
		{
			int nodeInd = stack[--stackSize];

			const Node &n = m_tree[nodeInd];

			// Check if we've already found a full set and the best 
			if (k < K || distToAabbSq(point, n.aabb) < worstDistSq)
			{
				// Leaf. we should check the points
				if (n.left < 0)
				{
					for (int i = n.start; i < n.end; ++i)
					{
						int ind = m_indices[i];

						float distSq = lengthSquared(DataAdaptor::position(samplePoints, ind) - point);
						if (k < K)
						{
							dists[k] = distSq;
							knn[k++] = ind;
							worstDistSq = std::max(worstDistSq, distSq);
						}
						else if (distSq < worstDistSq)
						{
							// reset
							worstDistSq = -std::numeric_limits<float>::max();

							int currInd = ind;
							for (int j = 0; j < K; ++j)
							{
								//float3 point = DataAdaptor::position(knn[j]);
								//float pDistSq = lengthSquared(DataAdaptor::position(samplePoints, knn[j]) - point);
								if (distSq < dists[j])
								{
									std::swap(currInd, knn[j]);
									std::swap(distSq, dists[j]);
									worstDistSq = std::max(worstDistSq, dists[j]);
								}
							}
						}
					}
				}
				else
				{
					// make sure we visit nearest node first
					int a = n.left;
					int b = n.right;
					if (point[n.sa] > n.splitPos)
					{
						std::swap(a, b);
					}
					// push farthest
					stack[stackSize++] = b;
					// push nearest:
					stack[stackSize++] = a;
				}
			}
		}
		return k;
	}

	std::vector<Node> m_tree;
	std::vector<int> m_indices;
	std::vector<int> m_leafNodeInds;
	chag::Aabb m_aabb;
	chag::int3 m_gridDims;
};

#endif // _NearTree_h_
