/*********************************************************************************
Copyright (c) 2019, Ola Olsson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************************/
#include "TemplateEditor.h"
#include <boost/filesystem.hpp>
#include <nlohmann/json.hpp>

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "imgui/imgui.h"

#include "utils/Rendering.h"
#include "utils/SimpleShader.h"
#include "utils/DebugRenderer.h"
#include "utils/PrimitiveRenderer.h"
#include "utils/ChagIntersection.h"
#include <utils/OBJModel.h>
#include <utils/ImGuiX.h>

#include "profiler/Profiler.h"

#include "TemplateLibrary.h"
#include "HostTrackingBackend.h"
#include "ApssMeshLab.h"
#include "utils/Random.h"

using json = nlohmann::json;
#undef max
using namespace chag;


inline Aabb make_aabb(const TemplateLibrary::Template &t)
{
	return make_aabb(int(t.discs.size()), [&](int i) { return t.discs[i].position; });
}


inline float weight(float r)
{
	const float epsilon = 1.0e-8f;
	const float maxVal = float(pow(double(epsilon), -2.0));
	if (r < epsilon)
	{
		return maxVal;
	}
	return powf(r + epsilon, -2.0f);
}


inline float weightSq(float rSq)
{
	const float epsilon = 1.0e-8f;
	const float epsilonSq = square(epsilon);
	const float maxVal = float(pow(double(epsilon), -2.0));
	if (rSq < epsilonSq)
	{
		return maxVal;
	}
	return 1.0f / rSq;
}


inline float weight(float r, float h)
{
	float r2 = (r*r) / (h * h);
	if (r2 < 1.0f)
	{
		return powf(1.0f - r2, 4.0f);

	}
	return 0.0f;
}


inline float weightSq(float rSq, float h)
{
	float r2 = rSq / (h * h);
	if (r2 < 1.0f)
	{
		float tmp = square(1.0f - r2);
		return square(tmp);
		//return powf(1.0f - r2, 4.0f);
	}
	return 0.0f;
}

inline float weightSqGrad(const float3 &delta, float h, float3 &grad, float &dW)
{
	float rSq = lengthSquared(delta);
	const float s = 1.0f / (h * h);
	const float aux = max(0.0f, 1.0f - rSq * s);
	
	float w = square(aux) * square(aux);

	dW = (-2.0f * s) * (4.0f * aux * aux * aux);
	grad = delta * dW;

	return w;
}


inline float weightSqGradRsqDist(const float3 &delta, float h, float3 &grad, float &dW)
{
	float rSq = lengthSquared(delta);

	const float epsilon = 1.0e-8f;
	const float epsilonSq = square(epsilon);
	const float maxVal = float(pow(double(epsilon), -2.0));

	float w = maxVal;

	if (rSq >= epsilonSq)
	{
		w = 1.0f / rSq;
	}

	dW = -2.0f * w * sqrtf(w);
	grad = delta * dW;

	return w;
}



template <int K, typename POINT_FN_T>//TODO?, typename WEIGHT_FN_T>
int getKnnLinear(const float3 &x, size_t inputSize, POINT_FN_T pointDirFn, std::pair<float3, float3> knn[K])
{
	int nnCount = 0;
	for (size_t i = 0; i < inputSize; ++i)
	{
		std::pair<float3, float3> pointDir = pointDirFn(i);
		if (nnCount < K)
		{
			knn[nnCount++] = pointDir;
		}
		else
		{
			for (int j = 0; j < nnCount; ++j)
			{
				if (lengthSquared(x-pointDir.first)  < lengthSquared(x-knn[j].first))
				{
					std::swap(pointDir, knn[j]);
				}
			}
		}
	}
	return nnCount;
}





float computeRmseDistance(const TemplateLibrary::Template &a, const TemplateLibrary::Template &b, float maxDist)
{	
	auto weightFnStd = [&](size_t i, const float3 &pt)->float {
		return weight(length(pt - b.discs[i].position), b.discs[i].radius * 15.0f);
	};
	const int K = 8;
	float sqDist = 0.0f;
	auto pointDirFn = [&](size_t i) { return std::make_pair(b.discs[i].position, b.discs[i].normal); };
	auto pointDirKnnFn = [&](const float3 &x, std::pair<float3, float3> *nns)
	{ 
		return getKnnLinear<8>(x, b.discs.size(), pointDirFn, nns);
	};
	auto weightFnKnn = [&](const std::pair<float3,float3> &pd, float h, const float3 &pt)->float {
		//return weight(length(pt - pd.first), h);
		return weight(length(pt - pd.first));
	};
	auto weightFnRimlsKnn = [&](const std::pair<float3, float3> &pd, float h, const float3 &pt, float3 &grad, float &dW)->float {
		//return weightSqGrad(pt - pd.first, h, grad, dW);
		return weightSqGradRsqDist(pt - pd.first, h, grad, dW);
		//return weightSq(lengthSquared(pt - pd.first));
		//return weight(length(pt - pd.first));
	};

	for (const auto &d : a.discs)
	{
		float3 pp = d.position;
		float3 pn = d.normal;
		//if (ApssMeshLab::project(d.position, b.discs.size(), pointDirFn, weightFnStd, pp, pn, 25, 0.5f, d.radius / 1000.0f))
		//if (ApssMeshLab::project_knn<K>(d.position, pointDirKnnFn, weightFnKnn, pp, pn, 25, 0.5f, d.radius / 10000.0f))
		if (ApssMeshLab::project_rimls_knn<K>(d.position, pointDirKnnFn, weightFnRimlsKnn, pp, pn, 125, 1.0e-9f))
		{
			sqDist += lengthSquared(pp-d.position);
		}
		else
		{
			sqDist += maxDist * maxDist;
		}
	}
	return sqrtf(sqDist / float(a.discs.size()));
}


float computeMatchConfidence(const TemplateLibrary::Template &a, const TemplateLibrary::Template &b, float maxDist)
{
	auto weightFnStd = [&](size_t i, const float3 &pt)->float {
		return weight(length(pt - b.discs[i].position), b.discs[i].radius * 15.0f);
	};
	const int K = 8;
	float sqDist = 0.0f;
	auto pointDirFn = [&](size_t i) { return std::make_pair(b.discs[i].position, b.discs[i].normal); };
	auto pointDirKnnFn = [&](const float3 &x, std::pair<float3, float3> *nns)
	{
		return getKnnLinear<8>(x, b.discs.size(), pointDirFn, nns);
	};
	auto weightFnKnn = [&](const std::pair<float3, float3> &pd, float h, const float3 &pt)->float {
		//return weight(length(pt - pd.first), h);
		return weight(length(pt - pd.first));
	};
	auto weightFnRimlsKnn = [&](const std::pair<float3, float3> &pd, float h, const float3 &pt, float3 &grad, float &dW)->float {
		//return weightSqGrad(pt - pd.first, h, grad, dW);
		return weightSqGradRsqDist(pt - pd.first, h, grad, dW);
		//return weightSq(lengthSquared(pt - pd.first));
		//return weight(length(pt - pd.first));
	};
	float totalWeight = 0.0f;
	for (const auto &d : a.discs)
	{
		float3 pp = d.position;
		float3 pn = d.normal;
		//if (ApssMeshLab::project(d.position, b.discs.size(), pointDirFn, weightFnStd, pp, pn, 25, 0.5f, d.radius / 1000.0f))
		//if (ApssMeshLab::project_knn<K>(d.position, pointDirKnnFn, weightFnKnn, pp, pn, 25, 0.5f, d.radius / 10000.0f))
		if (ApssMeshLab::project_rimls_knn<K>(d.position, pointDirKnnFn, weightFnRimlsKnn, pp, pn, 125, 1.0e-9f))
		{
			float dist = length(pp-d.position);
			float cosAngle = dot(d.normal, pn);

			float distWeight = max(0.0f, 1.0f - dist / maxDist);
			float dirWeight = max(0.0f, cosAngle);

			totalWeight += distWeight * dirWeight;
		}
	}
	return totalWeight / float(a.discs.size());
}



TemplateEditor::TemplateEditor(TemplateLibraryPtr lib, PrimitiveRenderer *primRenderer, SimpleShader *objModelShader, const std::string &dataDir)
		: m_currentPose(0),
		m_prevPose(-1),
		m_pitch(g_pi / 4.0f),
		m_yaw(g_pi / 4.0f),
		m_lastMousePos(int2{ 0,0 }),
		m_leftDownMousePos(int2{ 0,0 }),
		m_distanceScale(1.0f),
		m_mouseLookActive(false),
		m_selectTool(true),
		m_selectToolRange(0.05f),
		m_lib(lib),
		m_primRenderer(primRenderer),
		m_fov(60.0f),
		m_objModelShader(objModelShader),
		m_computeSimilarity(false),
		m_computeAnnotationSimilarity(true)
	{
		m_coordinatesModel = new OBJModel();
		m_coordinatesModel->load(dataDir + "/models/coordinate_system.obj");
		m_zArrowModel = new OBJModel(dataDir + "/models/z_arrow.obj");
		m_highLightedType = ST_Disc;
		m_selectionType = ST_Disc;
		m_trackingBackend = new HostTrackingBackend();
	}

void TemplateEditor::loadOptions(const std::string &optionsFileName)
{
	{
		std::ifstream i(optionsFileName);
		if (i.is_open())
		{
			json js;
			i >> js;

			m_selectTool = js["selectTool"];
			m_pitch = js["pitch"];
			m_yaw = js["yaw"];
			m_currentPose = js["currentPose"];
			m_distanceScale = js["distanceScale"];
			m_selectToolRange = js["selectToolRange"];
			m_selected = js["selected"].get<std::set<int>>();
			m_selectionType = js["selectionType"];
			m_visibleAnnotations = js["visibleAnnotations"].get<std::set<std::string>>();
		}
	}
}

void TemplateEditor::saveOptions(const std::string &optionsFileName)
{
	std::ofstream o(optionsFileName);
	if (o.is_open())
	{
		json js;
		js["selectTool"] = m_selectTool;
		js["pitch"] = m_pitch;
		js["yaw"] = m_yaw;
		js["currentPose"] = m_currentPose;
		js["distanceScale"] = m_distanceScale;
		js["selectToolRange"] = m_selectToolRange;
		js["selected"] = m_selected;
		js["selectionType"] = int(m_selectionType);
		js["visibleAnnotations"] = m_visibleAnnotations;
		o << std::setw(4) << js;
	}
}




void TemplateEditor::onMouse()
{
	//m_ yaw +=/
}




void TemplateEditor::onMouseMotion(const int2 &mousePos, bool leftDown, bool rightDown, bool middleDown)
{
	int2 delta = mousePos - m_lastMousePos;

	if (m_mouseLookActive)
	{
		if (leftDown)
		{
			m_yaw += float(delta.x) * 0.03f;
			m_pitch = clamp(m_pitch + float(delta.y) * 0.03f, -g_pi * 0.499f, g_pi * 0.499f);
		}
		else
		{
			m_mouseLookActive = false;
		}
	}
	if (rightDown)
	{
		if (!m_selected.empty() && m_selectionType == ST_Annotation && m_currentPose >= 0 && m_currentPose < int(m_lib->templates.size()))
		{
			TemplateLibrary::Template &t = m_lib->templates[m_currentPose];

			TemplateLibrary::Annotation &a = t.annotations[*m_selected.begin()];
			float sceneScale = length(m_aabb.getDiagonal());
			float2 deltaTrans = sceneScale * make_vector<float>(delta.x, -delta.y) / float(m_width);
			float4x4 viewToWorldTfm = inverse(m_viewMatrix);
			// deltaTrans
			float3 offset = transformDirection(viewToWorldTfm, make_vector3(deltaTrans, 0.0f));
			a.position += offset;
		}
		else
		{
			m_distanceScale = clamp(m_distanceScale + float(delta.y) * 0.1f, 0.0001f, 10.0f);
		}
	}
	if (middleDown)
	{
		if (!m_selected.empty() && m_selectionType == ST_Annotation && m_currentPose >= 0 && m_currentPose < int(m_lib->templates.size()))
		{
			TemplateLibrary::Template &t = m_lib->templates[m_currentPose];

			TemplateLibrary::Annotation &a = t.annotations[*m_selected.begin()];
			float2 deltaRot = make_vector<float>(delta) * 0.01f;
			float3x3 frame = make_rotation_from_zAxis(a.direction, make_vector(0.0f, 0.0f, 1.0f));
			float3x3 tmp = frame * make_rotation_y<float3x3>(deltaRot.x) * make_rotation_x<float3x3>(float(deltaRot.y));
			a.direction = normalize(tmp * make_vector(0.0f, 0.0f, 1.0f));
		}
	}
	m_lastMousePos = mousePos;
}




void TemplateEditor::onMouseButton(int button, int action, const int2 &mousePos)
{
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
	{
		m_leftDownMousePos = mousePos;
		m_lastMousePos = mousePos;
		m_mouseLookActive = true;
	}
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE)
	{
		if (m_leftDownMousePos == mousePos && m_selectTool)
		{
			if (m_selectionType != m_highLightedType)
			{
				m_selected.clear();
				m_selectionType = m_highLightedType;
			}
			m_selected.insert(m_highlighted.begin(), m_highlighted.end());
		}
		m_mouseLookActive = false;
	}
	if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_RELEASE)
	{
		if (m_selectTool)
		{
			for (int ind : m_highlighted)
			{
				m_selected.erase(ind);
			}
		}
	}
}




void TemplateEditor::onMouseScroll(float step)
{
	m_distanceScale = clamp(m_distanceScale + step * 0.1f, 0.0001f, 10.0f);
}




bool TemplateEditor::onKey(int key, int action)
{
	if (action == GLFW_PRESS)
	{
		switch (key)
		{
		case GLFW_KEY_LEFT:
			setPose(m_currentPose - 1);
			return true;
		case GLFW_KEY_RIGHT:
			setPose(m_currentPose + 1);
			return true;
		case GLFW_KEY_DELETE:
			deleteSelected();
			return true;
		}
	}
	return false;
}




void TemplateEditor::update(float dt)
{
}

namespace
{
	struct TemplateAdaptor
	{
		float coneAngle;
		float invTanConeAngle;
		inline void beginTemplate(const TemplateLibrary::Template &t)
		{ 
			coneAngle = acosf(1.0f - 1.0f / float(t.discs.size()));
			invTanConeAngle = 1.0f / tanf(coneAngle);
		}		
		inline chag::Aabb getAabb(const TemplateLibrary::Template &t) const 
		{ 
			return make_aabb(int(t.discs.size()), [&](int i) { return t.discs[i].position; });
		}
		inline const auto &getDiscs(const TemplateLibrary::Template &t) const 
		{ 
			return t.discs; 
		}
		inline const auto &getDirection(const TemplateLibrary::Disc &d) const { return d.normal; }
		inline float getOffset(const TemplateLibrary::Disc &d) const
		{
			return d.radius * invTanConeAngle;
		}
		inline float getConeAngle(const TemplateLibrary::Disc &d) const
		{
			return coneAngle;
		}
	};


	struct ClusterAdaptor
	{
		ClusterAdaptor(const TemplateLibrary::Annotation &sa) : m_sa(sa) {}
		inline const chag::float3 &getCentre(const TemplateLibrary::Disc &d) const { return d.position; }
		
		// weight by distance to annotaiotion
		inline float getPriorWeight(const TemplateLibrary::Disc &d) const 
		{
			float angW = max(0.0f, dot(d.normal, m_sa.direction)) * 0.5f + 0.5f;
			return weightSq(lengthSquared(d.position - m_sa.position), m_sa.radius * 6.0f) * angW;
		}

		const TemplateLibrary::Annotation &m_sa;
	};
}

void TemplateEditor::render(int width, int height)
{
	PROFILE_SCOPE("TemplateEditor", TT_Cpu);
	m_width = width;
	m_height = height;
	glDisable(GL_SCISSOR_TEST);
	glViewport(0, 0, width, height);
	glClearColor(0.0f, 0.0f, 0.3f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glEnable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	//glDepthMask(GL_FALSE);
	//glEnable(GL_BLEND);
	//glBlendFunc(GL_ONE, GL_ONE);

	CHECK_GL_ERROR();

	if (!m_lib || m_lib->templates.empty())
	{
		return;
	}

	m_currentPose = std::min(std::max(0, m_currentPose), int(m_lib->templates.size() - 1));
	static int numIters = 50;
	m_primRenderer->setTransform(make_identity<float4x4>());

	m_aabb = make_inverse_extreme_aabb();
	for (const auto &t : m_lib->templates)
	{
		m_aabb = combine(m_aabb, make_aabb(int(t.discs.size()), [&](int i) { return t.discs[i].position; }));
	}
	if (m_currentPose != m_prevPose || m_computeSimilarity && m_similarities.empty())
	{
		m_similarities.clear();
		if (m_computeSimilarity)
		{
			if (m_computeAnnotationSimilarity)
			{
				const TemplateLibrary::Template &st = m_lib->templates[m_currentPose];

				TemplateLibrary::Annotation sa;
				sa.radius = -1.0f;
				for (const TemplateLibrary::Annotation &_sa : st.annotations)
				{
					if (m_visibleAnnotations.count(_sa.id))
					{
						sa = _sa;
						//copyAnnotationToSimilar(sa, t, overWrite);
					}
				}
				if (sa.radius > 0.0f)
				{
					TemplateTrackingBackend::Params p;
					p.constrainTransform = false;
					p.minMaxRotXZ = 0.0f;
					p.minMaxTransZ = 0.0f;
					p.distanceWeightRangeScale = 0.1f;
					m_trackingBackend->setParams(p);

					// Only do if any changed!
					m_trackingBackend->setTemplates(m_lib->templates, TemplateAdaptor());

					using Disc = TemplateLibrary::Disc;
					using Annotation = TemplateLibrary::Annotation;

					// 1. get source discs.
					std::vector<Disc> sourceDiscs;
					for (const Disc &d : st.discs)
					{
						//if (overlapsSphereSphere(d.position, d.radius, sa.position, sa.radius))
						{
							sourceDiscs.push_back(d);
						}
					}

					HCluster hc;
					hc.centre = sa.position;
					hc.radius = 1000.0f;// sa.radius;
					hc.start = 0;
					hc.end = int(sourceDiscs.size());
					ClusterAdaptor ca(sa);
					m_trackingBackend->setPoints({ hc }, sourceDiscs, ca);

					//std::vector<HostTrackingBackend::BatchedTransformRes> r = m_trackingBackend->findTransformBatched(make_rotation_x<float4x4>(g_pi / 10.0f), numIters);
					std::vector<HostTrackingBackend::BatchedTransformRes> r = m_trackingBackend->findTransformBatched(make_identity<float4x4>(), numIters);

					float selfConf = r[m_currentPose].confidence;
					for (size_t i = 0; i < r.size(); ++i)
					{
						m_similarities.push_back(SimilarityData{ int(i), r[i].confidence / selfConf, r[i].transform });
					}

				}
				// Use selected anotation(s???) to find the best matching template and transform, it's a bit like one ought to re-use the machinery built for this really...
			}
			else
			{
				for (int i = 0; i < int(m_lib->templates.size()); ++i)
				{
					//float similarity = computeRmseDistance(m_lib->templates[i], m_lib->templates[m_currentPose], length(m_aabb.getDiagonal()));
					float similarity = 0.5f * (computeMatchConfidence(m_lib->templates[i], m_lib->templates[m_currentPose], length(m_aabb.getDiagonal()))
						+ computeMatchConfidence(m_lib->templates[m_currentPose], m_lib->templates[i], length(m_aabb.getDiagonal())));

					m_similarities.push_back(SimilarityData{ i, similarity, make_identity<float4x4>() });
				}
			}
			std::sort(m_similarities.begin(), m_similarities.end(), [&](const auto &a, const auto &b) { return a.similarity > b.similarity; });
		}
	}
	m_similarShown.resize(m_similarities.size(), false);



	float4x4 projectionMatrix = perspectiveMatrix(m_fov, float(width) / float(height), 0.01f, 1000.0f);


	float3 offset = make_rotation_z<float3x3>(m_yaw) * make_rotation_x<float3x3>(m_pitch) * make_vector(0.0f, m_aabb.getDiagonal().y * m_distanceScale, 0.0f);
	float3 cameraPos = m_aabb.getCentre() + offset;
	m_viewMatrix = lookAt(cameraPos, m_aabb.getCentre(), make_vector(0.0f, 0.0f, 1.0f));
	float4x4 clipToWorldTfm = inverse(projectionMatrix * m_viewMatrix);
	// to create camera picking ray dir
	float2 mouseClipPos = make_vector2(-1.0f) + 2.0f * make_vector<float>(m_lastMousePos.x, height - m_lastMousePos.y) / make_vector(float(width), float(height));
	float3 pickRayDir = normalize(transformPoint(clipToWorldTfm, make_vector3(mouseClipPos, -1.0f)) - transformPoint(clipToWorldTfm, make_vector3(mouseClipPos, 1.0f)));
	float3 pickRayOrigin = cameraPos;


	m_objModelShader->begin();

	m_objModelShader->setUniform("viewProjectionTransform", projectionMatrix * m_viewMatrix);
	m_objModelShader->setUniform("normalMat", m_viewMatrix);
	m_objModelShader->setUniform("worldUpDirection", make_vector(0.0f, 0.0f, 1.0f));

	m_objModelShader->setUniform("color", make_vector(1.0f, 1.0f, 1.0f, 1.0f));
	m_objModelShader->setUniform("modelTransform", chag::make_scale<chag::float4x4>(make_vector3(0.1f)));

	m_coordinatesModel->render(nullptr);
	m_objModelShader->end();

	//aabb.max *= 1.5f;
	//aabb.min *= 1.5f;

	SimpleShader *primShader = m_primRenderer->getDefautShader();

	primShader->begin();

	primShader->setUniform("viewProjectionTransform", projectionMatrix * m_viewMatrix);
	primShader->setUniform("normalMat", m_viewMatrix);
	primShader->setUniform("worldUpDirection", make_vector(0.0f, 0.0f, 1.0f));

	primShader->setUniform("color", make_vector(0.5f, 0.5f, 0.5f, 1.0f));

	const int gridRes = 10;
	for (int i = 0; i < gridRes; ++i)
	{
		{
			float x = lerp(m_aabb.min.x, m_aabb.max.x, float(i) / float(gridRes-1));
			float3 a = make_vector(x, m_aabb.min.y, 0.0f);
			float3 b = make_vector(x, m_aabb.max.y, 0.0f);
			m_primRenderer->drawLine(a, b, primShader);
		}
		{
			float y = lerp(m_aabb.min.y, m_aabb.max.y, float(i) / float(gridRes-1));
			float3 a = make_vector(m_aabb.min.x, y, 0.0f);
			float3 b = make_vector(m_aabb.max.x, y, 0.0f);
			m_primRenderer->drawLine(a, b, primShader);
		}
	}

	glLineWidth(3.0f);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);
	static int maxIters = 50;

	m_highLightedType = ST_Disc;
	m_highlighted.clear();
	if (!m_lib->templates.empty())
	{
		const TemplateLibrary::Template &t = m_lib->templates[m_currentPose];


		TemplateLibrary::Annotation sa;
		sa.radius = -1.0f;
		for (const TemplateLibrary::Annotation &_sa : t.annotations)
		{
			if (m_visibleAnnotations.count(_sa.id))
			{
				sa = _sa;
			}
		} 
		ClusterAdaptor ca(sa);
		{
			for (size_t i = 0; i < t.discs.size(); ++i)
			{
				const TemplateLibrary::Disc &d = t.discs[i];

				primShader->setUniform("color", DebugRenderer::randomColor(i));
				if (m_selectTool)
				{
					//if (m_selected.find(int(i)) == m_selected.end())
					{
						float3 dProj = cameraPos + pickRayDir * dot(pickRayDir, d.position - cameraPos);

						if (length(dProj - d.position) < m_selectToolRange)
						{
							primShader->setUniform("color", DebugRenderer::randomColor(i, 0.85f));
							m_highlighted.push_back(int(i));
						}
						else
						{
							primShader->setUniform("color", DebugRenderer::randomColor(i, 0.5f));
						}
					}
				}
				if (sa.radius > 0.0f)
				{
					primShader->setUniform("color", make_vector4(make_vector3(ca.getPriorWeight(d)), 1.0f));
				}
				m_primRenderer->drawDisc(d.position, d.normal, d.radius, primShader, false);
				m_primRenderer->drawLine(d.position, d.position + d.normal * d.radius, primShader);
			}
		}

		{
			const TemplateLibrary::Template &t = m_lib->templates[m_currentPose];
			primShader->setUniform("color", DebugRenderer::green);
			//glLineWidth(3.0f);
			for (size_t i = 0; i < t.discs.size(); ++i)
			{
				const TemplateLibrary::Disc &d = t.discs[i];

				if (m_selectionType == ST_Disc && m_selected.find(int(i)) != m_selected.end())
				{
					m_primRenderer->drawDisc(d.position, d.normal, d.radius, primShader, true);
				}
			}
		}

		for (int si = 0; si < int(m_similarities.size()); ++si)
		{
			const auto &s = m_similarities[si];
			//if (s.first != m_currentPose)
			const auto &t = m_lib->templates[s.index];
			if (m_similarShown[si])
			{


				primShader->setUniform("color", DebugRenderer::randomColor(si,0.8f));
				if (m_applyTransform)
				{
					m_primRenderer->setTransform(s.tfm);
				}
				//glLineWidth(3.0f);
				for (size_t i = 0; i < t.discs.size(); ++i)
				{
					const TemplateLibrary::Disc &d = t.discs[i];
					if (sa.radius > 0.0f)
					{
						primShader->setUniform("color", make_vector4(make_vector3(ca.getPriorWeight(d)), 1.0f));
					}
					m_primRenderer->drawDisc(d.position, d.normal, d.radius, primShader, false);
				}
			}
			m_primRenderer->setTransform(make_identity<float4x4>());
		}

		{
			const TemplateLibrary::Template &t = m_lib->templates[m_currentPose];
			auto weightFnStd = [&](size_t i, const float3 &pt)->float {
				return weight(length(pt - t.discs[i].position), t.discs[i].radius * 15.0f);
			};
			const int K = 8;
			float sqDist = 0.0f;
			auto pointDirFn = [&](size_t i) { return std::make_pair(t.discs[i].position, t.discs[i].normal); };
			auto pointDirKnnFn = [&](const float3 &x, std::pair<float3, float3> *nns)
			{
				return getKnnLinear<8>(x, t.discs.size(), pointDirFn, nns);
			};
			auto weightFnKnn = [&](const std::pair<float3, float3> &pd, float h, const float3 &pt)->float {
				return weightSq(lengthSquared(pt - pd.first), h);
				//return weightSq(lengthSquared(pt - pd.first));
				//return weight(length(pt - pd.first));
			};

			auto weightFnRimlsKnn = [&](const std::pair<float3, float3> &pd, float h, const float3 &pt, float3 &grad, float &dW)->float {
				//return weightSqGrad(pt - pd.first, h, grad, dW);
				return weightSqGradRsqDist(pt - pd.first, h, grad, dW);
				//return weightSq(lengthSquared(pt - pd.first));
				//return weight(length(pt - pd.first));
			};
			static std::vector<float3> failures;
			glLineWidth(1.0f);
			srand(0);

			//float 1e-4
#if 0
			int k = 0;
			for (float3 p : failures)
			{
				float3 pp = p;
				float3 pn = make_vector3(0.0f);
				//if (ApssMeshLab::project(d.position, b.discs.size(), pointDirFn, weightFnStd, pp, pn, 25, 0.5f, d.radius / 1000.0f))
				if (ApssMeshLab::project_knn<K>(p, pointDirKnnFn, weightFnKnn, pp, pn, maxIters, 0.5f, 1.0e-3f, [&](const float3 &c, float r, int iter)
				{
					//if (iter == 0)
					{
						primShader->setUniform("color", DebugRenderer::randomColor(iter, 0.8f));
						m_primRenderer->drawSphere(c, r, primShader, false);
					}
				}))
				{
					primShader->setUniform("color", DebugRenderer::randomColor(++k, 0.5f));
					m_primRenderer->drawSphere(pp, 0.003f, primShader, false);
					m_primRenderer->drawLine(p, pp, primShader);
					m_primRenderer->drawSphere(p, 0.001f, primShader, false);
				}
				else
				{
					primShader->setUniform("color", DebugRenderer::red);
					m_primRenderer->drawSphere(p, 0.003f, primShader, false);
				}
			}
			if (failures.empty())
#endif
			{
#if 0

				for (int i = 0; i < 10000; ++i)
				{
					float3 p = {
						randomRange(m_aabb.min.x, m_aabb.max.x),
						randomRange(m_aabb.min.y, m_aabb.max.y),
						randomRange(m_aabb.min.z, m_aabb.max.z),
					};
					float3 pp = p;
					float3 pn = make_vector3(0.0f);
					//if (ApssMeshLab::project(d.position, b.discs.size(), pointDirFn, weightFnStd, pp, pn, 25, 0.5f, d.radius / 1000.0f))
					if (rimls_meshlab_project_knn<K>(p, pointDirKnnFn, weightFnRimlsKnn, pp, pn, maxIters, 1.0e-5f))
					//if (ApssMeshLab::project_knn<K>(p, pointDirKnnFn, weightFnKnn, pp, pn, maxIters, 0.5f, 1.0e-5f))
					{
						primShader->setUniform("color", DebugRenderer::randomColor(i, 0.5f));
						m_primRenderer->drawSphere(pp, 0.0015f, primShader, false);
						//m_primRenderer->drawLine(p, pp, primShader);
						//m_primRenderer->drawSphere(p, 0.001f, primShader, false);
					}
					else
					{
						primShader->setUniform("color", DebugRenderer::red);
						m_primRenderer->drawSphere(p, 0.003f, primShader, false);
#if 0
						failures.push_back(p);
						break;
#endif
					}
				}
#endif
			}
		}
	}
	glDisable(GL_BLEND);
	primShader->end();

	// draw annotations
	{
		const TemplateLibrary::Template &t = m_lib->templates[m_currentPose];
		for (size_t i = 0; i < t.annotations.size(); ++i)
		{
			const TemplateLibrary::Annotation &a = t.annotations[i];
			if (m_visibleAnnotations.count(a.id) != 0)
			{
				float4x4 tfm = make_matrix_from_zAxis(a.position, a.direction, make_vector(0.0f, 0.0f, 1.0f)) * chag::make_scale<chag::float4x4>(make_vector3(a.radius * 2.0f));
				float4x4 invTfm = inverse(tfm);

				float3 localRayO = transformPoint(invTfm, pickRayOrigin);
				float3 localRayD = normalize(transformDirection(invTfm, pickRayDir));

				float t0 = 0.0f;
				float t1 = 0.0f;
				bool highLight = m_zArrowModel->traceRay(localRayO, localRayD) || intersectRaySphere(pickRayOrigin, pickRayDir, a.position, a.radius, t0, t1);
				float4 colour = DebugRenderer::yellow * 0.5f;
				if (highLight)
				{
					colour = DebugRenderer::yellow;
					m_highlighted.clear();
					m_highLightedType = ST_Annotation;
					m_highlighted.push_back(int(i));
				}
				primShader->begin();
				primShader->setUniform("color", colour);
				m_primRenderer->drawSphere(a.position, a.radius, primShader);
				//m_primRenderer->drawLine(a.position, a.position + a.direction * a.radius * 2.0f, primShader);

				if (m_selectionType == ST_Annotation && m_selected.find(int(i)) != m_selected.end())
				{
					primShader->setUniform("color", DebugRenderer::green);
					m_primRenderer->drawSphere(a.position, a.radius, primShader, true);
				}
				primShader->end();

				m_objModelShader->begin();

				m_objModelShader->setUniform("viewProjectionTransform", projectionMatrix * m_viewMatrix);
				m_objModelShader->setUniform("normalMat", m_viewMatrix);
				m_objModelShader->setUniform("worldUpDirection", make_vector(0.0f, 0.0f, 1.0f));

				m_objModelShader->setUniform("color", colour);
				m_objModelShader->setUniform("modelTransform", tfm);

				m_zArrowModel->render(nullptr);
				m_objModelShader->end();


			}
		}
	}

	m_prevPose = m_currentPose;
	bool deletePosePressed = false;
	std::set<size_t> anotationsToDelete;
	{
		int prevPose = m_currentPose;
		float height = 50.0f;
		ImGui::SetNextWindowPos(ImVec2(ImGui::GetIO().DisplaySize.x * 0.5f, ImGui::GetIO().DisplaySize.y - 5.0f), ImGuiCond_Always, ImVec2(0.5f, 1.0f));
		ImGui::SetNextWindowSize(ImVec2(ImGui::GetIO().DisplaySize.x * 0.75f, height), ImGuiCond_Always);
		ImGui::SetNextWindowBgAlpha(0.5f); // Transparent background
		if (ImGui::Begin("Current Pose", 0, ImGuiWindowFlags_NoTitleBar|ImGuiWindowFlags_NoResize|ImGuiWindowFlags_NoFocusOnAppearing))
		{
			const int numPoses = int(m_lib->templates.size());
			ImGui::PushItemWidth(-1);
			ImGui::SliderInt("##PoseIndex", &m_currentPose, 0, numPoses-1);
			setPose(m_currentPose);

			ImGui::PopItemWidth();
			if (ImGui::Button("Prev Pose"))
			{
				setPose(m_currentPose - 1);
			}
			ImGui::SameLine();
			if (ImGui::Button("Next Pose"))
			{
				setPose(m_currentPose + 1);
			}
			//ImGui::Separator();
			ImGui::SameLine();

			ImGui::End();
		}
		if (ImGui::Begin("Tools", 0))
		{
			ImGuiX::PropertyListBegin();
			ImGuiX::Property("maxIters", maxIters);
			ImGuiX::Property("numIters", numIters);
			ImGuiX::Property("Select Tool", m_selectTool);
			ImGuiX::Property("Select Range", m_selectToolRange);
			if (ImGuiX::PropertyButton("Delete Selected"))
			{
				deleteSelected();
			}
			deletePosePressed = ImGuiX::PropertyButton("Delete Pose");
			if (ImGuiX::PropertyButton("Clear Selection"))
			{
				m_selected.clear();
			}
			if (ImGuiX::PropertyButton("Re-Center Pose"))
			{
				recenterPose(m_lib->templates[m_currentPose]);
			}
			if (ImGuiX::PropertyButton("Re-Center All"))
			{
				for (auto &t : m_lib->templates)
				{
					recenterPose(t);
				}
			}
			if (ImGuiX::PropertyButton("Rotate 180 Deg"))
			{
				transformPose(m_lib->templates[m_currentPose], make_rotation_z<float4x4>(g_pi));
			}
			if (ImGuiX::PropertyButton("Rotate +90"))
			{
				transformPose(m_lib->templates[m_currentPose], make_rotation_z<float4x4>(g_pi/2.0f));
			}
			if (ImGuiX::PropertyButton("Rotate -90 Deg"))
			{
				transformPose(m_lib->templates[m_currentPose], make_rotation_z<float4x4>(-g_pi/2.0f));
			}
			if (ImGuiX::PropertyButton("Rotate All +90"))
			{
				for (auto &t : m_lib->templates)
				{
					transformPose(t, make_rotation_z<float4x4>(g_pi/2.0f));
				}
			}


			ImGuiX::PropertyListEnd();
			ImGui::End();
		}
		if (ImGui::Begin("Props", 0))
		{
			ImGui::LabelText("Library Name", m_lib->m_name.c_str());

			TemplateLibrary::Template &t = m_lib->templates[m_currentPose];

			ImGui::LabelText("Name", t.poseName.c_str());
			ImGui::LabelText("Num Discs:", "%d", int(t.discs.size()));
			if (ImGui::Button("Add Annotation"))
			{
				TemplateLibrary::Annotation a;
				a.id = "annotation";
				a.direction = make_vector(0.0f, 1.0f, 0.0f);
				a.position = make_vector(0.0f, 0.0f, 0.0f);
				a.radius = m_selectToolRange;

				if (!m_selected.empty())
				{
					a.direction = make_vector(0.0f, 0.0f, 0.0f);
					a.position = make_vector(0.0f, 0.0f, 0.0f);
					Aabb aabb = make_inverse_extreme_aabb();
					for (int ind : m_selected)
					{
						a.direction += t.discs[ind].normal;
						a.position += t.discs[ind].position;
						aabb = combine(aabb, t.discs[ind].position);
					}
					a.radius = length(aabb.getHalfSize());
					a.direction /= float(m_selected.size());
					a.position /= float(m_selected.size());
				}
				t.annotations.push_back(a);
				m_lib->m_modified = true;
			}
			ImGui::SameLine();
			bool copyAnotationsClicked = ImGui::Button("Copy Annotations To Similar");
			ImGui::SameLine();
			static bool overWrite = false;
			ImGui::Checkbox("overWrite", &overWrite);

			if (copyAnotationsClicked)
			{
				TemplateLibrary::Template &t = m_lib->templates[m_currentPose];

				for (const TemplateLibrary::Annotation &sa : t.annotations)
				{
					if (m_visibleAnnotations.count(sa.id))
					{
						copyAnnotationToSimilar(sa, t, overWrite);
					}
				}
			}

			ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(2, 1));
			ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(4, 2));
			ImGui::PushStyleColor(ImGuiCol_HeaderHovered, (ImVec4)ImColor(0.2f, 0.2f, 0.3f, 1.0f));
			ImGui::Columns(2);
			for (size_t i = 0; i < t.annotations.size(); ++i)
			{
				TemplateLibrary::Annotation &a = t.annotations[i];
				bool node_open = ImGui::TreeNodeEx("Annotation", ImGuiTreeNodeFlags_SpanAllColumns | ImGuiTreeNodeFlags_DefaultOpen);
				ImGui::PushID(&a);
				ImGui::NextColumn();
				ImGui::AlignTextToFramePadding();
				bool show = m_visibleAnnotations.count(a.id) != 0;
				ImGui::Checkbox("show", &show);
				ImGui::SameLine();
				if (ImGui::Button("Delete"))
				{
					anotationsToDelete.insert(i);
				}
				if (!show)
				{
					m_visibleAnnotations.erase(a.id);
				}
				else
				{
					m_visibleAnnotations.insert(a.id);
				}
				ImGui::NextColumn();

				if (node_open)
				{
					ImGuiX::Property("name", a.id);
					ImGuiX::Property("radius", a.radius);
					ImGuiX::Property("position", a.position);
					ImGuiX::Property("direction", a.direction);
					a.direction = normalize(a.direction);
					ImGui::TreePop();
				}
				ImGui::PopID();
			}
			ImGui::Columns(1);
			ImGui::PopStyleColor();
			ImGui::PopStyleVar(2);

			m_computeSimilarity = ImGui::TreeNodeEx("Similarity", ImGuiTreeNodeFlags_SpanAllColumns);
			ImGui::SameLine();
			ImGui::Checkbox("Use Annotation", &m_computeAnnotationSimilarity);
			ImGui::SameLine();
			ImGui::Checkbox("Tfm", &m_applyTransform);
			ImGui::SameLine();
			static bool showAll = true;
			ImGui::Checkbox("All", &showAll);
			if (m_computeSimilarity)
			{
				ImGui::PushID("Similarity");
				ImGui::Columns(3);
				//for (auto s : m_similarities)
				ImGui::SetColumnWidth(0, 220);

				TemplateLibrary::Template &ct = m_lib->templates[m_currentPose];
				TemplateLibrary::Annotation sa;
				sa.radius = -1.0f;
				for (const TemplateLibrary::Annotation &_sa : ct.annotations)
				{
					if (m_visibleAnnotations.count(_sa.id))
					{
						sa = _sa;
						//copyAnnotationToSimilar(sa, t, overWrite);
					}
				}

				for (int si = 0; si < int(m_similarities.size()); ++si)
				{
					const auto &s = m_similarities[si];
					const TemplateLibrary::Template &t = m_lib->templates[s.index];
					// if we're not showing all and the annotation already exists in the destination, then hide it
					if (!showAll && m_computeAnnotationSimilarity && sa.radius > 0.0f && std::find_if(t.annotations.begin(), t.annotations.end(), [&](auto a) { return a.id == sa.id; }) != t.annotations.end())
					{
						continue;
					}
					ImGui::PushID(s.index);
					ImGui::LabelText("##name", t.poseName.c_str());
					ImGui::NextColumn();
					ImGui::LabelText("##distance", "%0.3e", s.similarity);
					ImGui::NextColumn();
					ImGui::SetColumnWidth(-1, 32);
					bool shown = m_similarShown[si];
					ImGui::PushItemWidth(-1);
					ImGui::Checkbox("##show", &shown);
					ImGui::PopItemWidth();
					m_similarShown[si] = shown;
					ImGui::NextColumn();
					ImGui::PopID();
				}
				ImGui::PopID();
				ImGui::TreePop();
			}

			ImGui::End();
		}


	}

	{
		TemplateLibrary::Template &t = m_lib->templates[m_currentPose];
		auto tmp = t.annotations;
		t.annotations.clear();
		for (size_t i = 0; i < tmp.size(); ++i)
		{
			if (anotationsToDelete.count(i) == 0)
			{
				t.annotations.push_back(tmp[i]);
			}
			else
			{
				m_lib->m_modified = true;
			}
		}
	}

	if (deletePosePressed)
	{
		m_lib->templates.erase(m_lib->templates.begin() + m_currentPose);
		// ensure current pose not out of range...
		setPose(m_currentPose);
		m_selected.clear();
		m_highlighted.clear();
		m_lib->m_modified = true;
	}
}



void TemplateEditor::deleteSelected()
{
	TemplateLibrary::Template &t = m_lib->templates[m_currentPose];
	std::vector<TemplateLibrary::Disc> discs;
	for (int i = 0; i < int(t.discs.size()); ++i)
	{
		if (m_selected.find(i) == m_selected.end())
		{
			discs.push_back(t.discs[i]);
		}
	}
	t.discs = discs;
	m_selected.clear();
	m_lib->m_modified = true;
}




void TemplateEditor::recenterPose(TemplateLibrary::Template &t)
{
	float3 meanPos = make_vector3(0.0f);
	for (const TemplateLibrary::Disc &td : t.discs)
	{
		meanPos += td.position;
	}
	meanPos = meanPos / float(t.discs.size());

	meanPos.z = 0.0f;
	for (TemplateLibrary::Disc &td : t.discs)
	{
		td.position -= meanPos;
	}
	for (TemplateLibrary::Annotation &ta : t.annotations)
	{
		ta.position -= meanPos;
	}
	m_lib->m_modified = true;
}

void TemplateEditor::transformPose(TemplateLibrary::Template &t, const chag::float4x4 &m)
{
	for (TemplateLibrary::Disc &td : t.discs)
	{
		td.position = transformPoint(m, td.position);
		td.normal = transformDirection(m, td.normal);
	}
	for (TemplateLibrary::Annotation &ta : t.annotations)
	{
		ta.position = transformPoint(m, ta.position);
		ta.direction  = transformDirection(m, ta.direction);
	}
	m_lib->m_modified = true;
}



void TemplateEditor::copyAnnotationToSimilar(const TemplateLibrary::Annotation &as, TemplateLibrary::Template & st, bool overWrite)
{
	if (!m_computeSimilarity || m_similarShown.empty())
	{
		return;
	}

	using Disc = TemplateLibrary::Disc;
	using Annotation = TemplateLibrary::Annotation;


	for (int si = 0; si < int(m_similarities.size()); ++si)
	{
		const auto &s = m_similarities[si];
		//if (s.first != m_currentPose)
		TemplateLibrary::Template &dt = m_lib->templates[s.index];
		if (m_similarShown[si])
		{
			bool found = false;
			Annotation ta = as;
			float4x4 tfm = inverse(s.tfm);
			ta.direction = transformDirection(tfm, ta.direction);
			ta.position = transformPoint(tfm, ta.position);
			for (TemplateLibrary::Annotation &ad : dt.annotations)
			{
				if (as.id == ad.id)
				{
					ad = ta;
					found = true;
				}
			}
			if (!found)
			{
				dt.annotations.push_back(ta);
			}
		}
	}
	m_lib->m_modified = true;

}

void TemplateEditor::setPose(int newPoseInd)
{
	int prevPose = m_currentPose;
	const int numPoses = int(m_lib->templates.size());
	m_currentPose = std::min(std::max(0, newPoseInd), int(numPoses - 1));
	if (prevPose != m_currentPose)
	{
		m_highlighted.clear();
		m_selected.clear();
	}
}



void TemplateEditor::setPoseByName(const std::string &poseName)
{
	int ind = m_lib->findInd(poseName);
	if (ind >= 0)
	{
		setPose(ind);
	}
}
