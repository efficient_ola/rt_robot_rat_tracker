#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "wavefront.h"
#include "hull.h"
#include "TemplateLibrary.h"
#include <boost/program_options.hpp>
#include <utils/OBJModel.h>
#include <boost/filesystem.hpp>

namespace bfs = boost::filesystem;

int main(int argc,char **argv)
{
	std::string templateLibraryName;
	std::string templateLibraryPath;
	std::string inputSource;
	bool overwrite = false;
	bool generateHull = false;
	{
		bool showHelp = false;
		namespace po = boost::program_options;
		po::options_description desc("Allowed options");
		desc.add_options()
			("help,h", "produce help message")
			("input,i", po::value<std::string>(&inputSource), "Input OBJ file name")
			("output,o", po::value<std::string>(&templateLibraryName), "name of template library, defaults to the base name of the obj file.")
			("outpath,p", po::value<std::string>(&templateLibraryPath), "path of template library, used to contruct library output from input file name (not used if output specified).")
			("overwrite,f", po::bool_switch(&overwrite)->default_value(false), "If true, overwrites existing")
			("generateHull,h", po::bool_switch(&generateHull)->default_value(false), "If true, overwrites existing");
		try
		{
			po::variables_map vm;
			po::store(po::command_line_parser(argc, argv).
				options(desc).run(), vm);
			po::notify(vm);

			showHelp = vm.count("help") != 0;
			//useOurTree = !vm["use_their"].as<bool>();
		}
		catch (const po::unknown_option& eUO)
		{
			fprintf(stderr, "<error> %s\n", eUO.what());
			return 1;
		}
	}


	if (templateLibraryName.empty())
	{
		templateLibraryName = bfs::path(templateLibraryPath).append(bfs::path(inputSource).filename().string()).string();

		printf("Output name not set, using '%s', created from path '%s' and obj file name '%s'", templateLibraryName.c_str(), templateLibraryPath.c_str(), inputSource.c_str());
	}

	OBJModel model;
	if (model.load(inputSource))
	{
		using namespace chag;
		Aabb aabb = model.m_aabb;
		std::vector<chag::float3> points = model.m_positions;
		std::vector<chag::float3> normals = model.m_normals;

		const int numSteps = 6;
		float distThresh = 0.001f * std::min(aabb.getDiagonal().x, aabb.getDiagonal().y) / float(numSteps);

		assert(normals.size() == points.size());

		TemplateLibrary::Template res;

		for (int i = 0; i < int(points.size()); ++i)
		{
			// 1. assume scale is mm (it seems to be) - transform to metric
			float3 mp = points[i] * 0.001f;
			float3 n = normals[i];

			// Check that point is not on the inside (requires normals to be set for each vertex....)
			if (dot(mp, n) > 0.0f)
			{
				bool tooClose = false;
				for (auto p : res.discs)
				{
					if (lengthSquared(mp - p.position) < square(distThresh))
					{
						tooClose = true;
						break;
					}
				}
				if (!tooClose)
				{
					TemplateLibrary::Disc sd;
					sd.radius = distThresh / 1.75f;
					sd.normal = normalize(n);
					sd.position = mp;

					res.discs.push_back(sd);
				}

			}
		}

		TemplateLibrary lib;
		json meta;
		meta["source_type"] = "obj_model";
		meta["input_source_id"] = inputSource;
		lib.add(res, meta);

		lib.save(templateLibraryName);
	}

#if 0
	// TODO:
	if (generateHull)
	{
		HullDesc desc;
		desc.SetHullFlag(QF_TRIANGLES);
		WavefrontObj w;


		desc.mVcount = w.mVertexCount;
		desc.mVertices = new double[desc.mVcount*3];

		for (unsigned int i = 0; i<desc.mVcount; i++)
		{
			desc.mVertices[i*3+0] = w.mVertices[i*3+0];
			desc.mVertices[i*3+1] = w.mVertices[i*3+1];
			desc.mVertices[i*3+2] = w.mVertices[i*3+2];
		}

		desc.mVertexStride = sizeof(double)*3;

		printf("Read Wavefront OBJ %s with %d vertices and %d triangles.\r\n", fname, w.mVertexCount, w.mTriCount);
		printf("Now generating a convex hull.\r\n");

		HullResult dresult;
		HullLibrary hl;

		HullError ret = hl.CreateConvexHull(desc, dresult);

		if (ret == QE_OK)
		{
			FHullResult result(dresult); // convert it from doubles back into floats.

			printf("Successfully created convex hull.\r\n");

			if (result.mPolygons)
				printf("Hull contains %d poygons.\r\n", result.mNumFaces);
			else
				printf("Hull contains %d triangles.\r\n", result.mNumFaces);

			printf("Output hull contains %d vertices.\r\n ", result.mNumOutputVertices);
			printf("Output hull contains %d indices.\r\n", result.mNumIndices);

			FILE *fph = fopen("hull.obj", "wb");
			if (fph)
			{
				printf("Saving hull as 'hull.obj'\r\n");

				for (unsigned int i = 0; i<result.mNumOutputVertices; i++)
				{
					const float *v = &result.mOutputVertices[i*3];
					fprintf(fph, "v %0.9f %0.9f %0.9f\r\n", v[0], v[1], v[2]);
				}

				if (result.mPolygons)
				{
					const unsigned int *idx = result.mIndices;
					for (unsigned int i = 0; i<result.mNumFaces; i++)
					{
						unsigned int pcount = *idx++;
						fprintf(fph, "f ");
						for (unsigned int j = 0; j<pcount; j++)
						{
							fprintf(fph, "%4d ", idx[0]+1);
							idx++;
						}
						fprintf(fph, "\r\n");
					}
				}
				else
				{
					for (unsigned int i = 0; i<result.mNumFaces; i++)
					{
						unsigned int *idx = &result.mIndices[i*3];
						fprintf(fph, "f %4d %4d %4d\r\n", idx[0]+1, idx[1]+1, idx[2]+1);
					}

				}
				fclose(fph);
			}

			delete desc.mVertices;
			hl.ReleaseResult(dresult);
		}
		else
		{
			printf("Failed to create convex hull.\r\n");
			return 1;
		}
	}
#endif // 0
    return 0;
}
